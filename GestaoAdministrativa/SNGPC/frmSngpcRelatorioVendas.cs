﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcRelatorioVendas : Form, Botoes
    {

        private ToolStripButton tsbRelVendas = new ToolStripButton("Rel. Vendas SNGPC");
        private ToolStripSeparator tssRelVendas = new ToolStripSeparator();

        public frmSngpcRelatorioVendas(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";

                if (String.IsNullOrEmpty(dtInicial.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Inicial não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (String.IsNullOrEmpty(dtFinal.Text.Replace("/", "")))
                {
                    Principal.mensagem = "Data Final não pode ser em Branco!";
                    Funcoes.Avisa();
                    dtFinal.Focus();
                    return;
                }

                if (dtInicial.Value > dtFinal.Value)
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                Cursor = Cursors.WaitCursor;

                char ordenar = rdbCodigo.Checked.Equals(true) ? 'C' : 'D';
                frmRelatorioVendas vendas = new frmRelatorioVendas(dtInicial.Text, dtFinal.Text, ordenar, Convert.ToInt32(cmbTipoReceita.SelectedValue));
                vendas.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório Vendas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void frmSngpcRelatorioVendas_Load(object sender, EventArgs e)
        {
            try
            {
                CarregaCombo();
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Limpar();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório Vendas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
}

        public void CarregaCombo()
        {
            SngpcTabela tabelas = new SngpcTabela();
            DataTable dtTipoReceitas = tabelas.BuscaTabelaPorTipo(1);
            dtTipoReceitas.Rows.Add(0, "TODAS AS RECEITAS");
            cmbTipoReceita.DataSource = dtTipoReceitas;
            cmbTipoReceita.DisplayMember = "TAB_DESCRICAO";
            cmbTipoReceita.ValueMember = "TAB_SEQ";
            cmbTipoReceita.SelectedValue = 0;
        }

        public void Botoes()
        {
            try
            {
                this.tsbRelVendas.AutoSize = false;
                this.tsbRelVendas.Image = Properties.Resources.sngpc;
                this.tsbRelVendas.Size = new System.Drawing.Size(160, 20);
                this.tsbRelVendas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelVendas);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelVendas);
                tsbRelVendas.Click += delegate
                {
                    var relVendas = Application.OpenForms.OfType<frmSngpcRelatorioVendas>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relVendas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório Vendas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório Vendas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmSngpcRelatorioVendas_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void cmbTipoReceita_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void rdbCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void rdbDescricao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        private void btnVisualizar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F1)
            {
                btnVisualizar.PerformClick();
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            rdbCodigo.Checked = true;
            cmbTipoReceita.SelectedIndex = 0;
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelVendas);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelVendas);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório Vendas SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
