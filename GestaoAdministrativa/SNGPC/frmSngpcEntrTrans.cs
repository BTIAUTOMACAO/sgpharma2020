﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.IO;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcEntrTrans : Form, Botoes
    {
        //DECLARACAO DAS VARIÁVEIS//
        private DataTable dtEntradas = new DataTable();
        private int contRegistros;
        private string[,] dados = new string[11, 3];
        private bool emGrade;
        private ToolStripButton tsbEntradas = new ToolStripButton("Compra/Trans. de Med.");
        private ToolStripSeparator tssEntradas = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        private bool regMS;
        private DateTime data;

        public frmSngpcEntrTrans(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmSngpcEntrTrans_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbFBusca.SelectedIndex = 0;
                txtBDocto.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbFBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtFBusca.Focus();
        }


        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "")
                {
                    if (BuscaProdutos(txtCodBarras.Text, ""))
                    {
                        txtQtde.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }

        public bool BuscaProdutos(string codigoBarras, string descricao)

        {
            Produto pro = new Produto();

            if (descricao != "")
            {
                using (frmBuscaProd buscaProd = new frmBuscaProd(false))
                {
                    buscaProd.BuscaProd(txtDescr.Text.ToUpper(), false);
                    buscaProd.ShowDialog();
                }
            }

            string codBarras = codigoBarras != "" ? codigoBarras : Principal.codBarra;

            DataTable dtProduto = pro.BuscaProdutos(codBarras);

            if (dtProduto.Rows.Count != 0)
            {
                txtCodBarras.Text = dtProduto.Rows[0]["PROD_CODIGO"].ToString();
                txtDescr.Text = dtProduto.Rows[0]["PROD_DESCR"].ToString();
                txtReg.Text = dtProduto.Rows[0]["PROD_REGISTRO_MS"].ToString();
                chkControlado.Checked = dtProduto.Rows[0]["PROD_CONTROLADO"].ToString() == "S" ? true : false;
                cmbClasseTera.Text = dtProduto.Rows[0]["PROD_CLASSE_TERAP"].ToString();

                return true;
            }
            else
            {
                LimparFicha();
                MessageBox.Show("Produto não encontrado! ", "Busca de Produto", MessageBoxButtons.OK);
                return false;
            }
        }

        private void txtDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbTipo.Focus();
        }

        private void cmbTipo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtData.Focus();
        }

        private void dtData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtNF.Focus();
        }

        private void dtNF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbFBusca.Focus();
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (Principal.mdiPrincipal.btnIncluir.Enabled == true && txtCodBarras.Text.Trim() == "")
                {
                    if (BuscaProdutos("", txtDescr.Text))
                        txtQtde.Focus();
                }
                else
                    txtQtde.Focus();
            }
        }

        private void txtReg_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtLote.Focus();
        }

        public bool CarregaCombos()
        {
            try
            {
                //CARREGA TIPO DE OPERACAO//
                SngpcTabela tabelas = new SngpcTabela();

                Principal.dtPesq = tabelas.BuscaTabelaEntrTrans();
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de operação,\npara cadastrar uma compra ou transferência de medicamento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbTipo.DataSource = Principal.dtPesq;
                    cmbTipo.DisplayMember = "TAB_DESCRICAO";
                    cmbTipo.ValueMember = "TAB_SEQ";
                    cmbTipo.SelectedIndex = -1;

                    cmbBTipo.DataSource = Principal.dtPesq;
                    cmbBTipo.DisplayMember = "TAB_DESCRICAO";
                    cmbBTipo.ValueMember = "TAB_SEQ";
                    cmbBTipo.SelectedIndex = -1;

                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmSngpcEntrTrans");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, contRegistros));
            if (tcEntradas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcEntradas.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtEntID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbEntradas.AutoSize = false;
                this.tsbEntradas.Image = Properties.Resources.sngpc;
                this.tsbEntradas.Size = new System.Drawing.Size(165, 20);
                this.tsbEntradas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbEntradas);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssEntradas);
                tsbEntradas.Click += delegate
                {
                    var cadEntradas = Application.OpenForms.OfType<frmSngpcEntrTrans>().FirstOrDefault();
                    BotoesHabilitados();
                    cadEntradas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbEntradas);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssEntradas);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtEntID.Text = dtEntradas.Rows[linha]["ENT_SEQ"].ToString();
                txtDocto.Text = dtEntradas.Rows[linha]["ENT_DOCTO"].ToString();
                cmbTipo.Text = dtEntradas.Rows[linha]["TAB_DESCRICAO"].ToString();
                dtData.Value = Convert.ToDateTime(dtEntradas.Rows[linha]["ENT_DATA"]);
                dtNF.Value = Convert.ToDateTime(dtEntradas.Rows[linha]["ENT_DATA_NF"]);
                txtFCnpj.Text = dtEntradas.Rows[linha]["ENT_CNPJ"].ToString();
                Principal.dtRetorno = BancoDados.selecionarRegistros("SELECT CF_NOME, CF_CIDADE, CF_UF  FROM CLIFOR WHERE CF_DOCTO = '" + dtEntradas.Rows[linha]["ENT_CNPJ"] + "'");
                if (Principal.dtRetorno.Rows.Count != 0)
                {
                    txtFNome.Text = Principal.dtRetorno.Rows[0]["CF_NOME"].ToString();
                    txtFCidade.Text = Principal.dtRetorno.Rows[0]["CF_CIDADE"].ToString() + "/" + Principal.dtRetorno.Rows[0]["CF_UF"].ToString();
                }
                txtCodBarras.Text = dtEntradas.Rows[linha]["PROD_CODIGO"].ToString();
                txtDescr.Text = dtEntradas.Rows[linha]["PROD_DESCR"].ToString();
                txtReg.Text = dtEntradas.Rows[linha]["ENT_MS"].ToString();
                txtQtde.Text = dtEntradas.Rows[linha]["ENT_MED_QTDE"].ToString();
                txtLote.Text = dtEntradas.Rows[linha]["ENT_MED_LOTE"].ToString();
                txtData.Text = Funcoes.ChecaCampoVazio(dtEntradas.Rows[linha]["DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtEntradas.Rows[linha]["OPALTERACAO"].ToString());
                cmbClasseTera.Text = dtEntradas.Rows[linha]["PROD_CLASSE_TERAP"].ToString();
                chkControlado.Checked = dtEntradas.Rows[linha]["PROD_CONTROLADO"].ToString() == "S" ? true : false;
                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, linha));
                txtDocto.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtEntradas.Clear();
            dgEntradas.DataSource = dtEntradas;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            Array.Clear(dados, 0, 33);
            txtBDocto.Focus();
        }

        public void LimparFicha()
        {
            cmbClasseTera.SelectedIndex = -1;
            chkControlado.Checked = false;
            txtEntID.Text = "";
            txtDocto.Text = "";
            cmbTipo.SelectedIndex = -1;
            dtData.Value = DateTime.Now;
            dtNF.Value = DateTime.Now;
            cmbFBusca.SelectedIndex = 0;
            txtFBusca.Text = "";
            txtFNome.Text = "";
            txtFCnpj.Text = "";
            txtFCidade.Text = "";
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtReg.Text = "";
            txtLote.Text = "";
            txtQtde.Text = "";
            txtData.Text = "";
            txtUsuario.Text = "";
            Array.Clear(dados, 0, 33);
            txtDocto.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcEntradas.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEntradas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEntradas.CurrentCell = dgEntradas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEntradas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEntradas.CurrentCell = dgEntradas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Primeiro()
        {
            if (dtEntradas.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgEntradas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgEntradas.CurrentCell = dgEntradas.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcEntradas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
            else
            {

                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }

        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtEntradas.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgEntradas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgEntradas.CurrentCell = dgEntradas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos())
                {
                    DateTime dtDataLan = Convert.ToDateTime(Funcoes.ChecaParamentos(11, "01"));
                    if (dtData.Value.Date <= dtDataLan || dtNF.Value.Date <= dtDataLan)
                    {
                        MessageBox.Show("A data não pode ser menor que a ultima Transferência.\nCaso queria ATUALIZAR entre na Tela de Transfência e FAÇA A CORREÇÃO DO PERÍODO e tente novamente.", "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtData.Focus();
                        Cursor = Cursors.Default;
                        return false;
                    }
                    else
                    {
                        var prod = new Produto();
                        var entrada = new SngpcEntrada();
                        var controVenda = new SngpcControleVendas();
                        var prodVence = new ProdutoVencimento();

                        DataTable dtProd = prod.BuscaProdutos(txtCodBarras.Text);
                        DataTable dtVenda = controVenda.BuscaProduto(txtCodBarras.Text, txtLote.Text);
                        DataTable dtProdVence = prodVence.BuscaPorCodigoLote(txtCodBarras.Text, txtLote.Text);
                        Principal.dtBusca = Util.RegistrosPorEstabelecimento("SNGPC_ENTRADAS", "ENT_SEQ", txtEntID.Text);

                        if (dtProd.Rows.Count == 1 && Principal.dtBusca.Rows.Count == 1)
                        {
                            prod = new Produto()
                            {
                                ProdID = Convert.ToInt32(dtProd.Rows[0]["PROD_ID"]),
                                ProdRegistroMS = txtReg.Text.Trim(),
                                ProdControlado = chkControlado.Checked == true ? "S" : "N",
                                ProdClasTera = cmbClasseTera.Text
                            };

                            BancoDados.AbrirTrans();
                            if (prod.AtualizaProdutoSNGPC(prod, dtProd).Equals(true))
                            {
                                entrada = new SngpcEntrada()
                                {
                                    EmpCodigo = Principal.empAtual,
                                    EstCodigo = Principal.estAtual,
                                    EntSeq = Convert.ToInt32(txtEntID.Text),
                                    EntDocto = txtDocto.Text,
                                    EntTipoOperacao = Convert.ToInt32(cmbTipo.SelectedValue),
                                    EntDataNF = Convert.ToDateTime(dtNF.Text),
                                    EntCNPJ = txtFCnpj.Text,
                                    ProdCodigo = txtCodBarras.Text,
                                    EntMedLote = txtLote.Text,
                                    EntMedQtde = Convert.ToInt32(txtQtde.Text),
                                    EntData = Convert.ToDateTime(dtData.Text),
                                    EntRegistroMS = txtReg.Text
                                };


                                if (entrada.AtualizaDados(entrada, Principal.dtBusca).Equals(true))
                                {
                                    controVenda = new SngpcControleVendas()
                                    {
                                        ProCodigo = Principal.dtBusca.Rows[0]["PROD_CODIGO"].ToString(),
                                        Lote = Principal.dtBusca.Rows[0]["ENT_MED_LOTE"].ToString(),
                                        Qtde = Convert.ToInt32(Principal.dtBusca.Rows[0]["ENT_MED_QTDE"]),
                                        Data = DateTime.Now
                                    };
                                    //DEVOLVO A ENTRADA DO LOTE//
                                    if (!controVenda.SubtraiQtdeMesmoLoteEProduto(controVenda))
                                    {
                                        BancoDados.ErroTrans();
                                        MessageBox.Show("102 - Erro ao efetuar o cadastro", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return false;
                                    }

                                    //ENTRADA NO MESMO LOTE OU EM NOVO LOTE//
                                    controVenda = new SngpcControleVendas()
                                    {
                                        ProCodigo = txtCodBarras.Text,
                                        Lote = txtLote.Text,
                                        Qtde = Convert.ToInt32(txtQtde.Text),
                                        Data = DateTime.Now
                                    };

                                    if (controVenda.IdentificaSeJaTemLoteEProduto(controVenda.ProCodigo, controVenda.Lote).Equals(0))
                                    {
                                        if (!controVenda.Incluir(controVenda))
                                        {
                                            BancoDados.ErroTrans();
                                            MessageBox.Show("104 - Erro ao efetuar o cadastro", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        if (!controVenda.SomaQtdeMesmoLoteEProduto(controVenda))
                                        {
                                            BancoDados.ErroTrans();
                                            MessageBox.Show("101 - Erro ao efetuar o cadastro", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            return false;
                                        }
                                    }
                                    
                                    BancoDados.FecharTrans();
                                    if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                                    {
                                        //decremeta estoque 
                                        AtualizaEstoqueFilial(txtCodBarras.Text, Convert.ToInt32(Principal.dtBusca.Rows[0]["ENT_MED_QTDE"]));
                                        //acrecenta estoque
                                        EstoqueFilial(prod.BuscaProdutos(txtCodBarras.Text), "mais");
                                    }
                                    MessageBox.Show("Atualização efetuada com sucesso!", "Perda de Medicamentos", MessageBoxButtons.OK);
                                    Limpar();
                                    return true;
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 - Erro ao efetuar o cadastro", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("101 - Erro ao efetuar o cadastro", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                    return false;
                }
                else
                {
                    Cursor = Cursors.Default;
                    return false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        public bool Excluir()
        {
            try
            {
                //VERIFICA SE A DATA DO PARAMENTRO E MAIOR QUE O DATA//
                DateTime dtDataLan = Convert.ToDateTime(Funcoes.ChecaParamentos(11, "01"));
                if (dtData.Value.Date <= dtDataLan || dtNF.Value.Date <= dtDataLan)
                {
                    MessageBox.Show("A data não pode ser menor que a ultima Transferência ", "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtData.Focus();
                    Cursor = Cursors.Default;
                    return false;
                }
                else
                {
                    if (txtEntID.Text.Trim() != "" || txtDocto.Text.Trim() != "" || cmbTipo.SelectedIndex != -1)
                    {
                        if (MessageBox.Show("Confirma exclusão do Lançamento?", "Exclusão tabela Comp/Trans. de Medicamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                        {
                            SngpcEntrada entrada = new SngpcEntrada();
                            Principal.dtBusca = Util.RegistrosPorEstabelecimento("SNGPC_ENTRADAS", "ENT_SEQ", txtEntID.Text);

                            var conVendas = new SngpcControleVendas
                            {
                                ProCodigo = Principal.dtBusca.Rows[0]["PROD_CODIGO"].ToString(),
                                Qtde = Convert.ToInt32(Principal.dtBusca.Rows[0]["ENT_MED_QTDE"]),
                                Lote = Principal.dtBusca.Rows[0]["ENT_MED_LOTE"].ToString(),
                                Data = DateTime.Now
                            };

                            BancoDados.AbrirTrans();
                            if (entrada.ExcluirDados(Convert.ToInt32(txtEntID.Text)).Equals(true))
                            {
                                if (conVendas.SubtraiQtdeMesmoLoteEProduto(conVendas))
                                {
                                    if (Funcoes.LeParametro(6, "351", true).Equals("S"))
                                    {
                                        var prodVencimento = new ProdutoVencimento();
                                        if (prodVencimento.ExcluiProdutoViaEntradaDeNotas(Principal.estAtual, Principal.empAtual, txtDocto.Text) == -1)
                                        {
                                            BancoDados.ErroTrans();
                                            MessageBox.Show("102 -  Erro ao Excluir", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            return false;
                                        }
                                    }

                                    BancoDados.FecharTrans();
                                    if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                                    {
                                        AtualizaEstoqueFilial(Principal.dtBusca.Rows[0]["PROD_CODIGO"].ToString(), Convert.ToInt32(Principal.dtBusca.Rows[0]["ENT_MED_QTDE"]));
                                    }
                                    Funcoes.GravaLogExclusao("ENT_ID", txtEntID.Text, Principal.usuario, DateTime.Now, "SNGPC_ENTRADAS", txtEntID.Text, "ERRO EXCLUSÃO", Principal.estAtual);
                                    MessageBox.Show("Exclusão efetuada com sucesso", "Entrada e Transferência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    dtEntradas.Clear();
                                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                    dgEntradas.DataSource = dtEntradas;
                                    emGrade = false;
                                    tcEntradas.SelectedTab = tpGrade;
                                    tslRegistros.Text = "";
                                    Funcoes.LimpaFormularios(this);
                                    txtBDocto.Focus();
                                    return true;
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 - Erro ao Excluir", "Entrada e Transferência", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }

                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("101 - Erro ao Excluir", "Entrada e Transferência", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        return false;
                    }
                    else
                    {
                        MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    DateTime dtDataLan = Convert.ToDateTime(Funcoes.ChecaParamentos(11, "01"));
                    if (dtData.Value.Date <= dtDataLan)
                    {
                        MessageBox.Show("A data não pode ser menor que a ultima Transferência.\nCaso queria INCLUIR entre na Tela de Transfência e FAÇA A CORREÇÃO DO PERÍODO e tente novamente.", "Compra/Trans. de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtData.Focus();
                        Cursor = Cursors.Default;
                        return false;
                    }
                    else
                    {
                        var entrada = new SngpcEntrada()
                        {
                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            EntSeq = Funcoes.IdentificaVerificaID("SNGPC_ENTRADAS", "ENT_SEQ", 0, ""),
                            EntDocto = txtDocto.Text,
                            EntTipoOperacao = Convert.ToInt32(cmbTipo.SelectedValue),
                            EntDataNF = dtNF.Value,
                            EntCNPJ = txtFCnpj.Text,
                            ProdCodigo = txtCodBarras.Text,
                            EntMedLote = txtLote.Text,
                            EntMedQtde = Convert.ToInt32(txtQtde.Text),
                            EntData = dtData.Value,
                            EntRegistroMS = txtReg.Text,
                        };

                        var prod = new Produto();
                        DataTable dtProd = prod.BuscaProdutos(txtCodBarras.Text);

                        if (dtProd.Rows.Count == 1)
                        {
                            prod = new Produto()
                            {
                                ProdID = Convert.ToInt32(dtProd.Rows[0]["PROD_ID"]),
                                ProdRegistroMS = txtReg.Text.Trim(),
                                ProdControlado = chkControlado.Checked == true ? "S" : "N",
                                ProdClasTera = cmbClasseTera.Text
                            };

                            var controVenda = new SngpcControleVendas()
                            {
                                ProCodigo = txtCodBarras.Text,
                                Lote = txtLote.Text,
                                Qtde = Convert.ToInt32(txtQtde.Text),
                                Data = DateTime.Now
                            };

                            BancoDados.AbrirTrans();
                            if (controVenda.IdentificaSeJaTemLoteEProduto(controVenda.ProCodigo, controVenda.Lote).Equals(0))
                            {
                                if (!controVenda.Incluir(controVenda))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("104 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }
                            else
                            {
                                if (!controVenda.SomaQtdeMesmoLoteEProduto(controVenda))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("104 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }

                            if (prod.AtualizaProdutoSNGPC(prod, dtProd).Equals(true))
                            {
                                if (entrada.IncluirDados(entrada).Equals(true))
                                {
                                   
                                    if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                                    {
                                        EstoqueFilial(dtProd, "mais");
                                    }

                                    BancoDados.FecharTrans();
                                    MessageBox.Show("Cadastrado efetuada com sucesso!", "Produtos SNGPC", MessageBoxButtons.OK);
                                    Cursor = Cursors.Default;
                                    Limpar();
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("103 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    Cursor = Cursors.Default;
                                    return false;
                                }
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("102 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                Cursor = Cursors.Default;
                                return false;
                            }

                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("101 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        return true;
                    }
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        #region AtualizaEstoqueFilial
        public async void EstoqueFilial(DataTable dtProd, string operacao)
        {
            string codEstabelecimento = Funcoes.LeParametro(9, "52", false);
            string enderecoWebService = Funcoes.LeParametro(9, "54", true);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(enderecoWebService);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueEntradaNotas?codBarra=" + txtCodBarras.Text + "&descricao=" + txtDescr.Text + "&qtde=" + txtQtde.Text + "&precoCusto=" + dtProd.Rows[0]["PROD_ULTCUSME"].ToString().Replace(",", ".") + "&precoVenda=" + dtProd.Rows[0]["PRE_VALOR"].ToString().Replace(",", ".") + "&codEstabelecimento=" + codEstabelecimento + "&operacao=" + operacao);
                if (!response.IsSuccessStatusCode)
                {
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\EntradaTrasnferenciaSNGPC.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + txtCodBarras.Text + " / QTDE: " + txtQtde.Text + " / Operacação: " + operacao);
                        }
                    }
                }
            }
            
        }

        public async void AtualizaEstoqueFilial(string codBarras, int qtd)
        {
            string codEstabelecimento = Funcoes.LeParametro(9, "52", false);
            string enderecoWebService = Funcoes.LeParametro(9, "54", true);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(enderecoWebService);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueVenda?codEstab=" + codEstabelecimento + "&codBarras=" + codBarras + "&qtd=" + qtd + "&operacao=menos");
                if (!response.IsSuccessStatusCode)
                {
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\EntradaTrasnferenciaSNGPC.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + txtCodBarras.Text + " / QTDE: " + txtQtde.Text + " / Operacação: Atualização de Estoque ");
                        }
                    }
                }
            }

        }

        #endregion

        public void ImprimirRelatorio()
        {
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";


            if (String.IsNullOrEmpty(txtDocto.Text.Trim()))
            {
                Principal.mensagem = "Docto. não pode ser em branco.";
                Funcoes.Avisa();
                txtDocto.Focus();
                return false;
            }
            if (Equals(cmbTipo.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um tipo de operação.";
                Funcoes.Avisa();
                cmbTipo.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Cód. de Barras não pode ser em branco.";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtReg.Text.Trim()))
            {
                Principal.mensagem = "Reg. Min. da Saúde não pode ser em branco.";
                Funcoes.Avisa();
                txtReg.Focus();
                return false;

            }
            if (String.IsNullOrEmpty(txtQtde.Text.Trim()) || txtQtde.Text == "0")
            {
                Principal.mensagem = "Qtde não pode ser zero.";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }

            if (String.IsNullOrEmpty(txtLote.Text.Trim()))
            {
                Principal.mensagem = "Lote não pode ser em branco.";
                Funcoes.Avisa();
                txtLote.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(cmbClasseTera.Text.Trim()))
            {
                Principal.mensagem = "Informe a Classe Terapêutica";
                Funcoes.Avisa();
                cmbClasseTera.Focus();
                return false;
            }
            if (chkControlado.Checked == false)
            {
                Principal.mensagem = "O SNGPC aceita apenas produtos controlados, caso esse produto seja controloado marque a caixa Controlado";
                Funcoes.Avisa();
                chkControlado.Focus();
                return false;
            }

            return true;
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox10_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox10, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox5_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox5, "Campo Obrigatório");
        }

        private void pictureBox6_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox6, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox8_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox8, "Campo Obrigatório");
        }

        private void pictureBox9_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox9, "Campo Obrigatório");
        }

        private void tcEntradas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcEntradas.SelectedTab == tpGrade)
            {
                dgEntradas.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcEntradas.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmSngpcEntrTrans");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, contRegistros));
                    txtDocto.Focus();
                    cmbFBusca.SelectedIndex = 0;
                    dtData.Value = DateTime.Now;
                    dtNF.Value = DateTime.Now;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    emGrade = false;
                }
            }
        }

        private void txtBDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDocto.Text.Trim()))
                {
                    cmbBTipo.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }

        }

        private void cmbBTipo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbBTipo.SelectedIndex.Equals(-1))
                {
                    txtBCodBarra.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBCodBarra_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBCodBarra.Text.Trim()))
                {
                    txtBReg.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }

        }

        private void txtBReg_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void frmSngpcEntrTrans_Shown(object sender, EventArgs e)
        {
            CarregaCombos();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                SngpcEntrada entrada = new SngpcEntrada()
                {
                    EntDocto = txtBDocto.Text,
                    EntTipoOperacao = cmbBTipo.SelectedValue == null ? 0 : Convert.ToInt32(cmbBTipo.SelectedValue),
                    ProdCodigo = txtBCodBarra.Text,
                    EntRegistroMS = txtBReg.Text,
                };

                dtEntradas = entrada.BuscaEntrada(entrada, out strOrdem);
                if (dtEntradas.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtEntradas.Rows.Count;
                    dgEntradas.DataSource = dtEntradas;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, 0));
                    dgEntradas.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgEntradas.DataSource = dtEntradas;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBDocto.Focus();
                    Funcoes.LimpaFormularios(this);
                    emGrade = false;
                    tslRegistros.Text = "";
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgEntradas.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgEntradas);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgEntradas.RowCount;
                    for (i = 0; i <= dgEntradas.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgEntradas[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgEntradas.ColumnCount; k++)
                    {
                        if (dgEntradas.Columns[k].Visible == true)
                        {
                            switch (dgEntradas.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Cód. de Barras":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                                case "Reg. Min. da Saúde":
                                    numCel = Principal.GetColunaExcel(dgEntradas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgEntradas.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgEntradas.ColumnCount : indice) + Convert.ToString(dgEntradas.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void dgEntradas_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, contRegistros));
            emGrade = true;
        }

        private void dgEntradas_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcEntradas.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgEntradas_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtEntradas.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtEntradas.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtEntradas.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgEntradas_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgEntradas.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtEntradas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEntradas.Columns[e.ColumnIndex].Name + " DESC");
                        dgEntradas.DataSource = dtEntradas;
                        decrescente = true;
                    }
                    else
                    {
                        dtEntradas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgEntradas.Columns[e.ColumnIndex].Name + " ASC");
                        dgEntradas.DataSource = dtEntradas;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtEntradas, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtEntID") && (control.Name != "txtFNome") && (control.Name != "txtFCnpj") && (control.Name != "txtFCidade")
                    && (control.Name != "txtReg") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgEntradas.ColumnCount; i++)
                {
                    if (dgEntradas.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgEntradas.ColumnCount];
                string[] coluna = new string[dgEntradas.ColumnCount];

                for (int i = 0; i < dgEntradas.ColumnCount; i++)
                {
                    grid[i] = dgEntradas.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgEntradas.ColumnCount; i++)
                {
                    if (dgEntradas.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgEntradas.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgEntradas.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgEntradas.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgEntradas.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgEntradas.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgEntradas.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgEntradas.ColumnCount; i++)
                        {
                            dgEntradas.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Compra/Trans. de Medicamentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcEntradas.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcEntradas.SelectedTab = tpFicha;
        }



        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void cmbFBusca_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbFBusca.Text.Equals("CNPJ"))
            {
                this.txtFBusca.Mask = "99,999,999/9999-99";
            }
            else
            {
                this.txtFBusca.Mask = "";
            }

        }

        private void txtFBusca_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbFBusca.SelectedIndex != -1 && txtFBusca.Text.Trim() != "")
                {
                    var buscaFornecedor = new Cliente();
                    List<Cliente> cliente = new List<Cliente>();
                    if (cmbFBusca.SelectedIndex == 0)
                    {
                        cliente = buscaFornecedor.BuscaDadosFornecedor(1, txtFBusca.Text.Trim().ToUpper(), 1);
                    }
                    else if (cmbFBusca.SelectedIndex == 1)
                    {
                        long cnpj = Convert.ToInt64(txtFBusca.Text);
                        cliente = buscaFornecedor.BuscaDadosFornecedor(2, String.Format(@"{0:00\.000\.000\/0000\-00}", cnpj), 1);
                    }
                    if (cliente.Count.Equals(1))
                    {
                        txtFNome.Text = cliente[0].CfNome;
                        txtFCnpj.Text = cliente[0].CfDocto;
                        txtFCidade.Text = cliente[0].CfCidade + "/" + cliente[0].CfUF;
                        cmbFBusca.SelectedIndex = 0;
                        txtCodBarras.Focus();
                    }
                    else if (cliente.Count.Equals(0))
                    {
                        MessageBox.Show("Fornecedor não cadastrado! Faça nova pesquisa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbFBusca.SelectedIndex = 0;
                        txtFBusca.Text = "";
                        txtFBusca.Focus();
                    }
                    else
                    {
                        using (var t = new frmBuscaForn("fornecedor"))
                        {
                            for (int i = 0; i < cliente.Count; i++)
                            {
                                t.dgCliFor.Rows.Insert(t.dgCliFor.RowCount, new Object[] { cliente[i].CodigoConveniada, "", cliente[i].CfCodigo, cliente[i].CfNome, cliente[i].CfApelido, cliente[i].CfObservacao,
                                    cliente[i].CfDocto, cliente[i].CfTelefone, cliente[i].CfEndereco, cliente[i].CfBairro, cliente[i].CfCidade, cliente[i].CfUF, cliente[i].CfId, "0", "0", "0", cliente[i].CfStatus });
                            }
                            t.ShowDialog();
                        }

                        if (!String.IsNullOrEmpty(Principal.nomeCliFor))
                        {
                            txtFNome.Text = Principal.nomeCliFor;
                            txtFCnpj.Text = Principal.cnpjCliFor;
                            txtFCidade.Text = Principal.cidadeCliFor;
                            cmbFBusca.SelectedIndex = 0;
                            txtFBusca.Text = "";
                            txtCodBarras.Focus();
                        }
                        else
                        {
                            cmbFBusca.SelectedIndex = 0;
                            txtFBusca.Text = "";
                            txtFBusca.Focus();
                        }

                    }
                }
            }

        }

        private void dtValidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbClasseTera.Focus();
        }

        private void cmbClasseTera_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                chkControlado.Focus();
        }
    }
}
