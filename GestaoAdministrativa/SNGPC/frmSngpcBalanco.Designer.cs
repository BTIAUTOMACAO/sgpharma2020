﻿namespace GestaoAdministrativa.SNGPC
{
    partial class frmSngpcRelBalanco
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSngpcRelBalanco));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cmbMes = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtAno = new System.Windows.Forms.TextBox();
            this.rdb4 = new System.Windows.Forms.RadioButton();
            this.rdb3 = new System.Windows.Forms.RadioButton();
            this.rdb2 = new System.Windows.Forms.RadioButton();
            this.rdb1 = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.rdbAnual = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.rdbReceitasB = new System.Windows.Forms.RadioButton();
            this.rdbReceitasA = new System.Windows.Forms.RadioButton();
            this.rdbAquisicoes = new System.Windows.Forms.RadioButton();
            this.rdbCompleto = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbClassTera = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 163;
            this.groupBox1.TabStop = false;
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.groupBox4);
            this.panel2.Location = new System.Drawing.Point(7, 36);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(959, 495);
            this.panel2.TabIndex = 43;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.btnPesquisar);
            this.groupBox4.Controls.Add(this.groupBox3);
            this.groupBox4.Controls.Add(this.groupBox2);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.Black;
            this.groupBox4.Location = new System.Drawing.Point(12, 4);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox4.Size = new System.Drawing.Size(932, 212);
            this.groupBox4.TabIndex = 161;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Filtros";
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPesquisar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisar.Image")));
            this.btnPesquisar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPesquisar.Location = new System.Drawing.Point(784, 154);
            this.btnPesquisar.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(142, 48);
            this.btnPesquisar.TabIndex = 161;
            this.btnPesquisar.Text = "Pesquisar (F1)";
            this.btnPesquisar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPesquisar.UseVisualStyleBackColor = true;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cmbClassTera);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.cmbMes);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.txtAno);
            this.groupBox3.Controls.Add(this.rdb4);
            this.groupBox3.Controls.Add(this.rdb3);
            this.groupBox3.Controls.Add(this.rdb2);
            this.groupBox3.Controls.Add(this.rdb1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.rdbAnual);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(332, 22);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(446, 183);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Período";
            // 
            // cmbMes
            // 
            this.cmbMes.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbMes.FormattingEnabled = true;
            this.cmbMes.Items.AddRange(new object[] {
            "JANEIRO",
            "FEVEREIRO",
            "MARÇO",
            "ABRIL",
            "MAIO",
            "JUNHO",
            "JULHO",
            "AGOSTO",
            "SETEMBRO",
            "OUTUBRO",
            "NOVEMBRO",
            "DEZEMBRO"});
            this.cmbMes.Location = new System.Drawing.Point(22, 145);
            this.cmbMes.Name = "cmbMes";
            this.cmbMes.Size = new System.Drawing.Size(229, 24);
            this.cmbMes.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 126);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(116, 16);
            this.label3.TabIndex = 8;
            this.label3.Text = "Mês do Exercício";
            // 
            // txtAno
            // 
            this.txtAno.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAno.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAno.Location = new System.Drawing.Point(22, 50);
            this.txtAno.Name = "txtAno";
            this.txtAno.Size = new System.Drawing.Size(100, 22);
            this.txtAno.TabIndex = 7;
            this.txtAno.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAno.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtAno_KeyDown);
            this.txtAno.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAno_KeyPress);
            // 
            // rdb4
            // 
            this.rdb4.AutoSize = true;
            this.rdb4.ForeColor = System.Drawing.Color.Navy;
            this.rdb4.Location = new System.Drawing.Point(400, 83);
            this.rdb4.Name = "rdb4";
            this.rdb4.Size = new System.Drawing.Size(38, 20);
            this.rdb4.TabIndex = 6;
            this.rdb4.TabStop = true;
            this.rdb4.Text = "4°";
            this.rdb4.UseVisualStyleBackColor = true;
            this.rdb4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdb4_KeyDown);
            // 
            // rdb3
            // 
            this.rdb3.AutoSize = true;
            this.rdb3.ForeColor = System.Drawing.Color.Navy;
            this.rdb3.Location = new System.Drawing.Point(356, 83);
            this.rdb3.Name = "rdb3";
            this.rdb3.Size = new System.Drawing.Size(38, 20);
            this.rdb3.TabIndex = 5;
            this.rdb3.TabStop = true;
            this.rdb3.Text = "3°";
            this.rdb3.UseVisualStyleBackColor = true;
            this.rdb3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdb3_KeyDown);
            // 
            // rdb2
            // 
            this.rdb2.AutoSize = true;
            this.rdb2.ForeColor = System.Drawing.Color.Navy;
            this.rdb2.Location = new System.Drawing.Point(312, 83);
            this.rdb2.Name = "rdb2";
            this.rdb2.Size = new System.Drawing.Size(38, 20);
            this.rdb2.TabIndex = 4;
            this.rdb2.TabStop = true;
            this.rdb2.Text = "2°";
            this.rdb2.UseVisualStyleBackColor = true;
            this.rdb2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdb2_KeyDown);
            // 
            // rdb1
            // 
            this.rdb1.AutoSize = true;
            this.rdb1.ForeColor = System.Drawing.Color.Navy;
            this.rdb1.Location = new System.Drawing.Point(268, 83);
            this.rdb1.Name = "rdb1";
            this.rdb1.Size = new System.Drawing.Size(38, 20);
            this.rdb1.TabIndex = 3;
            this.rdb1.TabStop = true;
            this.rdb1.Text = "1°";
            this.rdb1.UseVisualStyleBackColor = true;
            this.rdb1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdb1_KeyDown);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(265, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Trimestral:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // rdbAnual
            // 
            this.rdbAnual.AutoSize = true;
            this.rdbAnual.ForeColor = System.Drawing.Color.Navy;
            this.rdbAnual.Location = new System.Drawing.Point(268, 29);
            this.rdbAnual.Name = "rdbAnual";
            this.rdbAnual.Size = new System.Drawing.Size(63, 20);
            this.rdbAnual.TabIndex = 1;
            this.rdbAnual.TabStop = true;
            this.rdbAnual.Text = "Anual";
            this.rdbAnual.UseVisualStyleBackColor = true;
            this.rdbAnual.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbAnual_KeyDown);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Ano Exercício";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.rdbReceitasB);
            this.groupBox2.Controls.Add(this.rdbReceitasA);
            this.groupBox2.Controls.Add(this.rdbAquisicoes);
            this.groupBox2.Controls.Add(this.rdbCompleto);
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(16, 22);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(301, 183);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tipo de Relatório";
            // 
            // rdbReceitasB
            // 
            this.rdbReceitasB.AutoSize = true;
            this.rdbReceitasB.ForeColor = System.Drawing.Color.Navy;
            this.rdbReceitasB.Location = new System.Drawing.Point(18, 138);
            this.rdbReceitasB.Name = "rdbReceitasB";
            this.rdbReceitasB.Size = new System.Drawing.Size(272, 36);
            this.rdbReceitasB.TabIndex = 3;
            this.rdbReceitasB.TabStop = true;
            this.rdbReceitasB.Text = "Balanço de Notificação de Receitas (B)\r\nPortaria B2";
            this.rdbReceitasB.UseVisualStyleBackColor = true;
            this.rdbReceitasB.CheckedChanged += new System.EventHandler(this.rdbReceitasB_CheckedChanged);
            this.rdbReceitasB.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbReceitasB_KeyDown);
            this.rdbReceitasB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbReceitasB_KeyPress);
            // 
            // rdbReceitasA
            // 
            this.rdbReceitasA.AutoSize = true;
            this.rdbReceitasA.ForeColor = System.Drawing.Color.Navy;
            this.rdbReceitasA.Location = new System.Drawing.Point(18, 91);
            this.rdbReceitasA.Name = "rdbReceitasA";
            this.rdbReceitasA.Size = new System.Drawing.Size(272, 36);
            this.rdbReceitasA.TabIndex = 2;
            this.rdbReceitasA.TabStop = true;
            this.rdbReceitasA.Text = "Balanço de Notificação de Receitas (A)\r\nPortaria A1, A2 e A3";
            this.rdbReceitasA.UseVisualStyleBackColor = true;
            this.rdbReceitasA.CheckedChanged += new System.EventHandler(this.rdbReceitasA_CheckedChanged);
            this.rdbReceitasA.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbReceitasA_KeyDown);
            this.rdbReceitasA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbReceitasA_KeyPress);
            // 
            // rdbAquisicoes
            // 
            this.rdbAquisicoes.AutoSize = true;
            this.rdbAquisicoes.ForeColor = System.Drawing.Color.Navy;
            this.rdbAquisicoes.Location = new System.Drawing.Point(18, 59);
            this.rdbAquisicoes.Name = "rdbAquisicoes";
            this.rdbAquisicoes.Size = new System.Drawing.Size(176, 20);
            this.rdbAquisicoes.TabIndex = 1;
            this.rdbAquisicoes.TabStop = true;
            this.rdbAquisicoes.Text = "Balanço das Aquisições";
            this.rdbAquisicoes.UseVisualStyleBackColor = true;
            this.rdbAquisicoes.CheckedChanged += new System.EventHandler(this.rdbAquisicoes_CheckedChanged);
            this.rdbAquisicoes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbAquisicoes_KeyDown);
            this.rdbAquisicoes.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbAquisicoes_KeyPress);
            // 
            // rdbCompleto
            // 
            this.rdbCompleto.AutoSize = true;
            this.rdbCompleto.ForeColor = System.Drawing.Color.Navy;
            this.rdbCompleto.Location = new System.Drawing.Point(18, 21);
            this.rdbCompleto.Name = "rdbCompleto";
            this.rdbCompleto.Size = new System.Drawing.Size(143, 20);
            this.rdbCompleto.TabIndex = 0;
            this.rdbCompleto.TabStop = true;
            this.rdbCompleto.Text = "Balanço Completo";
            this.rdbCompleto.UseVisualStyleBackColor = true;
            this.rdbCompleto.CheckedChanged += new System.EventHandler(this.rdbCompleto_CheckedChanged);
            this.rdbCompleto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbCompleto_KeyDown);
            this.rdbCompleto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.rdbCompleto_KeyPress);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(214, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Relátorio de Balanço SNGPC";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(19, 79);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(160, 16);
            this.label4.TabIndex = 11;
            this.label4.Text = "Tipo Classe Terapeutica";
            // 
            // cmbClassTera
            // 
            this.cmbClassTera.FormattingEnabled = true;
            this.cmbClassTera.Items.AddRange(new object[] {
            "TODAS AS CLASSES",
            "ANTIMICROBIANOS",
            "SUJEITO A CONTROLE ESPECIAL"});
            this.cmbClassTera.Location = new System.Drawing.Point(22, 99);
            this.cmbClassTera.Name = "cmbClassTera";
            this.cmbClassTera.Size = new System.Drawing.Size(172, 24);
            this.cmbClassTera.TabIndex = 12;
            // 
            // frmSngpcRelBalanco
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmSngpcRelBalanco";
            this.Text = "frmSngpcBalanco";
            this.Load += new System.EventHandler(this.frmSngpcRelBalanco_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmSngpcRelBalanco_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton rdbAquisicoes;
        private System.Windows.Forms.RadioButton rdbCompleto;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.Button btnPesquisar;
        public System.Windows.Forms.TextBox txtAno;
        public System.Windows.Forms.RadioButton rdb4;
        public System.Windows.Forms.RadioButton rdb3;
        public System.Windows.Forms.RadioButton rdb2;
        public System.Windows.Forms.RadioButton rdb1;
        public System.Windows.Forms.RadioButton rdbAnual;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.ComboBox cmbMes;
        public System.Windows.Forms.RadioButton rdbReceitasB;
        public System.Windows.Forms.RadioButton rdbReceitasA;
        private System.Windows.Forms.ComboBox cmbClassTera;
        private System.Windows.Forms.Label label4;
    }
}