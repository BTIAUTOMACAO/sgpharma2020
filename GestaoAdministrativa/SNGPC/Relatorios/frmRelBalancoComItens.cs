﻿using GestaoAdministrativa.Classes;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.SNGPC.Relatorios
{
    public partial class frmRelBalancoComItens : Form
    {
        private frmSngpcRelBalanco relatorio;
        private DataTable dtRelatorio;

        public frmRelBalancoComItens(frmSngpcRelBalanco frmRelatorio, DataTable dtBusca)
        {
            InitializeComponent();
            relatorio = frmRelatorio;
            dtRelatorio = dtBusca;
        }

        private void frmRelBalancoComItens_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rwRelatorio.RefreshReport();
            this.rwRelatorio.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[8];
            parametro[0] = new ReportParameter("CNPJ", Funcoes.LeParametro(6, "129", false));
            parametro[1] = new ReportParameter("ANO", relatorio.txtAno.Text);
            parametro[2] = new ReportParameter("TRI1", relatorio.rdb1.Checked ? "( X )" : "(  )");
            parametro[3] = new ReportParameter("TRI2", relatorio.rdb2.Checked ? "( X )" : "(  )");
            parametro[4] = new ReportParameter("TRI3", relatorio.rdb3.Checked ? "( X )" : "(  )");
            parametro[5] = new ReportParameter("TRI4", relatorio.rdb4.Checked ? "( X )" : "(  )");
            parametro[6] = new ReportParameter("ANUAL", relatorio.rdbAnual.Checked ? "( X )" : "(  )");
            parametro[7] = new ReportParameter("semregistro", dtRelatorio.Rows.Count == 0 ? "NENHUM REGISTRO ENCONTRADO" : " ");
            this.rwRelatorio.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            var aquisicoes = new ReportDataSource("dsCompleto", dtRelatorio);
            this.rwRelatorio.LocalReport.DataSources.Clear();
            this.rwRelatorio.LocalReport.DataSources.Add(aquisicoes);
        }
    }
}
