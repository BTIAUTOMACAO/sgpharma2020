﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmRelatorioCompras : Form
    {
        string dtInicial, dtFinal;
        char ordem;
        string tipoOperacao;
        int tipoMedicamento;

        public frmRelatorioCompras(string dtIni, string dtFin, char ordenar, string tipo, int tipoMed)
        {
            dtInicial = dtIni;
            dtFinal = dtFin;
            ordem = ordenar;
            tipoOperacao = tipo;
            tipoMedicamento = tipoMed;
            InitializeComponent();
        }

        private void frmSngpcCompras_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorioAnalitico();
            this.rpwRelatorioCompras.RefreshReport();
        }
        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", dtInicial);
            parametro[2] = new ReportParameter("dtFinal", dtFinal);
            rpwRelatorioCompras.LocalReport.SetParameters(parametro);
        }
        public void MontaRelatorioAnalitico()
        {
            SngpcEntrada entrada = new SngpcEntrada();
            DataTable dtEntrada = entrada.BuscaEntrasPorPeriodo(dtInicial, dtFinal, ordem, tipoOperacao, tipoMedicamento);
            var entradas = new ReportDataSource("Compras", dtEntrada);
            rpwRelatorioCompras.LocalReport.DataSources.Clear();
            rpwRelatorioCompras.LocalReport.DataSources.Add(entradas);

        }
    }
}
