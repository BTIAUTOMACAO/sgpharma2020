﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmRelatorioVendas : Form
    {
        string dtInicial, dtFinal;
        char ordem;
        int tipoReceita;

        public frmRelatorioVendas(string dtIni, string dtFim, char ordenar, int tipo)
        {
            dtInicial = dtIni;
            dtFinal = dtFim;
            ordem = ordenar;
            tipoReceita = tipo;

            InitializeComponent();
        }

        private void frmRelatorioVendas_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rpwVendas.RefreshReport();
        }
        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", dtInicial);
            parametro[2] = new ReportParameter("dtFinal", dtFinal);
            this.rpwVendas.LocalReport.SetParameters(parametro);
        }
        public void MontaRelatorio()
        {
            SngpcVenda venda = new SngpcVenda();
            DataTable dtVendas = venda.BuscaDadosVendaPeriodo(dtInicial, dtFinal, ordem, tipoReceita);

            var vendas = new ReportDataSource("Vendas", dtVendas);
            rpwVendas.LocalReport.DataSources.Clear();
            rpwVendas.LocalReport.DataSources.Add(vendas);
        }
    }
}
