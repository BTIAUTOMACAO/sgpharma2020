﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using Microsoft.VisualBasic;
using GestaoAdministrativa.Negocio;
using System.Net.Http;
using System.Net.Http.Headers;
using System.IO;

namespace GestaoAdministrativa.SNGPC
{
    public partial class frmSngpcVendas : Form, Botoes
    {
        //DECLARACAO DAS VARIÁVEIS//
        private DataTable dtVendas = new DataTable();
        private int contRegistros;
        private string[,] dados = new string[24, 3];
        private bool emGrade;
        private ToolStripButton tsbVendas = new ToolStripButton("Venda SNGPC");
        private ToolStripSeparator tssVendas = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        private int quantidade;

        public frmSngpcVendas(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmSngpcVendas_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtBDocto.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmSngpcVendas_Shown(object sender, EventArgs e)
        {
            CarregaCombos();

            txtCodBarras.ReadOnly = false;
            txtDescr.ReadOnly = false;
        }

        public bool CarregaCombos()
        {
            try
            {

                SngpcTabela tabelas = new SngpcTabela();
                //CARREGA TIPO DE RECEITUARIO//
                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(1);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de receituário,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbTipoRec.DataSource = Principal.dtPesq;
                    cmbTipoRec.DisplayMember = "TAB_DESCRICAO";
                    cmbTipoRec.ValueMember = "TAB_SEQ";
                    cmbTipoRec.SelectedIndex = -1;
                }

                //CARREGA TIPO DE DOCUMENTO//
                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(9);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de documento,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbDocto.DataSource = Principal.dtPesq;
                    cmbDocto.DisplayMember = "TAB_DESCRICAO";
                    cmbDocto.ValueMember = "TAB_SEQ";
                    cmbDocto.SelectedIndex = -1;

                }

                //CARREGA TIPO DE USO DO MEDICAMENTO//
                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(2);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de uso do medicamento,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbUMed.DataSource = Principal.dtPesq;
                    cmbUMed.DisplayMember = "TAB_DESCRICAO";
                    cmbUMed.ValueMember = "TAB_SEQ";
                    cmbUMed.SelectedIndex = -1;

                }

                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(4);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de conselho profissional,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbConselho.DataSource = Principal.dtPesq;
                    cmbConselho.DisplayMember = "TAB_DESCRICAO";
                    cmbConselho.ValueMember = "TAB_SEQ";
                    cmbConselho.SelectedIndex = -1;
                }

                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(5);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma unidade federativa,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbUF.DataSource = Principal.dtPesq;
                    cmbUF.DisplayMember = "TAB_CODIGO";
                    cmbUF.ValueMember = "TAB_SEQ";
                    cmbUF.SelectedIndex = -1;

                    cmbMUF.DataSource = Principal.dtPesq;
                    cmbMUF.DisplayMember = "TAB_CODIGO";
                    cmbMUF.ValueMember = "TAB_SEQ";
                    cmbMUF.SelectedIndex = -1;

                }

                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(10);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um orgão expedidor,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbOrgao.DataSource = Principal.dtPesq;
                    cmbOrgao.DisplayMember = "TAB_DESCRICAO";
                    cmbOrgao.ValueMember = "TAB_SEQ";
                    cmbOrgao.SelectedIndex = -1;

                }

                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(11);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um sexo,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbPSexo.DataSource = Principal.dtPesq;
                    cmbPSexo.DisplayMember = "TAB_DESCRICAO";
                    cmbPSexo.ValueMember = "TAB_SEQ";
                    cmbPSexo.SelectedIndex = -1;

                }

                Principal.dtPesq = tabelas.BuscaTabelaPorTipo(12);
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um tipo de unidade de idade,\npara realizadar uma venda.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbPUnidade.DataSource = Principal.dtPesq;
                    cmbPUnidade.DisplayMember = "TAB_DESCRICAO";
                    cmbPUnidade.ValueMember = "TAB_SEQ";
                    cmbPUnidade.SelectedIndex = -1;

                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmSngpcVendas");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtVendas, contRegistros));
            if (tcVendas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcVendas.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtVenID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbVendas.AutoSize = false;
                this.tsbVendas.Image = Properties.Resources.sngpc;
                this.tsbVendas.Size = new System.Drawing.Size(140, 20);
                this.tsbVendas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbVendas);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssVendas);
                tsbVendas.Click += delegate
                {
                    var cadVendas = Application.OpenForms.OfType<frmSngpcVendas>().FirstOrDefault();
                    BotoesHabilitados();
                    cadVendas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbVendas);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssVendas);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtVenID.Text = dtVendas.Rows[linha]["VEN_SEQ"].ToString();
                cmbDocto.Text = dtVendas.Rows[linha]["VEN_COMP_TD"].ToString();
                cmbOrgao.Text = dtVendas.Rows[linha]["VEN_COMP_OE"].ToString();
                cmbUF.Text = dtVendas.Rows[linha]["VEN_COMP_UF"].ToString();
                txtDocto.Text = dtVendas.Rows[linha]["VEN_COMP_DOCTO"].ToString();
                txtNome.Text = dtVendas.Rows[linha]["VEN_COMP_NOME"].ToString();
                dtData.Value = Convert.ToDateTime(dtVendas.Rows[linha]["VEN_DATA"]);
                txtPNome.Text = Funcoes.ChecaCampoVazio(dtVendas.Rows[linha]["VEN_PAC_NOME"].ToString());
                txtPIdade.Text = Funcoes.ChecaCampoVazio(dtVendas.Rows[linha]["VEN_PAC_IDADE"].ToString());
                if (Funcoes.ChecaCampoVazio(dtVendas.Rows[linha]["VEN_PAC_UNIDADE"].ToString()) == "")
                {
                    cmbPUnidade.SelectedIndex = -1;
                }
                else
                {
                    cmbPUnidade.Text = dtVendas.Rows[linha]["VEN_PAC_UNIDADE"].ToString();
                }
                if (Funcoes.ChecaCampoVazio(dtVendas.Rows[linha]["VEN_PAC_SEXO"].ToString()) == "")
                {
                    cmbPSexo.SelectedIndex = -1;
                }
                else
                {
                    cmbPSexo.Text = dtVendas.Rows[linha]["VEN_PAC_SEXO"].ToString();
                }
                txtCrm.Text = dtVendas.Rows[linha]["VEN_PRESC_NUMERO"].ToString();
                txtMedico.Text = dtVendas.Rows[linha]["VEN_PRESC_NOME"].ToString();
                cmbConselho.Text = dtVendas.Rows[linha]["VEN_PRESC_CP"].ToString();
                txtNReceita.Text = dtVendas.Rows[linha]["VEN_NUM_NOT"].ToString();
                dtReceita.Value = Convert.ToDateTime(dtVendas.Rows[linha]["VEN_PRESC_DT"]);
                cmbMUF.Text = dtVendas.Rows[linha]["VEN_PRESC_UF"].ToString();
                cmbUMed.Text = dtVendas.Rows[linha]["VEN_USO"].ToString();
                cmbTipoRec.Text = dtVendas.Rows[linha]["VEN_TP_RECEITA"].ToString();
                txtCodBarras.Text = dtVendas.Rows[linha]["PROD_CODIGO"].ToString();
                txtDescr.Text = dtVendas.Rows[linha]["PROD_DESCR"].ToString();
                txtReg.Text = dtVendas.Rows[linha]["PROD_REGISTRO_MS"].ToString();

                BuscaLotes();
                cmbLote.Text = dtVendas.Rows[linha]["VEN_MED_LOTE"].ToString();

                if (cmbLote.Text == "")
                {
                    txtLote.Visible = true;
                    lblLote.Visible = true;
                    txtLote.Text = dtVendas.Rows[linha]["VEN_MED_LOTE"].ToString();
                }
                else
                {
                    txtLote.Visible = false;
                    lblLote.Visible = false;
                }
                
                txtQtde.Text = dtVendas.Rows[linha]["VEN_MED_QTDE"].ToString();
                quantidade = Convert.ToInt32(dtVendas.Rows[linha]["VEN_MED_QTDE"]);
                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtVendas, linha));
                cmbDocto.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtVendas.Clear();
            dgVendas.DataSource = dtVendas;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            emGrade = false;
            Array.Clear(dados, 0, 72);
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            txtBDocto.Focus();
        }

        public void LimparFicha()
        {
            cmbLote.DataSource = null;
            txtVenID.Text = "";
            cmbDocto.SelectedIndex = -1;
            cmbOrgao.SelectedIndex = -1;
            cmbUF.SelectedIndex = -1;
            txtDocto.Text = "";
            txtNome.Text = "";
            dtData.Value = DateTime.Now;
            txtPNome.Text = "";
            txtPIdade.Text = "";
            cmbPUnidade.SelectedIndex = -1;
            cmbPSexo.SelectedIndex = -1;
            txtCrm.Text = "";
            txtMedico.Text = "";
            cmbConselho.SelectedIndex = -1;
            txtNReceita.Text = "";
            dtReceita.Value = DateTime.Now;
            cmbMUF.SelectedIndex = -1;
            cmbUMed.SelectedIndex = -1;
            cmbTipoRec.SelectedIndex = -1;
            txtCodBarras.Text = "";
            txtDescr.Text = "";
            txtReg.Text = "";
            txtLote.Text = "";
            txtQtde.Text = "0";
            cmbDocto.Text = "CARTEIRA DE IDENTIDADE";
            cmbOrgao.Text = "SECRETÁRIA DE SEGURANÇA PÚBLICA";
            cmbUF.Text = "SP";
            txtDocto.Focus();
            dtData.Value = DateTime.Now;
            dtReceita.Value = DateTime.Now;
            txtLote.Visible = false;
            lblLote.Visible = false;
            imgLote.Visible = false;

            txtCodBarras.ReadOnly = false;
            txtDescr.ReadOnly = false;
            Array.Clear(dados, 0, 72);
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = true;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            cmbDocto.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcVendas.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgVendas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgVendas.CurrentCell = dgVendas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgVendas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgVendas.CurrentCell = dgVendas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Primeiro()
        {
            if (dtVendas.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgVendas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgVendas.CurrentCell = dgVendas.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcVendas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
            else
            {

                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtVendas.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgVendas.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgVendas.CurrentCell = dgVendas.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Atualiza()
        {
            try
            {

                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    DateTime dtDataLan = Convert.ToDateTime(Funcoes.LeParametro(11, "01", true));
                    if (DateTime.Compare(dtData.Value, dtDataLan) <= 0)
                    {
                        MessageBox.Show("A data não pode ser menor que a ultima Transferência.\nCaso queria ATUALIZAR entre na Tela de Transfência e FAÇA A CORREÇÃO DO PERÍODO e tente novamente.", "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtData.Focus();
                        Cursor = Cursors.Default;
                        return false;
                    }
                    else
                    {
                        SngpcVenda venda = new SngpcVenda()
                        {
                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            VenID = Convert.ToInt32(txtVenID.Text),
                            VenCompTd = cmbDocto.SelectedValue.ToString(),
                            VenCompOE = cmbOrgao.SelectedValue.ToString(),
                            VenCompUF = cmbUF.SelectedValue.ToString(),
                            VenCompDocto = txtDocto.Text.Trim(),
                            VenCompNome = txtNome.Text.Trim(),
                            VenData = Convert.ToDateTime(dtData.Text),
                            VenPacNome = txtPNome.Text.Trim(),
                            VenPacIdade = Convert.ToInt32(txtPIdade.Text.Trim()),
                            VenPacUnidade = cmbPUnidade.SelectedValue.ToString(),
                            VenPacSexo = cmbPSexo.SelectedValue.ToString(),
                            VenPrescNum = txtCrm.Text.Trim(),
                            VenPrescNome = txtMedico.Text.Trim(),
                            VenPrescCP = cmbConselho.SelectedValue.ToString(),
                            VenNumNota = txtNReceita.Text.Trim(),
                            VenPrescData = Convert.ToDateTime(dtReceita.Text),
                            VenPrescUF = cmbMUF.SelectedValue.ToString(),
                            VenUso = cmbUMed.SelectedValue.ToString(),
                            VenTipoReceita = cmbTipoRec.SelectedValue.ToString(),
                            ProdCodigo = txtCodBarras.Text.Trim(),
                            VenMedLote = cmbLote.Text == "Informar Lote" ? txtLote.Text : cmbLote.Text,
                            VenMedQtde = Convert.ToInt32(txtQtde.Text),
                            VenMS = txtReg.Text.Trim()
                        };

                        Principal.dtBusca = Util.RegistrosPorEstabelecimento("SNGPC_VENDAS", "VEN_SEQ", txtVenID.Text);

                        var controVenda = new SngpcControleVendas();
                        BancoDados.AbrirTrans();
                        
                        controVenda = new SngpcControleVendas()
                        {
                            ProCodigo = Principal.dtBusca.Rows[0]["PROD_CODIGO"].ToString(),
                            Lote = Principal.dtBusca.Rows[0]["VEN_MED_LOTE"].ToString(),
                            Qtde = Convert.ToInt32(Principal.dtBusca.Rows[0]["VEN_MED_QTDE"]),
                            Data = DateTime.Now
                        };
                        //DEVOLVO O LOTE VENDIDO//
                        if (!controVenda.SomaQtdeMesmoLoteEProduto(controVenda))
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("102 - Erro ao efetuar o Atualizar  ", "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                        
                        //VENDO O NOVO OU O MESMO LOTE//
                        controVenda = new SngpcControleVendas()
                        {
                            ProCodigo = venda.ProdCodigo,
                            Lote = venda.VenMedLote,
                            Qtde = venda.VenMedQtde,
                            Data = DateTime.Now
                        };
                        
                        if (!controVenda.SubtraiQtdeMesmoLoteEProduto(controVenda))
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("102 - Erro ao efetuar o Atualizar  ", "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }

                        var prod = new Produto();

                        if (venda.AtualizaVenda(venda, Principal.dtBusca).Equals(true))
                        {
                            BancoDados.FecharTrans();

                            if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                            {
                                //decremeta estoque 
                                AtualizaEstoqueFilial(txtCodBarras.Text, Convert.ToInt32(Principal.dtBusca.Rows[0]["VEN_MED_QTDE"]));
                                //acrecenta estoque
                                EstoqueFilial(prod.BuscaProdutos(txtCodBarras.Text), "menos");
                            }

                            MessageBox.Show("Venda Atualizada com Sucesso", "Venda SNGPC", MessageBoxButtons.OK);
                            Limpar();
                        }
                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        public async void AtualizaEstoqueFilial(string codBarras, int qtd)
        {
            string codEstabelecimento = Funcoes.LeParametro(9, "52", false);
            string enderecoWebService = Funcoes.LeParametro(9, "54", true);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(enderecoWebService);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueVenda?codEstab=" + codEstabelecimento + "&codBarras=" + codBarras + "&qtd=" + qtd + "&operacao=mais");
                if (!response.IsSuccessStatusCode)
                {
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\VendasSNGPC.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + txtCodBarras.Text + " / QTDE: " + txtQtde.Text + " / Operacação: Atualização de Estoque ");
                        }
                    }
                }
            }
        }


        public bool Excluir()
        {
            try
            {
                //VERIFICA SE A DATA DO PARAMENTRO E MAIOR QUE O DATA//
                DateTime dtDataLan = Convert.ToDateTime(Funcoes.LeParametro(11, "01", true));
                if (DateTime.Compare(dtData.Value, dtDataLan) <= 0)
                {
                    MessageBox.Show("Este lançamento não pode ser excluído, já foi feito a interface para esta venda.", "Venda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtData.Focus();
                    Cursor = Cursors.Default;
                    return false;
                }

                if (txtVenID.Text.Trim() != "")
                {
                    if (MessageBox.Show("Confirma exclusão da venda do medicamentos?", "Exclusão tabela Venda de Medicamento", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        SngpcControleVendas controVenda = new SngpcControleVendas();
                        SngpcVenda venda = new SngpcVenda();
                        Principal.dtBusca = Util.RegistrosPorEstabelecimento("SNGPC_VENDAS", "VEN_SEQ", txtVenID.Text);
                        DataTable dtControVenda = controVenda.BuscaProduto(txtCodBarras.Text, Principal.dtBusca.Rows[0]["VEN_MED_LOTE"].ToString());
                        BancoDados.AbrirTrans();

                        controVenda = new SngpcControleVendas()
                        {
                            ProCodigo = txtCodBarras.Text,
                            Lote = Principal.dtBusca.Rows[0]["VEN_MED_LOTE"].ToString(),
                            Qtde = Convert.ToInt32(Principal.dtBusca.Rows[0]["VEN_MED_QTDE"]),
                            Data = DateTime.Now
                        };

                        if (dtControVenda.Rows.Count != 0)
                        {
                            if (!controVenda.IdentificaSeJaTemLoteEProdutoExclusao(controVenda.ProCodigo, controVenda.Lote).Equals(0))
                            {
                                if (controVenda.Incluir(controVenda).Equals(false))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 - Erro ao efetuar a exclusão  ", "SNGPC Perca de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }
                            else
                            {
                                if (controVenda.SomaQtdeMesmoLoteEProduto(controVenda).Equals(false))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 - Erro ao efetuar a exclusão  ", "SNGPC Perca de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            if (controVenda.Incluir(controVenda).Equals(false))
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("102 - Erro ao efetuar a exclusão  ", "SNGPC Perca de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }

                        if (venda.ExcluiDados(Convert.ToInt32(txtVenID.Text)).Equals(true))
                        {
                            if (Funcoes.LeParametro(9, "51", true).Equals("S"))
                            {
                                AtualizaEstoqueFilial(txtCodBarras.Text, Convert.ToInt32(txtQtde.Text));
                            }

                            Funcoes.GravaLogExclusao("VEN_ID", txtVenID.Text, Principal.usuario, DateTime.Now, "SNGPC_VENDAS", txtVenID.Text, "ERRO EXCLUSÃO", Principal.estAtual);
                            BancoDados.FecharTrans();
                            MessageBox.Show("Exclusão realizado com sucesso", "SNGPC Venda", MessageBoxButtons.OK);
                            dtVendas.Clear();
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dgVendas.DataSource = dtVendas;
                            emGrade = false;
                            tcVendas.SelectedTab = tpGrade;
                            tslRegistros.Text = "";
                            Funcoes.LimpaFormularios(this);
                            txtBDocto.Focus();
                            return true;
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                        }
                    }
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos().Equals(true))
                {
                    DateTime dtDataLan = Convert.ToDateTime(Funcoes.ChecaParamentos(11, "01"));
                    if (DateTime.Compare(dtData.Value, dtDataLan) <= 0)
                    {
                        MessageBox.Show("A data não pode ser menor que a ultima Transferência.\nCaso queria INCLUIR entre na Tela de Transfência e FAÇA A CORREÇÃO DO PERÍODO e tente novamente. ", "Perda de Medicamento", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtData.Focus();
                        Cursor = Cursors.Default;
                        return false;
                    }
                    else
                    {
                        SngpcVenda venda = new SngpcVenda()
                        {

                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            VenID = Funcoes.IdentificaVerificaID("SNGPC_VENDAS", "VEN_SEQ", 0, ""),
                            VenCompTd = cmbDocto.SelectedValue.ToString(),
                            VenCompOE = cmbOrgao.SelectedValue.ToString(),
                            VenCompUF = cmbUF.SelectedValue.ToString(),
                            VenCompDocto = txtDocto.Text.Trim(),
                            VenCompNome = txtNome.Text.Trim(),
                            VenData = Convert.ToDateTime(dtData.Text),
                            VenPacNome = txtPNome.Text.Trim(),
                            VenPacIdade = txtPIdade.Text.Trim() == "" ? 0 : Convert.ToInt32(txtPIdade.Text.Trim()),
                            VenPacUnidade = cmbPUnidade.SelectedValue.ToString(),
                            VenPacSexo = cmbPSexo.SelectedValue.ToString(),
                            VenPrescNum = txtCrm.Text,
                            VenPrescNome = txtMedico.Text,
                            VenPrescCP = cmbConselho.SelectedValue.ToString(),
                            VenNumNota = txtNReceita.Text,
                            VenPrescData = Convert.ToDateTime(dtReceita.Text),
                            VenPrescUF = cmbMUF.SelectedValue.ToString(),
                            VenUso = cmbUMed.SelectedValue.ToString(),
                            VenTipoReceita = cmbTipoRec.SelectedValue.ToString(),
                            ProdCodigo = txtCodBarras.Text.Trim(),
                            VenMedLote = cmbLote.Text == "Informar Lote" ? txtLote.Text : cmbLote.Text,
                            VenMedQtde = Convert.ToInt32(txtQtde.Text),
                            VenMS = txtReg.Text.Trim()

                        };
                        BancoDados.AbrirTrans();
                        if (venda.IncluirVenda(venda).Equals(true))
                        {
                            var prod = new Produto();
                            DataTable dtProd = prod.BuscaProdutos(txtCodBarras.Text);

                            if (cmbLote.Text != "Informar Lote")
                            {
                                SngpcControleVendas controVenda = new SngpcControleVendas();

                                controVenda = new SngpcControleVendas()
                                {
                                    ProCodigo = txtCodBarras.Text,
                                    Lote = cmbLote.Text,
                                    Qtde = Convert.ToInt32(txtQtde.Text),
                                    Data = DateTime.Now
                                };

                                if (controVenda.SubtraiQtdeMesmoLoteEProduto(controVenda).Equals(false))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("102 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    Cursor = Cursors.Default;
                                    return false;
                                }
                                else
                                {
                                    BancoDados.FecharTrans();

                                    MessageBox.Show("Venda Realizada com sucesso", "Vendas SNGPC", MessageBoxButtons.OK);
                                    Cursor = Cursors.Default;
                                    if (MessageBox.Show("Mantém os dados do Paciente?", "Vendas SNGPC", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                                    {
                                        Limpar();
                                        dtVendas.Clear();
                                        dgVendas.DataSource = dtVendas;
                                    }
                                    else
                                    {
                                        txtVenID.Text = "";
                                        txtCodBarras.Text = "";
                                        txtDescr.Text = "";
                                        txtReg.Text = "";
                                        cmbLote.SelectedIndex = -1;
                                        txtQtdeEstoque.Text = "0";
                                        txtLote.Text = "";
                                        txtLote.Visible = false;
                                        txtQtde.Text = "0";
                                        txtCodBarras.Focus();
                                    }
                                    return true;
                                }
                            }
                            else
                            {
                                BancoDados.FecharTrans();
                                
                                MessageBox.Show("Venda Realizada com sucesso", "Vendas SNGPC", MessageBoxButtons.OK);
                                Cursor = Cursors.Default;
                                if (MessageBox.Show("Matem os dados do Paciente?", "Vendas SNGPC", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                                {
                                    Limpar();
                                    dtVendas.Clear();
                                    dgVendas.DataSource = dtVendas;
                                }
                                else
                                {
                                    txtVenID.Text = "";
                                    txtCodBarras.Text = "";
                                    txtDescr.Text = "";
                                    txtReg.Text = "";
                                    cmbLote.SelectedIndex = -1;
                                    txtQtdeEstoque.Text = "0";
                                    txtLote.Text = "";
                                    txtLote.Visible = false;
                                    txtQtde.Text = "0";
                                    txtCodBarras.Focus();
                                }
                                return true;
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("101 - Erro ao efetuar o cadastro  ", "Produtos SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            Cursor = Cursors.Default;
                            return false;
                        }
                    }
                }
                else
                {
                    Cursor = Cursors.Default;
                    return false;

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }


        public async void EstoqueFilial(DataTable dtProd, string operacao)
        {
            string codEstabelecimento = Funcoes.LeParametro(9, "52", false);
            string enderecoWebService = Funcoes.LeParametro(9, "54", true);
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(enderecoWebService);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("Produtos/AtualizaEstoqueEntradaNotas?codBarra=" + txtCodBarras.Text + "&descricao=" + txtDescr.Text + "&qtde=" + txtQtde.Text + "&precoCusto=" + dtProd.Rows[0]["PROD_ULTCUSME"].ToString().Replace(",", ".") + "&precoVenda=" + dtProd.Rows[0]["PRE_VALOR"].ToString().Replace(",", ".") + "&codEstabelecimento=" + codEstabelecimento + "&operacao=" + operacao);
                if (!response.IsSuccessStatusCode)
                {
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\VendaSNGPC.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + txtCodBarras.Text + " / QTDE: " + txtQtde.Text + " / Operacação: " + operacao);
                        }
                    }
                }
            }
        }

        public void ImprimirRelatorio()
        {
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (Equals(cmbDocto.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um tipo de documento.";
                Funcoes.Avisa();
                cmbDocto.Focus();
                return false;
            }
            if (Equals(cmbOrgao.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um orgão expedidor.";
                Funcoes.Avisa();
                cmbOrgao.Focus();
                return false;
            }
            if (Equals(cmbUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbOrgao.Focus();
                return false;
            }
            if (Equals(cmbUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbUF.Focus();
                return false;
            }
            if (Equals(cmbPSexo.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione o sexo do paciente.";
                Funcoes.Avisa();
                cmbPSexo.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCrm.Text.Trim()))
            {
                Principal.mensagem = "CRM não pode ser em branco.";
                Funcoes.Avisa();
                txtCrm.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtMedico.Text.Trim()))
            {
                Principal.mensagem = "Nome do médico não pode ser em branco.";
                Funcoes.Avisa();
                txtMedico.Focus();
                return false;
            }
            if (Equals(cmbConselho.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione o conselho profissional.";
                Funcoes.Avisa();
                cmbConselho.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtNReceita.Text.Trim()))
            {
                Principal.mensagem = "Número da receita não pode ser em branco.";
                Funcoes.Avisa();
                txtNReceita.Focus();
                return false;
            }
            if (Equals(cmbMUF.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um estado.";
                Funcoes.Avisa();
                cmbMUF.Focus();
                return false;
            }
            if (Equals(cmbUMed.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione o uso do medicamento.";
                Funcoes.Avisa();
                cmbUMed.Focus();
                return false;
            }
            if (Equals(cmbTipoRec.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um tipo de receita.";
                Funcoes.Avisa();
                cmbTipoRec.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtCodBarras.Text.Trim()))
            {
                Principal.mensagem = "Cód. de Barras não pode ser em branco.";
                Funcoes.Avisa();
                txtCodBarras.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtReg.Text.Trim()))
            {
                Principal.mensagem = "Reg. Min. da Saúde não pode ser em branco.";
                Funcoes.Avisa();
                txtReg.Focus();
                return false;

            }
            if (String.IsNullOrEmpty(txtQtde.Text.Trim()) || Equals(txtQtde.Text, 0))
            {
                Principal.mensagem = "Qtde não pode ser zero.";
                Funcoes.Avisa();
                txtQtde.Focus();
                return false;
            }
            if (cmbLote.SelectedIndex == -1)
            {
                Principal.mensagem = "Lote não pode estar vazio.";
                Funcoes.Avisa();
                cmbLote.Focus();
                return false;
            }
            if (txtLote.Visible == true && String.IsNullOrEmpty(txtLote.Text.Trim()))
            {
                Principal.mensagem = "Lote não pode ser em branco.";
                Funcoes.Avisa();
                txtLote.Focus();
                return false;
            }
            if (Principal.mdiPrincipal.btnAtualiza.Enabled == false)
            {
                if (cmbLote.Text != "Informar Lote" && Convert.ToInt32(txtQtde.Text) > Convert.ToInt32(txtQtdeEstoque.Text))
                {
                    Principal.mensagem = "Qtde não pode ser maior doque a Qtde em Estoque";
                    Funcoes.Avisa();
                    txtQtde.Focus();
                    return false;
                }
            }
            else
            {
                if (cmbLote.Text != "Informar Lote" && Convert.ToInt32(txtQtde.Text) > Convert.ToInt32(txtQtdeEstoque.Text) && Convert.ToInt32(txtQtde.Text) > quantidade)
                {
                    Principal.mensagem = "Qtde não pode ser maior do que a Qtde em Estoque";
                    Funcoes.Avisa();
                    txtQtde.Focus();
                    return false;
                }

            }

            return true;
        }

        private void tcVendas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcVendas.SelectedTab == tpGrade)
            {
                dgVendas.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcVendas.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmSngpcVendas");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtVendas, contRegistros));
                    cmbDocto.Text = "CARTEIRA DE IDENTIDADE";
                    cmbOrgao.Text = "SECRETÁRIA DE SEGURANÇA PÚBLICA";
                    cmbUF.Text = "SP";
                    txtDocto.Focus();
                    dtData.Value = DateTime.Now;
                    dtReceita.Value = DateTime.Now;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    txtLote.Visible = false;
                    lblLote.Visible = false;
                    imgLote.Visible = false;
                    emGrade = false;
                }
            }
        }

        private void txtCodBarras_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (txtCodBarras.Text.Trim() != "" && txtCodBarras.ReadOnly == false)
                {
                    if (BuscaProdutos(txtCodBarras.Text, "").Equals(true))
                    {
                        if (BuscaLotes())
                            cmbLote.Focus();
                        else
                            cmbConselho.Focus();
                    }
                }
                else
                    txtDescr.Focus();
            }
        }



        public bool BuscaProdutos(string prodCodigo, string descricao)

        {
            Produto pro = new Produto();

            if (descricao != "")
            {
                using (frmBuscaProd buscaProd = new frmBuscaProd(false))
                {
                    buscaProd.BuscaProd(txtDescr.Text.ToUpper(), false);
                    buscaProd.ShowDialog();
                }
            }
            string codBarras = prodCodigo != "" ? prodCodigo : Principal.codBarra;

            DataTable dtProduto = pro.BuscaProdutos(codBarras);

            if (dtProduto.Rows.Count != 0)
            {
                txtCodBarras.Text = dtProduto.Rows[0]["PROD_CODIGO"].ToString();
                txtDescr.Text = dtProduto.Rows[0]["PROD_DESCR"].ToString();
                txtReg.Text = dtProduto.Rows[0]["PROD_REGISTRO_MS"].ToString();

                return true;
            }
            else
            {
                txtCodBarras.Focus();
                MessageBox.Show("Produto não encontrado! ", "Busca de Produto", MessageBoxButtons.OK);
                return false;
            }
        }


        public bool BuscaLotes()
        {
            try
            {
                var vendas = new SngpcControleVendas();
                DataTable dtLotes = vendas.BuscaLotes(txtCodBarras.Text);

                dtLotes.Rows.Add(0, "Informar Lote");
                cmbLote.DataSource = dtLotes;
                cmbLote.DisplayMember = "LOTE";
                cmbLote.ValueMember = "QTDE";
                cmbLote.SelectedIndex = -1;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void cmbDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbOrgao.Focus();
        }

        private void cmbOrgao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUF.Focus();
        }

        private void cmbUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtDocto.Focus();
        }

        private void txtDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNome.Focus();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtData.Focus();
        }

        private void dtData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPNome.Focus();
        }

        private void txtPNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPIdade.Focus();
        }

        private void txtPIdade_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                cmbPUnidade.Focus();
        }

        private void cmbPUnidade_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbPSexo.Focus();
        }

        private void cmbPSexo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCrm.Focus();
        }

        private void txtCrm_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (txtCrm.Text.Trim() != "")
                    {
                        Cursor = Cursors.WaitCursor;
                        Medico med = new Medico();
                        txtMedico.Text = med.BuscaMedicoPorCRM(txtCrm.Text);
                        Cursor = Cursors.Default;
                    }
                    else
                        txtMedico.Focus();

                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtMedico_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbConselho.Focus();
        }

        private void cmbConselho_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtNReceita.Focus();
        }

        private void txtNReceita_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtReceita.Focus();
        }

        private void dtReceita_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbMUF.Focus();
        }

        private void cmbMUF_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbUMed.Focus();
        }

        private void cmbUMed_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbTipoRec.Focus();
        }

        private void cmbTipoRec_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodBarras.Focus();
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)

        {
            if (e.KeyChar == 13)
            {
                if (txtDescr.Text.Trim() != "" && txtCodBarras.Text.Trim() == "" && txtDescr.ReadOnly == false)
                {
                    if (BuscaProdutos("", txtDescr.Text).Equals(true))
                    {
                        BuscaLotes();
                    }
                }
                txtQtde.Focus();
            }
        }

        private void txtReg_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }

        private void txtQtde_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtLote.Focus();
        }

        private void txtBDocto_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDocto.Text.Trim()))
                {
                    txtBNome.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBNome.Text.Trim()))
                {
                    txtBData.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBData_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBData.Text.Replace("/", "").Trim()))
                {
                    txtBCrm.Focus();
                }
                btnBuscar.PerformClick();
            }
        }

        private void txtBCrm_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void txtBReceita_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                SngpcVenda venda = new SngpcVenda()
                {
                    VenCompDocto = Funcoes.RemoveCaracter(txtBDocto.Text).Trim(),
                    VenCompNome = txtBNome.Text,
                    VenData = Funcoes.RemoveCaracter(txtBData.Text).Trim() != "" ? Convert.ToDateTime(txtBData.Text) : Convert.ToDateTime("01/01/0001"),
                    VenMS = txtBCrm.Text
                };

                dtVendas = venda.BuscaDados(venda, out strOrdem);
                if (dtVendas.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtVendas.Rows.Count;
                    dgVendas.DataSource = dtVendas;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtVendas, 0));
                    dgVendas.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgVendas.DataSource = dtVendas;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBDocto.Focus();
                    Funcoes.LimpaFormularios(this);
                    emGrade = false;
                    tslRegistros.Text = "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgVendas_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtVendas, contRegistros));
            emGrade = true;
        }

        private void dgVendas_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcVendas.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgVendas_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtVendas.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtVendas.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtVendas.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgVendas_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgVendas.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtVendas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgVendas.Columns[e.ColumnIndex].Name + " DESC");
                        dgVendas.DataSource = dtVendas;
                        decrescente = true;
                    }
                    else
                    {
                        dtVendas = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgVendas.Columns[e.ColumnIndex].Name + " ASC");
                        dgVendas.DataSource = dtVendas;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtVendas, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgVendas.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgVendas);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgVendas.RowCount;
                    for (i = 0; i <= dgVendas.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgVendas[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgVendas.ColumnCount; k++)
                    {
                        if (dgVendas.Columns[k].Visible == true)
                        {
                            switch (dgVendas.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgVendas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgVendas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgVendas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgVendas.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Cod. Barras":
                                    numCel = Principal.GetColunaExcel(dgVendas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgVendas.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                                case "Reg. Min. da Saúde":
                                    numCel = Principal.GetColunaExcel(dgVendas.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgVendas.RowCount + 1));
                                    celulas.NumberFormat = "0000000000000";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgVendas.ColumnCount : indice) + Convert.ToString(dgVendas.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }


        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void pictureBox11_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox11, "Campo Obrigatório");
        }

        private void pictureBox12_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox12, "Campo Obrigatório");
        }

        private void pictureBox13_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox13, "Campo Obrigatório");
        }

        private void pictureBox14_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox14, "Campo Obrigatório");
        }

        private void pictureBox15_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox15, "Campo Obrigatório");
        }

        private void pictureBox16_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox16, "Campo Obrigatório");
        }

        private void pictureBox17_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox17, "Campo Obrigatório");
        }

        private void pictureBox18_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox18, "Campo Obrigatório");
        }

        private void pictureBox19_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox7_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox7, "Campo Obrigatório");
        }

        private void pictureBox8_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox8, "Campo Obrigatório");
        }

        private void pictureBox9_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox9, "Campo Obrigatório");
        }

        private void pictureBox10_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.imgLote, "Campo Obrigatório");
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtVenID") && (control.Name != "txtReg") && (control.Name != "txtQtdeEstoque"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgVendas.ColumnCount; i++)
                {
                    if (dgVendas.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgVendas.ColumnCount];
                string[] coluna = new string[dgVendas.ColumnCount];

                for (int i = 0; i < dgVendas.ColumnCount; i++)
                {
                    grid[i] = dgVendas.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgVendas.ColumnCount; i++)
                {
                    if (dgVendas.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgVendas.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgVendas.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgVendas.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgVendas.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgVendas.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgVendas.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgVendas.ColumnCount; i++)
                        {
                            dgVendas.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Venda SNGPC", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcVendas.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcVendas.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void cmbLote_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbLote.Text == "Informar Lote")
            {
                txtLote.Visible = true;
                lblLote.Visible = true;
                imgLote.Visible = true;
                txtQtdeEstoque.Text = "0";
                txtLote.Focus();
            }
            else
            {
                txtLote.Visible = false;
                lblLote.Visible = false;
                imgLote.Visible = false;

                txtQtdeEstoque.Text = Convert.ToString(cmbLote.SelectedValue);
            }
        }

        private void cmbLote_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                if(cmbLote.Text.Equals("Informar Lote"))
                {
                    txtLote.Focus();
                }
                else
                {
                    txtQtde.Focus();
                }
            }
        }

        private void txtLote_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtQtde.Focus();
        }
    }
}
