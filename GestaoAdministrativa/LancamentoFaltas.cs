﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GestaoAdministrativa
{
    public class LancamentoFaltas
    {
        public int EmpCodigo { get; set; }
        public int EstCodigo { get; set; }
        public int ID { get; set; }
        public string ProdCodigo { get; set; }
        public int Qtde { get; set; }
        public string Cliente { get; set; }
        public string Observacao { get; set; }
        public string Estacao { get; set; }
        public string Status { get; set; }
        public int ColCodigo { get; set; }
        public DateTime DtCadastro { get; set; }
        public string OpCadastro { get; set; }
        public DateTime DtAlteracao { get; set; }
        public string OpAlteracao { get; set; }
        public int IdTransferencia { get; set; }

        public LancamentoFaltas() { }

        public LancamentoFaltas(int empCodigo, int estCodigo, int id, string prodCodigo, int qtde, string cliente, string observacao,
            string estacao, string status, int colCodigo, DateTime dtCadastro, string opCadastro, DateTime dtAlteracao, string opAlteracao, int idTransferencia)
        {
            this.EstCodigo = estCodigo;
            this.EmpCodigo = empCodigo;
            this.ID = id;
            this.ProdCodigo = prodCodigo;
            this.Qtde = qtde;
            this.Cliente = cliente;
            this.Observacao = observacao;
            this.Status = status;
            this.Estacao = estacao;
            this.ColCodigo = colCodigo;
            this.DtCadastro = dtCadastro;
            this.OpCadastro = opCadastro;
            this.DtAlteracao = dtAlteracao;
            this.OpCadastro = opAlteracao;
            this.IdTransferencia = idTransferencia;
        }

        public bool InsereRegistros(LancamentoFaltas dados, bool transAberta = false)
        {
            string strCmd = "INSERT INTO LANCAMENTO_FALTAS(EMP_CODIGO, EST_CODIGO, ID, PROD_CODIGO, QTDE,OBSERVACAO," +
                " CLIENTE, STATUS, ESTACAO, DTCADASTRO, OPCADASTRO, ID_TRANSFERENCIA) VALUES (" +
                dados.EmpCodigo + "," +
                dados.EstCodigo + "," +
                dados.ID + ",'" +
                dados.ProdCodigo + "'," +
                dados.Qtde + ",'" +
                dados.Observacao + "','" +
                dados.Cliente + "','" +
                dados.Status + "','" +
                dados.Estacao + "'," +
                Funcoes.BDataHora(dados.DtCadastro) + ",'" +
                dados.OpCadastro + "'," + dados.IdTransferencia + ")";
            if (transAberta)
            {
                if (!BancoDados.ExecuteNoQueryTrans(strCmd, null).Equals(-1))
                {
                    return true;
                }
                else
                    return false;
            }
            else
            {
                if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
                {
                    return true;
                }
                else
                    return false;
            }

        }

        public DataTable BuscaProdutosParaReposicaoFalta(string dtInicial, string dtFinal, string horaInicial, string horaFinal, int empCodigo, int estCodigo, char status )
        {
            string sql = " SELECT A.PROD_CODIGO, B.PROD_DESCR, B.PROD_UNIDADE, A.PROD_ESTATUAL, "
                    + "    A.PROD_QTDE_UN_COMP, COALESCE(A.PROD_ESTMIN,1) AS PROD_ESTMIN, 0 AS QTDE_VENDIDA, C.QTDE, "
                    + "    F.FAB_DESCRICAO, COALESCE(A.PROD_ULTCUSME, A.PROD_PRECOMPRA) AS PROD_CUSTO,"
                    + "    E.PRE_VALOR, C.ID"
                    + "    FROM PRODUTOS_DETALHE A"
                    + "    INNER JOIN PRODUTOS B ON A.PROD_CODIGO = B.PROD_CODIGO"
                    + "    INNER JOIN LANCAMENTO_FALTAS C ON A.PROD_CODIGO = C.PROD_CODIGO"
                    + "    INNER JOIN PRECOS E ON A.PROD_CODIGO = E.PROD_CODIGO"
                    + "    LEFT JOIN FABRICANTES F ON (A.FAB_CODIGO = F.FAB_CODIGO AND A.EMP_CODIGO = F.EMP_CODIGO)"
                    + "    WHERE A.EMP_CODIGO = " + empCodigo
                    + "    AND A.EST_CODIGO = " + estCodigo
                    + "    AND A.EMP_CODIGO = E.EMP_CODIGO"
                    + "    AND A.EST_CODIGO = E.EST_CODIGO"
                    + "    AND A.EST_CODIGO = C.EST_CODIGO"
                    + "    AND A.EMP_CODIGO = C.EMP_CODIGO"
                    + "    AND A.PROD_SITUACAO = 'A'"
                    + "    AND C.STATUS = '"+status+ "' AND (A.PROD_BLOQ_COMPRA = 'N' OR A.PROD_BLOQ_COMPRA IS NULL) "
                    + "    AND C.DTCADASTRO BETWEEN TO_DATE('" + dtInicial + " " + horaInicial + "', 'DD/MM/YYYY HH24:MI:SS') AND"
                    + "    TO_DATE('" + dtFinal + " " + horaFinal + "', 'DD/MM/YYYY HH24:MI:SS')"
                    + "    GROUP BY A.PROD_CODIGO, B.PROD_DESCR, B.PROD_UNIDADE, A.PROD_ESTATUAL, F.FAB_DESCRICAO, A.PROD_ULTCUSME, A.PROD_PRECOMPRA,E.PRE_VALOR, A.PROD_QTDE_UN_COMP, A.PROD_ESTMIN, C.QTDE, C.ID"
                    + "    ORDER BY 2";
            return BancoDados.GetDataTable(sql, null);
        }
        
        public bool AtualizaStatusDaFaltaPorID(string prodCodigo,int id, int estCodigo, int empCodigo, string status)
        {
            string strCmd = "UPDATE LANCAMENTO_FALTAS SET STATUS = '" + status + "', DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) + 
                ", OPALTERACAO = '" + Principal.usuario + "' WHERE PROD_CODIGO = '" + prodCodigo + "' AND ID = " + id
                + " AND EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo;
            if (BancoDados.ExecuteNoQuery(strCmd, null).Equals(1))
            {
                return true;
            }
            else
                return false;

        }

        public bool AtualizaStatusDaFaltaPorCodigo(int idTransferencia, int estCodigo, int empCodigo, string status)
        {
            string strCmd = "UPDATE LANCAMENTO_FALTAS SET STATUS = '" + status + "', DTALTERACAO = " + Funcoes.BDataHora(DateTime.Now) +
                ", OPALTERACAO = '" + Principal.usuario + "' WHERE ID_TRANSFERENCIA = " + idTransferencia
                + " AND EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo;
            if (!BancoDados.ExecuteNoQuery(strCmd, null).Equals(-1))
            {
                return true;
            }
            else
                return false;

        }


        public DataTable BuscaFaltasPorIdTransferencia(int empCodigo, int estCodigo, int idTransferencia)
        {
            string sql = " SELECT * FROM LANCAMENTO_FALTAS WHERE EMP_CODIGO = " + empCodigo + " AND EST_CODIGO = " + estCodigo + " AND ID_TRANSFERENCIA = " + idTransferencia;
            return BancoDados.GetDataTable(sql, null);
        }
    }
}
