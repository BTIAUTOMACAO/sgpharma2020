﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa.Relatorios
{
    public partial class frmRelatorioCaixaEspecieAnalitco : Form
    {
        DateTime dtInicial, dtFinal;
        string usuario;


        public frmRelatorioCaixaEspecieAnalitco(DateTime dtIni, DateTime dtFi, string caixa)
        {
            InitializeComponent();
            this.WindowState = FormWindowState.Maximized;
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            dtInicial = dtIni;
            dtFinal = dtFi.AddHours(23).AddMinutes(59).AddSeconds(59);
            usuario = caixa;

            rpwEspecieAnalitico.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
        }

        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            CarregaSubRelatorio(e);
        }

        private void frmRelatorioCaixaEspecieAnalitco_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            CarregaEspecies();


            this.rpwEspecieAnalitico.RefreshReport();
        }

        public void CarregaEspecies()
        {
            Especie especie = new Especie();
            DataTable dtEspecies = especie.BuscaDados('N','N');

            var dsEspecie = new ReportDataSource("Especie", dtEspecies);
            rpwEspecieAnalitico.LocalReport.DataSources.Clear();
            rpwEspecieAnalitico.LocalReport.DataSources.Add(dsEspecie);
        }

        public void CarregaSubRelatorio(SubreportProcessingEventArgs e)
        {
            MovimentoCaixaEspecie caixa = new MovimentoCaixaEspecie();

            DataTable dtEntradaSaida = caixa.BuscaEntradaSaidaPorOperacao(dtInicial, dtFinal, usuario, Convert.ToInt32(e.Parameters[0].Values[0]));
            var EntradaSaidaPorOperadacao = new ReportDataSource("MovimentoEspecie", dtEntradaSaida);

            e.DataSources.Add(EntradaSaidaPorOperadacao);
        }
        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[4];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("caixa", usuario.ToUpper());
            parametro[2] = new ReportParameter("dtInicial", dtInicial.ToString("dd/MM/yyyy"));
            parametro[3] = new ReportParameter("dtFinal", dtFinal.ToString("dd/MM/yyyy"));

            this.rpwEspecieAnalitico.LocalReport.SetParameters(parametro);
        }
    }
}
