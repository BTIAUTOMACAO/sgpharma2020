﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmAFCaixa : Form, Botoes
    {
        private ToolStripButton tsbAFCaixa = new ToolStripButton("Abertura do Caixa");
        private ToolStripSeparator tssAFCaixa = new ToolStripSeparator();
        /// <summary>
        /// </summary>

        public frmAFCaixa(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;

        }

        private void frmAFCaixa_Load(object sender, EventArgs e)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                lblUsuario.Text = Principal.usuario.ToUpper();
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                VerficaStatusCaixa();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Abertura e Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        public void Botao()
        {
            try
            {
                this.tsbAFCaixa.AutoSize = false;
                this.tsbAFCaixa.Image = Properties.Resources.caixa;
                this.tsbAFCaixa.Size = new System.Drawing.Size(140, 20);
                this.tsbAFCaixa.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbAFCaixa);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssAFCaixa);
                tsbAFCaixa.Click += delegate
                {
                    var Caixa = Application.OpenForms.OfType<frmAFCaixa>().FirstOrDefault();
                    Util.BotoesGenericos();
                    Caixa.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Abertura e Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbAFCaixa);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssAFCaixa);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Abertura e Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Abertura e Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnProcessar_Click(object sender, EventArgs e)
        {
            try
            {

                AFCaixa caixa = new AFCaixa()
                {
                    EmpCodigo = Principal.empAtual,
                    EstCodigo = Principal.estAtual,
                    Usuario = Principal.usuario,
                    CaixaAberto = "S",
                    DataAbertura = DateTime.Now
                };

                DataTable dtCaixa = new DataTable();
                dtCaixa = caixa.VerificaSeCaixaJaFoiFechado(Principal.usuario);
                if (dtCaixa.Rows.Count == 0 || dtCaixa.Rows[0]["CAIXA_ABERTO"].ToString().Equals("S"))
                {
                    Cursor = Cursors.WaitCursor;
                    dtCaixa = caixa.BuscaCaixaStatus(Principal.usuario, 'S');
                    if (dtCaixa.Rows.Count != 0)
                    {
                        //FECHA
                        if (caixa.AtualizaStatusCaixa(Principal.usuario, 'N', Convert.ToChar(dtCaixa.Rows[0]["CAIXA_ABERTO"]), Convert.ToDateTime(dtCaixa.Rows[0]["DATA_ABERTURA"].ToString())).Equals(true))
                        {
                            GeraRelatorio(Convert.ToDateTime(dtCaixa.Rows[0]["DATA_ABERTURA"].ToString()), Principal.usuario);
                            MessageBox.Show("Fechamento de Caixa realizado com sucesso", "Abertura e Fechamento de Caixa", MessageBoxButtons.OK);
                            Sair();
                            this.Close();
                        }
                    }
                    else
                    {
                        //ABRE CAIXA
                        if (caixa.AbreCaixa(caixa).Equals(true))
                        {
                            MessageBox.Show("Abertura de Caixa realizada com sucesso!", "Abertura e Fechamento de Caixa", MessageBoxButtons.OK);
                            Sair();
                            this.Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("O caixa para essa data já foi fechado!\nLogue-se como outro usuário ou aguarde o dia seguinte!", "Abertura e Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    btnProcessar.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Abertura e Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        public void VerficaStatusCaixa()
        {

            try
            {
                AFCaixa caixa = new AFCaixa();

                DataTable dtCaixa = caixa.BuscaCaixaStatus(Principal.usuario, 'S');
                if (dtCaixa.Rows.Count == 0)
                {
                    lblAFCaixa.Text = "ABERTURA DO CAIXA";
                    lblData.Text = DateTime.Now.ToString("dd/MM/yyyy");
                }
                else
                {
                    lblAFCaixa.Text = "FECHAMENTO DO CAIXA";
                    lblData.Text = Convert.ToDateTime(dtCaixa.Rows[0]["DATA_ABERTURA"]).ToString("dd/MM/yyyy");
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Abertura / Fechamento do Caixa", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        public async void GeraRelatorio(DateTime dtFechamento, string usuario)
        {
            try
            {
                RelatorioCaixa relatorio = new RelatorioCaixa();
                RelatorioFechaDiario fechaDiario = new RelatorioFechaDiario();
                DataTable dtMovimentos;

                #region Relatório Caixa Sintetico Espécie

                dtMovimentos = fechaDiario.MovimentoPorEspecie(Convert.ToDateTime(dtFechamento.ToString("dd/MM/yyyy") + " 00:00:01"),
                    Convert.ToDateTime(dtFechamento.ToString("dd/MM/yyyy") + " 23:59:59"), usuario, false);

                for (int i = 0; i < dtMovimentos.Rows.Count; i++)
                {
                    bool retorno = await relatorio.InserirCaixaEspecieSintetico(Convert.ToInt32(dtMovimentos.Rows[i]["ESP_CODIGO"]),
                        Convert.ToDouble(dtMovimentos.Rows[i]["TOTAL"]), Util.SelecionaCampoEspecificoDaTabela("USUARIOS", "ID", "LOGIN_ID", usuario, false, true),
                        dtFechamento);
                    if (retorno.Equals(false))
                    {
                        MessageBox.Show("Erro: ao importar os dados para nuvem ", "Gerar Relatório Espécie", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                }
                #endregion

                #region Relatório Recebimento de Particular
                Recebimentos recebe = new Recebimentos();
                dtMovimentos = recebe.RecebimentosPorPeriodoParticular(Convert.ToDateTime(dtFechamento.ToString("dd/MM/yyyy") + " 00:00:01"), Convert.ToDateTime(dtFechamento.ToString("dd/MM/yyyy") + " 23:59:59"),
                    usuario, Convert.ToInt32(Funcoes.LeParametro(4, "13", false)));
                for (int i = 0; i < dtMovimentos.Rows.Count; i++)
                {
                    bool retorno = await relatorio.InserirRecebimentoParticular(Convert.ToInt32(dtMovimentos.Rows[i]["ESP_CODIGO"]),
                        Convert.ToDouble(dtMovimentos.Rows[i]["TOTAL"]), Util.SelecionaCampoEspecificoDaTabela("USUARIOS", "ID", "LOGIN_ID", usuario, false, true),
                        dtFechamento);
                    if (retorno.Equals(false))
                    {
                        MessageBox.Show("Erro: ao importar os dados para nuvem ", "Gerar Relatório Recebimento Particular", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    }
                }

                #endregion

                #region Relatório Movimento Caixa
                MovimentoCaixa mov = new MovimentoCaixa();
                List<Object> excecoes = new List<object>();
                excecoes.Add(Funcoes.LeParametro(6, "75", false));
                excecoes.Add(Funcoes.LeParametro(4, "13", false));
                dtMovimentos = mov.SomaMovimentoPorOperacaoDetalhado(excecoes, Convert.ToDateTime(dtFechamento.ToString("dd/MM/yyyy") + " 00:00:01"),
                    Convert.ToDateTime(dtFechamento.ToString("dd/MM/yyyy") + " 23:59:59"), usuario);

                for (int i = 0; i < dtMovimentos.Rows.Count; i++)
                {
                    bool retorno = await relatorio.InserirMovimento(Convert.ToDouble(dtMovimentos.Rows[i]["VALOR"]), dtMovimentos.Rows[i]["ODC_DESCRICAO"].ToString(),
                        Util.SelecionaCampoEspecificoDaTabela("USUARIOS", "ID", "LOGIN_ID", usuario, false, true), dtFechamento, Convert.ToInt32(dtMovimentos.Rows[i]["ESP_CODIGO"]));
                    if (retorno.Equals(false))
                    {
                        MessageBox.Show("Erro: ao importar os dados para nuvem ", "Gerar Relatório MOvimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Abertura / Fechamento do Caixa", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        #region BOTOES DE NAVEGAÇAO
        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
        #endregion

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }
    }
}
