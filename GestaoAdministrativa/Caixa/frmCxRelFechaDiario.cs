﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.SAT;
using GestaoAdministrativa.Impressao;
using System.IO;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmCxRelFechaDiario : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Fechamento");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmCxRelFechaDiario(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(125, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmCxRelFechaDiario>().FirstOrDefault();
                    Util.BotoesGenericos();
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmCxRelFechaDiario_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                dtInicial.Value = DateTime.Now;
                rdbSintetico.Checked = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmCxRelFechaDiario_Shown(object sender, EventArgs e)
        {
            try
            {
                Usuario usuario = new Usuario();
                cmbUsuario.DataSource = usuario.BuscaTodosUsuario("S");
                cmbUsuario.DisplayMember = "LOGIN_ID";
                cmbUsuario.ValueMember = "LOGIN_ID";
                cmbUsuario.SelectedValue = Principal.usuario;
                dtInicial.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool VerificaStatusCaixa()
        {
            try
            {
                AFCaixa caixa = new AFCaixa();
                string retorno = caixa.VerifcaStatusCaixa(Convert.ToDateTime(dtInicial.Text), Convert.ToDateTime(dtInicial.Text + " 23:59:59"), cmbUsuario.Text);

                if (String.IsNullOrEmpty(retorno) || retorno.Equals("S"))
                {
                    if (retorno.Equals("S"))
                    {
                        MessageBox.Show("Relatorio não pode ser gerado!\nCaixa do usuario " + cmbUsuario.Text + " está aberto", "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                    else
                    {
                        MessageBox.Show("Relatorio não pode ser gerado!\nNão ha Abertura de caixa para o usuario " + cmbUsuario.Text + " no dia " + dtInicial.Text, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return false;
                    }
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificaStatusCaixa().Equals(true))
                {
                    Cursor = Cursors.WaitCursor;

                    if (rdbAnalitico.Checked.Equals(true))
                    {
                        frmRelatorioAnalitico analitico = new frmRelatorioAnalitico(Convert.ToDateTime(dtInicial.Text), cmbUsuario.Text);
                        analitico.ShowDialog();
                    }
                    else
                    {
                        frmRelatorioSintetico sintetico = new frmRelatorioSintetico(Convert.ToDateTime(dtInicial.Text), cmbUsuario.Text);
                        sintetico.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (VerificaStatusCaixa().Equals(true))
                {
                    Cursor = Cursors.WaitCursor;

                    Estabelecimento dadosEstabelecimento = new Estabelecimento();

                    List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                    if (rdbAnalitico.Checked.Equals(true))
                    {
                        MontaRelatorioImpressaoAnalitico(retornoEstab);
                    }
                    else
                    {
                        MontaRelatorioImpressaoSintetico(retornoEstab);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        public void MontaRelatorioImpressaoAnalitico(List<Estabelecimento> retornoEstab)
        {
            try
            {
                DateTime dtFinal = Convert.ToDateTime(dtInicial.Text + " 23:59:59");
                RelatorioFechaDiario fechaDiario = new RelatorioFechaDiario();
                Recebimentos recebe = new Recebimentos();
                MovimentoCaixa caixa = new MovimentoCaixa();

                DataTable dtVendas = fechaDiario.VendasPorPeriodo(Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text);
                DataTable dtMovimentos = fechaDiario.MovimentoPorEspecie(Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text);
                DataTable dtParticulares = fechaDiario.RecebimentosPorPeriodo(Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text);
                DataTable dtRecebimentoParticular = recebe.SomaRecebimentosPorEspeciePeriodo(Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text, Convert.ToInt32(Funcoes.LeParametro(4, "13", false)));
               
                List<Object> excecoes = new List<object>();
                excecoes.Add(Funcoes.LeParametro(6, "75", false));
                excecoes.Add(Funcoes.LeParametro(4, "13", false));

                DataTable dtEntradas = caixa.SomaMovimentoPorOperacaoDetalhadoEntrada(Funcoes.LeParametro(4, "79", false), Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text);
                DataTable dtCaixa = caixa.SomaMovimentoPorOperacaoDetalhado(excecoes, Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text);

                if (retornoEstab.Count == 1)
                {
                    decimal total = 0, totalParticular = 0, totalRetiradas = 0, totalEntradas = 0;

                    string relatorio = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n"
                                     + Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n"
                                     + "CNPJ: " + retornoEstab[0].EstCNPJ + "\n"
                                     + "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n"
                                     + "_________________________________________" + "\n"
                                     + "DATA: " + String.Format("{0:dd/MM/yyyy}", DateTime.Now).PadRight(22, ' ') + "HORA: " + String.Format("{0:HH:mm:ss}", DateTime.Now) + "\n"
                                     + "CAIXA: " + cmbUsuario.Text.ToUpper() + "\n"
                                     + "DATA FECHAMENTO:" + dtInicial.Text + "\n"
                                     + "_________________________________________ \n"
                                     + Funcoes.CentralizaTexto("MOVIMENTACAO DAS VENDAS: ", 43) + "\n"
                                     + "_________________________________________" + "\n"
                                     + "HISTORICO:".PadRight(32, ' ') + " VALORES:" + "\n";
                    for (int i = 0; i < dtVendas.Rows.Count; ++i)
                    {
                        relatorio += dtVendas.Rows[i]["VENDA_ID"].ToString().PadRight(8 - (dtVendas.Rows[i]["VENDA_ID"].ToString().Length), ' ') + dtVendas.Rows[i]["CLIENTE"].ToString().PadRight(22, ' ') + string.Format("{0:0.00}", dtVendas.Rows[i]["VENDA_ESPECIE_VALOR"].ToString()).PadRight(12 - (string.Format("{0:000.00}", dtVendas.Rows[i]["VENDA_ESPECIE_VALOR"])).ToString().Length, ' ') + dtVendas.Rows[i]["ESP_DESCRICAO"] + "\n";
                    }
                    relatorio += "\n \n";
                    for (int i = 0; i < dtMovimentos.Rows.Count; i++)
                    {
                        relatorio += dtMovimentos.Rows[i]["ESP_DESCRICAO"].ToString().PadRight(41 - (string.Format("{0:0.00}", dtMovimentos.Rows[i]["TOTAL"]).ToString().Length), ' ') + string.Format("{0:0.00}", dtMovimentos.Rows[i]["TOTAL"]) + "\n";
                        total = total + Convert.ToDecimal(dtMovimentos.Rows[i]["TOTAL"]);
                    }

                    relatorio += " ".PadLeft(35, ' ') + "______" + "\n"
                              + "TOTAL: ".PadRight(41 - (string.Format("{0:0.00}", total).Length), ' ') + string.Format("{0:0.00}", total) + "\n"
                              + "_________________________________________" + "\n"
                              + Funcoes.CentralizaTexto("RECEBIMENTOS PARTICULARES:", 43) + "\n"
                              + "_________________________________________" + "\n"
                              + "HISTORICO:".PadRight(32, ' ') + " VALORES:" + "\n";

                    for (int i = 0; i < dtParticulares.Rows.Count; i++)
                    {
                        relatorio += dtParticulares.Rows[i]["COBRANCA_ID"].ToString().PadRight(8 - (dtParticulares.Rows[i]["COBRANCA_ID"].ToString().Length), ' ') + dtParticulares.Rows[i]["CLIENTE"].ToString().PadRight(34 - (string.Format("{0:0.00}", dtParticulares.Rows[i]["COBRANCA_MOV_VL_PAGO"].ToString()).Length), ' ') + string.Format("{0:0.00}", dtParticulares.Rows[i]["COBRANCA_MOV_VL_PAGO"].ToString()) + "\n";
                        if (!Convert.ToDecimal(dtParticulares.Rows[i]["COBRANCA_MOV_VL_ACRESCIMO"]).Equals(0))
                        {
                            relatorio += "JUROS:".PadLeft(13, ' ').PadRight(40 - (string.Format("{0:0.00}", dtParticulares.Rows[i]["COBRANCA_MOV_VL_ACRESCIMO"]).Length), ' ') + string.Format("{0:0.00}", dtParticulares.Rows[i]["COBRANCA_MOV_VL_ACRESCIMO"]);
                        }
                    }
                    relatorio += "\n \n";

                    for (int i = 0; i < dtRecebimentoParticular.Rows.Count; ++i)
                    {
                        relatorio += dtRecebimentoParticular.Rows[i]["ESP_DESCRICAO"].ToString().PadRight(41 - (string.Format("{0:0.00}", dtRecebimentoParticular.Rows[i]["TOTAL"]).ToString().Length), ' ') + string.Format("{0:0.00}", dtRecebimentoParticular.Rows[i]["TOTAL"]) + "\n";
                        totalParticular = totalParticular + Convert.ToDecimal(dtRecebimentoParticular.Rows[i]["TOTAL"]);
                    }
                    relatorio += " ".PadLeft(35, ' ') + "______" + "\n"
                              + "TOTAL: ".PadRight(41 - (string.Format("{0:0.00}", totalParticular).Length), ' ') + string.Format("{0:0.00}", totalParticular) + "\n"
                              + "________________________________________" + "\n"
                              + Funcoes.CentralizaTexto("ENTRADAS: ", 43) + "\n"
                              + "________________________________________" + "\n"
                              + "HISTORICO:".PadRight(32, ' ') + " VALORES:" + "\n";
                    for (int i = 0; i < dtEntradas.Rows.Count; ++i)
                    {
                        relatorio += dtEntradas.Rows[i]["MOVIMENTO_CAIXA_OBS"].ToString().ToUpper().PadRight(30, ' ') + string.Format("{0:0.00}", dtEntradas.Rows[i]["VALOR"]).PadRight(5, ' ') + " " + dtEntradas.Rows[i]["ESP_DESCRICAO"].ToString().Substring(0,3) + "\n";
                        totalEntradas = totalEntradas + Convert.ToDecimal(dtEntradas.Rows[i]["VALOR"]);
                    }
                    relatorio += " ".PadLeft(35, ' ') + "______" + "\n"
                              + "TOTAL: ".PadRight(41 - (string.Format("{0:0.00}", totalEntradas).Length), ' ') + string.Format("{0:0.00}", totalEntradas) + "\n"
                               + "________________________________________" + "\n"
                              + Funcoes.CentralizaTexto("SAIDAS: ", 43) + "\n"
                              + "________________________________________" + "\n"
                              + "HISTORICO:".PadRight(32, ' ') + " VALORES:" + "\n";
                    for (int i = 0; i < dtCaixa.Rows.Count; ++i)
                    {
                        relatorio += dtCaixa.Rows[i]["MOVIMENTO_CAIXA_OBS"].ToString().ToUpper().PadRight(30, ' ') + string.Format("{0:0.00}", dtCaixa.Rows[i]["VALOR"]).PadRight(5, ' ') + " " + dtCaixa.Rows[i]["ESP_DESCRICAO"].ToString().Substring(0, 3) + "\n";
                        totalRetiradas = totalRetiradas + Convert.ToDecimal(dtCaixa.Rows[i]["VALOR"]);
                    }
                    relatorio += " ".PadLeft(35, ' ') + "______" + "\n"
                              + "TOTAL: ".PadRight(41 - (string.Format("{0:0.00}", totalRetiradas).Length), ' ') + string.Format("{0:0.00}", totalRetiradas) + "\n\n\n\n";

                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(relatorio, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n\r\n\r\n\r\n\r\n\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                            iRetorno = BematechImpressora.AcionaGuilhotina(0);

                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(relatorio);

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                        {
                            using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\RelatorioCaixa.txt", true))
                            {
                                writer.WriteLine(relatorio);
                            }
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            relatorio += "\n\n\n\n\n";
                            Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), relatorio);
                            if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                            {
                                Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                            }
                        }
                    }
                    else
                    {
                        relatorio += "\n\n\n\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(relatorio, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Verifique o cadastro de Estabelecimento", "Impressão Comprovante", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void MontaRelatorioImpressaoSintetico(List<Estabelecimento> retornoEstab)
        {

            try
            {
                RelatorioFechaDiario fechaDiario = new RelatorioFechaDiario();
                Recebimentos recebe = new Recebimentos();
                MovimentoCaixa mov = new MovimentoCaixa();


                DateTime dtFinal = Convert.ToDateTime(dtInicial.Text + " 23:59:59");
                DataTable dtMovimentos = fechaDiario.MovimentoPorEspecie(Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text, true, true);
                DataTable dtMovimentoFormaPagamento = fechaDiario.MovimentoPorEspecieBeneficio(Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text);
                DataTable dtRecebimentoParticular = recebe.SomaRecebimentosPorEspeciePeriodo(Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text, Convert.ToInt32(Funcoes.LeParametro(4, "13", false)));
                List<Object> excecoes = new List<object>();
                excecoes.Add(Funcoes.LeParametro(6, "75", false));
                excecoes.Add(Funcoes.LeParametro(4, "13", false));

                DataTable dtEntradas = mov.SomaMovimentoPorOperacaoDetalhadoEntrada(Funcoes.LeParametro(4, "79", false), Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text);
                DataTable dtMovimentosRetiradas = mov.SomaMovimentoPorOperacaoDetalhado(excecoes, Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text);
                if (retornoEstab.Count == 1)
                {
                    string comprovante;
                    decimal total = 0, totalFormaPagamento = 0, totalParticular = 0, totalRetiradas = 0, totalEntradas = 0;

                    comprovante = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n"
                                     + Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n"
                                     + "CNPJ: " + retornoEstab[0].EstCNPJ + "\n"
                                     + "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n"
                                     + "_________________________________________" + "\n"
                                     + "DATA: " + String.Format("{0:dd/MM/yyyy}", DateTime.Now).PadRight(20, ' ') + "HORA: " + String.Format("{0:HH:mm:ss}", DateTime.Now) + "\n"
                                     + "CAIXA: " + cmbUsuario.Text.ToUpper() + "\n"
                                     + "DATA FECHAMENTO:" + dtInicial.Text + "\n"
                                     + "_________________________________________\n"
                                     + Funcoes.CentralizaTexto("MOVIMENTO POR ESPECIE EM CAIXA", 43) + "\n"
                                     + "_________________________________________" + "\n"
                                     + "VALOR".PadLeft(41, ' ') + "\n";
                    for (int i = 0; i < dtMovimentos.Rows.Count; i++)
                    {
                        comprovante += dtMovimentos.Rows[i]["ESP_DESCRICAO"].ToString().PadRight(41 - (string.Format("{0:0.00}", dtMovimentos.Rows[i]["TOTAL"]).ToString().Length), ' ') + string.Format("{0:0.00}", dtMovimentos.Rows[i]["TOTAL"]) + "\n";
                        total = total + Convert.ToDecimal(dtMovimentos.Rows[i]["TOTAL"]);

                    }

                    comprovante += " ".PadLeft(34, ' ') + "______" + "\n"
                                      + " ".PadLeft(41 - (string.Format("{0:0.00}", total).Length), ' ') + string.Format("{0:0.00}", total) + "\n"
                                      + "_________________________________________" + "\n"
                                      + Funcoes.CentralizaTexto("MOVIMENTO POR ESPECIE BENEFICIO", 43) + "\n"
                                      + "_________________________________________" + "\n"
                                      + "VALOR".PadLeft(41, ' ') + "\n";

                    for (int y = 0; y < dtMovimentoFormaPagamento.Rows.Count; ++y)
                    {
                        comprovante += dtMovimentoFormaPagamento.Rows[y]["ESP_DESCRICAO"].ToString().PadRight(41 - (string.Format("{0:0.00}", dtMovimentoFormaPagamento.Rows[y]["TOTAL"]).ToString().Length), ' ') + string.Format("{0:0.00}", dtMovimentoFormaPagamento.Rows[y]["TOTAL"]) + "\n";
                        totalFormaPagamento = totalFormaPagamento + Convert.ToDecimal(dtMovimentoFormaPagamento.Rows[y]["TOTAL"]);
                    }
                    comprovante += " ".PadLeft(34, ' ') + "______" + "\n"
                                      + " ".PadLeft(41 - (string.Format("{0:0.00}", totalFormaPagamento).Length), ' ') + string.Format("{0:0.00}", totalFormaPagamento) + "\n\n"
                                      + " ".PadLeft(41 - (("TOTAL GERAL: " + string.Format("{0:0.00}", totalFormaPagamento + total)).Length), ' ') + "TOTAL GERAL: " + string.Format("{0:0.00}", totalFormaPagamento + total) + "\n"


                                      + "_________________________________________" + "\n"
                                      + Funcoes.CentralizaTexto("RECEBIMENTO PARTICULAR", 43) + "\n"
                                      + "_________________________________________" + "\n";

                    for (int y = 0; y < dtRecebimentoParticular.Rows.Count; ++y)
                    {
                        comprovante += dtRecebimentoParticular.Rows[y]["ESP_DESCRICAO"].ToString().PadRight(41 - (string.Format("{0:0.00}", dtRecebimentoParticular.Rows[y]["TOTAL"]).ToString().Length), ' ') + string.Format("{0:0.00}", dtRecebimentoParticular.Rows[y]["TOTAL"]) + "\n";
                        totalParticular = totalParticular + Convert.ToDecimal(dtRecebimentoParticular.Rows[y]["TOTAL"]);
                    }
                    comprovante += " ".PadLeft(34, ' ') + "______" + "\n"
                                      + " ".PadLeft(41 - (string.Format("{0:0.00}", totalParticular).Length), ' ') + string.Format("{0:0.00}", totalParticular) + "\n"
                                      + "_________________________________________" + "\n"
                                      + Funcoes.CentralizaTexto("MOVIMENTOS DE CAIXA (+)", 43) + "\n"
                                      + "_________________________________________" + "\n";
                    for (int y = 0; y < dtEntradas.Rows.Count; ++y)
                    {
                        comprovante += (dtEntradas.Rows[y]["ODC_DESCRICAO"].ToString().Substring(0,3) + "  " + dtEntradas.Rows[y]["ESP_DESCRICAO"].ToString() + "  " 
                            + (dtEntradas.Rows[y]["MOVIMENTO_CAIXA_OBS"].ToString().Length > 13 ? dtEntradas.Rows[y]["MOVIMENTO_CAIXA_OBS"].ToString().Substring(0,12) : dtEntradas.Rows[y]["MOVIMENTO_CAIXA_OBS"].ToString())
                            ).PadRight(41 - (string.Format("{0:0.00}", dtEntradas.Rows[y]["VALOR"]).ToString().Length), ' ') + string.Format("{0:0.00}", dtEntradas.Rows[y]["VALOR"]) + "\n";
                        totalEntradas = totalEntradas + Convert.ToDecimal(dtEntradas.Rows[y]["VALOR"]);
                    }
                    comprovante += " ".PadLeft(34, ' ') + "______" + "\n"
                                      + " ".PadLeft(41 - (string.Format("{0:0.00}", totalEntradas).Length), ' ') + string.Format("{0:0.00}", totalEntradas) + "\n"
                                      + "_________________________________________" + "\n"
                                       + Funcoes.CentralizaTexto("MOVIMENTOS DE CAIXA (-)", 43) + "\n"
                                      + "_________________________________________" + "\n";
                    for (int y = 0; y < dtMovimentosRetiradas.Rows.Count; ++y)
                    {
                        comprovante += (dtMovimentosRetiradas.Rows[y]["ODC_DESCRICAO"].ToString().Substring(0, 3) + "  " + dtMovimentosRetiradas.Rows[y]["ESP_DESCRICAO"].ToString() + "  "
                            + (dtMovimentosRetiradas.Rows[y]["MOVIMENTO_CAIXA_OBS"].ToString().Length > 13 ? dtMovimentosRetiradas.Rows[y]["MOVIMENTO_CAIXA_OBS"].ToString().Substring(0,12) : dtMovimentosRetiradas.Rows[y]["MOVIMENTO_CAIXA_OBS"].ToString())
                            ).PadRight(41 - (string.Format("{0:0.00}", dtMovimentosRetiradas.Rows[y]["VALOR"]).ToString().Length), ' ') + string.Format("{0:0.00}", dtMovimentosRetiradas.Rows[y]["VALOR"]) + "\n";
                        totalRetiradas = totalRetiradas + Convert.ToDecimal(dtMovimentosRetiradas.Rows[y]["VALOR"]);
                    }
                    comprovante += " ".PadLeft(34, ' ') + "______" + "\n"
                                    + " ".PadLeft(41 - (string.Format("{0:0.00}", totalRetiradas).Length), ' ') + string.Format("{0:0.00}", totalRetiradas) + "\n"
                                    + "_________________________________________" + "\n"
                                    + Funcoes.CentralizaTexto("CAIXA :", 43) + "\n"
                                    + "_________________________________________" + "\n"
                                    + "TOTAL EM CAIXA: ".PadRight(41 - (string.Format("{0:0.00}", total + totalParticular + totalEntradas).Length), ' ') + string.Format("{0:0.00}", total + totalParticular + totalEntradas) + "\n"
                                    + "TOTAL SAIDAS: ".PadRight(41 - (string.Format("{0:0.00}", totalRetiradas).Length), ' ') + string.Format("{0:0.00}", totalRetiradas) + "\n"
                                    + " ".PadLeft(34, ' ') + "______" + "\n"
                                    + "TOTAL EM CAIXA: ".PadRight(41 - (string.Format("{0:0.00}", totalRetiradas - (total + totalParticular + totalEntradas)).Length), ' ') + string.Format("{0:0.00}", totalRetiradas - (total + totalParticular + totalEntradas)) + "\n";

                    if(Funcoes.LeParametro(4,"75", false).Equals("S"))
                    {
                        DataTable dtRecarga = fechaDiario.RecargaDeCelular(Convert.ToDateTime(dtInicial.Text), dtFinal, cmbUsuario.Text, Funcoes.LeParametro(4, "76", true));
                            comprovante += "_________________________________________" + "\n"
                                           + Funcoes.CentralizaTexto("RECARGA DE CELULAR", 43) + "\n"
                                           + "_________________________________________" + "\n";

                        for (int z=0; z < dtRecarga.Rows.Count; z++)
                        {
                            comprovante += "TOTAL RECARGA: ".PadRight(41 - (string.Format("{0:0.00}", dtRecarga.Rows[z]["TOTAL"]).ToString().Length), ' ') + string.Format("{0:0.00}", dtRecarga.Rows[z]["TOTAL"]) + "\n";
                        }

                    }

                    comprovante += "\n\n\n\n";

                    if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                    {
                        if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                             && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            string s_cmdTX = "\n";
                            if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                            }
                            else
                            {
                                iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                            }

                            iRetorno = BematechImpressora.IniciaPorta("USB");

                            iRetorno = BematechImpressora.FormataTX(comprovante, 3, 0, 0, 0, 0);

                            s_cmdTX = "\r\n\r\n\r\n\r\n\r\n\r\n";
                            iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                            iRetorno = BematechImpressora.AcionaGuilhotina(0);

                            iRetorno = BematechImpressora.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                        {
                            int iRetorno;
                            iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                            iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante);

                            iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                            iRetorno = InterfaceEpsonNF.FechaPorta();
                        }
                        else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                        {
                            using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\RelatorioCaixa2.txt", true))
                            {
                                writer.WriteLine(comprovante);
                            }
                        }
                        else
                        {
                            //IMPRIME COMPROVANTE
                            comprovante += "\n\n\n\n\n";
                            Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);
                            if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                            {
                                Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                            }
                        }
                    }
                    else
                    {
                        comprovante += "\n\n\n\n";
                        //IMPRIME COMPROVANTE
                        int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovante, 0);
                        if (iRetorno != 1)
                        {
                            MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Verifique o cadastro de Estabelecimento", "Impressão Comprovante", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
