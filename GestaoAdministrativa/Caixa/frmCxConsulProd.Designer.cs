﻿namespace GestaoAdministrativa.Caixa
{
    partial class frmCxConsulProd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCxConsulProd));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.txtAjuste = new System.Windows.Forms.TextBox();
            this.txtSTotal = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dgProdutos = new System.Windows.Forms.DataGridView();
            this.EST_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PED_NUMERO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PED_EMISSAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PED_TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PED_SUBTOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PED_DIFER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLI_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLI_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COD_BARRA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEM_QTDE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEM_VLUNIT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEM_DIFER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ITEM_TOTAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COL_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtCliente = new System.Windows.Forms.TextBox();
            this.txtPedido = new System.Windows.Forms.TextBox();
            this.txtEmissao = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(6, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(819, 330);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.txtTotal);
            this.panel1.Controls.Add(this.txtAjuste);
            this.panel1.Controls.Add(this.txtSTotal);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.dgProdutos);
            this.panel1.Controls.Add(this.txtCliente);
            this.panel1.Controls.Add(this.txtPedido);
            this.panel1.Controls.Add(this.txtEmissao);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(6, 15);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(807, 309);
            this.panel1.TabIndex = 1;
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.Color.White;
            this.txtTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTotal.Enabled = false;
            this.txtTotal.Location = new System.Drawing.Point(433, 278);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.Size = new System.Drawing.Size(101, 22);
            this.txtTotal.TabIndex = 12;
            this.txtTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtAjuste
            // 
            this.txtAjuste.BackColor = System.Drawing.Color.White;
            this.txtAjuste.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAjuste.Enabled = false;
            this.txtAjuste.Location = new System.Drawing.Point(279, 278);
            this.txtAjuste.Name = "txtAjuste";
            this.txtAjuste.ReadOnly = true;
            this.txtAjuste.Size = new System.Drawing.Size(101, 22);
            this.txtAjuste.TabIndex = 11;
            this.txtAjuste.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtSTotal
            // 
            this.txtSTotal.BackColor = System.Drawing.Color.White;
            this.txtSTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtSTotal.Enabled = false;
            this.txtSTotal.Location = new System.Drawing.Point(83, 278);
            this.txtSTotal.Name = "txtSTotal";
            this.txtSTotal.ReadOnly = true;
            this.txtSTotal.Size = new System.Drawing.Size(101, 22);
            this.txtSTotal.TabIndex = 10;
            this.txtSTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(391, 284);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 16);
            this.label4.TabIndex = 9;
            this.label4.Text = "Total";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(196, 284);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 16);
            this.label5.TabIndex = 8;
            this.label5.Text = "Total Ajuste";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(12, 284);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(65, 16);
            this.label6.TabIndex = 7;
            this.label6.Text = "SubTotal";
            // 
            // dgProdutos
            // 
            this.dgProdutos.AllowUserToAddRows = false;
            this.dgProdutos.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            this.dgProdutos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgProdutos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgProdutos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgProdutos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.EST_CODIGO,
            this.PED_NUMERO,
            this.PED_EMISSAO,
            this.PED_TOTAL,
            this.PED_SUBTOTAL,
            this.PED_DIFER,
            this.CLI_ID,
            this.CLI_NOME,
            this.PROD_CODIGO,
            this.COD_BARRA,
            this.PROD_DESCR,
            this.ITEM_QTDE,
            this.ITEM_VLUNIT,
            this.ITEM_DIFER,
            this.ITEM_TOTAL,
            this.COL_NOME});
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProdutos.DefaultCellStyle = dataGridViewCellStyle9;
            this.dgProdutos.GridColor = System.Drawing.Color.LightGray;
            this.dgProdutos.Location = new System.Drawing.Point(15, 35);
            this.dgProdutos.MultiSelect = false;
            this.dgProdutos.Name = "dgProdutos";
            this.dgProdutos.ReadOnly = true;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProdutos.RowHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgProdutos.RowHeadersVisible = false;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.LightGray;
            this.dgProdutos.RowsDefaultCellStyle = dataGridViewCellStyle11;
            this.dgProdutos.Size = new System.Drawing.Size(777, 237);
            this.dgProdutos.TabIndex = 6;
            // 
            // EST_CODIGO
            // 
            this.EST_CODIGO.DataPropertyName = "EST_CODIGO";
            this.EST_CODIGO.HeaderText = "Estabelecimento";
            this.EST_CODIGO.MaxInputLength = 18;
            this.EST_CODIGO.Name = "EST_CODIGO";
            this.EST_CODIGO.ReadOnly = true;
            this.EST_CODIGO.Visible = false;
            // 
            // PED_NUMERO
            // 
            this.PED_NUMERO.DataPropertyName = "PED_NUMERO";
            this.PED_NUMERO.HeaderText = "N° Pedido";
            this.PED_NUMERO.MaxInputLength = 18;
            this.PED_NUMERO.Name = "PED_NUMERO";
            this.PED_NUMERO.ReadOnly = true;
            this.PED_NUMERO.Visible = false;
            // 
            // PED_EMISSAO
            // 
            this.PED_EMISSAO.DataPropertyName = "PED_EMISSAO";
            this.PED_EMISSAO.HeaderText = "Emissão";
            this.PED_EMISSAO.MaxInputLength = 10;
            this.PED_EMISSAO.Name = "PED_EMISSAO";
            this.PED_EMISSAO.ReadOnly = true;
            this.PED_EMISSAO.Visible = false;
            // 
            // PED_TOTAL
            // 
            this.PED_TOTAL.DataPropertyName = "PED_TOTAL";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle3.Format = "N2";
            dataGridViewCellStyle3.NullValue = null;
            this.PED_TOTAL.DefaultCellStyle = dataGridViewCellStyle3;
            this.PED_TOTAL.HeaderText = "Ped. Total";
            this.PED_TOTAL.MaxInputLength = 18;
            this.PED_TOTAL.Name = "PED_TOTAL";
            this.PED_TOTAL.ReadOnly = true;
            this.PED_TOTAL.Visible = false;
            // 
            // PED_SUBTOTAL
            // 
            this.PED_SUBTOTAL.DataPropertyName = "PED_SUBTOTAL";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.PED_SUBTOTAL.DefaultCellStyle = dataGridViewCellStyle4;
            this.PED_SUBTOTAL.HeaderText = "Ped. SubTotal";
            this.PED_SUBTOTAL.MaxInputLength = 18;
            this.PED_SUBTOTAL.Name = "PED_SUBTOTAL";
            this.PED_SUBTOTAL.ReadOnly = true;
            this.PED_SUBTOTAL.Visible = false;
            // 
            // PED_DIFER
            // 
            this.PED_DIFER.DataPropertyName = "PED_DIFER";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.PED_DIFER.DefaultCellStyle = dataGridViewCellStyle5;
            this.PED_DIFER.HeaderText = "Ped. Ajuste";
            this.PED_DIFER.MaxInputLength = 18;
            this.PED_DIFER.Name = "PED_DIFER";
            this.PED_DIFER.ReadOnly = true;
            this.PED_DIFER.Visible = false;
            // 
            // CLI_ID
            // 
            this.CLI_ID.DataPropertyName = "CLI_ID";
            this.CLI_ID.HeaderText = "ID Cliente";
            this.CLI_ID.MaxInputLength = 18;
            this.CLI_ID.Name = "CLI_ID";
            this.CLI_ID.ReadOnly = true;
            this.CLI_ID.Visible = false;
            // 
            // CLI_NOME
            // 
            this.CLI_NOME.DataPropertyName = "CLI_NOME";
            this.CLI_NOME.HeaderText = "Cliente";
            this.CLI_NOME.MaxInputLength = 50;
            this.CLI_NOME.Name = "CLI_NOME";
            this.CLI_NOME.ReadOnly = true;
            this.CLI_NOME.Visible = false;
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.DataPropertyName = "PROD_CODIGO";
            this.PROD_CODIGO.HeaderText = "Cód. Produto";
            this.PROD_CODIGO.MaxInputLength = 18;
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.ReadOnly = true;
            this.PROD_CODIGO.Visible = false;
            // 
            // COD_BARRA
            // 
            this.COD_BARRA.DataPropertyName = "COD_BARRA";
            this.COD_BARRA.HeaderText = "Cód. de Barras";
            this.COD_BARRA.MaxInputLength = 13;
            this.COD_BARRA.Name = "COD_BARRA";
            this.COD_BARRA.ReadOnly = true;
            this.COD_BARRA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.COD_BARRA.Width = 120;
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.DataPropertyName = "PROD_DESCR";
            this.PROD_DESCR.HeaderText = "Descrição";
            this.PROD_DESCR.MaxInputLength = 50;
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.ReadOnly = true;
            this.PROD_DESCR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PROD_DESCR.Width = 200;
            // 
            // ITEM_QTDE
            // 
            this.ITEM_QTDE.DataPropertyName = "ITEM_QTDE";
            this.ITEM_QTDE.HeaderText = "Quantidade";
            this.ITEM_QTDE.MaxInputLength = 18;
            this.ITEM_QTDE.Name = "ITEM_QTDE";
            this.ITEM_QTDE.ReadOnly = true;
            this.ITEM_QTDE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ITEM_QTDE.Width = 80;
            // 
            // ITEM_VLUNIT
            // 
            this.ITEM_VLUNIT.DataPropertyName = "ITEM_VLUNIT";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.ITEM_VLUNIT.DefaultCellStyle = dataGridViewCellStyle6;
            this.ITEM_VLUNIT.HeaderText = "Vl. Unitário";
            this.ITEM_VLUNIT.MaxInputLength = 18;
            this.ITEM_VLUNIT.Name = "ITEM_VLUNIT";
            this.ITEM_VLUNIT.ReadOnly = true;
            this.ITEM_VLUNIT.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ITEM_VLUNIT.Width = 95;
            // 
            // ITEM_DIFER
            // 
            this.ITEM_DIFER.DataPropertyName = "ITEM_DIFER";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            this.ITEM_DIFER.DefaultCellStyle = dataGridViewCellStyle7;
            this.ITEM_DIFER.HeaderText = "Ajuste";
            this.ITEM_DIFER.MaxInputLength = 18;
            this.ITEM_DIFER.Name = "ITEM_DIFER";
            this.ITEM_DIFER.ReadOnly = true;
            this.ITEM_DIFER.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ITEM_DIFER.Width = 70;
            // 
            // ITEM_TOTAL
            // 
            this.ITEM_TOTAL.DataPropertyName = "ITEM_TOTAL";
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle8.Format = "N2";
            dataGridViewCellStyle8.NullValue = null;
            this.ITEM_TOTAL.DefaultCellStyle = dataGridViewCellStyle8;
            this.ITEM_TOTAL.HeaderText = "Total";
            this.ITEM_TOTAL.MaxInputLength = 18;
            this.ITEM_TOTAL.Name = "ITEM_TOTAL";
            this.ITEM_TOTAL.ReadOnly = true;
            this.ITEM_TOTAL.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ITEM_TOTAL.Width = 80;
            // 
            // COL_NOME
            // 
            this.COL_NOME.DataPropertyName = "COL_NOME";
            this.COL_NOME.HeaderText = "Vendedor";
            this.COL_NOME.MaxInputLength = 50;
            this.COL_NOME.Name = "COL_NOME";
            this.COL_NOME.ReadOnly = true;
            this.COL_NOME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.COL_NOME.Width = 128;
            // 
            // txtCliente
            // 
            this.txtCliente.BackColor = System.Drawing.Color.White;
            this.txtCliente.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCliente.Enabled = false;
            this.txtCliente.Location = new System.Drawing.Point(466, 7);
            this.txtCliente.Name = "txtCliente";
            this.txtCliente.ReadOnly = true;
            this.txtCliente.Size = new System.Drawing.Size(326, 22);
            this.txtCliente.TabIndex = 5;
            // 
            // txtPedido
            // 
            this.txtPedido.BackColor = System.Drawing.Color.White;
            this.txtPedido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPedido.Enabled = false;
            this.txtPedido.Location = new System.Drawing.Point(281, 7);
            this.txtPedido.Name = "txtPedido";
            this.txtPedido.ReadOnly = true;
            this.txtPedido.Size = new System.Drawing.Size(130, 22);
            this.txtPedido.TabIndex = 4;
            this.txtPedido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtEmissao
            // 
            this.txtEmissao.BackColor = System.Drawing.Color.White;
            this.txtEmissao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEmissao.Enabled = false;
            this.txtEmissao.Location = new System.Drawing.Point(105, 7);
            this.txtEmissao.Name = "txtEmissao";
            this.txtEmissao.ReadOnly = true;
            this.txtEmissao.Size = new System.Drawing.Size(83, 22);
            this.txtEmissao.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(413, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cliente";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(190, 13);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "N° do Pedido";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Data Emissão";
            // 
            // frmCxConsulProd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 335);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCxConsulProd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Consulta de Produtos Vendidos por Títulos";
            this.Load += new System.EventHandler(this.frmCxConsulProd_Load);
            this.Shown += new System.EventHandler(this.frmCxConsulProd_Shown);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProdutos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView dgProdutos;
        private System.Windows.Forms.TextBox txtCliente;
        private System.Windows.Forms.TextBox txtPedido;
        private System.Windows.Forms.TextBox txtEmissao;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EST_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_NUMERO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_EMISSAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_TOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_SUBTOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn PED_DIFER;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLI_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLI_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn COD_BARRA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEM_QTDE;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEM_VLUNIT;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEM_DIFER;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEM_TOTAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn COL_NOME;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.TextBox txtAjuste;
        private System.Windows.Forms.TextBox txtSTotal;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
    }
}