﻿namespace GestaoAdministrativa.Caixa
{
    partial class frmCxRecebConv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCxRecebConv));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcConveniadas = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.dtFinal = new System.Windows.Forms.DateTimePicker();
            this.dtInicial = new System.Windows.Forms.DateTimePicker();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.btnVPagar = new System.Windows.Forms.Button();
            this.txtVTotal = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.btnDesmarcar = new System.Windows.Forms.Button();
            this.btnMarcar = new System.Windows.Forms.Button();
            this.dgConveniadas = new System.Windows.Forms.DataGridView();
            this.label15 = new System.Windows.Forms.Label();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgPagamentos = new System.Windows.Forms.DataGridView();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.gbEspecie = new System.Windows.Forms.GroupBox();
            this.lblTroco = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.dgEspecie = new System.Windows.Forms.DataGridView();
            this.ESP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESP_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESP_VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEValor = new System.Windows.Forms.TextBox();
            this.cmbEspecie = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblEstab = new System.Windows.Forms.Label();
            this.ESCOLHA = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EMPRESA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLI_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_PARCELA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_PARCELA_VENCIMENTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_PARCELA_NUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_PARCELA_SALDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcConveniadas.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgConveniadas)).BeginInit();
            this.tpFicha.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPagamentos)).BeginInit();
            this.gbEspecie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEspecie)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcConveniadas);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 28;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(370, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Recebimento de Títulos de Empresas Conveniadas";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcConveniadas
            // 
            this.tcConveniadas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcConveniadas.Controls.Add(this.tpGrade);
            this.tcConveniadas.Controls.Add(this.tpFicha);
            this.tcConveniadas.Location = new System.Drawing.Point(6, 33);
            this.tcConveniadas.Name = "tcConveniadas";
            this.tcConveniadas.SelectedIndex = 0;
            this.tcConveniadas.Size = new System.Drawing.Size(965, 499);
            this.tcConveniadas.TabIndex = 40;
            this.tcConveniadas.SelectedIndexChanged += new System.EventHandler(this.tcConveniadas_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.BackColor = System.Drawing.Color.White;
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.dtFinal);
            this.tpGrade.Controls.Add(this.dtInicial);
            this.tpGrade.Controls.Add(this.txtNome);
            this.tpGrade.Controls.Add(this.txtCodigo);
            this.tpGrade.Controls.Add(this.btnVPagar);
            this.tpGrade.Controls.Add(this.txtVTotal);
            this.tpGrade.Controls.Add(this.label8);
            this.tpGrade.Controls.Add(this.label2);
            this.tpGrade.Controls.Add(this.label1);
            this.tpGrade.Controls.Add(this.label11);
            this.tpGrade.Controls.Add(this.btnDesmarcar);
            this.tpGrade.Controls.Add(this.btnMarcar);
            this.tpGrade.Controls.Add(this.dgConveniadas);
            this.tpGrade.Controls.Add(this.label15);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(957, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade";
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(662, 422);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnBuscar.TabIndex = 183;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            this.btnBuscar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnBuscar_KeyDown);
            // 
            // dtFinal
            // 
            this.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinal.Location = new System.Drawing.Point(543, 438);
            this.dtFinal.Name = "dtFinal";
            this.dtFinal.Size = new System.Drawing.Size(94, 22);
            this.dtFinal.TabIndex = 182;
            this.dtFinal.Value = new System.DateTime(2014, 2, 25, 0, 0, 0, 0);
            this.dtFinal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtFinal_KeyDown);
            this.dtFinal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtFinal_KeyPress);
            // 
            // dtInicial
            // 
            this.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicial.Location = new System.Drawing.Point(433, 438);
            this.dtInicial.Name = "dtInicial";
            this.dtInicial.Size = new System.Drawing.Size(94, 22);
            this.dtInicial.TabIndex = 181;
            this.dtInicial.Value = new System.DateTime(2014, 2, 25, 0, 0, 0, 0);
            this.dtInicial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dtInicial_KeyDown);
            this.dtInicial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dtInicial_KeyPress);
            // 
            // txtNome
            // 
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNome.ForeColor = System.Drawing.Color.Black;
            this.txtNome.Location = new System.Drawing.Point(115, 438);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(301, 22);
            this.txtNome.TabIndex = 180;
            this.txtNome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            this.txtNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            this.txtNome.Validated += new System.EventHandler(this.txtNome_Validated);
            // 
            // txtCodigo
            // 
            this.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodigo.Location = new System.Drawing.Point(6, 438);
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(95, 22);
            this.txtCodigo.TabIndex = 179;
            this.txtCodigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigo_KeyDown);
            this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigo_KeyPress);
            // 
            // btnVPagar
            // 
            this.btnVPagar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVPagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVPagar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVPagar.ForeColor = System.Drawing.Color.Black;
            this.btnVPagar.Image = ((System.Drawing.Image)(resources.GetObject("btnVPagar.Image")));
            this.btnVPagar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVPagar.Location = new System.Drawing.Point(845, 365);
            this.btnVPagar.Name = "btnVPagar";
            this.btnVPagar.Size = new System.Drawing.Size(106, 50);
            this.btnVPagar.TabIndex = 178;
            this.btnVPagar.Text = "Pagar (F1)";
            this.btnVPagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVPagar.UseVisualStyleBackColor = true;
            this.btnVPagar.Click += new System.EventHandler(this.btnVPagar_Click);
            this.btnVPagar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnVPagar_KeyDown);
            // 
            // txtVTotal
            // 
            this.txtVTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVTotal.BackColor = System.Drawing.Color.White;
            this.txtVTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVTotal.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtVTotal.Location = new System.Drawing.Point(845, 335);
            this.txtVTotal.Name = "txtVTotal";
            this.txtVTotal.ReadOnly = true;
            this.txtVTotal.Size = new System.Drawing.Size(106, 26);
            this.txtVTotal.TabIndex = 177;
            this.txtVTotal.Text = "0,00";
            this.txtVTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtVTotal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtVTotal_KeyDown);
            this.txtVTotal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVTotal_KeyPress);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(842, 316);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 16);
            this.label8.TabIndex = 176;
            this.label8.Text = "Valor Total";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(543, 419);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 16);
            this.label2.TabIndex = 171;
            this.label2.Text = "Data Final";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(433, 419);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(80, 16);
            this.label1.TabIndex = 170;
            this.label1.Text = "Data Inicial";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(112, 419);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(184, 16);
            this.label11.TabIndex = 168;
            this.label11.Text = "Nome Empresa Conveniada";
            // 
            // btnDesmarcar
            // 
            this.btnDesmarcar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDesmarcar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDesmarcar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesmarcar.ForeColor = System.Drawing.Color.Black;
            this.btnDesmarcar.Image = ((System.Drawing.Image)(resources.GetObject("btnDesmarcar.Image")));
            this.btnDesmarcar.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnDesmarcar.Location = new System.Drawing.Point(845, 84);
            this.btnDesmarcar.Name = "btnDesmarcar";
            this.btnDesmarcar.Size = new System.Drawing.Size(106, 72);
            this.btnDesmarcar.TabIndex = 160;
            this.btnDesmarcar.Text = "Desmarcar Todos (F11)";
            this.btnDesmarcar.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnDesmarcar.UseVisualStyleBackColor = true;
            this.btnDesmarcar.Click += new System.EventHandler(this.btnDesmarcar_Click);
            this.btnDesmarcar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnDesmarcar_KeyDown);
            // 
            // btnMarcar
            // 
            this.btnMarcar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMarcar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMarcar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarcar.ForeColor = System.Drawing.Color.Black;
            this.btnMarcar.Image = ((System.Drawing.Image)(resources.GetObject("btnMarcar.Image")));
            this.btnMarcar.ImageAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnMarcar.Location = new System.Drawing.Point(845, 6);
            this.btnMarcar.Name = "btnMarcar";
            this.btnMarcar.Size = new System.Drawing.Size(106, 72);
            this.btnMarcar.TabIndex = 159;
            this.btnMarcar.Text = "     Marcar       Todos (F10)";
            this.btnMarcar.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnMarcar.UseVisualStyleBackColor = true;
            this.btnMarcar.Click += new System.EventHandler(this.btnMarcar_Click);
            this.btnMarcar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnMarcar_KeyDown);
            // 
            // dgConveniadas
            // 
            this.dgConveniadas.AllowUserToAddRows = false;
            this.dgConveniadas.AllowUserToDeleteRows = false;
            this.dgConveniadas.AllowUserToResizeColumns = false;
            this.dgConveniadas.AllowUserToResizeRows = false;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.Black;
            this.dgConveniadas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle32;
            this.dgConveniadas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgConveniadas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle33.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle33.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgConveniadas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle33;
            this.dgConveniadas.ColumnHeadersHeight = 25;
            this.dgConveniadas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ESCOLHA,
            this.dataGridViewTextBoxColumn1,
            this.EMPRESA,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.CON_CODIGO,
            this.CLI_NOME,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.COBRANCA_PARCELA_ID});
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgConveniadas.DefaultCellStyle = dataGridViewCellStyle37;
            this.dgConveniadas.GridColor = System.Drawing.Color.Black;
            this.dgConveniadas.Location = new System.Drawing.Point(6, 6);
            this.dgConveniadas.MultiSelect = false;
            this.dgConveniadas.Name = "dgConveniadas";
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgConveniadas.RowHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.dgConveniadas.RowHeadersVisible = false;
            this.dgConveniadas.RowHeadersWidth = 33;
            this.dgConveniadas.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle39.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.Black;
            this.dgConveniadas.RowsDefaultCellStyle = dataGridViewCellStyle39;
            this.dgConveniadas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgConveniadas.Size = new System.Drawing.Size(833, 408);
            this.dgConveniadas.TabIndex = 135;
            this.dgConveniadas.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgConveniadas_CellEndEdit);
            this.dgConveniadas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgConveniadas_KeyDown);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(3, 419);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(53, 16);
            this.label15.TabIndex = 60;
            this.label15.Text = "Código";
            // 
            // tpFicha
            // 
            this.tpFicha.BackColor = System.Drawing.Color.White;
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(957, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.CausesValidation = false;
            this.groupBox2.Controls.Add(this.dgPagamentos);
            this.groupBox2.Controls.Add(this.btnConfirmar);
            this.groupBox2.Controls.Add(this.gbEspecie);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(945, 458);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // dgPagamentos
            // 
            this.dgPagamentos.AllowUserToAddRows = false;
            this.dgPagamentos.AllowUserToDeleteRows = false;
            this.dgPagamentos.AllowUserToResizeColumns = false;
            this.dgPagamentos.AllowUserToResizeRows = false;
            dataGridViewCellStyle24.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPagamentos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle24;
            this.dgPagamentos.BackgroundColor = System.Drawing.Color.White;
            this.dgPagamentos.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPagamentos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgPagamentos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.COBRANCA_PARCELA_VENCIMENTO,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.COBRANCA_PARCELA_NUM,
            this.COBRANCA_PARCELA_SALDO,
            this.Column1});
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPagamentos.DefaultCellStyle = dataGridViewCellStyle29;
            this.dgPagamentos.GridColor = System.Drawing.Color.Black;
            this.dgPagamentos.Location = new System.Drawing.Point(9, 41);
            this.dgPagamentos.MultiSelect = false;
            this.dgPagamentos.Name = "dgPagamentos";
            this.dgPagamentos.ReadOnly = true;
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPagamentos.RowHeadersDefaultCellStyle = dataGridViewCellStyle30;
            this.dgPagamentos.RowHeadersVisible = false;
            this.dgPagamentos.RowHeadersWidth = 30;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.White;
            this.dgPagamentos.RowsDefaultCellStyle = dataGridViewCellStyle31;
            this.dgPagamentos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPagamentos.Size = new System.Drawing.Size(513, 116);
            this.dgPagamentos.TabIndex = 125;
            this.dgPagamentos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgPagamentos_KeyDown);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar.Image")));
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(358, 336);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(164, 66);
            this.btnConfirmar.TabIndex = 122;
            this.btnConfirmar.Text = "Confirmar\r\nPagamento (F2)";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Visible = false;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            this.btnConfirmar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnConfirmar_KeyDown);
            // 
            // gbEspecie
            // 
            this.gbEspecie.Controls.Add(this.lblTroco);
            this.gbEspecie.Controls.Add(this.lblTotal);
            this.gbEspecie.Controls.Add(this.dgEspecie);
            this.gbEspecie.Controls.Add(this.txtEValor);
            this.gbEspecie.Controls.Add(this.cmbEspecie);
            this.gbEspecie.Controls.Add(this.label4);
            this.gbEspecie.Controls.Add(this.label3);
            this.gbEspecie.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbEspecie.ForeColor = System.Drawing.Color.Navy;
            this.gbEspecie.Location = new System.Drawing.Point(9, 176);
            this.gbEspecie.Name = "gbEspecie";
            this.gbEspecie.Size = new System.Drawing.Size(513, 150);
            this.gbEspecie.TabIndex = 118;
            this.gbEspecie.TabStop = false;
            this.gbEspecie.Text = "Baixa";
            this.gbEspecie.Visible = false;
            // 
            // lblTroco
            // 
            this.lblTroco.Location = new System.Drawing.Point(171, 127);
            this.lblTroco.Name = "lblTroco";
            this.lblTroco.Size = new System.Drawing.Size(180, 20);
            this.lblTroco.TabIndex = 120;
            this.lblTroco.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotal
            // 
            this.lblTotal.Location = new System.Drawing.Point(315, 127);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(180, 20);
            this.lblTotal.TabIndex = 119;
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgEspecie
            // 
            this.dgEspecie.AllowUserToAddRows = false;
            this.dgEspecie.AllowUserToResizeColumns = false;
            this.dgEspecie.AllowUserToResizeRows = false;
            dataGridViewCellStyle40.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEspecie.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle40;
            this.dgEspecie.BackgroundColor = System.Drawing.Color.White;
            this.dgEspecie.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEspecie.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle41;
            this.dgEspecie.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ESP_CODIGO,
            this.ESP_DESCR,
            this.ESP_VALOR});
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle44.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle44.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEspecie.DefaultCellStyle = dataGridViewCellStyle44;
            this.dgEspecie.GridColor = System.Drawing.Color.Black;
            this.dgEspecie.Location = new System.Drawing.Point(199, 39);
            this.dgEspecie.MultiSelect = false;
            this.dgEspecie.Name = "dgEspecie";
            this.dgEspecie.ReadOnly = true;
            dataGridViewCellStyle45.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle45.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle45.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle45.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEspecie.RowHeadersDefaultCellStyle = dataGridViewCellStyle45;
            this.dgEspecie.RowHeadersWidth = 30;
            dataGridViewCellStyle46.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle46.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle46.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle46.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle46.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEspecie.RowsDefaultCellStyle = dataGridViewCellStyle46;
            this.dgEspecie.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEspecie.Size = new System.Drawing.Size(296, 85);
            this.dgEspecie.TabIndex = 118;
            this.dgEspecie.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgEspecie_RowsRemoved);
            this.dgEspecie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgEspecie_KeyDown);
            // 
            // ESP_CODIGO
            // 
            this.ESP_CODIGO.DataPropertyName = "ESP_CODIGO";
            dataGridViewCellStyle42.Format = "C2";
            dataGridViewCellStyle42.NullValue = null;
            this.ESP_CODIGO.DefaultCellStyle = dataGridViewCellStyle42;
            this.ESP_CODIGO.HeaderText = "Esp. Codigo";
            this.ESP_CODIGO.MaxInputLength = 18;
            this.ESP_CODIGO.Name = "ESP_CODIGO";
            this.ESP_CODIGO.ReadOnly = true;
            this.ESP_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ESP_CODIGO.Visible = false;
            // 
            // ESP_DESCR
            // 
            this.ESP_DESCR.DataPropertyName = "ESP_DESCRICAO";
            this.ESP_DESCR.HeaderText = "Espécie";
            this.ESP_DESCR.MaxInputLength = 20;
            this.ESP_DESCR.Name = "ESP_DESCR";
            this.ESP_DESCR.ReadOnly = true;
            this.ESP_DESCR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ESP_DESCR.Width = 135;
            // 
            // ESP_VALOR
            // 
            this.ESP_VALOR.DataPropertyName = "ESP_VALOR";
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle43.Format = "C2";
            dataGridViewCellStyle43.NullValue = null;
            this.ESP_VALOR.DefaultCellStyle = dataGridViewCellStyle43;
            this.ESP_VALOR.HeaderText = "Valor";
            this.ESP_VALOR.MaxInputLength = 18;
            this.ESP_VALOR.Name = "ESP_VALOR";
            this.ESP_VALOR.ReadOnly = true;
            this.ESP_VALOR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ESP_VALOR.Width = 125;
            // 
            // txtEValor
            // 
            this.txtEValor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEValor.Location = new System.Drawing.Point(12, 102);
            this.txtEValor.Name = "txtEValor";
            this.txtEValor.Size = new System.Drawing.Size(156, 22);
            this.txtEValor.TabIndex = 3;
            this.txtEValor.Text = "0,00";
            this.txtEValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEValor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEValor_KeyDown);
            this.txtEValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEValor_KeyPress);
            this.txtEValor.Validated += new System.EventHandler(this.txtEValor_Validated);
            // 
            // cmbEspecie
            // 
            this.cmbEspecie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEspecie.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEspecie.FormattingEnabled = true;
            this.cmbEspecie.Location = new System.Drawing.Point(12, 39);
            this.cmbEspecie.Name = "cmbEspecie";
            this.cmbEspecie.Size = new System.Drawing.Size(156, 24);
            this.cmbEspecie.TabIndex = 2;
            this.cmbEspecie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbEspecie_KeyDown);
            this.cmbEspecie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbEspecie_KeyPress);
            this.cmbEspecie.Validated += new System.EventHandler(this.cmbEspecie_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "Valor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Espécie";
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(6, 21);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 109;
            // 
            // ESCOLHA
            // 
            this.ESCOLHA.HeaderText = "";
            this.ESCOLHA.Name = "ESCOLHA";
            this.ESCOLHA.Width = 30;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "EST_CODIGO";
            this.dataGridViewTextBoxColumn1.HeaderText = "Estabelecimento";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // EMPRESA
            // 
            this.EMPRESA.DataPropertyName = "EMP_CODIGO";
            this.EMPRESA.HeaderText = "Empresa";
            this.EMPRESA.Name = "EMPRESA";
            this.EMPRESA.ReadOnly = true;
            this.EMPRESA.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "COBRANCA_ID";
            this.dataGridViewTextBoxColumn2.HeaderText = "Código";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Visible = false;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "CF_DOCTO";
            this.dataGridViewTextBoxColumn3.HeaderText = "CPF";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 14;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Visible = false;
            // 
            // CON_CODIGO
            // 
            this.CON_CODIGO.DataPropertyName = "CON_CODIGO";
            this.CON_CODIGO.HeaderText = "Conveniada";
            this.CON_CODIGO.MaxInputLength = 18;
            this.CON_CODIGO.Name = "CON_CODIGO";
            this.CON_CODIGO.ReadOnly = true;
            this.CON_CODIGO.Visible = false;
            // 
            // CLI_NOME
            // 
            this.CLI_NOME.DataPropertyName = "CF_NOME";
            this.CLI_NOME.HeaderText = "Nome";
            this.CLI_NOME.MaxInputLength = 50;
            this.CLI_NOME.Name = "CLI_NOME";
            this.CLI_NOME.ReadOnly = true;
            this.CLI_NOME.Width = 270;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "COBRANCA_VENDA_ID";
            this.dataGridViewTextBoxColumn4.HeaderText = "Título";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 90;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "COBRANCA_PARCELA_NUM";
            this.dataGridViewTextBoxColumn5.HeaderText = "Parcela";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.ReadOnly = true;
            this.dataGridViewTextBoxColumn5.Width = 70;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "COBRANCA_PARCELA_VALOR";
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle34.Format = "N2";
            dataGridViewCellStyle34.NullValue = null;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle34;
            this.dataGridViewTextBoxColumn6.HeaderText = "Valor";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 90;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "COBRANCA_PARCELA_SALDO";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle35.Format = "N2";
            dataGridViewCellStyle35.NullValue = null;
            this.dataGridViewTextBoxColumn8.DefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridViewTextBoxColumn8.HeaderText = "Saldo";
            this.dataGridViewTextBoxColumn8.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "COBRANCA_DATA";
            this.dataGridViewTextBoxColumn10.HeaderText = "Data";
            this.dataGridViewTextBoxColumn10.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 90;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "COBRANCA_PARCELA_VENCIMENTO";
            this.dataGridViewTextBoxColumn7.HeaderText = "Vencimento";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 90;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "COBRANCA_VENDA_ID";
            this.dataGridViewTextBoxColumn11.HeaderText = "N° do Pedido";
            this.dataGridViewTextBoxColumn11.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.ReadOnly = true;
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Visible = false;
            this.dataGridViewTextBoxColumn11.Width = 120;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "COBRANCA_PARCELA_STATUS";
            this.dataGridViewTextBoxColumn12.HeaderText = "Pago";
            this.dataGridViewTextBoxColumn12.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Visible = false;
            this.dataGridViewTextBoxColumn12.Width = 75;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "DT_CADASTRO";
            dataGridViewCellStyle36.Format = "G";
            this.dataGridViewTextBoxColumn14.DefaultCellStyle = dataGridViewCellStyle36;
            this.dataGridViewTextBoxColumn14.HeaderText = "Dt. Alteração";
            this.dataGridViewTextBoxColumn14.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            this.dataGridViewTextBoxColumn14.Visible = false;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "OP_CADASTRO";
            this.dataGridViewTextBoxColumn15.HeaderText = "Usuário";
            this.dataGridViewTextBoxColumn15.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Visible = false;
            // 
            // COBRANCA_PARCELA_ID
            // 
            this.COBRANCA_PARCELA_ID.DataPropertyName = "COBRANCA_PARCELA_ID";
            this.COBRANCA_PARCELA_ID.HeaderText = "COBRANCA_PARCELA_ID";
            this.COBRANCA_PARCELA_ID.Name = "COBRANCA_PARCELA_ID";
            this.COBRANCA_PARCELA_ID.Visible = false;
            // 
            // COBRANCA_PARCELA_VENCIMENTO
            // 
            this.COBRANCA_PARCELA_VENCIMENTO.DataPropertyName = "COBRANCA_PARCELA_VENCIMENTO";
            dataGridViewCellStyle26.Format = "d";
            dataGridViewCellStyle26.NullValue = null;
            this.COBRANCA_PARCELA_VENCIMENTO.DefaultCellStyle = dataGridViewCellStyle26;
            this.COBRANCA_PARCELA_VENCIMENTO.HeaderText = "Data";
            this.COBRANCA_PARCELA_VENCIMENTO.MaxInputLength = 20;
            this.COBRANCA_PARCELA_VENCIMENTO.Name = "COBRANCA_PARCELA_VENCIMENTO";
            this.COBRANCA_PARCELA_VENCIMENTO.ReadOnly = true;
            this.COBRANCA_PARCELA_VENCIMENTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.COBRANCA_PARCELA_VENCIMENTO.Width = 120;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "COBRANCA_ID";
            this.dataGridViewTextBoxColumn16.HeaderText = "Cod. Receber";
            this.dataGridViewTextBoxColumn16.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Visible = false;
            this.dataGridViewTextBoxColumn16.Width = 135;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "COBRANCA_VENDA_ID";
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.NullValue = null;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle27;
            this.dataGridViewTextBoxColumn17.HeaderText = "Título";
            this.dataGridViewTextBoxColumn17.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.ReadOnly = true;
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Width = 125;
            // 
            // COBRANCA_PARCELA_NUM
            // 
            this.COBRANCA_PARCELA_NUM.DataPropertyName = "COBRANCA_PARCELA_NUM";
            this.COBRANCA_PARCELA_NUM.HeaderText = "Parcela";
            this.COBRANCA_PARCELA_NUM.MaxInputLength = 18;
            this.COBRANCA_PARCELA_NUM.Name = "COBRANCA_PARCELA_NUM";
            this.COBRANCA_PARCELA_NUM.ReadOnly = true;
            this.COBRANCA_PARCELA_NUM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // COBRANCA_PARCELA_SALDO
            // 
            this.COBRANCA_PARCELA_SALDO.DataPropertyName = "COBRANCA_PARCELA_SALDO";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle28.Format = "N2";
            dataGridViewCellStyle28.NullValue = null;
            this.COBRANCA_PARCELA_SALDO.DefaultCellStyle = dataGridViewCellStyle28;
            this.COBRANCA_PARCELA_SALDO.HeaderText = "Valor";
            this.COBRANCA_PARCELA_SALDO.MaxInputLength = 18;
            this.COBRANCA_PARCELA_SALDO.Name = "COBRANCA_PARCELA_SALDO";
            this.COBRANCA_PARCELA_SALDO.ReadOnly = true;
            this.COBRANCA_PARCELA_SALDO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.COBRANCA_PARCELA_SALDO.Width = 140;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "COBRANCA_PARCELA_ID";
            this.Column1.HeaderText = "COBRANCA_PARCELA_ID";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Visible = false;
            // 
            // frmCxRecebConv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmCxRecebConv";
            this.Text = "frmCxRecebConv";
            this.Load += new System.EventHandler(this.frmCxRecebConv_Load);
            this.Shown += new System.EventHandler(this.frmCxRecebConv_Shown);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmCxRecebConv_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcConveniadas.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgConveniadas)).EndInit();
            this.tpFicha.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPagamentos)).EndInit();
            this.gbEspecie.ResumeLayout(false);
            this.gbEspecie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEspecie)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.TabControl tcConveniadas;
        private System.Windows.Forms.TabPage tpGrade;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.DataGridView dgConveniadas;
        private System.Windows.Forms.Button btnDesmarcar;
        private System.Windows.Forms.Button btnMarcar;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnVPagar;
        private System.Windows.Forms.TextBox txtVTotal;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgPagamentos;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.GroupBox gbEspecie;
        private System.Windows.Forms.Label lblTroco;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.DataGridView dgEspecie;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_VALOR;
        private System.Windows.Forms.TextBox txtEValor;
        private System.Windows.Forms.ComboBox cmbEspecie;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.DateTimePicker dtFinal;
        private System.Windows.Forms.DateTimePicker dtInicial;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ESCOLHA;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn EMPRESA;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLI_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_VENCIMENTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_NUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_SALDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
    }
}