﻿namespace GestaoAdministrativa.Caixa
{
    partial class frmRelatorioResumoGeralVendasPeriodo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rpwResumoGeral = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rpwResumoGeral
            // 
            this.rpwResumoGeral.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpwResumoGeral.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Caixa.Relatorios.RelatorioResumoGeralVendasPeriodo.rdlc";
            this.rpwResumoGeral.Location = new System.Drawing.Point(0, 0);
            this.rpwResumoGeral.Name = "rpwResumoGeral";
            this.rpwResumoGeral.Size = new System.Drawing.Size(994, 578);
            this.rpwResumoGeral.TabIndex = 0;
            // 
            // frmRelatorioResumoGeralVendasPeriodo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rpwResumoGeral);
            this.Name = "frmRelatorioResumoGeralVendasPeriodo";
            this.Text = "Relatorio Resumo Geral de Vendas Por Periodo";
            this.Load += new System.EventHandler(this.frmRelatorioResumoGeralVendasPeriodo_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpwResumoGeral;
    }
}