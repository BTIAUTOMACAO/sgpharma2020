﻿namespace GestaoAdministrativa.Caixa
{
    partial class frmCxRecebimentosProdutos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            this.RelatorioProdutosComprovanteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rpwRelatorioProdutos = new Microsoft.Reporting.WinForms.ReportViewer();
            this.EstabelecimentoBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioProdutosComprovanteBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstabelecimentoBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // RelatorioProdutosComprovanteBindingSource
            // 
            this.RelatorioProdutosComprovanteBindingSource.DataSource = typeof(GestaoAdministrativa.Negocio.RelatorioProdutosComprovante);
            // 
            // rpwRelatorioProdutos
            // 
            this.rpwRelatorioProdutos.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "DadosComprovante";
            reportDataSource1.Value = this.RelatorioProdutosComprovanteBindingSource;
            this.rpwRelatorioProdutos.LocalReport.DataSources.Add(reportDataSource1);
            this.rpwRelatorioProdutos.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Caixa.RelatorioProdutosMaster.rdlc";
            this.rpwRelatorioProdutos.Location = new System.Drawing.Point(0, 0);
            this.rpwRelatorioProdutos.Name = "rpwRelatorioProdutos";
            this.rpwRelatorioProdutos.Size = new System.Drawing.Size(994, 578);
            this.rpwRelatorioProdutos.TabIndex = 0;
            this.rpwRelatorioProdutos.Load += new System.EventHandler(this.rpwRelatorioProdutos_Load);
            // 
            // EstabelecimentoBindingSource
            // 
            this.EstabelecimentoBindingSource.DataSource = typeof(GestaoAdministrativa.Negocio.Estabelecimento);
            // 
            // frmCxRecebimentosProdutos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rpwRelatorioProdutos);
            this.Name = "frmCxRecebimentosProdutos";
            this.Text = "frmCxRecebimentosProdutos";
            this.Load += new System.EventHandler(this.frmCxRecebimentosProdutos_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioProdutosComprovanteBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstabelecimentoBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpwRelatorioProdutos;
        private System.Windows.Forms.BindingSource EstabelecimentoBindingSource;
        private System.Windows.Forms.BindingSource RelatorioProdutosComprovanteBindingSource;
    }
}