﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmRelatorioResumoGeralVendasPeriodo : Form
    {
        DateTime dtInicial, dtFinal;

        public frmRelatorioResumoGeralVendasPeriodo(DateTime dtIni, DateTime dtFi)
        {
            InitializeComponent();
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            this.WindowState = FormWindowState.Maximized;
            dtInicial = Convert.ToDateTime(dtIni.ToString("dd/MM/yyyy") + " 00:00:00");
            dtFinal = Convert.ToDateTime(dtFi.ToString("dd/MM/yyyy") + " 23:59:59");
        }

        private void frmRelatorioResumoGeralVendasPeriodo_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorioAnalitico();
            this.rpwResumoGeral.RefreshReport();
        }
        public void MontaDadosEstabelecimento()
        {
            VendasItens vendas = new VendasItens();
            ReportParameter[] parametro = new ReportParameter[6];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("dtInicial", dtInicial.ToString("dd/MM/yyyy"));
            parametro[2] = new ReportParameter("dtFinal", dtFinal.ToString("dd/MM/yyyy"));
            parametro[3] = new ReportParameter("TotalVendas", Convert.ToString(vendas.NumeroVendasPorPeriodo(dtInicial, dtFinal)));
            parametro[4] = new ReportParameter("TotalEntregas", Convert.ToString(vendas.NumeroEntregasPorPeriodo(dtInicial, dtFinal, 'S')));
            parametro[5] = new ReportParameter("TotalCupons", Convert.ToString(vendas.NumeroCuponsPorPeriodo(dtInicial, dtFinal, 'S')));
            this.rpwResumoGeral.LocalReport.SetParameters(parametro);
        }
        public void MontaRelatorioAnalitico()
        {
            Departamento departamento = new Departamento();
            DataTable dtVendasDepartamento = departamento.BuscaTotalVendasPorDepartamento(dtInicial, dtFinal);

            FormasPagamento formas = new FormasPagamento();
            DataTable dtFormasPagamento = formas.VendasPorFormaDePagamento(dtInicial, dtFinal);

            Especie esp = new Especie();
            DataTable dtEspecie = esp.VendasPorEspecie(dtInicial, dtFinal);

            VendasDados dados = new VendasDados();
            DataTable dtVenda = dados.BuscaSomatorioDiferenca(dtInicial, dtFinal);

            var formasPagamento = new ReportDataSource("VendasPorFormaPagamento", dtFormasPagamento);
            var vendasDepartamento = new ReportDataSource("VendasPorDepartamento", dtVendasDepartamento);
            var vendasEspecie = new ReportDataSource("VendasPorEspecie", dtEspecie);
            var diferenca = new ReportDataSource("diferenca", dtVenda);

            rpwResumoGeral.LocalReport.DataSources.Clear();
            rpwResumoGeral.LocalReport.DataSources.Add(vendasDepartamento);
            rpwResumoGeral.LocalReport.DataSources.Add(formasPagamento);
            rpwResumoGeral.LocalReport.DataSources.Add(vendasEspecie);
            rpwResumoGeral.LocalReport.DataSources.Add(diferenca);
        }

    }
}
