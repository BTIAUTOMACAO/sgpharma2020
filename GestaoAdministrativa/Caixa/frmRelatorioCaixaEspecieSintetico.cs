﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa.Relatorios
{
    public partial class frmRelatorioCaixaEspecieSintetico : Form
    {
        DateTime dtInicial, dtFinal;
        string usuario;

        public frmRelatorioCaixaEspecieSintetico(DateTime dtIni, DateTime dtFi, string caixa)
        {
            this.WindowState = FormWindowState.Maximized;
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            InitializeComponent();
            dtInicial = dtIni;
            dtFinal = dtFi.AddHours(23).AddMinutes(59).AddSeconds(59); 
            usuario = caixa;
        }

        private void frmRelatorioCaixaEspecieSintetico_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorioSintetico();
            this.rpwEspecieSintetico.RefreshReport();
        }


        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[4];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("caixa", usuario.ToUpper());
            parametro[2] = new ReportParameter("dtInicial", dtInicial.ToString("dd/MM/yyyy"));
            parametro[3] = new ReportParameter("dtFinal", dtFinal.ToString("dd/MM/yyyy"));

            this.rpwEspecieSintetico.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorioSintetico()
        {
            MovimentoCaixaEspecie movimento = new MovimentoCaixaEspecie();
            DataTable dtEntradaSaida =  movimento.BuscaEntradaSaidaPorEspecie(dtInicial, dtFinal, usuario);

            var entradaSaidaPorEspecie = new ReportDataSource("CaixaPorEspecie", dtEntradaSaida);
            rpwEspecieSintetico.LocalReport.DataSources.Clear();
            rpwEspecieSintetico.LocalReport.DataSources.Add(entradaSaidaPorEspecie);
        }
    }
}
