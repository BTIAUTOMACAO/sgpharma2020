﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Globalization;
using GestaoAdministrativa.Vendas;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.SAT;
using System.IO;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmCxMovCaixa : Form, Botoes
    {
        private string retorno;
        private DateTime dtCaixa;
        private double valorDiv;
        private double totalEsp;
        private bool emGrade;
        private DataTable dtMovCaixa = new DataTable();
        private int contRegistros;
        private ToolStripButton tsbMovCaixa = new ToolStripButton("Mov. de Caixa");
        private ToolStripSeparator tssMovCaixa = new ToolStripSeparator();
        private string strOrdem;
        private bool decrescente;
        private string hora;
        private string sequencia;
        private string[,] dados = new string[5, 3];
        private string[] comando = new string[10];
        private int contComando;
        private DateTime dtMovimentoCaixa;
        private bool validou;
        private bool todos;

        public frmCxMovCaixa(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCxMovCaixa_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");

                if (Principal.usuADM.Equals("1"))
                {
                    todos = true;
                }
                else
                    todos = false;

                txtBData.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmCxMovCaixa_Shown(object sender, EventArgs e)
        {
            try
            {
                AFCaixa caixa = new AFCaixa();

                DataTable dtCaixa = caixa.BuscaCaixaStatus(Principal.usuario, 'S');

                if (dtCaixa.Rows.Count.Equals(0))
                {
                    MessageBox.Show("Não há nenhuma data de caixa aberto! Utilize a opção 'Caixa - Abertura/Fechamento' para abrir o caixa", "Movimentação de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                }
                else
                {
                    VerificaDataCaixa(Convert.ToDateTime(dtCaixa.Rows[0]["DATA_ABERTURA"]));

                    txtMData.Text = Convert.ToDateTime(dtCaixa.Rows[0]["DATA_ABERTURA"]).ToString("dd/MM/yyyy");

                    CarregaCombos();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public bool CarregaCombos()
        {
            try
            {

                OperacaoDC opera = new OperacaoDC()
                {
                    OdcLiberado = "SIM",
                    OdcTipo = "TODOS"
                };
                //CARREGA OPERAÇÕES_DC//
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("ODC_NUM", "ODC_DESCRICAO", "OPERACOES_DC", false, "ODC_DESABILITADO = 'N' AND ODC_TIPO = 'C'");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma Operação DC,\npara cadastrar uma movimentação de caixa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbOperacao.DataSource = Principal.dtPesq;
                    cmbOperacao.DisplayMember = "ODC_DESCRICAO";
                    cmbOperacao.ValueMember = "ODC_NUM";

                    cmbOperacao.SelectedIndex = -1;

                    cmbBOperacao.DataSource = Principal.dtPesq;
                    cmbBOperacao.DisplayMember = "ODC_DESCRICAO";
                    cmbBOperacao.ValueMember = "ODC_NUM";
                    cmbBOperacao.SelectedIndex = -1;

                }


                ////CARREGA ESPECIES//
                Especie esp = new Especie();

                Principal.dtPesq = esp.BuscaDados('N');

                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma Espécie,\npara cadastrar uma movimentação de caixa.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                    return false;
                }
                else
                {
                    cmbEspecie.DataSource = Principal.dtPesq;
                    cmbEspecie.DisplayMember = "ESP_DESCRICAO";
                    cmbEspecie.ValueMember = "ESP_CODIGO";

                    cmbEspecie.SelectedIndex = -1;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }


        }

        private void tcMovCaixa_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcMovCaixa.SelectedTab == tpGrade)
            {
                dgMovCaixa.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcMovCaixa.SelectedTab == tpFicha)
            {
                Funcoes.BotoesCadastro("frmCxMovCaixa");
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMovCaixa, contRegistros));
                    cmbOperacao.Focus();
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = true;
                    emGrade = false;
                }
            }
        }

        private void cmbOperacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbOperacao.Text.Equals("SANGRIA"))
                {
                    if (!Principal.usuADM.Equals("S") && Funcoes.LeParametro(4, "72", false).Equals("S"))
                    {
                        frmVenSupSenha senhaSup = new frmVenSupSenha(0);
                        senhaSup.ShowDialog();
                        if (senhaSup.sValida.Equals(false))
                        {
                            MessageBox.Show("Não é possível prosseguir o lançamento sem a senha de autorização!", "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                        else
                            validou = true;
                    }
                }

                txtValor.Focus();
            }
        }

        private void txtValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtObs.Focus();
        }

        private void txtValor_Validated(object sender, EventArgs e)
        {
            if (txtValor.Text.Trim() == "")
            {
                txtValor.Text = "0,00";
            }
            else
                txtValor.Text = String.Format("{0:N}", Convert.ToDecimal(txtValor.Text));
        }

        private void cmbEspecie_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (txtEValor.Text.Trim() != "")
                    {
                        if (Convert.ToDouble(txtEValor.Text) == 0)
                        {
                            txtEValor.Text = String.Format("{0:N}", Convert.ToDouble(txtValor.Text) - totalEsp);
                        }
                    }
                    txtEValor.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbEspecie_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtValor.Text.Trim() != "")
                {
                    if (txtEValor.Text.Trim() != "")
                    {
                        if (Convert.ToDouble(txtEValor.Text) == 0)
                        {
                            txtEValor.Text = String.Format("{0:N}", Convert.ToDouble(txtValor.Text) - totalEsp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtEValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Funcoes.SomenteNumeros(e);
                if (e.KeyChar == 13)
                {
                    if (ConsisteCampos().Equals(true))
                    {
                        dgEspecie.Rows.Insert(dgEspecie.RowCount, new Object[] { cmbEspecie.SelectedValue, cmbEspecie.Text, txtEValor.Text });

                        valorDiv = 0;
                        totalEsp = 0;

                        for (int i = 0; i < dgEspecie.RowCount; i++)
                        {
                            valorDiv = valorDiv + Convert.ToDouble(dgEspecie.Rows[i].Cells[2].Value);
                            totalEsp = totalEsp + Convert.ToDouble(dgEspecie.Rows[i].Cells[2].Value);
                        }

                        lblTotal.Text = "Total: " + Funcoes.LeParametro(0, "01", false) + " " + String.Format("{0:N}", valorDiv);
                        txtEValor.Text = "0,00";
                        cmbEspecie.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool ConsisteCampos()
        {

            if (txtEValor.Text.Trim() == "")
            {
                txtEValor.Text = "0,00";
            }
            else
                txtEValor.Text = String.Format("{0:N}", Convert.ToDecimal(txtEValor.Text));

            if (cmbEspecie.SelectedIndex == -1)
            {
                MessageBox.Show("Selecione a espécie.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbEspecie.Focus();
                return false;
            }
            if (Convert.ToDouble(txtEValor.Text) <= 0)
            {
                MessageBox.Show("O valor deve ser maior que zero.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtEValor.Focus();
                return false;
            }
            for (int i = 0; i < dgEspecie.RowCount; i++)
            {
                if (cmbEspecie.SelectedValue.Equals(dgEspecie.Rows[i].Cells[0].Value))
                {
                    MessageBox.Show("Já existe um valor lançado para esta espécie.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbEspecie.Focus();
                    return false;
                }
            }

            return true;
        }

        private void txtEValor_Validated(object sender, EventArgs e)
        {
            if (txtEValor.Text.Trim() != "")
            {
                txtEValor.Text = String.Format("{0:N}", txtEValor.Text);
            }
            else
                txtEValor.Text = "0,00";
        }

        private void dgEspecie_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            try
            {
                valorDiv = 0;
                for (int i = 0; i < dgEspecie.RowCount; i++)
                {
                    valorDiv = valorDiv + Convert.ToDouble(dgEspecie.Rows[i].Cells[2].Value);
                }
                if (valorDiv == 0)
                {
                    lblTotal.Text = "";
                    totalEsp = 0;
                }
                else
                {
                    lblTotal.Text = "Total: " + Funcoes.LeParametro(0, "01", false) + " " + String.Format("{0:N}", valorDiv);
                    totalEsp = valorDiv;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            if (dtMovCaixa.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgMovCaixa.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgMovCaixa.CurrentCell = dgMovCaixa.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcMovCaixa.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtMovCaixa.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgMovCaixa.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgMovCaixa.CurrentCell = dgMovCaixa.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgMovCaixa.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgMovCaixa.CurrentCell = dgMovCaixa.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgMovCaixa.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgMovCaixa.CurrentCell = dgMovCaixa.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Botao()
        {
            try
            {
                this.tsbMovCaixa.AutoSize = false;
                this.tsbMovCaixa.Image = Properties.Resources.caixa;
                this.tsbMovCaixa.Size = new System.Drawing.Size(125, 20);
                this.tsbMovCaixa.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbMovCaixa);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssMovCaixa);
                tsbMovCaixa.Click += delegate
                {
                    var movCaixa = Application.OpenForms.OfType<frmCxMovCaixa>().FirstOrDefault();
                    Funcoes.BotoesCadastro(movCaixa.Name);
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMovCaixa, contRegistros));
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (tcMovCaixa.SelectedTab == tpGrade)
                    {
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnLimpar.Enabled = true;
                    }
                    else if (tcMovCaixa.SelectedTab == tpFicha &&  emGrade == true)
                    {
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnLimpar.Enabled = true;
                    }
                    
                    movCaixa.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Funcoes.BotoesCadastro("frmCxMovCaixa");
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMovCaixa, contRegistros));
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                if (tcMovCaixa.SelectedTab == tpGrade)
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnLimpar.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtMovCaixa.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbMovCaixa);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssMovCaixa);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dgEspecie.Rows.Clear();
            dtMovCaixa.Clear();
            dgMovCaixa.DataSource = dtMovCaixa;
            txtMData.Text = dtCaixa.ToString("dd/MM/yyyy");
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            emGrade = false;
            Array.Clear(dados, 0, 15);
            hora = "";
            txtValor.Text = "0,00";
            cmbEspecie.SelectedIndex = -1;
            txtEValor.Text = "0,00";
            txtID.Text = "";
            validou = false;
            txtBData.Focus();
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
        }

        public void LimparFicha()
        {
            dtMovCaixa.Clear();
            validou = false;
            dgMovCaixa.DataSource = dtMovCaixa;
            txtMData.Text = dtCaixa.ToString("dd/MM/yyyy");
            cmbOperacao.SelectedIndex = -1;
            txtValor.Text = "0,00";
            cmbEspecie.SelectedIndex = -1;
            txtEValor.Text = "0,00";
            dgEspecie.Rows.Clear();
            lblTotal.Text = "";
            hora = "";
            txtID.Text = "";
            txtObs.Text = "";
            cmbOperacao.Focus();
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = true;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcMovCaixa.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();
        }

        public void CarregarDados(int linha)
        {
            try
            {

                MovimentoCaixa movCaixa = new MovimentoCaixa();

                dgEspecie.Rows.Clear();
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                txtID.Text = dtMovCaixa.Rows[linha]["MOVIMENTO_CAIXA_ID"].ToString();
                txtMData.Text = Funcoes.ChecaCampoVazio(dtMovCaixa.Rows[linha]["MOVIMENTO_CAIXA_DATA"].ToString().Substring(0, 10));
                dtMovimentoCaixa = Convert.ToDateTime(dtMovCaixa.Rows[linha]["MOVIMENTO_CAIXA_DATA"]);
                hora = Funcoes.ChecaCampoVazio(dtMovCaixa.Rows[linha]["MOVIMENTO_CAIXA_DATA"].ToString().Substring(11));
                cmbOperacao.SelectedValue = Funcoes.ChecaCampoVazio(dtMovCaixa.Rows[linha]["MOVIMENTO_CAIXA_ODC_ID"].ToString());
                txtValor.Text = String.Format("{0:N}", dtMovCaixa.Rows[linha]["MOVIMENTO_CAIXA_VALOR"]);
                sequencia = Funcoes.ChecaCampoVazio(dtMovCaixa.Rows[linha]["MOVIMENTO_CAIXA_ID"].ToString());
                txtData.Text = Funcoes.ChecaCampoVazio(dtMovCaixa.Rows[linha]["DT_CADASTRO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtMovCaixa.Rows[linha]["OP_CADASTRO"].ToString());
                txtObs.Text = Funcoes.ChecaCampoVazio(dtMovCaixa.Rows[linha]["MOVIMENTO_CAIXA_OBS"].ToString());

                valorDiv = 0;
                totalEsp = 0;
                Principal.dtPesq = movCaixa.BuscaMovimentoCaixaPorEspecieData(Convert.ToDateTime(dtMovCaixa.Rows[linha]["MOVIMENTO_CAIXA_DATA"]));
                for (int i = 0; i < Principal.dtPesq.Rows.Count; i++)
                {
                    dgEspecie.Rows.Insert(dgEspecie.RowCount, new Object[] { Principal.dtPesq.Rows[i]["MOVIMENTO_CX_ESPECIE_CODIGO"], Principal.dtPesq.Rows[i]["ESP_DESCRICAO"], Principal.dtPesq.Rows[i]["MOVIMENTO_CX_ESPECIE_VALOR"] });
                }

                for (int i = 0; i < dgEspecie.RowCount; i++)
                {
                    valorDiv = valorDiv + Convert.ToDouble(dgEspecie.Rows[i].Cells[2].Value);
                    totalEsp = totalEsp + Convert.ToDouble(dgEspecie.Rows[i].Cells[2].Value);
                }

                lblTotal.Text = "Total: " + Funcoes.LeParametro(0, "01", false) + " " + String.Format("{0:N}", valorDiv);
                txtEValor.Text = "0,00";
                cmbEspecie.SelectedIndex = -1;

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMovCaixa, linha));
                cmbOperacao.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (CamposIncluir().Equals(true))
                {
                    if (VerificaDataCaixa(dtCaixa).Equals(true))
                    {
                        string tipo = Util.SelecionaCampoEspecificoDaTabela("OPERACOES_DC", "ODC_CLASSE", "ODC_NUM", Convert.ToDouble(cmbOperacao.SelectedValue).ToString());
                        DateTime dtLancamento = Convert.ToDateTime(txtMData.Text + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second);
                        MovimentoCaixa movCaixa = new MovimentoCaixa()
                        {
                            EmpCodigo = Principal.empAtual,
                            EstCodigo = Principal.estAtual,
                            MovimentoCaixaID = Funcoes.IdentificaVerificaID("MOVIMENTO_CAIXA", "MOVIMENTO_CAIXA_ID", 0, ""),
                            MovimentoCaixaData = dtLancamento,// Convert.ToDateTime(txtMData.Text + " " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second),
                            MovimentoCaixaOdcID = Convert.ToInt32(Convert.ToDouble(cmbOperacao.SelectedValue)),
                            MovimentoCaixaOdcClasse = Convert.ToDouble(tipo) == 0 ? "-" : "+",
                            MovimentoCaixaValor = Double.Parse(String.Format("{0:N}", valorDiv)),//double.Parse((lblTotal.Text), new CultureInfo("pt-BR")),
                            MovimentoCaixaUsuario = Principal.usuario,
                            MovimentoCaixaObs = txtObs.Text,
                            DtCadastro = DateTime.Now,
                            OpCadastro = Principal.usuario,
                            VendaID = 0
                        };
                        BancoDados.AbrirTrans();
                        if (movCaixa.InserirDados(movCaixa).Equals(true))
                        {

                            for (int i = 0; i < dgEspecie.RowCount; i++)
                            {
                                MovimentoCaixaEspecie movCaixaEspecie = new MovimentoCaixaEspecie()
                                {

                                    EmpCodigo = Principal.empAtual,
                                    EstCodigo = Principal.estAtual,
                                    MovimentoCxEspecieID = i + Funcoes.IdentificaVerificaID("MOVIMENTO_CAIXA_ESPECIE", "MOVIMENTO_CX_ESPECIE_ID"),
                                    MovimentoCxEspecieData = dtLancamento,
                                    MovimentoCxEspecieCodigo = Convert.ToInt32(dgEspecie.Rows[i].Cells["ESP_CODIGO"].Value),
                                    MovimentoCxEspecieValor = Convert.ToDouble(dgEspecie.Rows[i].Cells["ESP_VALOR"].Value),
                                    MovimentoCxEspecieUsuario = Principal.usuario,
                                    DtCadastro = DateTime.Now,
                                    OpCadastro = Principal.usuario

                                };

                                if (movCaixaEspecie.InserirDados(movCaixaEspecie).Equals(false))
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("Erro ao efetuar o cadastro", "Movimento Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }

                            BancoDados.FecharTrans();
                            if (MessageBox.Show("Deseja imprimir comprovante?", "Movimento Caixa", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                            {
                                ImprimirComprovante();
                            }

                            LimparFicha();
                            return true;
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("Erro ao efetuar o cadastro", "Movimento Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool ImprimirComprovante()
        {
            try
            {
                Estabelecimento dadosEstabelecimento = new Estabelecimento();
                List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);

                string comprovante = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n"
                                    + Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n"
                                    + "CNPJ: " + retornoEstab[0].EstCNPJ + "\n"
                                    + "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n"
                                    + "_________________________________________" + "\n"
                                    + "DATA: " + String.Format("{0:dd/MM/yyyy}", DateTime.Now).PadRight(20, ' ') + "HORA: " + String.Format("{0:HH:mm:ss}", DateTime.Now) + "\n"
                                    + "CAIXA: " + Principal.usuario + "\n"
                                    + "_________________________________________\n"
                                    + Funcoes.CentralizaTexto("MOVIMENTACAO DE CAIXA", 43) + "\n"
                                    + "_________________________________________" + "\n"
                                    + "Operacao: " + cmbOperacao.Text + "\n"
                                    + "Valor: " + txtValor.Text + "\n"
                                    + "Obs: " + txtObs.Text + "\n"
                                    + "_________________________________________" + "\n"
                                    + Funcoes.CentralizaTexto("ESPECIE(S)", 43) + "\n";

                for (int i = 0; i < dgEspecie.RowCount; i++)
                {
                    comprovante += Util.SelecionaCampoEspecificoDaTabela("CAD_ESPECIES","ESP_DESCRICAO","ESP_CODIGO",
                        dgEspecie.Rows[i].Cells["ESP_CODIGO"].Value.ToString()).PadRight(41 - (string.Format("{0:0.00}", dgEspecie.Rows[i].Cells["ESP_VALOR"].Value).ToString().Length), ' ') + string.Format("{0:0.00}", dgEspecie.Rows[i].Cells["ESP_VALOR"].Value) + "\n";
                  
                }
                comprovante += "_________________________________________" + "\n\n\n";
                if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                {
                    if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                         && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                    {
                        int iRetorno;
                        string s_cmdTX = "\n";
                        if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                        {
                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                        }
                        else
                        {
                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                        }

                        iRetorno = BematechImpressora.IniciaPorta("USB");

                        iRetorno = BematechImpressora.FormataTX(comprovante, 3, 0, 0, 0, 0);

                        s_cmdTX = "\r\n";
                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                        iRetorno = BematechImpressora.AcionaGuilhotina(0);

                        iRetorno = BematechImpressora.FechaPorta();
                    }
                    else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                    {
                        int iRetorno;
                        iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                        iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovante);

                        iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                        iRetorno = InterfaceEpsonNF.FechaPorta();
                    }
                    else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                    {
                        using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\MovCaixa.txt", true))
                        {
                            writer.WriteLine(comprovante);
                        }
                    }
                    else
                    {
                        //IMPRIME COMPROVANTE
                        comprovante += "\n\n\n\n\n";
                        Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovante);
                        if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                        {
                            Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        }
                    }
                }
                else
                {
                    comprovante += "\n\n\n\n";
                    //IMPRIME COMPROVANTE
                    int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovante, 0);
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Atualiza()
        {
            return false;
        }

        public bool Excluir()
        {
            try
            {
                if (MessageBox.Show("Confirma a exclusão do  Movimento de Caixa?", "Exclusão Movimento de Caixa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    //if (RegrasCadastro.ExcluirMovimentoCaixa(Convert.ToInt32(txtID.Text)).Equals(true))
                    //{
                        using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }
                        Cursor = Cursors.WaitCursor;

                        MovimentoCaixa movCaixa = new MovimentoCaixa();
                        BancoDados.AbrirTrans();
                        if (movCaixa.ExcluiMovimentacao(Convert.ToInt32(txtID.Text)).Equals(true))
                        {
                            MovimentoCaixaEspecie movCaixaEspecie = new MovimentoCaixaEspecie();
                            if ((movCaixaEspecie.ExcluirMovimentoCaixaEspeciePorDataHora(dtMovimentoCaixa)).Equals(true))
                            {
                                BancoDados.FecharTrans();
                                Funcoes.GravaLogExclusao("MOVIMENTO_FINANCEIRO_ID", txtID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "MOVIMENTO_CAIXA", cmbOperacao.Text, Principal.motivo, Principal.estAtual);
                                MessageBox.Show("Movimento de Caixa excluido com sucesso!", "Movimento de Caixa");
                                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                                tslRegistros.Text = "";
                                LimparFicha();
                                LimparGrade();
                                return true;
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("Erro ao efetuar a exclusão", "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("Erro ao efetuar a exclusão", "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return false;
                        }
                    //}
                    //else
                    //{
                    //    return false;
                    //}
                }
                else
                {
                return false;
            }
        }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        public bool CamposIncluir()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (Equals(cmbOperacao.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione uma operação.";
                Funcoes.Avisa();
                cmbOperacao.Focus();
                return false;
            }
            if (Convert.ToDouble(txtValor.Text) <= 0)
            {
                Principal.mensagem = "O valor deve ser maior que zero.";
                Funcoes.Avisa();
                txtValor.Focus();
                return false;
            }
            valorDiv = 0;
            for (int i = 0; i < dgEspecie.RowCount; i++)
            {
                valorDiv = valorDiv + Convert.ToDouble(dgEspecie.Rows[i].Cells[2].Value);
            }
            if (String.Format("{0:N}", valorDiv) != String.Format("{0:N}", txtValor.Text))
            {
                Principal.mensagem = "O valor da divisão por espécie não confere com o valor do documento.";
                Funcoes.Avisa();
                txtEValor.Focus();
                return false;
            }
            return true;
        }

        public bool VerificaDataCaixa(DateTime dtDataCaixa)
        {
            dtCaixa = dtDataCaixa;
            if (DateTime.Now.ToString("dd/MM/yyyy") != Convert.ToDateTime(dtDataCaixa).ToString("dd/MM/yyyy"))
            {
                if (MessageBox.Show("Caro usuário, este é somente um aviso!\n Data do sistema é diferente da data do caixa.\n "
                                + "Data do sistema: " + DateTime.Now.ToString("dd / MM / yyyy") + " - Data do caixa: " + Convert.ToDateTime(dtDataCaixa).ToString("dd / MM / yyyy") + ".",
                                "Movimento de Caixa", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return true;
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                MovimentoCaixa movCaixa = new MovimentoCaixa()
                {
                    EmpCodigo = Principal.estAtual,
                    MovimentoCaixaData = Funcoes.RemoveCaracter(txtBData.Text) == "" ? Convert.ToDateTime("01/01/0001") : Convert.ToDateTime(txtBData.Text),
                    MovimentoCaixaValor = txtBValor.Text.Trim() == "" ? 0 : Convert.ToDouble(txtBValor.Text.Trim()),
                    MovimentoCaixaOdcID = cmbBOperacao.SelectedIndex == -1 ? -1 : Convert.ToInt32(cmbBOperacao.SelectedValue),
                    MovimentoCaixaUsuario = todos ? "" : Principal.usuario
                };

                dtMovCaixa = movCaixa.BuscaDados(movCaixa, out strOrdem);
                if (dtMovCaixa.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtMovCaixa.Rows.Count;
                    dgMovCaixa.DataSource = dtMovCaixa;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMovCaixa, 0));
                    dgMovCaixa.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgMovCaixa.DataSource = dtMovCaixa;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBData.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbBOperacao.SelectedIndex = -1;
                    txtMData.Text = dtCaixa.ToString("dd/MM/yyyy");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtBValor_Validated(object sender, EventArgs e)
        {
            if (txtBValor.Text.Trim() != "")
            {
                txtBValor.Text = String.Format("{0:N}", txtBValor.Text);
            }
        }

        private void dgMovCaixa_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMovCaixa, contRegistros));
            emGrade = true;
        }

        private void dgMovCaixa_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcMovCaixa.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgMovCaixa_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtMovCaixa.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtMovCaixa.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtMovCaixa.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgMovCaixa_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgMovCaixa.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        dtMovCaixa = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgMovCaixa.Columns[e.ColumnIndex].Name + " DESC");
                        dgMovCaixa.DataSource = dtMovCaixa;
                        decrescente = true;
                    }
                    else
                    {
                        dtMovCaixa = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgMovCaixa.Columns[e.ColumnIndex].Name + " ASC");
                        dgMovCaixa.DataSource = dtMovCaixa;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtMovCaixa, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgMovCaixa.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgMovCaixa);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgMovCaixa.RowCount;
                    for (i = 0; i <= dgMovCaixa.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgMovCaixa[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }
                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgMovCaixa.ColumnCount; k++)
                    {
                        if (dgMovCaixa.Columns[k].Visible == true)
                        {
                            switch (dgMovCaixa.Columns[k].HeaderText)
                            {
                                case "Data Cadastro":
                                    numCel = Principal.GetColunaExcel(dgMovCaixa.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgMovCaixa.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Saldo":
                                    numCel = Principal.GetColunaExcel(dgMovCaixa.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgMovCaixa.RowCount + 1));
                                    celulas.NumberFormat = "#,##0.00";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgMovCaixa.ColumnCount : indice) + Convert.ToString(dgMovCaixa.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }

        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtMData") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgMovCaixa.ColumnCount; i++)
                {
                    if (dgMovCaixa.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgMovCaixa.ColumnCount];
                string[] coluna = new string[dgMovCaixa.ColumnCount];

                for (int i = 0; i < dgMovCaixa.ColumnCount; i++)
                {
                    grid[i] = dgMovCaixa.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgMovCaixa.ColumnCount; i++)
                {
                    if (dgMovCaixa.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgMovCaixa.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgMovCaixa.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgMovCaixa.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgMovCaixa.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgMovCaixa.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgMovCaixa.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgMovCaixa.ColumnCount; i++)
                        {
                            dgMovCaixa.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Movimento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcMovCaixa.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcMovCaixa.SelectedTab = tpFicha;
        }

        private void txtBValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void txtBData_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtBData.Text)))
                {
                    btnBuscar.PerformClick();
                }
                else
                    cmbBOperacao.Focus();
            }
        }

        private void cmbBOperacao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbBOperacao.SelectedIndex != -1)
                {
                    btnBuscar.PerformClick();
                }
                else
                    txtBValor.Focus();
            }
        }



        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void txtObs_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbEspecie.Focus();
        }
    }
}
