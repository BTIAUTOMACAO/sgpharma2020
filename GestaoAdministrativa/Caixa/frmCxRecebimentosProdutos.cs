﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmCxRecebimentosProdutos : Form
    {
        List<object> cobrancaId;

        public frmCxRecebimentosProdutos(List<object> cobranca)
        {
            InitializeComponent();
            cobrancaId = cobranca;

          rpwRelatorioProdutos.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
        }

        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            DataTable dadosProduto = CarregaProdutosComprovante();
         
            var dataSource = new ReportDataSource("DadosProdutos", dadosProduto);
            e.DataSources.Add(dataSource);
        }


        private void frmCxRecebimentosProdutos_Load(object sender, EventArgs e)
        {

            this.rpwRelatorioProdutos.RefreshReport();
        }

        private void rpwRelatorioProdutos_Load(object sender, EventArgs e)
        {
            DataTable dadosComprovante = CarregaDadosComprovante();

            var dataSource = new Microsoft.Reporting.WinForms.ReportDataSource("DadosComprovante", dadosComprovante);
            this.rpwRelatorioProdutos.LocalReport.DataSources.Clear();
            this.rpwRelatorioProdutos.LocalReport.DataSources.Add(dataSource);
        }

        public DataTable CarregaDadosComprovante()
        {
            RelatorioDadosComprovante comp = new RelatorioDadosComprovante();

            DataTable dtDadosComprovante = comp.BuscaDadosClienteComprovantePorCobranca(cobrancaId);
            return (dtDadosComprovante);
        }

        public DataTable CarregaProdutosComprovante() {

            RelatorioProdutosComprovante produtos = new RelatorioProdutosComprovante();
            DataTable dtProdutos = produtos.BuscaProdutosComprovante(cobrancaId);
            return (dtProdutos);
        }
    }
}
