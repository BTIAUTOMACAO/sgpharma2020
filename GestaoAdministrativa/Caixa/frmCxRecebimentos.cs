﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Vendas;
using GestaoAdministrativa.SAT;
using GestaoAdministrativa.Impressao;
using System.IO;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmCxRecebimentos : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private DataTable dtRecebimentos = new DataTable();
        private DataTable dtPagos = new DataTable();
        private DataTable dtClientes = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private decimal valorTotal;
        private decimal valorDiv;
        private decimal totalEsp;
        private DateTime data;
        private decimal restSaldo;
        private decimal total;
        private ToolStripButton tsbRecebimentos = new ToolStripButton("Receb. de Títulos");
        private ToolStripSeparator tssRecebimentos = new ToolStripSeparator();
        private string strOrdem;
        private bool decrescente;
        private int cliID;
        private string linha;
        private bool liberado;
        #endregion

        public frmCxRecebimentos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCxRecebimentos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtBNome.Focus();
                cmbClienteDebitos.SelectedIndex = 0;
                dgEspecie.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgEspecie.ForeColor = System.Drawing.Color.Black;
                dgEspecie.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgEspecie.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;

                dgRecebimentos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgRecebimentos.ForeColor = System.Drawing.Color.Black;
                dgRecebimentos.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgRecebimentos.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtBNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtBNome.Text.Trim()))
                {
                    btnBuscar.PerformClick();
                }
                else
                    txtBCpf.Focus();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Recebimentos recebimentos = new Recebimentos();
                int codigo = txtBCodigo.Text.Trim() == "" ? 0 : Convert.ToInt32(txtBCodigo.Text.Trim());

                dtClientes = recebimentos.BuscaClienteComCobranca(txtBNome.Text.Trim(), txtBCpf.Text.Trim(), codigo, Funcoes.RemoveCaracter(txtBTelefone.Text.Trim()), cmbClienteDebitos.Text, out strOrdem);

                if (dtClientes.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtClientes.Rows.Count;
                    dgClientes.DataSource = dtClientes;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, 0));
                    dgClientes.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Pesquisa de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgClientes.DataSource = dtClientes;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBNome.Focus();
                    tslRegistros.Text = "";
                    emGrade = false;
                    Funcoes.LimpaFormularios(this);
                    cmbClienteDebitos.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtBCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtBCpf.Text.Trim()))
                {
                    btnBuscar.PerformClick();
                }
                else
                    txtBCodigo.Focus();
            }
        }

        private void txtBCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtBCodigo.Text.Trim()))
                {
                    btnBuscar.PerformClick();
                }
                else
                    txtBTelefone.Focus();
            }
        }

        private void txtBTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }


        public void CarregarDados(int linha)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                lblCliente.Text = "Cliente: " + dtClientes.Rows[linha]["CF_NOME"].ToString();
                lblDevedor.Text = "Cliente: " + dtClientes.Rows[linha]["CF_NOME"].ToString();
                cliID = Convert.ToInt32(dtClientes.Rows[linha]["CF_ID"]);
                dtInicial.Value = DateTime.Now;
                dtFinal.Value = DateTime.Now;
                CobrancaParcela cobranca = new CobrancaParcela();
                dtRecebimentos = cobranca.BuscaCobrancaParcelada(Convert.ToInt32(dtClientes.Rows[linha]["CF_ID"]), 'A');
                if (dtRecebimentos.Rows.Count > 0)
                {
                    for (int i = 0; i < dtRecebimentos.Rows.Count; i++)
                    {
                        dtRecebimentos.Rows[i]["VALOR_ATUALIZADO"] = string.Format("{0:0.00}", RegrasCaixa.CalculaJuros(dtRecebimentos.Rows[i]["COBRANCA_PARCELA_VENCIMENTO"].ToString().Substring(0, 10), DateTime.Now.ToString("dd/MM/yyyy"), Convert.ToDouble(dtRecebimentos.Rows[i]["COBRANCA_PARCELA_SALDO"]))); ;
                        dtRecebimentos.Rows[i]["ACRESCIMO"] = string.Format("{0:0.00}", Convert.ToDouble(dtRecebimentos.Rows[i]["VALOR_ATUALIZADO"]) - Convert.ToDouble(dtRecebimentos.Rows[i]["COBRANCA_PARCELA_SALDO"]));
                    }
                    dgRecebimentos.DataSource = dtRecebimentos;
                    dgRecebimentos.Focus();
                    btnImprimir.Visible = true;
                    btnVisualizar.Visible = true;
                    gbValor.Visible = true;
                    btnMarcar.Visible = true;
                    btnDesmarcar.Visible = true;
                }
                else
                {
                    dgRecebimentos.DataSource = dtRecebimentos;
                    LimparPagar();
                }

                LimparPagos();

                decimal totalComJuros = 0, tatalSemJuros = 0;
                for (int i = 0; i < dgRecebimentos.RowCount; i++)
                {
                    totalComJuros = totalComJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value);
                    tatalSemJuros = tatalSemJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);
                }

                lblTotalJuros.Text = "Total com Juros: " + String.Format("{0:N}", totalComJuros);
                lblTotalSaldo.Text = "Total do Saldo: " + String.Format("{0:N}", tatalSemJuros);

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, linha));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcRecebimentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcRecebimentos.SelectedTab == tpGrade)
            {
                dgRecebimentos.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcRecebimentos.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);

                    if (Funcoes.LeParametro(4, "80", true).Equals("S"))
                    {
                        txtTCJuros.Enabled = true;
                        txtTSJuros.Enabled = true;
                    }
                    else
                    {
                        txtTCJuros.Enabled = false;
                        txtTSJuros.Enabled = false;
                    }
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, contRegistros));
                    emGrade = false;
                }
            }
        }

        private void frmCxRecebimentos_Shown(object sender, EventArgs e)
        {
            try
            {
                Especie especie = new Especie();

                //CARREGA ESPECIES//
                if(Funcoes.LeParametro(4,"81",false).Equals("S"))
                {
                    Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("ESP_CODIGO", "ESP_DESCRICAO", "CAD_ESPECIES", false, "ESP_DESABILITADO = 'N' AND ESP_CAIXA = 'S'");
                }
                else
                {
                    Principal.dtPesq = especie.BuscaDados('N');
                }
              
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma Espécie,\npara realizadar um recebimento de título.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Principal.mdiPrincipal.btnSair.PerformClick();
                }
                else
                {
                    cmbEspecie.DataSource = Principal.dtPesq;
                    cmbEspecie.DisplayMember = "ESP_DESCRICAO";
                    cmbEspecie.ValueMember = "ESP_CODIGO";

                    cmbEspecie.SelectedIndex = -1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbEspecie_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtTCJuros.Text.Trim() != "" || txtTSJuros.Text.Trim() != "")
                {
                    if (txtEValor.Text.Trim() != "")
                    {
                        if (Convert.ToDouble(txtEValor.Text) == 0)
                        {
                            if (rdbCJuros.Checked == true)
                            {
                                txtEValor.Text = String.Format("{0:N}", Convert.ToDecimal(txtTCJuros.Text) - totalEsp);
                            }
                            else
                                txtEValor.Text = String.Format("{0:N}", Convert.ToDecimal(txtTSJuros.Text) - totalEsp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbEspecie_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (txtEValor.Text.Trim() != "")
                    {
                        if (Convert.ToDouble(txtEValor.Text) == 0)
                        {
                            if (rdbCJuros.Checked == true)
                            {
                                txtEValor.Text = String.Format("{0:N}", Convert.ToDecimal(txtTCJuros.Text) - totalEsp);
                            }
                            else
                                txtEValor.Text = String.Format("{0:N}", Convert.ToDecimal(txtTSJuros.Text) - totalEsp);
                        }
                    }
                    txtEValor.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtEValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Funcoes.SomenteNumeros(e);
                if (e.KeyChar == 13)
                {
                    if (ConsisteCampos().Equals(true))
                    {
                        dgEspecie.Rows.Insert(dgEspecie.RowCount, new Object[] { cmbEspecie.SelectedValue, cmbEspecie.Text, txtEValor.Text });


                        decimal valorDivida = rdbCJuros.Checked == true ? Convert.ToDecimal(txtTCJuros.Text) : Convert.ToDecimal(txtTSJuros.Text);
                        decimal totalRecebido = 0;

                        for (int i = 0; i < dgEspecie.RowCount; i++)
                        {
                            totalRecebido = totalRecebido + Convert.ToDecimal(dgEspecie.Rows[i].Cells[2].Value);
                        }

                        lblTotal.Text = "Total: " + String.Format("{0:N}", valorDivida);

                        if ((totalRecebido - valorDivida) < 0)
                        {
                            lblTroco.Text = "Falta: " + String.Format("{0:N}", Math.Abs(valorDivida - totalRecebido));
                            txtEValor.Text = (valorDivida - totalRecebido).ToString();

                        }
                        else
                        {
                            lblTroco.Text = "Troco: " + String.Format("{0:N}", Math.Abs(totalRecebido - valorDivida));
                            txtEValor.Text = "0,00";

                        }
                        cmbEspecie.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        public bool ConsisteCampos()
        {
            if (txtEValor.Text.Trim() == "")
            {
                txtEValor.Text = "0,00";
            }
            else
                txtEValor.Text = String.Format("{0:N}", Convert.ToDecimal(txtEValor.Text));

            if (cmbEspecie.SelectedIndex == -1)
            {
                MessageBox.Show("Selecione a espécie.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbEspecie.Focus();
                return false;
            }
            if (Convert.ToDouble(txtEValor.Text) <= 0)
            {
                MessageBox.Show("O valor deve ser maior que zero.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtEValor.Focus();
                return false;
            }
            for (int i = 0; i < dgEspecie.RowCount; i++)
            {
                if (cmbEspecie.SelectedValue.Equals(dgEspecie.Rows[i].Cells[0].Value))
                {
                    MessageBox.Show("Já existe um valor lançado para esta espécie.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbEspecie.Focus();
                    return false;
                }
            }

            return true;
        }

        private void txtEValor_Validated(object sender, EventArgs e)
        {
            if (txtEValor.Text.Trim() != "")
            {
                txtEValor.Text = String.Format("{0:N}", txtEValor.Text);
            }
            else
                txtEValor.Text = "0,00";
        }

        private void dgEspecie_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            try
            {
                valorDiv = 0;
                for (int i = 0; i < dgEspecie.RowCount; i++)
                {
                    valorDiv = valorDiv + Convert.ToDecimal(dgEspecie.Rows[i].Cells[2].Value);
                }
                if (valorDiv == 0)
                {
                    lblTotal.Text = "";
                    lblTroco.Text = "";
                    totalEsp = 0;
                }
                else
                {
                    lblTotal.Text = "Total: " + Funcoes.LeParametro(0, "01", false) + " " + String.Format("{0:N}", valorDiv);
                    if (valorDiv > total)
                    {
                        lblTroco.Text = "Troco: " + Funcoes.LeParametro(0, "01", false) + " " + String.Format("{0:N}", Math.Abs(total - valorDiv));
                    }
                    else
                    {
                        lblTroco.Text = "";
                    }
                    totalEsp = valorDiv;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnPagar_Click(object sender, EventArgs e)
        {
            AFCaixa cx = new AFCaixa();
            DataTable statusCaixa = cx.BuscaCaixaStatus(Principal.usuario, 'S');

            if (statusCaixa.Rows.Count.Equals(0))
            {
                MessageBox.Show("Necessario efetuar a abertura do caixa ", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else if (!statusCaixa.Rows.Count.Equals(0) && !Convert.ToDateTime(statusCaixa.Rows[0]["DATA_ABERTURA"]).ToString("dd/MM/yyyy").Equals(DateTime.Now.ToString("dd/MM/yyyy")))
            {
                MessageBox.Show("Necessario efetuar a abertura do caixa ", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                if (dgRecebimentos.RowCount != 0)
                {
                    if (rdbCJuros.Checked == false && rdbSJuros.Checked == false)
                    {
                        MessageBox.Show("Selecione a forma de cobrança do pagamento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rdbCJuros.Focus();
                    }
                    else
                    {
                        gbEspecie.Visible = true;
                        cmbEspecie.SelectedIndex = -1;
                        txtEValor.Text = "0,00";
                        dgEspecie.Rows.Clear();
                        btnConfirmar.Visible = true;

                        cmbEspecie.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Necessário ter parcelas em aberto para efetuar o recebimento do título.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTSJuros.Text = "";
                    txtTCJuros.Text = "";
                    dgRecebimentos.Focus();
                }
            }
        }

        public bool ConsisteParcelas()
        {
            decimal valParcelas = 0;

            if (valParcelas < valorTotal)
            {
                for (int i = 1; i < dgRecebimentos.SelectedRows.Count; i++)
                {
                    if (Convert.ToDecimal(dgRecebimentos.Rows[i].Cells[8].Value) > valParcelas)
                    {
                        return false;
                    }
                }
            }
            return true;
        }


        private void txtTCJuros_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnPagar.PerformClick();
        }
        private void txtTSJuros_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                btnPagar.PerformClick();
        }

        public bool ConsisteIncluir()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";
            if (rdbSJuros.Checked == false && rdbCJuros.Checked == false)
            {
                Principal.mensagem = "Selecione a forma de cobrança do pagamento.";
                Funcoes.Avisa();
                rdbCJuros.Focus();
                return false;
            }
            if (total != 0)
            {
                var caixa = new CaixaStatus();
                List<CaixaStatus> cxStatus = caixa.DataCaixa(Principal.estAtual, "A", Principal.usuario);
                if (cxStatus.Count.Equals(0))
                {
                    Principal.mensagem = "Lançamento com movimentação de caixa que ainda não foi aberto!";
                    Funcoes.Avisa();
                    return false;
                }
                if (DateTime.Now.ToString("dd/MM/yyyy") != cxStatus[0].CxData.ToString("dd/MM/yyyy"))
                {
                    Principal.mensagem = "Lançamento com movimentação de caixa com data diferente do caixa aberto!";
                    Funcoes.Avisa();
                    return false;
                }
                if (rdbSJuros.Checked == true)
                {
                    total = Convert.ToDecimal(txtTSJuros.Text);
                }
                else
                {
                    total = Convert.ToDecimal(txtTCJuros.Text);
                }
            }
            return true;

        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma o Pagamento?", "Recebimento de Títulos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                if (ConsisteIncluir().Equals(true))
                {
                    bool desconto = false;
                    if(Funcoes.LeParametro(4,"80", false).Equals("S"))
                    {
                        if(MessageBox.Show("Pagamento de parcela(s) com Desconto?","Recebimentos", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                        {
                            desconto = true;
                        }
                    }

                    if (IncluirRecebimentos(desconto).Equals(true))
                    {
                        LimparPagar();
                        CarregarDados(contRegistros);
                    }
                    else
                        BancoDados.ErroTrans();
                }
            }
        }

        public bool ComprovantePagto(DateTime dtRecebimento)
        {
            try
            {
                Estabelecimento dadosEstabelecimento = new Estabelecimento();
                CobrancaMovimento cobMov = new CobrancaMovimento();
                Cliente cliete = new Cliente();
                Recebimentos rec = new Recebimentos();


                List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                DataTable dtParcelas = cobMov.BuscaMovimentoPorData(dtRecebimento);
                string dtCliente = cliete.BuscaClientePorDataVenda(dtRecebimento);
                DataTable dtReciboPorEspecie = rec.BuscaEspeciesVendaPorData(dtRecebimento);
                double juros = 0, total = 0, parcela = 0;

                if (retornoEstab.Count == 1)
                {
                    string comprovanteVenda;
                    string tamanho;

                    for (int i = 0; i < Convert.ToInt32(Funcoes.LeParametro(2, "26", false)); i++)
                    {
                        total = 0;
                        parcela = 0;
                        comprovanteVenda = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                        comprovanteVenda += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                        comprovanteVenda += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                        comprovanteVenda += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                        tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                        comprovanteVenda += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                        comprovanteVenda += retornoEstab[0].EstBairro + "\n";
                        comprovanteVenda += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                        comprovanteVenda += "__________________________________________" + "\n";
                        comprovanteVenda += "CLIENTE: " + dtCliente + "\n";
                        comprovanteVenda += "Data: " + Convert.ToDateTime(dtRecebimento).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(dtRecebimento).ToString("HH:mm:ss") + "\n";
                        comprovanteVenda += "__________________________________________" + "\n";
                        comprovanteVenda += Funcoes.CentralizaTexto("RECIBO DE PAGAMENTO:", 43) + "\n";
                        comprovanteVenda += "__________________________________________" + "\n";
                        comprovanteVenda += "PARCELA(S) PAGA(S): " + "\n";
                        comprovanteVenda += "TITULOS  VENCTO      PAGTO    VAL.PARC." + "\n";

                        for (int y = 0; y < dtParcelas.Rows.Count; ++y)
                        {
                            comprovanteVenda += dtParcelas.Rows[y]["COBRANCA_MOV_ID"].ToString().PadRight(9, ' ') + dtParcelas.Rows[y]["COBRANCA_PARCELA_VENCIMENTO"].ToString().PadRight(12, ' ')
                                             + string.Format("{0:N}", Convert.ToDouble(dtParcelas.Rows[y]["COBRANCA_MOV_VL_PAGO"])).PadRight(9, ' ')
                                             + string.Format("{0:N}", (Convert.ToDouble(dtParcelas.Rows[y]["COBRANCA_MOV_VALOR_PARCELA"]) + Convert.ToDouble(dtParcelas.Rows[y]["COBRANCA_MOV_VALOR_JUROS"]))) + "\n";

                            juros = juros + (Convert.ToDouble(dtParcelas.Rows[y]["COBRANCA_MOV_VL_ACRESCIMO"]));
                            total = total + (Convert.ToDouble(dtParcelas.Rows[y]["COBRANCA_MOV_VL_PAGO"]));
                            parcela = parcela + (Convert.ToDouble(dtParcelas.Rows[y]["COBRANCA_MOV_VALOR_PARCELA"]) + Convert.ToDouble(dtParcelas.Rows[y]["COBRANCA_MOV_VALOR_JUROS"]));

                        }

                        double totalEspecie = 0;
                        for (int w = 0; w < dtReciboPorEspecie.Rows.Count; ++w)
                        {
                            totalEspecie += Convert.ToDouble(dtReciboPorEspecie.Rows[w]["MOVIMENTO_CX_ESPECIE_VALOR"]);
                        }

                        comprovanteVenda += "\n" + "JUROS: " + string.Format("{0:N}", (juros == 0 ? juros : (totalEspecie - total) == 0 ? juros : totalEspecie - total)) + "\n";
                        comprovanteVenda += ("VALOR PARCELA(S): " + string.Format("{0:N}", parcela)).PadLeft(40, ' ') + "\n";
                        comprovanteVenda += ("TOTAL PAGO: " + string.Format("{0:N}", total)).PadLeft(40, ' ') + "\n";
                       
                        for (int z = 0; z < dtReciboPorEspecie.Rows.Count; ++z)
                        {
                            comprovanteVenda += (dtReciboPorEspecie.Rows[z]["ESP_DESCRICAO"] + " : " + dtReciboPorEspecie.Rows[z]["MOVIMENTO_CX_ESPECIE_VALOR"]).PadLeft(40, ' ') + "\n";
                        }
                        comprovanteVenda += "__________________________________________" + "\n";
                        comprovanteVenda += Funcoes.CentralizaTexto("OBRIGADO PELA PREFERENCIA!!!", 43) + "\n";
                        comprovanteVenda += "__________________________________________" + "\n";


                        if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                        {
                            if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                 && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                string s_cmdTX = "\n";
                                if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                }
                                else
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                }

                                iRetorno = BematechImpressora.IniciaPorta("USB");

                                iRetorno = BematechImpressora.FormataTX(comprovanteVenda, 3, 0, 0, 0, 0);

                                s_cmdTX = "\r\n";
                                iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                iRetorno = BematechImpressora.AcionaGuilhotina(0);

                                iRetorno = BematechImpressora.FechaPorta();
                            }
                            else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                    && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                                iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovanteVenda);

                                iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                                iRetorno = InterfaceEpsonNF.FechaPorta();
                            }
                            else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(2, "23", false, Principal.nomeEstacao).Equals("S"))
                            {
                                using (StreamWriter writer = new StreamWriter(Funcoes.LeParametro(2, "24", true) + @"\Recebimento.txt", true))
                                {
                                    writer.WriteLine(comprovanteVenda);
                                }
                            }
                            else
                            {
                                //IMPRIME COMPROVANTE
                                comprovanteVenda += "\n\n\n\n\n";
                                Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteVenda);
                                if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                {
                                    Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                                }
                            }
                        }
                        else
                        {
                            comprovanteVenda += "\n\n\n\n";
                            //IMPRIME COMPROVANTE
                            int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteVenda, 0);
                            if (iRetorno != 1)
                            {
                                MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Verifique o cadastro de Estabelecimento", "Impressão Comprovante", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public bool IncluirRecebimentos(bool desconto)
        {
            try
            {

                Cursor = Cursors.WaitCursor;
                restSaldo = total;

                int movimentoCaixaID = Funcoes.IdentificaVerificaID("MOVIMENTO_CAIXA", "MOVIMENTO_CAIXA_ID");
                int movimentoCaixaEspecieID = Funcoes.IdentificaVerificaID("MOVIMENTO_CAIXA_ESPECIE", "MOVIMENTO_CX_ESPECIE_ID");
                int cobrancaMovimento = Funcoes.IdentificaVerificaID("COBRANCA_MOVIMENTO", "COBRANCA_MOV_ID");

                BancoDados.AbrirTrans();
                DateTime dtRecebimento = DateTime.Now;

                if (IncluirMovimentoCaixa(dtRecebimento, movimentoCaixaID).Equals(true))
                {
                    if (IncluirMovimentoCaixaEspecie(dtRecebimento, movimentoCaixaEspecieID).Equals(true))
                    {
                        if (IncluirCobrancaMovimento(dtRecebimento, cobrancaMovimento, desconto).Equals(true))
                        {
                            if (AtualizarCobrancaParcela(dtRecebimento, desconto).Equals(true))
                            {
                                if (AtualizacCobranca(dtRecebimento).Equals(true))
                                {
                                    Funcoes.GravaLogInclusao("CX_DATA", data.ToString("dd/MM/yyyy HH:mm:ss"), Principal.usuario, "MOV_CAIXA", Convert.ToString(total), Principal.estAtual);

                                    BancoDados.FecharTrans();

                                    //IMPRIME COMPROVANTE DE PAGAMENTO//
                                    if (MessageBox.Show("Imprime o comprovante de pagamento?", "Recebimento de Títulos", MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        ComprovantePagto(dtRecebimento);
                                    }

                                    return true;
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("03 - Erro ao efetuar o recebimento ", "Recebimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("02 - Erro ao efetuar o recebimento ", "Recebimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("02 - Erro ao efetuar o recebimento ", "Recebimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    else
                    {
                        BancoDados.ErroTrans();
                        MessageBox.Show("01 - Erro ao efetuar o recebimento ", "Recebimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                else
                {
                    BancoDados.ErroTrans();
                    MessageBox.Show("00 - Erro ao efetuar o recebimento ", "Recebimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        public bool IncluirMovimentoCaixa(DateTime dtRecebimento, int movimentoCaixaID)
        {
            try
            {
                decimal totalRecebido = 0, valorEntrada = 0;
                for (int i = 0; i < dgEspecie.RowCount; i++)
                {
                    totalRecebido = totalRecebido + Convert.ToDecimal(dgEspecie.Rows[i].Cells[2].Value);
                }
                decimal valorDivida = rdbCJuros.Checked == true ? Convert.ToDecimal(txtTCJuros.Text) : Convert.ToDecimal(txtTSJuros.Text);


                if ((totalRecebido - valorDivida) < 0)
                {
                    valorEntrada = totalRecebido;
                }
                else
                {
                    valorEntrada = valorDivida;
                }

                MovimentoCaixa movCaixa = new MovimentoCaixa()
                {
                    EmpCodigo = Principal.empAtual,
                    EstCodigo = Principal.estAtual,
                    MovimentoCaixaID = movimentoCaixaID,
                    MovimentoCaixaData = dtRecebimento,
                    MovimentoCaixaOdcID = Convert.ToInt32(Funcoes.LeParametro(4, "13", false)),
                    MovimentoCaixaOdcClasse = "+",
                    MovimentoCaixaValor = Convert.ToDouble(valorEntrada),
                    MovimentoCaixaUsuario = Principal.usuario,
                    DtCadastro = DateTime.Now,
                    OpCadastro = Principal.usuario,
                    VendaID = 0
                };

                return movCaixa.InserirDados(movCaixa);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimentos de Titulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool IncluirMovimentoCaixaEspecie(DateTime dtRecebimento, int movimentoCaixaEspecie)
        {
            try
            {
                for (int i = 0; i < dgEspecie.RowCount; i++)
                {
                    if(dgEspecie.Rows[i].Cells["ESP_DESCR"].Value.Equals("DINHEIRO") && lblTroco.Text != "Troco: 0,00")
                    {
                        if (!lblTroco.Text.Substring(0, 1).Equals("F"))
                        {
                            dgEspecie.Rows[i].Cells["ESP_VALOR"].Value = rdbCJuros.Checked ? txtTCJuros.Text : txtTSJuros.Text;
                        }
                    }
                    MovimentoCaixaEspecie movCaixaEspecie = new MovimentoCaixaEspecie()
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        MovimentoCxEspecieID = i + movimentoCaixaEspecie,
                        MovimentoCxEspecieData = dtRecebimento,
                        MovimentoCxEspecieCodigo = Convert.ToInt32(dgEspecie.Rows[i].Cells["ESP_CODIGO"].Value),
                        MovimentoCxEspecieValor = Convert.ToDouble(dgEspecie.Rows[i].Cells["ESP_VALOR"].Value),
                        MovimentoCxEspecieUsuario = Principal.usuario,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    if (!movCaixaEspecie.InserirDados(movCaixaEspecie))
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool IncluirCobrancaMovimento(DateTime dtRecebimento, int cobrancaMovimento, bool desconto)
        {
            try
            {
                double totalRecebido = 0, valorPago = 0, valorJaBaixado = 0, valorDesconto = 0;
                double valorDivida = rdbCJuros.Checked == true ? Convert.ToDouble(txtTCJuros.Text) : Convert.ToDouble(txtTSJuros.Text);
                for (int i = 0; i < dgEspecie.RowCount; i++)
                {
                    totalRecebido = totalRecebido + Convert.ToDouble(dgEspecie.Rows[i].Cells["ESP_VALOR"].Value);
                }

                if (desconto)
                {
                    totalRecebido = valorDivida;
                }

                for (int i = 0; i < dgRecebimentos.RowCount; i++)
                {
                    if (dgRecebimentos.Rows[i].Selected == true)
                    {
                        if (totalRecebido > 0)
                        {
                            if (totalRecebido >= valorDivida)
                            {
                                valorDesconto = rdbCJuros.Checked == true ? Convert.ToDouble(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value) : Convert.ToDouble(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);
                                valorPago = rdbCJuros.Checked == true ? Convert.ToDouble(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value) : Convert.ToDouble(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);
                            }
                            else
                            {
                                valorJaBaixado =  (rdbCJuros.Checked == true ? Convert.ToDouble(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value) : Convert.ToDouble(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value));

                                if (valorJaBaixado <= totalRecebido)
                                {
                                    valorPago = Convert.ToDouble(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);
                                    valorDesconto = Convert.ToDouble(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);
                                }
                                else
                                {
                                    if (desconto)
                                    {
                                        valorDesconto = (rdbCJuros.Checked == true ? Convert.ToDouble(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value) : Convert.ToDouble(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value));
                                    }

                                    double diferenca = valorJaBaixado - totalRecebido;
                                    valorPago = (rdbCJuros.Checked == true ? Convert.ToDouble(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value) : Convert.ToDouble(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value)) -  diferenca;
                                }

                                totalRecebido = totalRecebido - valorPago;
                            }

                            CobrancaMovimento cobranca = new CobrancaMovimento()
                            {
                                EmpCodigo = Principal.empAtual,
                                EstCodigo = Principal.estAtual,
                                CobrancaMovID = i + cobrancaMovimento,
                                CobrancaParcelaID = Convert.ToInt32(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_ID"].Value),
                                CobrancaID = Convert.ToInt32(dgRecebimentos.Rows[i].Cells["COBRANCA_ID"].Value),
                                CobrancaMovDataBaixa = dtRecebimento,
                                CobrancaMovValorPago = desconto ? valorDesconto : valorPago,
                                CobrancaMovValorAcrecimo = rdbCJuros.Checked ? Convert.ToDouble(dgRecebimentos.Rows[i].Cells["ACRESCIMO"].Value) : 0,
                                DataCadastro = DateTime.Now,
                                OperadorCadastro = Principal.usuario,
                                CobrancaMovValorParcela = Convert.ToDouble(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value),
                                CobrancaMovValorJuros = rdbCJuros.Checked ? Convert.ToDouble(dgRecebimentos.Rows[i].Cells["ACRESCIMO"].Value) : 0
                            };
                            if (cobranca.InsereDados(cobranca).Equals(false))
                            {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool AtualizarCobrancaParcela(DateTime dtRecebimento, bool desconto)
        {
            try
            {
                double totalRecebido = 0, valorPago = 0, valorJaBaixado = 0, valorParcela = 0;
                double valorDivida = rdbCJuros.Checked == true ? Convert.ToDouble(txtTCJuros.Text) : Convert.ToDouble(txtTSJuros.Text);

                for (int i = 0; i < dgEspecie.RowCount; i++)
                {
                    totalRecebido = totalRecebido + Convert.ToDouble(dgEspecie.Rows[i].Cells["ESP_VALOR"].Value);
                }

                if(desconto)
                {
                    totalRecebido = valorDivida;
                }

                for (int i = 0; i < dgRecebimentos.RowCount; ++i)
                {
                    if(totalRecebido > 0)
                    {
                        if (dgRecebimentos.Rows[i].Selected == true)
                        {
                            if (totalRecebido >= valorDivida)
                            {
                                valorPago = 0;
                            }
                            else
                            {
                                valorJaBaixado = (rdbCJuros.Checked == true ? Convert.ToDouble(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value) : Convert.ToDouble(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value));

                                if (valorJaBaixado <= totalRecebido)
                                {
                                    valorPago = 0;
                                    valorParcela = valorJaBaixado;
                                }
                                else
                                {
                                    valorPago = valorJaBaixado - totalRecebido;
                                    valorParcela = valorJaBaixado - valorPago;
                                }
                            }

                            CobrancaParcela cobParcela = new CobrancaParcela()
                            {
                                EmpCodigo = Principal.empAtual,
                                EstCodigo = Principal.estAtual,
                                CobrancaParcelaID = Convert.ToInt32(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_ID"].Value),
                                CobrancaID = Convert.ToInt32(dgRecebimentos.Rows[i].Cells["COBRANCA_ID"].Value),
                                CobrancaParcelaSaldo = valorPago == 0 ? valorPago : desconto == true ? 0 : valorPago,
                                CobrancaParcelaStatus = valorPago == 0 ? "F" : desconto == true ? "F" : "A",
                                DtRecebimento = dtRecebimento,
                                DtAlteracao = DateTime.Now,
                                OpAlteracao = Principal.usuario,
                            };

                            if (cobParcela.AtualizaParcela(cobParcela).Equals(false))
                            {
                                return false;
                            }

                            totalRecebido = totalRecebido - valorParcela;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool AtualizacCobranca(DateTime dtRecebimento)
        {
            Cobranca cobranca = new Cobranca();
            for (int i = 0; i < dgRecebimentos.RowCount; ++i)
            {
                if (dgRecebimentos.Rows[i].Selected == true)
                {
                    if (cobranca.AtualizaCobranca(Convert.ToInt32(dgRecebimentos.Rows[i].Cells["COBRANCA_ID"].Value)).Equals(false))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public void LimparGrade()
        {
            #region LIMPA OS CAMPOS
            dtClientes.Clear();
            dgClientes.DataSource = dtClientes;
            dtRecebimentos.Clear();
            dgRecebimentos.DataSource = dtRecebimentos;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            valorTotal = 0;
            valorDiv = 0;
            totalEsp = 0;
            restSaldo = 0;
            total = 0;
            tslRegistros.Text = "";
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            emGrade = false;
            btnImprimir.Visible = false;
            btnVisualizar.Visible = false;
            gbValor.Visible = false;
            btnMarcar.Visible = false;
            btnDesmarcar.Visible = false;
            txtBNome.Focus();
            Cursor = Cursors.Default;
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            lblCliente.Text = "";
            btnCImprimir.Enabled = false;
            btnConfirmar.Visible = false;
            btnCBuscar.Enabled = false;
            btnCancelar.Enabled = false;
            liberado = false;
            #endregion
        }

        public void LimparPagar()
        {
            #region LIMPA OS CAMPOS
            dtRecebimentos.Clear();
            dgRecebimentos.DataSource = dtRecebimentos;
            cmbEspecie.SelectedIndex = -1;
            txtEValor.Text = "0,00";
            dgEspecie.Rows.Clear();
            lblTotal.Text = "";
            lblTroco.Text = "";
            txtTCJuros.Text = "";
            txtTSJuros.Text = "";
            rdbCJuros.Checked = false;
            rdbSJuros.Checked = false;
            gbEspecie.Visible = false;
            btnConfirmar.Visible = false;
            valorTotal = 0;
            valorDiv = 0;
            totalEsp = 0;
            restSaldo = 0;
            total = 0;
            btnImprimir.Visible = false;
            btnVisualizar.Visible = false;
            btnMarcar.Visible = false;
            btnDesmarcar.Visible = false;
            gbValor.Visible = false;
            liberado = false;
            lblTotalJuros.Text = "";
            lblTotalSaldo.Text = "";
            if (Funcoes.LeParametro(4, "80", true).Equals("S"))
            {
                txtTCJuros.Enabled = true;
                txtTSJuros.Enabled = true;
            }
            else
            {
                txtTCJuros.Enabled = false;
                txtTSJuros.Enabled = false;
            }
            Cursor = Cursors.Default;
            #endregion
        }

        public void LimparPagos()
        {
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            dtPagos.Clear();
            dgPagos.DataSource = dtPagos;
            liberado = false;
            if (dgClientes.RowCount <= 0)
            {
                btnCBuscar.Enabled = false;
                btnCancelar.Enabled = false;
            }
            else
            {
                btnCBuscar.Enabled = true;
                btnCancelar.Enabled = true;
            }
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcRecebimentos.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
            {
                if (tcPagto.SelectedTab == tpPagar)
                {
                    LimparPagar();
                }
                else if (tcPagto.SelectedTab == tpPagos)
                {
                    LimparPagos();
                }
            }

            Util.BotoesGenericos();
        }

        public void Primeiro()
        {
            if (dtClientes.Rows.Count != 0)
            {
                contRegistros = 0;
                dgClientes.CurrentCell = dgClientes.Rows[contRegistros].Cells[0];
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcRecebimentos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            contRegistros = dtClientes.Rows.Count - 1;
            dgClientes.CurrentCell = dgClientes.Rows[contRegistros].Cells[0];
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            contRegistros = contRegistros + 1;
            dgClientes.CurrentCell = dgClientes.Rows[contRegistros].Cells[0];
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            contRegistros = contRegistros - 1;
            dgClientes.CurrentCell = dgClientes.Rows[contRegistros].Cells[0];
            CarregarDados(contRegistros);
        }

        public void Botao()
        {
            try
            {
                this.tsbRecebimentos.AutoSize = false;
                this.tsbRecebimentos.Image = Properties.Resources.caixa;
                this.tsbRecebimentos.Size = new System.Drawing.Size(130, 20);
                this.tsbRecebimentos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRecebimentos);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRecebimentos);
                tsbRecebimentos.Click += delegate
                {
                    var recTitulos = Application.OpenForms.OfType<frmCxRecebimentos>().FirstOrDefault();
                    Util.BotoesGenericos();
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtRecebimentos, contRegistros));
                    recTitulos.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, contRegistros));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtClientes.Clear();
                dtRecebimentos.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRecebimentos);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRecebimentos);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtTCJuros") && (control.Name != "txtTSJuros"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }


        public bool Incluir() { return true; }

        public bool Atualiza() { return true; }

        public bool Excluir() { return true; }

        public void ImprimirRelatorio() { }

        private void dgClientes_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, contRegistros));
            emGrade = true;
        }

        private void dgClientes_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcRecebimentos.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgClientes_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtClientes.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtClientes.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtClientes.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void btnCImprimir_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtInicial.Value > dtFinal.Value)
                {
                    MessageBox.Show("A data final deve ser maior que a data inicial", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtInicial.Focus();
                }
                else
                {
                    Cursor = Cursors.WaitCursor;
                    //IMPRIME COMPROVANTE POR PERÍODO//
                    DateTime dtInicia = Convert.ToDateTime(dtInicial.Text + " 00:00:00");
                    DateTime dtFim = Convert.ToDateTime(dtFinal.Text + " 23:59:59");
                    Recebimentos recebimentos = new Recebimentos();
                    DataTable dtDataVendas = recebimentos.BuscaDataComprovantePorPerido(dtInicia, dtFim, 'F', cliID);

                    if (dtDataVendas.Rows.Count != 0)
                    {
                        for (int i = 0; i < dtDataVendas.Rows.Count; i++)
                        {
                            ComprovantePagto(Convert.ToDateTime(dtDataVendas.Rows[i]["DT_RECEBIMENTO"]));
                        }
                    }
                    else
                    {
                        MessageBox.Show(" Não há pagamentos neste período", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtInicial.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void tcPagto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcPagto.SelectedTab == tpPagos)
            {
                if (emGrade == true)
                {
                    btnCImprimir.Enabled = true;
                }
                else
                {
                    btnCImprimir.Enabled = false;
                }

                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                dtInicial.Value = DateTime.Now;
                dtFinal.Value = DateTime.Now;

                if (dgClientes.RowCount <= 0)
                {
                    btnCBuscar.Enabled = false;
                    btnCancelar.Enabled = false;
                }
                else
                {
                    btnCBuscar.Enabled = true;
                    btnCancelar.Enabled = true;
                }
            }
            else if (tcPagto.SelectedTab == tpPagar)
            {
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;

                if(Funcoes.LeParametro(4,"80",true).Equals("S"))
                {
                    txtTCJuros.Enabled = true;
                    txtTSJuros.Enabled = true;
                }
                else
                {
                    txtTCJuros.Enabled = false;
                    txtTSJuros.Enabled = false;
                }
            }
        }

        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                List<Object> cobrancaId = new List<object>();
                for (int i = 0; i < dgRecebimentos.RowCount; ++i)
                {
                    if (dgRecebimentos.Rows[i].Selected == true)
                    {
                        cobrancaId.Add(Convert.ToInt32(dgRecebimentos.Rows[i].Cells["COBRANCA_ID"].Value));
                    }
                }
                if (cobrancaId.Count > 0)
                {
                    frmCxRecebimentosProdutos pro = new frmCxRecebimentosProdutos(cobrancaId.Distinct().ToList());
                    pro.ShowDialog();
                }
                else
                {
                    MessageBox.Show("Necessário selecionar alguma parcela, para visualizar os produtos!", "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgRecebimentos.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F10:
                    btnConfirmar.PerformClick();
                    break;
                case Keys.F8:
                    btnVisualizar.PerformClick();
                    break;
                case Keys.F9:
                    btnImprimir.PerformClick();
                    break;
                case Keys.F3:
                    btnMarcar.PerformClick();
                    break;
                case Keys.F4:
                    btnDesmarcar.PerformClick();
                    break;
            }
        }

        public void BuscaProdutos()
        {

        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            List<Object> cobrancaId = new List<object>();
            for (int i = 0; i < dgRecebimentos.RowCount; ++i)
            {
                if (dgRecebimentos.Rows[i].Selected == true)
                {
                    cobrancaId.Add(Convert.ToInt32(dgRecebimentos.Rows[i].Cells["COBRANCA_ID"].Value));
                }
            }
            if (cobrancaId.Count > 0)
            {
                List<Object> cobrancaIdSemDuplicatas = cobrancaId.Distinct().ToList();
                for (int y = 0; y < cobrancaIdSemDuplicatas.Count; ++y)
                {
                    MontaComprovanteProduto(Convert.ToInt32(cobrancaIdSemDuplicatas[y]));
                }
            }
            else
            {
                MessageBox.Show("Necessário selecionar alguma parcela, para imprimir os produtos!", "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dgRecebimentos.Focus();
            }
        }

        public void MontaComprovanteProduto(int cobrancaId)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                Estabelecimento dadosEstabelecimento = new Estabelecimento();
                Comprovante comp = new Comprovante();

                List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                DataTable dtProdutos = comp.BuscaProdutosComprovantePorCobranca(cobrancaId);
                DataTable dtDadosComprovante = comp.BuscaDadosComprovantePorCobranca(cobrancaId);

                if (dtDadosComprovante.Rows.Count > 0)
                {
                    if (retornoEstab.Count == 1)
                    {
                        string comprovanteVenda, tamanho;
                        for (int y = 0; y < Convert.ToInt32(Funcoes.LeParametro(2, "26", false)); y++)
                        {
                            comprovanteVenda = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                            comprovanteVenda += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                            comprovanteVenda += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                            comprovanteVenda += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                            tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                            comprovanteVenda += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                            comprovanteVenda += retornoEstab[0].EstBairro + "\n";
                            comprovanteVenda += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                            comprovanteVenda += "__________________________________________" + "\n";
                            comprovanteVenda += "CLIENTE: " + dtDadosComprovante.Rows[0]["CF_NOME"] + "\n";
                            comprovanteVenda += "DATA VENDA:" + Convert.ToDateTime(dtDadosComprovante.Rows[0]["DT_VENDA"]).ToString("dd/MM/yyyy") + " HORA VENDA:" + Convert.ToDateTime(dtDadosComprovante.Rows[0]["HORA_VENDA"]).ToString("HH:mm:ss") + "\n";
                            comprovanteVenda += "__________________________________________" + "\n";
                            comprovanteVenda += Funcoes.CentralizaTexto("#|COD| QT| UN| VL UN R$| ST| AQ|VL ITEM R$", 43) + "\n";
                            comprovanteVenda += "__________________________________________" + "\n";
                            for (int z = 0; z < dtProdutos.Rows.Count; ++z)
                            {
                                comprovanteVenda += dtProdutos.Rows[z]["PROD_CODIGO"].ToString().PadRight(15, ' ') + dtProdutos.Rows[z]["PROD_DESCR"].ToString() + "\n";
                                if (Convert.ToDecimal(dtProdutos.Rows[z]["VALOR_DESCONTO"]) != 0)
                                {
                                    comprovanteVenda += "R$ " + dtProdutos.Rows[z]["VENDA_ITEM_UNITARIO"].ToString().PadRight(10, ' ') + "R$ " + dtProdutos.Rows[z]["VALOR_DESCONTO"].ToString() + "(R$ DESC) \n";
                                }
                                comprovanteVenda += ("R$ " + dtProdutos.Rows[z]["VENDA_VALOR_ITEM"]).PadRight(15, ' ') + "x".PadRight(7, ' ') + dtProdutos.Rows[z]["VENDA_ITEM_QTDE"].ToString().PadRight(7, ' ') + " = ".PadRight(5, ' ') + "R$ " + dtProdutos.Rows[z]["VENDA_ITEM_TOTAL"].ToString() + "\n \n";
                            }

                            comprovanteVenda += ("TOTAL: R$ " + dtDadosComprovante.Rows[0]["COBRANCA_TOTAL"].ToString()).PadLeft(42, ' ') + "\n";

                            comprovanteVenda += "__________________________________________" + "\n";
                            comprovanteVenda += Funcoes.CentralizaTexto("OBRIGADO PELA PREFERENCIA!!!", 43) + "\n";
                            comprovanteVenda += "__________________________________________" + "\n";

                            if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                            {
                                if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                     && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                {
                                    int iRetorno;
                                    string s_cmdTX = "\n";
                                    if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                    {
                                        iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                    }
                                    else
                                    {
                                        iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                    }

                                    iRetorno = BematechImpressora.IniciaPorta("USB");

                                    iRetorno = BematechImpressora.FormataTX(comprovanteVenda, 3, 0, 0, 0, 0);

                                    s_cmdTX = "\r\n";
                                    iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                    iRetorno = BematechImpressora.AcionaGuilhotina(0);

                                    iRetorno = BematechImpressora.FechaPorta();
                                }
                                else if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("EPSON") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                            && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                                {
                                    int iRetorno;
                                    iRetorno = InterfaceEpsonNF.IniciaPorta("USB");

                                    iRetorno = InterfaceEpsonNF.ImprimeTexto(comprovanteVenda);

                                    iRetorno = InterfaceEpsonNF.AcionaGuilhotina(0);

                                    iRetorno = InterfaceEpsonNF.FechaPorta();

                                }
                                else
                                {
                                    //IMPRIME COMPROVANTE
                                    comprovanteVenda += "\n\n\n\n\n";
                                    Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteVenda);
                                    if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                    {
                                        Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                                    }
                                }
                            }
                            else
                            {
                                comprovanteVenda += "\n\n\n\n";
                                //IMPRIME COMPROVANTE
                                int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteVenda, 0);
                                if (iRetorno != 1)
                                {
                                    MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }
                        }
                    }
                    else
                        MessageBox.Show("Verifique o cadastro de Estabelecimento", "Impressão Comprovante", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    MessageBox.Show("Cobrança realizada pela Tela de Lançamento de Títulos", "Impressão Comprovante", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnMarcar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (dgRecebimentos.RowCount != 0)
                {
                    decimal totalComJuros = 0, tatalSemJuros = 0;

                    for (int i = 0; i < dgRecebimentos.RowCount; i++)
                    {
                        dgRecebimentos.Rows[i].Selected = true;

                        totalComJuros = totalComJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value);
                        tatalSemJuros = tatalSemJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);
                        dgRecebimentos.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
                    }

                    txtTCJuros.Text = String.Format("{0:N}", totalComJuros);
                    txtTSJuros.Text = String.Format("{0:N}", tatalSemJuros);

                    dgRecebimentos.Refresh();
                    rdbCJuros.Focus();
                }
                else
                {
                    MessageBox.Show("Necessário ter registros para marca-los", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgRecebimentos.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnDesmarcar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                for (int i = 0; i < dgRecebimentos.RowCount; i++)
                {
                    dgRecebimentos.Rows[i].Selected = false;
                    dgRecebimentos.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                }
                txtTCJuros.Text = String.Format("{0:N}", 0);
                txtTSJuros.Text = String.Format("{0:N}", 0);
                dgRecebimentos.Refresh();
                dgRecebimentos.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        #region atalhos
        public void AtalhoGrade()
        {
            tcRecebimentos.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcRecebimentos.SelectedTab = tpFicha;
        }



        private void btnConfirmar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnVisualizar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnImprimir_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnMarcar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnDesmarcar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dgEspecie_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dgRecebimentos_KeyDown(object sender, KeyEventArgs e)
        {
            try
            {
                decimal totalComJuros = 0, tatalSemJuros = 0;
                for (int i = 0; i < dgRecebimentos.RowCount; i++)
                {
                    if (dgRecebimentos.Rows[i].Selected == true)
                    {
                        totalComJuros = totalComJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value);
                        tatalSemJuros = tatalSemJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);

                    }
                }

                txtTCJuros.Text = String.Format("{0:N}", totalComJuros);
                txtTSJuros.Text = String.Format("{0:N}", tatalSemJuros);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            teclaAtalho(e);
        }

        private void txtEValor_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtTCJuros_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtTSJuros_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbCJuros_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void rdbSJuros_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        #endregion

        private void btnCBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Recebimentos recebe = new Recebimentos();
                DateTime dtIncial = Convert.ToDateTime(dtFiltroInicial.Text + " 00:00:01");
                DateTime dtFinal = Convert.ToDateTime(dtFiltroFinal.Text + " 23:59:59");


                dtPagos = recebe.BuscaRecebimentosPorPeriodo(dtIncial, dtFinal, cliID);
                if (dtPagos.Rows.Count == 0)
                {
                    dgPagos.DataSource = dtPagos;
                    MessageBox.Show("Nenhum registro encontrado!", "Títulos Pagos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    btnCBuscar.Focus();
                }
                else
                {
                    dgPagos.DataSource = dtPagos;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtClientes, 0));
                    emGrade = false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbEspecie_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dgPagos_BackgroundColorChanged(object sender, EventArgs e)
        {
            for (int i = 0; i < dgPagos.RowCount; i++)
            {
                if (Convert.ToString(dgPagos.Rows[i].Cells[0].Value).Equals("True"))
                {
                    dgPagos.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
                }
            }
        }

        private void dgPagos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                decimal pagosTotal = 0;

                if (dgPagos.RowCount > 0)
                {
                    for (int i = 0; i < dgPagos.RowCount; i++)
                    {
                        if (Convert.ToString(dgPagos.Rows[i].Cells[0].Value).Equals("True"))
                        {
                            dgPagos.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;

                            pagosTotal = pagosTotal + (dgPagos.Rows[i].Cells[7].Value.ToString() == "" ? 0 : Convert.ToDecimal(dgPagos.Rows[i].Cells[7].Value));
                        }
                        else if (Convert.ToString(dgPagos.Rows[i].Cells[0].Value).Equals("False"))
                        {
                            dgPagos.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool CancelarRecebimento()
        {
            try
            {
                BancoDados.AbrirTrans();

                MovimentoCaixa movCaixa = new MovimentoCaixa();
                MovimentoCaixaEspecie movCaixaEspecie = new MovimentoCaixaEspecie();
                CobrancaParcela cobParcela = new CobrancaParcela();
                CobrancaMovimento cobrancaMovi = new CobrancaMovimento();
                Cobranca cobranca = new Cobranca();

                for (int i = 0; i < dgPagos.RowCount; i++)
                {
                    if (Convert.ToString(dgPagos.Rows[i].Cells["ESCOLHECANCELAR"].Value).Equals("true"))
                    {
                        DateTime dtRecebimento = Convert.ToDateTime(dgPagos.Rows[i].Cells["DT_RECEBIMENTO"].Value);

                        if (movCaixa.ExcluirPorData(dtRecebimento).Equals(true))
                        {
                            if (movCaixaEspecie.ExcluirMovimentoCaixaEspeciePorDataHora(dtRecebimento).Equals(true))
                            {
                                if (cobParcela.AtualizaParcelaPorData(dtRecebimento, 'A').Equals(true))
                                {
                                    if (cobranca.AtualizaCobrancaPorPeriodo(dtRecebimento, 'A').Equals(true))
                                    {
                                        if (cobrancaMovi.ExcluirCobrancaMovimento(dtRecebimento).Equals(false))
                                        {
                                            BancoDados.ErroTrans();
                                            MessageBox.Show("04 - Erro ao efetuar o cancelamento ", "Recebimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            return false;
                                        }
                                    }
                                    else
                                    {
                                        BancoDados.ErroTrans();
                                        MessageBox.Show("03 - Erro ao efetuar o cancelamento ", "Recebimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        return false;
                                    }
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("02 - Erro ao efetuar o cancelamento ", "Recebimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("01 - Erro ao efetuar o cancelamento ", "Recebimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("00 - Erro ao efetuar o cancelamento ", "Recebimento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                }
                BancoDados.FecharTrans();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Principal.usuADM.Equals("S") || liberado.Equals(true))
                {
                    if (CancelarRecebimento().Equals(false))
                    {
                        MessageBox.Show("Erro ao reativar os recebimentos!", "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        MessageBox.Show("Recebimentos reativados com sucesso!", "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LimparPagos();
                        btnCBuscar.Focus();
                    }
                }
                else
                {
                    if (MessageBox.Show("Operação não permitida! Deseja solicitar liberação?", "Cancelamento de Recebimento", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                        MessageBoxDefaultButton.Button2) == System.Windows.Forms.DialogResult.Yes)
                    {
                        frmVenSupSenha senhaSup = new frmVenSupSenha(0);
                        senhaSup.ShowDialog();
                        if (senhaSup.sValida.Equals(true))
                        {
                            liberado = true;
                        }
                        else
                            liberado = false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }


        private void dgRecebimentos_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                decimal totalComJuros = 0, tatalSemJuros = 0;
                for (int i = 0; i < dgRecebimentos.RowCount; i++)
                {
                    if (dgRecebimentos.Rows[i].Selected == true)
                    {
                        totalComJuros = totalComJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value);
                        tatalSemJuros = tatalSemJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);

                    }
                }

                txtTCJuros.Text = String.Format("{0:N}", totalComJuros);
                txtTSJuros.Text = String.Format("{0:N}", tatalSemJuros);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgRecebimentos_MouseClick(object sender, MouseEventArgs e)
        {
            try
            {
                decimal totalComJuros = 0, tatalSemJuros = 0;
                for (int i = 0; i < dgRecebimentos.RowCount; i++)
                {
                    if (dgRecebimentos.Rows[i].Selected == true)
                    {
                        totalComJuros = totalComJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value);
                        tatalSemJuros = tatalSemJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);

                    }
                }

                txtTCJuros.Text = String.Format("{0:N}", totalComJuros);
                txtTSJuros.Text = String.Format("{0:N}", tatalSemJuros);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdbCJuros_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                decimal totalComJuros = 0, tatalSemJuros = 0;
                for (int i = 0; i < dgRecebimentos.RowCount; i++)
                {
                    if (dgRecebimentos.Rows[i].Selected == true)
                    {
                        totalComJuros = totalComJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value);
                        tatalSemJuros = tatalSemJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);

                    }
                }

                txtTCJuros.Text = String.Format("{0:N}", totalComJuros);
                txtTSJuros.Text = String.Format("{0:N}", tatalSemJuros);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void rdbSJuros_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                decimal totalComJuros = 0, tatalSemJuros = 0;
                for (int i = 0; i < dgRecebimentos.RowCount; i++)
                {
                    if (dgRecebimentos.Rows[i].Selected == true)
                    {
                        totalComJuros = totalComJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["VALOR_ATUALIZADO"].Value);
                        tatalSemJuros = tatalSemJuros + Convert.ToDecimal(dgRecebimentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);

                    }
                }

                txtTCJuros.Text = String.Format("{0:N}", totalComJuros);
                txtTSJuros.Text = String.Format("{0:N}", tatalSemJuros);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgRecebimentos_KeyDown_1(object sender, KeyEventArgs e)
        {

        }

        private void btnVisualizarProdutosPagos_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime dtInicia = Convert.ToDateTime(dtInicial.Text + " 00:00:00");
                DateTime dtFim = Convert.ToDateTime(dtFinal.Text + " 23:59:59");
                Recebimentos recebimentos = new Recebimentos();
                DataTable dtDataVendas = recebimentos.BuscaVendaIDComprovantePorPerido(dtInicia, dtFim, 'F', cliID);
                List<Object> cobrancaId = new List<object>();
                if (dtDataVendas.Rows.Count != 0)
                {
                    for (int i = 0; i < dtDataVendas.Rows.Count; i++)
                    {
                        cobrancaId.Add(Convert.ToInt32(dtDataVendas.Rows[i]["COBRANCA_ID"]));
                    }

                    
                    frmCxRecebimentosProdutos pro = new frmCxRecebimentosProdutos(cobrancaId.Distinct().ToList());
                    pro.ShowDialog();
                    
                }
                else
                {
                    MessageBox.Show(" Não há pagamentos neste período", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtInicial.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void gbEspecie_Enter(object sender, EventArgs e)
        {

        }
    }
}
