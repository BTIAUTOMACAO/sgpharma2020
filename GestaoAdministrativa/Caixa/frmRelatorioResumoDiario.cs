﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmRelatorioResumoDiario : Form
    {
        DateTime dtInicial, dtFinal;
        string caixa;
        public frmRelatorioResumoDiario(DateTime inicial, DateTime final, string usuario)
        {
            this.WindowState = FormWindowState.Maximized;
            dtInicial = Convert.ToDateTime(inicial.ToString("dd/MM/yyyy") + " 00:00:00");
            dtFinal = Convert.ToDateTime(final.ToString("dd/MM/yyyy") + " 23:59:59");
            caixa = usuario;
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            InitializeComponent();
        }

        private void frmRelatorioResumoDiario_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaCorpoRelatorio();
            this.rpwResumoDiario.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[4];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("caixa", caixa.ToUpper());
            parametro[2] = new ReportParameter("dtInicial", dtInicial.ToString("dd/MM/yyyy"));
            parametro[3] = new ReportParameter("dtFinal", dtFinal.ToString("dd/MM/yyyy"));
            this.rpwResumoDiario.LocalReport.SetParameters(parametro);
        }

        public void MontaCorpoRelatorio()
        {

            MovimentoCaixaEspecie movCaixaEspecie = new MovimentoCaixaEspecie();
            DataTable dtMovimentoPorEspecie = movCaixaEspecie.CalculaMovimentoPorEspecie(dtInicial, dtFinal, caixa);
            Cobranca cobranca = new Cobranca();
            DataTable dtDadosCobranca = cobranca.BuscaDadosCobranca(dtInicial, dtFinal, caixa);
            MovimentoCaixa movCaixa = new MovimentoCaixa();
            DataTable dtMovimentoPorOperacao = movCaixa.BuscaMovimentoPorOperacao(dtInicial, dtFinal, caixa, Convert.ToInt32(Funcoes.LeParametro(6,"75", false)));

            var movPorEspecie = new ReportDataSource("EntradasSaidas", dtMovimentoPorEspecie);
            var dadosCobranca = new ReportDataSource("DadosCobranca", dtDadosCobranca);
            var movPorOperacao = new ReportDataSource("Operacao", dtMovimentoPorOperacao);

            rpwResumoDiario.LocalReport.DataSources.Clear();
            rpwResumoDiario.LocalReport.DataSources.Add(movPorEspecie);
            rpwResumoDiario.LocalReport.DataSources.Add(dadosCobranca);
            rpwResumoDiario.LocalReport.DataSources.Add(movPorOperacao);
        }
    }

}
