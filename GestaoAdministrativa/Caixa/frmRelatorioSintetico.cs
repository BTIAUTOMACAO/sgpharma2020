﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmRelatorioSintetico : Form
    {
        DateTime dtInicial, dtFinal;
        string caixa;

        public frmRelatorioSintetico(DateTime dtIni, string usuario)
        {
            dtInicial = dtIni;
            dtFinal = Convert.ToDateTime(dtIni.ToString("dd/MM/yyyy") + " 23:59:59");
            caixa = usuario;
            InitializeComponent();
        }

        private void frmRelatorioSintetico_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorioSintetico();
            this.rpwSintetico.RefreshReport();
        }

        public void MontaRelatorioSintetico()
        {
            RelatorioFechaDiario fechaDiario = new RelatorioFechaDiario();
            Recebimentos recebe = new Recebimentos();
            MovimentoCaixa mov = new MovimentoCaixa();

            DataTable dtMovimentos = fechaDiario.MovimentoPorEspecie(dtInicial, dtFinal, caixa, true, true);
            DataTable dtEntradas = mov.SomaMovimentoPorOperacaoDetalhadoEntrada(Funcoes.LeParametro(4, "79", false), dtInicial, dtFinal, caixa);
            DataTable dtMovimentoFormaPagamento = fechaDiario.MovimentoPorEspecieBeneficio(dtInicial, dtFinal, caixa);
            DataTable dtRecebimentoParticular = recebe.SomaRecebimentosPorEspeciePeriodo(dtInicial, dtFinal, caixa, Convert.ToInt32(Funcoes.LeParametro(4, "13", false)));
            List<Object> excecoes = new List<object>();
            excecoes.Add(Funcoes.LeParametro(6, "75", false));
            excecoes.Add(Funcoes.LeParametro(4, "13", false));
            DataTable dtMovimentosRetiradas = mov.SomaMovimentoPorOperacaoDetalhado(excecoes, dtInicial, dtFinal, caixa);

            var movimentoPorEspecie = new ReportDataSource("MovimentoEspecie", dtMovimentos);
            var movimentoEntrada = new ReportDataSource("movimentoEntrada", dtEntradas);
            var movimentoPorFormaPagamento = new ReportDataSource("Beneficio", dtMovimentoFormaPagamento);
            var recebimentoParticular = new ReportDataSource("RecebimentosParticulares", dtRecebimentoParticular);
            var movimentosRetiradas = new ReportDataSource("MovimentoCaixa", dtMovimentosRetiradas);

            this.rpwSintetico.LocalReport.DataSources.Clear();
            this.rpwSintetico.LocalReport.DataSources.Add(movimentoPorEspecie);
            this.rpwSintetico.LocalReport.DataSources.Add(movimentosRetiradas);
            this.rpwSintetico.LocalReport.DataSources.Add(movimentoPorFormaPagamento);
            this.rpwSintetico.LocalReport.DataSources.Add(recebimentoParticular);
            this.rpwSintetico.LocalReport.DataSources.Add(movimentoEntrada);
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("caixa", caixa.ToUpper());
            parametro[2] = new ReportParameter("dtFechamento", dtInicial.ToString("dd/MM/yyyy"));
            this.rpwSintetico.LocalReport.SetParameters(parametro);
        }
    }
}
