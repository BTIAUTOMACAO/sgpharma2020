﻿namespace GestaoAdministrativa.Caixa
{
    partial class frmRelatorioSintetico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.rpwSintetico = new Microsoft.Reporting.WinForms.ReportViewer();
            this.RelatorioProdutosComprovanteBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioProdutosComprovanteBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // rpwSintetico
            // 
            this.rpwSintetico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpwSintetico.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Caixa.RelatorioSinteticoMaster.rdlc";
            this.rpwSintetico.Location = new System.Drawing.Point(0, 0);
            this.rpwSintetico.Name = "rpwSintetico";
            this.rpwSintetico.Size = new System.Drawing.Size(994, 578);
            this.rpwSintetico.TabIndex = 0;
            // 
            // RelatorioProdutosComprovanteBindingSource
            // 
            this.RelatorioProdutosComprovanteBindingSource.DataSource = typeof(GestaoAdministrativa.Negocio.RelatorioProdutosComprovante);
            // 
            // frmRelatorioSintetico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rpwSintetico);
            this.Name = "frmRelatorioSintetico";
            this.Text = "Relatorio Sintetico";
            this.Load += new System.EventHandler(this.frmRelatorioSintetico_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RelatorioProdutosComprovanteBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpwSintetico;
        private System.Windows.Forms.BindingSource RelatorioProdutosComprovanteBindingSource;
    }
}