﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmRelatorioAnalitico : Form
    {
        DateTime dtInicial, dtFinal;
        string caixa;

        public frmRelatorioAnalitico(DateTime dtIni, string usuario)
        {
            this.WindowState = FormWindowState.Maximized;
            dtInicial = dtIni;
            dtFinal = Convert.ToDateTime(dtIni.ToString("dd/MM/yyyy") + " 23:59:59");
            caixa = usuario;
            InitializeComponent();
        }

        private void frmRelatorioAnalitico_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorioAnalitico();
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            this.rwpAnalitico.RefreshReport();
        }
        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[3];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("caixa", caixa.ToUpper());
            parametro[2] = new ReportParameter("dtFechamento", dtInicial.ToString("dd/MM/yyyy"));
            this.rwpAnalitico.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorioAnalitico()
        {
            RelatorioFechaDiario fechaDiario = new RelatorioFechaDiario();
            Recebimentos recebe = new Recebimentos();
            MovimentoCaixa cx = new MovimentoCaixa();

            DataTable dtVendas = fechaDiario.VendasPorPeriodo(dtInicial, dtFinal, caixa);
            DataTable dtMovimentos = fechaDiario.MovimentoPorEspecie(dtInicial, dtFinal, caixa);
            DataTable dtParticulares = fechaDiario.RecebimentosPorPeriodo(dtInicial, dtFinal, caixa);
            DataTable dtRecebimentoParticular = recebe.SomaRecebimentosPorEspeciePeriodo(dtInicial, dtFinal, caixa, Convert.ToInt32(Funcoes.LeParametro(4, "13", false)));
            List<Object> excecoes = new List<object>();
            excecoes.Add(Funcoes.LeParametro(6, "75", false));
            excecoes.Add(Funcoes.LeParametro(4, "13", false));
            DataTable dtCaixa = cx.SomaMovimentoPorOperacaoDetalhado(excecoes, dtInicial, dtFinal, caixa);
            DataTable dtEntradas = cx.SomaMovimentoPorOperacaoDetalhadoEntrada(Funcoes.LeParametro(4, "79", false), dtInicial, dtFinal, caixa);

            // DataTable dtCaixa = cx.SomaMovimentoPorClassePorEspecie(dtInicial, dtFinal, caixa, "-");

            var vendasPorPeriodo = new ReportDataSource("VendasPorPeriodo", dtVendas);
            var movimentoPorEspecie = new ReportDataSource("MovimentoEspecie", dtMovimentos);
            var recebimentosPorPeriodo = new ReportDataSource("RecebimentoPorPeriodo", dtParticulares);
            var recebimentosPorEspeciePeriodo = new ReportDataSource("RecebimentosPorEspeciePeriodo", dtRecebimentoParticular);
            var movimentoPorClassePorEspecie = new ReportDataSource("MovimentoPorClassePorEspecie", dtCaixa);
            var movimentoEntrada = new ReportDataSource("movimentoEntrada", dtEntradas);

            rwpAnalitico.LocalReport.DataSources.Clear();
            rwpAnalitico.LocalReport.DataSources.Add(vendasPorPeriodo);
            rwpAnalitico.LocalReport.DataSources.Add(movimentoPorEspecie);
            rwpAnalitico.LocalReport.DataSources.Add(recebimentosPorPeriodo);
            rwpAnalitico.LocalReport.DataSources.Add(recebimentosPorEspeciePeriodo);
            rwpAnalitico.LocalReport.DataSources.Add(movimentoPorClassePorEspecie);
            rwpAnalitico.LocalReport.DataSources.Add(movimentoEntrada);
        }
    }
}
