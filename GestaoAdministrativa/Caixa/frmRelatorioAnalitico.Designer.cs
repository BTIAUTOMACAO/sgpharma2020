﻿namespace GestaoAdministrativa.Caixa
{
    partial class frmRelatorioAnalitico
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rwpAnalitico = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rwpAnalitico
            // 
            this.rwpAnalitico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rwpAnalitico.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Caixa.RelatorioAnaliticoMaster.rdlc";
            this.rwpAnalitico.Location = new System.Drawing.Point(0, 0);
            this.rwpAnalitico.Name = "rwpAnalitico";
            this.rwpAnalitico.Size = new System.Drawing.Size(994, 578);
            this.rwpAnalitico.TabIndex = 0;
            // 
            // frmRelatorioAnalitico
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rwpAnalitico);
            this.Name = "frmRelatorioAnalitico";
            this.Text = "Relatorio Analitico";
            this.Load += new System.EventHandler(this.frmRelatorioAnalitico_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rwpAnalitico;
    }
}