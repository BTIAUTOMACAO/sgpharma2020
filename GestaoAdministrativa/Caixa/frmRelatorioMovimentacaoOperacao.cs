﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmRelatorioMovimentacaoOperacao : Form
    {
        DateTime dtInicial, dtFinal;
        string caixa;

        public frmRelatorioMovimentacaoOperacao(DateTime inicial, DateTime final, string usuario)
        {
            dtInicial = Convert.ToDateTime(inicial.ToString("dd/MM/yyyy") + " 00:00:00");
            dtFinal = Convert.ToDateTime(final.ToString("dd/MM/yyyy") + " 23:59:59");
            caixa = usuario;
            InitializeComponent();
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            this.WindowState = FormWindowState.Maximized;
            rpwMovimentoOperacao.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;

        }

        void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            CarregaSubRelatorio(e);
        }

        private void frmRelatorioMovimentacaoOperacao_Load(object sender, EventArgs e)
        {
            MontaDadosEstabelecimento();
            MontaRelatorio();
            this.rpwMovimentoOperacao.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[4];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("caixa", caixa.ToUpper());
            parametro[2] = new ReportParameter("dtInicial", dtInicial.ToString("dd/MM/yyyy"));
            parametro[3] = new ReportParameter("dtFinal", dtFinal.ToString("dd/MM/yyyy"));
            this.rpwMovimentoOperacao.LocalReport.SetParameters(parametro);
        }

        public void MontaRelatorio()
        {
            MovimentoCaixa movCaixa = new MovimentoCaixa();
            DataTable dtMovCaixa = movCaixa.BuscaMovimentosPorOperacacao(dtInicial, dtFinal, caixa);

            var movimentosPorOperacacao = new ReportDataSource("Datas", dtMovCaixa);

            rpwMovimentoOperacao.LocalReport.DataSources.Clear();
            rpwMovimentoOperacao.LocalReport.DataSources.Add(movimentosPorOperacacao);
        }

        public void CarregaSubRelatorio(SubreportProcessingEventArgs e)
        {

            MovimentoCaixa movCaixa = new MovimentoCaixa();
            DataTable dtMovCaixa = movCaixa.BuscaMovimentosPorOperacaoEspecie(dtInicial, dtFinal, caixa);

            var movimentosPorOperacacaoEspecie = new ReportDataSource("OperacaoPorEspecie", dtMovCaixa);
            e.DataSources.Add(movimentosPorOperacacaoEspecie);
        }

    }
}
