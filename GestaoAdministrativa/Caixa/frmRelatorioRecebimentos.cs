﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmRelatorioRecebimentos : Form
    {
        DateTime dtInicial, dtFinal;
        string caixa;

        public frmRelatorioRecebimentos(DateTime dtIni, DateTime dtFim, string usuario)
        {
            dtInicial = Convert.ToDateTime(dtIni.ToString("dd/MM/yyyy") + " 00:00:00");
            dtFinal = Convert.ToDateTime(dtFim.ToString("dd/MM/yyyy") + " 23:59:59");
            caixa = usuario;
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            this.WindowState = FormWindowState.Maximized;
            InitializeComponent();
        }

        private void frmRelatorioRecebimentos_Load(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Maximized;
            MontaDadosEstabelecimento();
            MontaCorpoRelatorio();
            this.rpwRecebimentos.RefreshReport();
        }

        public void MontaDadosEstabelecimento()
        {
            ReportParameter[] parametro = new ReportParameter[4];
            parametro[0] = new ReportParameter("estabelecimento", Principal.nomeAtual);
            parametro[1] = new ReportParameter("caixa", caixa.ToUpper());
            parametro[2] = new ReportParameter("dtInicial", dtInicial.ToString("dd/MM/yyyy"));
            parametro[3] = new ReportParameter("dtFinal", dtFinal.ToString("dd/MM/yyyy"));
            this.rpwRecebimentos.LocalReport.SetParameters(parametro);
        }
        public void MontaCorpoRelatorio()
        {
            CobrancaMovimento cobrancaMov = new CobrancaMovimento();
            DataTable dtMovPorIntervaloDatas = cobrancaMov.BuscaMovimentoPorIntervaloDatas(dtInicial, dtFinal, caixa);

            var MovPorIntervaloDatas = new ReportDataSource("Recebimentos", dtMovPorIntervaloDatas);
            rpwRecebimentos.LocalReport.DataSources.Clear();
            rpwRecebimentos.LocalReport.DataSources.Add(MovPorIntervaloDatas);
        }
    }
}
