﻿namespace GestaoAdministrativa.Caixa
{
    partial class frmRelatorioMovimentacaoOperacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource1 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource2 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource3 = new Microsoft.Reporting.WinForms.ReportDataSource();
            Microsoft.Reporting.WinForms.ReportDataSource reportDataSource4 = new Microsoft.Reporting.WinForms.ReportDataSource();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmRelatorioMovimentacaoOperacao));
            this.MovimentoEspecieBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.DsRelatorioSintetico = new GestaoAdministrativa.Caixa.DsRelatorioSintetico();
            this.MovimentoVendaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.RecebimentosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.MovimentoCaixaBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.rpwMovimentoOperacao = new Microsoft.Reporting.WinForms.ReportViewer();
            ((System.ComponentModel.ISupportInitialize)(this.MovimentoEspecieBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DsRelatorioSintetico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MovimentoVendaBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecebimentosBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MovimentoCaixaBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // MovimentoEspecieBindingSource
            // 
            this.MovimentoEspecieBindingSource.DataMember = "MovimentoEspecie";
            this.MovimentoEspecieBindingSource.DataSource = this.DsRelatorioSintetico;
            // 
            // DsRelatorioSintetico
            // 
            this.DsRelatorioSintetico.DataSetName = "DsRelatorioSintetico";
            this.DsRelatorioSintetico.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // MovimentoVendaBindingSource
            // 
            this.MovimentoVendaBindingSource.DataMember = "MovimentoVenda";
            this.MovimentoVendaBindingSource.DataSource = this.DsRelatorioSintetico;
            // 
            // RecebimentosBindingSource
            // 
            this.RecebimentosBindingSource.DataMember = "Recebimentos";
            this.RecebimentosBindingSource.DataSource = this.DsRelatorioSintetico;
            // 
            // MovimentoCaixaBindingSource
            // 
            this.MovimentoCaixaBindingSource.DataMember = "MovimentoCaixa";
            this.MovimentoCaixaBindingSource.DataSource = this.DsRelatorioSintetico;
            // 
            // rpwMovimentoOperacao
            // 
            this.rpwMovimentoOperacao.Dock = System.Windows.Forms.DockStyle.Fill;
            reportDataSource1.Name = "MovimentoEspecie";
            reportDataSource1.Value = this.MovimentoEspecieBindingSource;
            reportDataSource2.Name = "MovimentoVenda";
            reportDataSource2.Value = this.MovimentoVendaBindingSource;
            reportDataSource3.Name = "RecebimentosParticulares";
            reportDataSource3.Value = this.RecebimentosBindingSource;
            reportDataSource4.Name = "MovimentoCaixa";
            reportDataSource4.Value = this.MovimentoCaixaBindingSource;
            this.rpwMovimentoOperacao.LocalReport.DataSources.Add(reportDataSource1);
            this.rpwMovimentoOperacao.LocalReport.DataSources.Add(reportDataSource2);
            this.rpwMovimentoOperacao.LocalReport.DataSources.Add(reportDataSource3);
            this.rpwMovimentoOperacao.LocalReport.DataSources.Add(reportDataSource4);
            this.rpwMovimentoOperacao.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Caixa.Relatorios.RelatorioMovimentoPorOperacaoMaster.rdlc";
            this.rpwMovimentoOperacao.Location = new System.Drawing.Point(0, 0);
            this.rpwMovimentoOperacao.Name = "rpwMovimentoOperacao";
            this.rpwMovimentoOperacao.Size = new System.Drawing.Size(1160, 711);
            this.rpwMovimentoOperacao.TabIndex = 0;
            // 
            // frmRelatorioMovimentacaoOperacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 711);
            this.Controls.Add(this.rpwMovimentoOperacao);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmRelatorioMovimentacaoOperacao";
            this.Text = "Relatorio Movimentacao Por Operacao";
            this.Load += new System.EventHandler(this.frmRelatorioMovimentacaoOperacao_Load);
            ((System.ComponentModel.ISupportInitialize)(this.MovimentoEspecieBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DsRelatorioSintetico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MovimentoVendaBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RecebimentosBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MovimentoCaixaBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpwMovimentoOperacao;
        private System.Windows.Forms.BindingSource MovimentoEspecieBindingSource;
        private DsRelatorioSintetico DsRelatorioSintetico;
        private System.Windows.Forms.BindingSource MovimentoVendaBindingSource;
        private System.Windows.Forms.BindingSource RecebimentosBindingSource;
        private System.Windows.Forms.BindingSource MovimentoCaixaBindingSource;
    }
}