﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmAFCancelaCaixa : Form, Botoes
    {
        private ToolStripButton tsbAFCancelaCaixa = new ToolStripButton("Can. Abre/Fecha do Caixa");
        private ToolStripSeparator tssAFCancelaCaixa = new ToolStripSeparator();

        AFCaixa caixa = new AFCaixa();
        DataTable dtCaixa = new DataTable();

        public frmAFCancelaCaixa(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        private void frmAFCancelaCaixa_Load(object sender, EventArgs e)
        {
            try
            {
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                PreencheCombo();
                lblRetorno.Text = "";
               
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex, "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            try
            {
                dtCaixa = caixa.BuscaCaixaStatus(cmbUsuario.Text, ' ');

                if (dtCaixa.Rows.Count > 0)
                {
                    DateTime dtInicial = Convert.ToDateTime(dtCaixa.Rows[0]["DATA_ABERTURA"]);
                    DateTime dtFinal = String.IsNullOrEmpty(dtCaixa.Rows[0]["DATA_FECHAMENTO"].ToString()) ? Convert.ToDateTime(dtInicial.Day + "/" + dtInicial.Month + "/" + dtInicial.Year + " 23:59:59") : Convert.ToDateTime(dtCaixa.Rows[0]["DATA_FECHAMENTO"]);

                    Cursor = Cursors.WaitCursor;
                    if (dtCaixa.Rows[0]["CAIXA_ABERTO"].Equals("S"))
                    {
                        if (VerificaMovimentacao(cmbUsuario.Text, dtInicial, dtFinal).Equals(true))
                        {
                            if (caixa.CancelaAbertura(cmbUsuario.Text, Convert.ToDateTime(dtCaixa.Rows[0]["DATA_ABERTURA"])).Equals(true))
                            {
                                ExcluiRelatorio(Convert.ToDateTime(dtCaixa.Rows[0]["DATA_ABERTURA"]), Util.SelecionaCampoEspecificoDaTabela("USUARIOS", "ID", "LOGIN_ID", cmbUsuario.Text, false, true));

                                MessageBox.Show("Cancelamento da Abertura do Caixa Realizado com Sucesso", "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK);
                                Sair();
                                this.Close();
                            }
                            else
                            {
                                MessageBox.Show("Erro: ao efetuar o cancelamento", "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show("Usuario " + cmbUsuario.Text.ToUpper() + " possue movimentações de caixa, a abertura de caixa não pode ser cancelada", "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        CancelaAbreturaFechamento(cmbUsuario.Text, Convert.ToDateTime(dtCaixa.Rows[0]["DATA_ABERTURA"]), 'S', Convert.ToChar(dtCaixa.Rows[0]["CAIXA_ABERTO"]));
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex, "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void CancelaAbreturaFechamento(string usuario, DateTime dtData, char statusNovo, char statusAtual)
        {
            AFCaixa caixa = new AFCaixa();
            try
            {
                if (caixa.AtualizaStatusCaixa(usuario, statusNovo, statusAtual, dtData, true).Equals(true))
                {
                    ExcluiRelatorio(dtData, Util.SelecionaCampoEspecificoDaTabela("USUARIOS", "ID", "LOGIN_ID", Principal.usuario, false, true));

                    MessageBox.Show("Cancelamento do usuario " + usuario + " realizado com sucesso", "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK);
                    Sair();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex, "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        public bool VerificaMovimentacao(string usuario, DateTime dtInicial, DateTime dtFinal)
        {
            try
            {
                MovimentoCaixa movCaixa = new MovimentoCaixa();
                if ((movCaixa.VerficaMovimentoCaixa(usuario, dtInicial, dtFinal).Rows.Count).Equals(0))
                {
                    MovimentoCaixaEspecie movCxEspecie = new MovimentoCaixaEspecie();
                    if ((movCxEspecie.VerficaMovimentoCaixaEspecie(usuario, dtInicial, dtFinal).Rows.Count).Equals(0))
                    {
                        MovimentoFinanceiro movFinan = new MovimentoFinanceiro();
                        if ((movFinan.VerficaMovimentoFinanceiro(usuario, dtInicial, dtFinal).Rows.Count).Equals(0))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex, "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public void PreencheCombo()
        {
            try
            {
                Usuario usuario = new Usuario();
                DataTable dtUsuarios = usuario.BuscaTodosUsuario("S");
                cmbUsuario.DataSource = dtUsuarios;
                cmbUsuario.DisplayMember = "LOGIN_ID";
                cmbUsuario.ValueMember = "LOGIN_ID";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex, "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #region ExcluiRelatorio

        public async void ExcluiRelatorio(DateTime dtFechamento, string usuario)
        {
            RelatorioCaixa relatorio = new RelatorioCaixa();
            bool retorno;

            #region Exclui Relatório Caixa Sintetico Espécie

            retorno = await relatorio.ExcluirCaixaEspecieSintetico(usuario, dtFechamento);
            if (retorno.Equals(false))
            {
                MessageBox.Show("Erro: ao importar os dados para nuvem ", "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            #endregion

            #region Exclui Relatório Caixa Sintetico Forma de Pagamento

            retorno = await relatorio.ExcluirCaixaSinteticoFormaPagamento(usuario, dtFechamento);
            if (retorno.Equals(false))
            {
                MessageBox.Show("Erro: ao importar os dados para nuvem ", "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            #endregion

            #region Exclui Relatório Recebimentos Particular

            retorno = await relatorio.ExcluirCaixaRecebimentosParticular(usuario, dtFechamento);
            if (retorno.Equals(false))
            {
                MessageBox.Show("Erro: ao importar os dados para nuvem ", "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion

            #region Excluir Relatório Movimento Caixa

            retorno = await relatorio.ExcluirMovimentoCaixa(usuario, dtFechamento);
            if (retorno.Equals(false))
            {
                MessageBox.Show("Erro: ao importar os dados para nuvem ", "Cancela Abertura/Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion
        }


        #endregion

        #region BOTOES DE NAVEGAÇAO
        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            throw new NotImplementedException();
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbAFCancelaCaixa);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssAFCancelaCaixa);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Abertura e Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void Botao()
        {
            try
            {
                this.tsbAFCancelaCaixa.AutoSize = false;
                this.tsbAFCancelaCaixa.Image = Properties.Resources.caixa;
                this.tsbAFCancelaCaixa.Size = new System.Drawing.Size(200, 20);
                this.tsbAFCancelaCaixa.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbAFCancelaCaixa);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssAFCancelaCaixa);
                tsbAFCancelaCaixa.Click += delegate
                {
                    var Caixa = Application.OpenForms.OfType<frmAFCancelaCaixa>().FirstOrDefault();
                    Util.BotoesGenericos();

                    Caixa.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Abertura e Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        internal void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Abertura e Fechamento de Caixa", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void Botoes.FormularioFoco()
        {
            throw new NotImplementedException();
        }
        #endregion

        private void cmbUsuario_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbUsuario.SelectedIndex != -1)
            {
                dtCaixa = caixa.BuscaCaixaStatus(cmbUsuario.Text, ' ');
                if (dtCaixa.Rows.Count > 0)
                {
                    if (dtCaixa.Rows[0]["CAIXA_ABERTO"].ToString().Equals("S"))
                    {
                        lblRetorno.Text = "Cancela Abertura do Caixa";
                    }
                    else if (dtCaixa.Rows[0]["CAIXA_ABERTO"].ToString().Equals("N"))
                    {
                        lblRetorno.Text = "Cancela Fechamento do Caixa";
                    }
                }
                else
                {
                    lblRetorno.Text = "";
                }
            }
        }
    }
}
