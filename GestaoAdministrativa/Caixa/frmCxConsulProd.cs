﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmCxConsulProd : Form
    {
        private string codReceber;
        private string strSql;

        public frmCxConsulProd(string recCodigo)
        {
            InitializeComponent();
            codReceber = recCodigo;
        }

        private void frmCxConsulProd_Load(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Consulta Produtos por Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmCxConsulProd_Shown(object sender, EventArgs e)
        {
            try
            {
                strSql = "SELECT A.EST_CODIGO, B.PED_NUMERO, B.PED_EMISSAO, B.PED_TOTAL, B.PED_SUBTOTAL, B.PED_DIFER, A.CLI_ID, G.CLI_NOME, C.PROD_CODIGO, F.COD_BARRA, E.PROD_DESCR,"
                     + " C.ITEM_QTDE, C.ITEM_VLUNIT, C.ITEM_DIFER, C.ITEM_TOTAL, D.COL_NOME FROM (((((RECEBER A "
                     + " LEFT JOIN PEDIDOS B ON B.EST_CODIGO = A.EST_CODIGO AND B.PED_NUMERO = A.PED_NUMERO)"
                     + " LEFT JOIN PEDIDOS_ITENS C ON C.EST_CODIGO = B.EST_CODIGO AND C.PED_NUMERO = B.PED_NUMERO)"
                     + " LEFT JOIN COLABORADORES D ON D.COL_CODIGO = C.COL_CODIGO)"
                     + " LEFT JOIN PRODUTOS E ON E.EST_CODIGO = C.EST_CODIGO AND E.PROD_CODIGO = C.PROD_CODIGO)"
                     + " LEFT JOIN PRODUTOS_BARRA F ON F.EST_CODIGO = E.EST_CODIGO AND F.PROD_CODIGO = E.PROD_CODIGO)"
                     + " LEFT JOIN CLIENTES G ON G.CLI_ID = A.CLI_ID)"
                     + " WHERE A.EST_CODIGO = " + Principal.estAtual + " AND A.REC_CODIGO = " + codReceber
                     + " AND F.DTCADASTRO = (SELECT MAX(DTCADASTRO) FROM PRODUTOS_BARRA WHERE EST_CODIGO = " + Principal.estAtual + " AND PROD_CODIGO = C.PROD_CODIGO)"
                     + " ORDER BY C.PROD_CODIGO";
                Principal.dtPesq = BancoDados.selecionarRegistros(strSql);
                if (Principal.dtPesq.Rows.Count != 0)
                {
                    txtEmissao.Text = Funcoes.ChecaCampoVazio(Principal.dtPesq.Rows[0]["PED_EMISSAO"].ToString().Substring(0, 10));
                    txtPedido.Text = Funcoes.ChecaCampoVazio(Principal.dtPesq.Rows[0]["PED_NUMERO"].ToString());
                    txtCliente.Text = Funcoes.ChecaCampoVazio(Principal.dtPesq.Rows[0]["CLI_NOME"].ToString());
                    dgProdutos.DataSource = Principal.dtPesq;
                    txtSTotal.Text = String.Format("{0:N}", Principal.dtPesq.Rows[0]["PED_SUBTOTAL"].ToString());
                    txtAjuste.Text = String.Format("{0:N}", Principal.dtPesq.Rows[0]["PED_DIFER"].ToString());
                    txtTotal.Text = String.Format("{0:N}", Principal.dtPesq.Rows[0]["PED_TOTAL"].ToString());
                }
                dgProdutos.Focus();
                    
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Consulta Produtos por Títulos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

    }
}
