﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.SAT;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmCxRecebConv : Form, Botoes
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private DataTable dtRecebConv = new DataTable();
        private bool emGrade;
        private double saldoTotal;
        private DateTime data;
        private double valorDiv;
        private double totalEsp;
        private double total;
        private string strOrdem;
        private string retorno;
        private string[,] logAtualiza = new string[6, 3];
        private int contMatriz;
        private double restSaldo;
        private double valorParcela;
        private int conCodigo;
        private ToolStripButton tsbRecebimentos = new ToolStripButton("Receb. de Conveniadas");
        private ToolStripSeparator tssRecebimentos = new ToolStripSeparator();
        decimal totalRecebido = 0, valorEntrada = 0;
        #endregion

        public frmCxRecebConv(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmCxRecebConv_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                txtCodigo.Focus();
                dtInicial.Value = DateTime.Now;
                dtFinal.Value = DateTime.Now;
                data = DateTime.Now;

                dgEspecie.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgEspecie.ForeColor = System.Drawing.Color.Black;
                dgEspecie.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgEspecie.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;

                dgPagamentos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgPagamentos.ForeColor = System.Drawing.Color.Black;
                dgPagamentos.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgPagamentos.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmCxRecebConv_Shown(object sender, EventArgs e)
        {
            try
            {
                Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("ESP_CODIGO", "ESP_DESCRICAO", "CAD_ESPECIES", false, "ESP_DESABILITADO = 'N'");

                DataRow row1 = Principal.dtPesq.NewRow();
                Principal.dtPesq.Rows.InsertAt(row1, 0);
                cmbEspecie.DataSource = Principal.dtPesq;
                cmbEspecie.DisplayMember = "ESP_DESCRICAO";
                cmbEspecie.ValueMember = "ESP_CODIGO";
                cmbEspecie.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                if (dtInicial.Value > dtFinal.Value)
                {
                    MessageBox.Show("A data final deve ser maior que a data inicial.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtInicial.Focus();
                }
                else if (txtCodigo.Text.Trim() == "" || txtCodigo.Text == "0")
                {
                    MessageBox.Show("Empresa conveniada não pode ser em branco.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodigo.Focus();
                }
                else
                {

                    var busca = new Conveniada();
                    dtRecebConv = busca.BuscaParcelarPorConveniada(txtCodigo.Text, dtInicial.Text, dtFinal.Text, Principal.empAtual, Principal.estAtual);
                    if (dtRecebConv.Rows.Count != 0)
                    {
                        dgConveniadas.DataSource = dtRecebConv;
                        dgConveniadas.Focus();
                        emGrade = true;
                    }
                    else
                    {
                        MessageBox.Show("Nenhum registro encontrado.", "Pesquisa de Clientes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dgConveniadas.DataSource = dtRecebConv;
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        txtCodigo.Focus();
                        dtInicial.Value = DateTime.Now;
                        dtFinal.Value = DateTime.Now;
                        tslRegistros.Text = "";
                        emGrade = false;
                        Funcoes.LimpaFormularios(this);
                        txtVTotal.Text = "0,00";
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnMarcar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (dgConveniadas.RowCount != 0)
                {
                    for (int i = 0; i < dgConveniadas.RowCount; i++)
                    {
                        dgConveniadas.Rows[i].Cells[0].Value = "True";
                    }

                    saldoTotal = 0;
                    if (dgConveniadas.RowCount > 0)
                    {
                        for (int i = 0; i < dgConveniadas.RowCount; i++)
                        {
                            if (Convert.ToString(dgConveniadas.Rows[i].Cells[0].Value).Equals("True"))
                            {
                                saldoTotal = saldoTotal + (dgConveniadas.Rows[i].Cells[9].Value.ToString() == "" ? 0 : Convert.ToDouble(dgConveniadas.Rows[i].Cells[9].Value));
                                dgConveniadas.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
                            }
                        }
                        txtVTotal.Text = String.Format("{0:N}", saldoTotal);
                    }

                    dgConveniadas.Refresh();
                    btnVPagar.Focus();
                }
                else
                {
                    MessageBox.Show("Necessário ter registros para marca-los", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodigo.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnDesmarcar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (dgConveniadas.RowCount != 0)
                {
                    for (int i = 0; i < dgConveniadas.RowCount; i++)
                    {
                        dgConveniadas.Rows[i].Cells[0].Value = "False";
                        dgConveniadas.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                    }

                    saldoTotal = 0;
                    txtVTotal.Text = String.Format("{0:N}", saldoTotal);
                    dgConveniadas.Refresh();
                    txtCodigo.Focus();
                }
                else
                {
                    MessageBox.Show("Necessário ter registros para desmarca-los", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodigo.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgConveniadas_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                saldoTotal = 0;
                if (dgConveniadas.RowCount > 0)
                {
                    for (int i = 0; i < dgConveniadas.RowCount; i++)
                    {
                        if (Convert.ToString(dgConveniadas.Rows[i].Cells[0].Value).Equals("True"))
                        {
                            saldoTotal = saldoTotal + (dgConveniadas.Rows[i].Cells[9].Value.ToString() == "" ? 0 : Convert.ToDouble(dgConveniadas.Rows[i].Cells[9].Value));
                            dgConveniadas.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.Silver;
                        }
                        else if (Convert.ToString(dgConveniadas.Rows[i].Cells[0].Value).Equals("False"))
                        {
                            dgConveniadas.Rows[i].DefaultCellStyle.BackColor = System.Drawing.Color.White;
                        }
                    }
                    txtVTotal.Text = String.Format("{0:N}", saldoTotal);
                    txtVTotal.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnVPagar_Click(object sender, EventArgs e)
        {
            bool seleciona = false;
            if (dgConveniadas.RowCount > 0)
            {
                for (int i = 0; i < dgConveniadas.RowCount; i++)
                {
                    if (Convert.ToString(dgConveniadas.Rows[i].Cells[0].Value).Equals("True"))
                    {
                        seleciona = true;
                    }
                }
                if (seleciona == false)
                {
                    MessageBox.Show("Necessário selecionar alguma parcela para efetuar o pagamento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgConveniadas.Focus();
                }
                else
                {
                    tcConveniadas.SelectedTab = tpFicha;
                    CarregarDados();
                }
            }
            else
            {
                MessageBox.Show("Necessário parcela(s) para pagamento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodigo.Focus();
            }
            
        }


        public void CarregarDados()
        {
            try
            {
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                dgPagamentos.Rows.Clear();
                dgEspecie.Rows.Clear();
                lblEstab.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                conCodigo = Convert.ToInt32(dtRecebConv.Rows[0]["CON_CODIGO"]);
                total = Convert.ToDouble(txtVTotal.Text);
                gbEspecie.Visible = true;
                btnConfirmar.Visible = true;
                cmbEspecie.SelectedIndex = -1;
                cmbEspecie.Focus();
                for (int i = 0; i < dgConveniadas.RowCount; i++)
                {
                    if (Convert.ToString(dgConveniadas.Rows[i].Cells[0].Value).Equals("True"))
                    {
                        dgPagamentos.Rows.Insert(dgPagamentos.RowCount, new Object[] {  dgConveniadas.Rows[i].Cells["dataGridViewTextBoxColumn7"].Value, dgConveniadas.Rows[i].Cells["dataGridViewTextBoxColumn2"].Value,
                            dgConveniadas.Rows[i].Cells["dataGridViewTextBoxColumn4"].Value, dgConveniadas.Rows[i].Cells["dataGridViewTextBoxColumn5"].Value,
                            dgConveniadas.Rows[i].Cells["dataGridViewTextBoxColumn8"].Value, dgConveniadas.Rows[i].Cells["COBRANCA_PARCELA_ID"].Value });
                    }
                }
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtVTotal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnVPagar.PerformClick();
        }

        private void cmbEspecie_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    if (txtEValor.Text.Trim() != "")
                    {
                        if (Convert.ToDouble(txtEValor.Text) == 0)
                        {
                            txtEValor.Text = String.Format("{0:N}", Convert.ToDouble(txtVTotal.Text) - totalEsp);
                        }
                    }
                    txtEValor.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbEspecie_Validated(object sender, EventArgs e)
        {
            try
            {
                if (txtVTotal.Text.Trim() != "")
                {
                    if (txtEValor.Text.Trim() != "")
                    {
                        if (Convert.ToDouble(txtEValor.Text) == 0)
                        {
                            txtEValor.Text = String.Format("{0:N}", Convert.ToDouble(txtVTotal.Text) - totalEsp);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtEValor_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                Funcoes.SomenteNumeros(e);
                if (e.KeyChar == 13)
                {
                    if (ConsisteCampos().Equals(true))
                    {
                        dgEspecie.Rows.Insert(dgEspecie.RowCount, new Object[] { cmbEspecie.SelectedValue, cmbEspecie.Text, txtEValor.Text });

                        valorDiv = 0;
                        totalEsp = 0;

                        for (int i = 0; i < dgEspecie.RowCount; i++)
                        {
                            valorDiv = valorDiv + Convert.ToDouble(dgEspecie.Rows[i].Cells[2].Value);
                            totalEsp = totalEsp + Convert.ToDouble(dgEspecie.Rows[i].Cells[2].Value);
                        }

                        lblTotal.Text = "Total: " + Funcoes.LeParametro(0, "01", false) + " " + String.Format("{0:N}", valorDiv);
                        if (valorDiv > total)
                        {
                            lblTroco.Text = "Troco: " + Funcoes.LeParametro(0, "01", false) + " " + String.Format("{0:N}", Math.Abs(total - valorDiv));
                        }
                        else
                        {
                            lblTroco.Text = "Troco: 0,00";
                        }
                        txtEValor.Text = "0,00";
                        cmbEspecie.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public bool ConsisteCampos()
        {
            if (txtEValor.Text.Trim() == "")
            {
                txtEValor.Text = "0,00";
            }
            else
                txtEValor.Text = String.Format("{0:N}", Convert.ToDecimal(txtEValor.Text));

            if (cmbEspecie.SelectedIndex == -1)
            {
                MessageBox.Show("Selecione a espécie.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmbEspecie.Focus();
                return false;
            }
            if (Convert.ToDouble(txtEValor.Text) <= 0)
            {
                MessageBox.Show("O valor deve ser maior que zero.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtEValor.Focus();
                return false;
            }
            for (int i = 0; i < dgEspecie.RowCount; i++)
            {
                if (cmbEspecie.SelectedValue.Equals(dgEspecie.Rows[i].Cells[0].Value))
                {
                    MessageBox.Show("Já existe um valor lançado para esta espécie.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbEspecie.Focus();
                    return false;
                }
            }

            return true;
        }

        private void txtEValor_Validated(object sender, EventArgs e)
        {
            if (txtEValor.Text.Trim() != "")
            {
                txtEValor.Text = String.Format("{0:N}", txtEValor.Text);
            }
            else
                txtEValor.Text = "0,00";
        }

        private void txtVPagto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnVPagar.PerformClick();
        }

        private void dgEspecie_RowsRemoved(object sender, DataGridViewRowsRemovedEventArgs e)
        {
            try
            {
                valorDiv = 0;
                for (int i = 0; i < dgEspecie.RowCount; i++)
                {
                    valorDiv = valorDiv + Convert.ToDouble(dgEspecie.Rows[i].Cells[2].Value);
                }
                if (valorDiv == 0)
                {
                    lblTotal.Text = "";
                    lblTroco.Text = "";
                    totalEsp = 0;
                }
                else
                {
                    lblTotal.Text = "Total: " + Funcoes.LeParametro(0, "01", false) + " " + String.Format("{0:N}", valorDiv);
                    if (valorDiv > total)
                    {
                        lblTroco.Text = "Troco: " + Funcoes.LeParametro(0, "01", false) + " " + String.Format("{0:N}", Math.Abs(total - valorDiv));
                    }
                    else
                    {
                        lblTroco.Text = "";
                    }
                    totalEsp = valorDiv;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void tcConveniadas_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcConveniadas.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                if (dgPagamentos.RowCount > 0)
                {
                    LimparFicha();
                }
            }
            else if (tcConveniadas.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                if (emGrade == true)
                {
                    bool seleciona = false;
                    saldoTotal = 0;
                    for (int i = 0; i < dgConveniadas.RowCount; i++)
                    {
                        if (Convert.ToString(dgConveniadas.Rows[i].Cells[0].Value).Equals("True"))
                        {
                            seleciona = true;
                            saldoTotal = saldoTotal + (dgConveniadas.Rows[i].Cells[9].Value.ToString() == "" ? 0 : Convert.ToDouble(dgConveniadas.Rows[i].Cells[9].Value));
                        }
                    }
                    txtVTotal.Text = String.Format("{0:N}", saldoTotal);
                    if (seleciona == false)
                    {
                        MessageBox.Show("Necessário selecionar alguma parcela para efetuar o pagamento.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        tcConveniadas.SelectedTab = tpGrade;
                        dgConveniadas.Focus();
                    }
                    else
                    {
                        tcConveniadas.SelectedTab = tpFicha;
                        CarregarDados();
                    }
                }
                else
                {
                    LimparFicha();
                }
            }
        }

        public void LimparGrade()
        {
            #region LIMPA OS CAMPOS
            dgPagamentos.Rows.Clear();
            dtRecebConv.Clear();
            dgConveniadas.DataSource = dtRecebConv;
            Funcoes.LimpaFormularios(this);
            saldoTotal = 0;
            valorDiv = 0;
            totalEsp = 0;
            restSaldo = 0;
            valorParcela = 0;
            total = 0;
            tslRegistros.Text = "";
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            emGrade = false;
            Array.Clear(logAtualiza, 0, 18);
            txtCodigo.Focus();
            Cursor = Cursors.Default;
            dtInicial.Value = DateTime.Now;
            dtFinal.Value = DateTime.Now;
            btnConfirmar.Visible = false;
            txtVTotal.Text = "0,00";
            #endregion
        }

        public void LimparFicha()
        {
            #region LIMPA OS CAMPOS
            dgPagamentos.Rows.Clear();
            cmbEspecie.SelectedIndex = -1;
            txtEValor.Text = "0,00";
            dgEspecie.Rows.Clear();
            lblTotal.Text = "";
            lblTroco.Text = "";
            Array.Clear(logAtualiza, 0, 18);
            gbEspecie.Visible = false;
            btnConfirmar.Visible = false;
            saldoTotal = 0;
            valorDiv = 0;
            totalEsp = 0;
            restSaldo = 0;
            valorParcela = 0;
            total = 0;
            Cursor = Cursors.Default;
            #endregion
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcConveniadas.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else if (tcConveniadas.SelectedTab == tpFicha)
            {
                LimparFicha();
            }

            Util.BotoesGenericos();
        }

        private void btnConfirmar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Confirma o Pagamento?", "Recebimento de Conveniadas", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
            {
                if (ConsisteIncluir().Equals(true))
                {
                    if (IncluirRecebimentos().Equals(true))
                    {
                        LimparFicha();
                        tcConveniadas.SelectedTab = tpGrade;
                        data = DateTime.Now;
                        txtVTotal.Text = "0,00";
                        btnBuscar.PerformClick();
                    }
                }
            }
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F10:
                    btnMarcar.PerformClick();
                    break;
                case Keys.F8:
                    btnDesmarcar.PerformClick();
                    break;
                case Keys.F1:
                    btnVPagar.PerformClick();
                    break;
                case Keys.F2:
                    btnConfirmar.PerformClick();
                    break;
            }
        }

        public bool ConsisteIncluir()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";
            if (total != 0)
            {
                var caixa = new AFCaixa();
                List<AFCaixa> cxStatus = caixa.DataCaixa(Principal.estAtual, Principal.empAtual, "S", Principal.usuario);
                if (cxStatus.Count.Equals(0))
                {
                    MessageBox.Show("Lançamento com movimentação de caixa com caixa que ainda não foi aberto!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                else if (DateTime.Now.ToString("dd/MM/yyyy") != cxStatus[0].DataAbertura.ToString("dd/MM/yyyy"))
                {
                    Principal.mensagem = "Lançamento com movimentação de caixa com data diferente do caixa aberto!";
                    Funcoes.Avisa();
                    return false;
                }

                total = Convert.ToDouble(txtVTotal.Text);

                valorDiv = 0;
                for (int i = 0; i < dgEspecie.RowCount; i++)
                {
                    valorDiv = valorDiv + Convert.ToDouble(dgEspecie.Rows[i].Cells[2].Value);
                }
                if (valorDiv < total)
                {
                    Principal.mensagem = "O valor da divisão por espécie não confere com o valor do documento.";
                    Funcoes.Avisa();
                    txtEValor.Focus();
                    return false;
                }
            }
            return true;

        }

        public bool IncluirRecebimentos()
        {
            try
            {

                Cursor = Cursors.WaitCursor;
                restSaldo = total;

                int movimentoCaixaID = Funcoes.IdentificaVerificaID("MOVIMENTO_CAIXA", "MOVIMENTO_CAIXA_ID");
                int movimentoCaixaEspecieID = Funcoes.IdentificaVerificaID("MOVIMENTO_CAIXA_ESPECIE", "MOVIMENTO_CX_ESPECIE_ID");
                int cobrancaMovimento = Funcoes.IdentificaVerificaID("COBRANCA_MOVIMENTO", "COBRANCA_MOV_ID");

                BancoDados.AbrirTrans();
                DateTime dtRecebimento = DateTime.Now;

                if (IncluirMovimentoCaixa(dtRecebimento, movimentoCaixaID).Equals(true))
                {
                    if (IncluirMovimentoCaixaEspecie(dtRecebimento, movimentoCaixaEspecieID).Equals(true))
                    {
                        if (IncluirCobrancaMovimento(dtRecebimento, cobrancaMovimento).Equals(true))
                        {
                            if (AtualizarCobrancaParcela(dtRecebimento).Equals(true))
                            {
                                if (AtualizacCobranca(dtRecebimento).Equals(true))
                                {
                                    Funcoes.GravaLogInclusao("CX_DATA", data.ToString("dd/MM/yyyy HH:mm:ss"), Principal.usuario, "MOV_CAIXA", Convert.ToString(total), Principal.estAtual);

                                    BancoDados.FecharTrans();

                                    //IMPRIME COMPROVANTE DE PAGAMENTO//
                                    if (MessageBox.Show("Imprime o comprovante de pagamento?", "Recebimento de Conveniadas", MessageBoxButtons.YesNo,
                                                MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == System.Windows.Forms.DialogResult.Yes)
                                    {
                                        ComprovantePagto(dtRecebimento);
                                    }

                                    return true;
                                }
                                else
                                {
                                    BancoDados.ErroTrans();
                                    MessageBox.Show("03 - Erro ao efetuar o recebimento ", "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return false;
                                }
                            }
                            else
                            {
                                BancoDados.ErroTrans();
                                MessageBox.Show("02 - Erro ao efetuar o recebimento ", "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return false;
                            }
                        }
                        else
                        {
                            BancoDados.ErroTrans();
                            MessageBox.Show("02 - Erro ao efetuar o recebimento ", "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return false;
                        }
                    }
                    else
                    {
                        BancoDados.ErroTrans();
                        MessageBox.Show("01 - Erro ao efetuar o recebimento ", "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                else
                {
                    BancoDados.ErroTrans();
                    MessageBox.Show("00 - Erro ao efetuar o recebimento ", "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool IncluirMovimentoCaixa(DateTime dtRecebimento, int movimentoCaixaID)
        {
            try
            {
                totalRecebido = 0;
                for (int i = 0; i < dgPagamentos.RowCount; i++)
                {
                    totalRecebido = totalRecebido + Convert.ToDecimal(dgPagamentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value);
                }
                
                valorEntrada = totalRecebido;
                
                MovimentoCaixa movCaixa = new MovimentoCaixa()
                {
                    EmpCodigo = Principal.empAtual,
                    EstCodigo = Principal.estAtual,
                    MovimentoCaixaID = movimentoCaixaID,
                    MovimentoCaixaData = dtRecebimento,
                    MovimentoCaixaOdcID = Convert.ToInt32(Funcoes.LeParametro(4, "13", false)),
                    MovimentoCaixaOdcClasse = "+",
                    MovimentoCaixaValor = Convert.ToDouble(valorEntrada),
                    MovimentoCaixaUsuario = Principal.usuario,
                    DtCadastro = DateTime.Now,
                    OpCadastro = Principal.usuario,
                    VendaID = 0
                };

                return movCaixa.InserirDados(movCaixa);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool IncluirMovimentoCaixaEspecie(DateTime dtRecebimento, int movimentoCaixaEspecie)
        {
            try
            {
                for (int i = 0; i < dgEspecie.RowCount; i++)
                {
                    if (dgEspecie.Rows[i].Cells["ESP_DESCR"].Value.Equals("DINHEIRO") && lblTroco.Text != "Troco: 0,00")
                    {
                        if (!lblTroco.Text.Substring(0, 1).Equals("F"))
                        {
                            dgEspecie.Rows[i].Cells["ESP_VALOR"].Value = totalRecebido;
                        }
                    }
                    MovimentoCaixaEspecie movCaixaEspecie = new MovimentoCaixaEspecie()
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        MovimentoCxEspecieID = i + movimentoCaixaEspecie,
                        MovimentoCxEspecieData = dtRecebimento,
                        MovimentoCxEspecieCodigo = Convert.ToInt32(dgEspecie.Rows[i].Cells["ESP_CODIGO"].Value),
                        MovimentoCxEspecieValor = Convert.ToDouble(dgEspecie.Rows[i].Cells["ESP_VALOR"].Value),
                        MovimentoCxEspecieUsuario = Principal.usuario,
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario
                    };

                    if (!movCaixaEspecie.InserirDados(movCaixaEspecie))
                        return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool IncluirCobrancaMovimento(DateTime dtRecebimento, int cobrancaMovimento)
        {
            try
            { 
                for (int i = 0; i < dgPagamentos.RowCount; i++)
                {
                    CobrancaMovimento cobranca = new CobrancaMovimento()
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        CobrancaMovID = i + cobrancaMovimento,
                        CobrancaParcelaID = Convert.ToInt32(dgPagamentos.Rows[i].Cells["Column1"].Value),
                        CobrancaID = Convert.ToInt32(dgPagamentos.Rows[i].Cells["dataGridViewTextBoxColumn16"].Value),
                        CobrancaMovDataBaixa = dtRecebimento,
                        CobrancaMovValorPago = Convert.ToDouble(dgPagamentos.Rows[i].Cells["COBRANCA_PARCELA_SALDO"].Value),
                        CobrancaMovValorAcrecimo = 0,
                        DataCadastro = DateTime.Now,
                        OperadorCadastro = Principal.usuario
                    };
                    if (cobranca.InsereDados(cobranca).Equals(false))
                    {
                        return false;
                    }

                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool AtualizarCobrancaParcela(DateTime dtRecebimento)
        {
            try
            {
                for (int i = 0; i < dgPagamentos.RowCount; ++i)
                {
                    CobrancaParcela cobParcela = new CobrancaParcela()
                    {
                        EmpCodigo = Principal.empAtual,
                        EstCodigo = Principal.estAtual,
                        CobrancaParcelaID = Convert.ToInt32(dgPagamentos.Rows[i].Cells["Column1"].Value),
                        CobrancaID = Convert.ToInt32(dgPagamentos.Rows[i].Cells["dataGridViewTextBoxColumn16"].Value),
                        CobrancaParcelaSaldo = 0,
                        CobrancaParcelaStatus = "F",
                        DtRecebimento = dtRecebimento,
                        DtAlteracao = DateTime.Now,
                        OpAlteracao = Principal.usuario,
                    };

                    if (cobParcela.AtualizaParcela(cobParcela).Equals(false))
                    {
                        return false;
                    }
                    
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool AtualizacCobranca(DateTime dtRecebimento)
        {
            Cobranca cobranca = new Cobranca();
            for (int i = 0; i < dgPagamentos.RowCount; ++i)
            {
                if (cobranca.AtualizaCobranca(Convert.ToInt32(dgPagamentos.Rows[i].Cells["dataGridViewTextBoxColumn16"].Value)).Equals(false))
                {
                    return false;
                }
                
            }
            return true;
        }

        public bool ComprovantePagto(DateTime dtRecebimento)
        {
            try
            {
                Estabelecimento dadosEstabelecimento = new Estabelecimento();
                CobrancaMovimento cobMov = new CobrancaMovimento();
                Cliente cliete = new Cliente();
                Recebimentos rec = new Recebimentos();


                List<Estabelecimento> retornoEstab = dadosEstabelecimento.DadosFiscaisEstabelecimento(Principal.estAtual, Principal.empAtual);
                DataTable dtParcelas = cobMov.BuscaMovimentoPorData(dtRecebimento);
                string dtCliente = cliete.BuscaClientePorDataVenda(dtRecebimento);
                DataTable dtReciboPorEspecie = rec.BuscaEspeciesVendaPorData(dtRecebimento);
                double juros = 0, total = 0;

                if (retornoEstab.Count == 1)
                {
                    string comprovanteVenda;
                    string tamanho;

                    for (int i = 0; i < Convert.ToInt32(Funcoes.LeParametro(2, "26", false)); i++)
                    {
                        total = 0;
                        comprovanteVenda = Funcoes.CentralizaTexto(retornoEstab[0].EstFantasia, 43) + "\n";
                        comprovanteVenda += Funcoes.CentralizaTexto(retornoEstab[0].EstFone, 43) + "\n";
                        comprovanteVenda += "CNPJ: " + retornoEstab[0].EstCNPJ + "\n";
                        comprovanteVenda += "IE: " + retornoEstab[0].EstInscricaoEstadual + "\n";
                        tamanho = retornoEstab[0].EstEndereco + ", N. " + retornoEstab[0].EstNum;
                        comprovanteVenda += tamanho.Length > 43 ? tamanho.Substring(0, 42) : tamanho + "\n";
                        comprovanteVenda += retornoEstab[0].EstBairro + "\n";
                        comprovanteVenda += retornoEstab[0].EstCidade + " - " + retornoEstab[0].EstUf + " CEP: " + Convert.ToDouble(retornoEstab[0].EstCep) / 100 + "\n";
                        comprovanteVenda += "__________________________________________" + "\n";
                        comprovanteVenda += "CONVENIADA: " + txtCodigo.Text + " - " + txtNome.Text + "\n";
                        comprovanteVenda += "Data: " + Convert.ToDateTime(dtRecebimento).ToString("dd/MM/yyyy") + "     Hora: " + Convert.ToDateTime(dtRecebimento).ToString("HH:mm:ss") + "\n";
                        comprovanteVenda += "__________________________________________" + "\n";
                        comprovanteVenda += Funcoes.CentralizaTexto("RECIBO DE PAGAMENTO:", 43) + "\n";
                        comprovanteVenda += "__________________________________________" + "\n";
                        comprovanteVenda += "PARCELA(S) PAGA(S): " + "\n";
                        comprovanteVenda += "TITULOS  VENCTO      PAGTO    PARCELA(S)" + "\n";

                        for (int y = 0; y < dtParcelas.Rows.Count; ++y)
                        {
                            comprovanteVenda += dtParcelas.Rows[y]["COBRANCA_MOV_ID"].ToString().PadRight(9, ' ') + dtParcelas.Rows[y]["COBRANCA_PARCELA_VENCIMENTO"].ToString().PadRight(12, ' ')
                                             + dtParcelas.Rows[y]["COBRANCA_MOV_VL_PAGO"].ToString().PadRight(9, ' ')
                                             + dtParcelas.Rows[y]["COBRANCA_PARCELA_NUM"].ToString() + "\n";

                            juros = juros + (Convert.ToDouble(dtParcelas.Rows[y]["COBRANCA_MOV_VL_ACRESCIMO"]));
                            total = total + (Convert.ToDouble(dtParcelas.Rows[y]["COBRANCA_MOV_VL_PAGO"]));

                        }
                        comprovanteVenda += "\n" + "JUROS: " + juros + "\n";
                        comprovanteVenda += ("TOTAL : " + string.Format("{0:0.00}", total.ToString())).PadLeft(40, ' ') + "\n";

                        for (int z = 0; z < dtReciboPorEspecie.Rows.Count; ++z)
                        {
                            comprovanteVenda += (dtReciboPorEspecie.Rows[z]["ESP_DESCRICAO"] + " : " + dtReciboPorEspecie.Rows[z]["MOVIMENTO_CX_ESPECIE_VALOR"]).PadLeft(40, ' ') + "\n";
                        }
                        comprovanteVenda += "__________________________________________" + "\n";
                        comprovanteVenda += Funcoes.CentralizaTexto("OBRIGADO PELA PREFERENCIA!!!", 43) + "\n";
                        comprovanteVenda += "__________________________________________" + "\n";


                        if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                        {
                            if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                                 && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                            {
                                int iRetorno;
                                string s_cmdTX = "\n";
                                if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                                }
                                else
                                {
                                    iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                                }

                                iRetorno = BematechImpressora.IniciaPorta("USB");

                                iRetorno = BematechImpressora.FormataTX(comprovanteVenda, 3, 0, 0, 0, 0);

                                s_cmdTX = "\r\n";
                                iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                                iRetorno = BematechImpressora.AcionaGuilhotina(0);

                                iRetorno = BematechImpressora.FechaPorta();
                            }
                            else
                            {
                                //IMPRIME COMPROVANTE
                                comprovanteVenda += "\n\n\n\n\n";
                                Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), comprovanteVenda);
                                if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                                {
                                    Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                                }
                            }
                        }
                        else
                        {
                            comprovanteVenda += "\n\n\n\n";
                            //IMPRIME COMPROVANTE
                            int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(comprovanteVenda, 0);
                            if (iRetorno != 1)
                            {
                                MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return false;
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Verifique o cadastro de Estabelecimento", "Impressão Comprovante", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private void dtInicial_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dtFinal.Focus();
        }

        private void dtFinal_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        public bool LeConveniada()
        {
            try
            {
                var dados = new Conveniada();

                dtRecebConv = dados.BuscaConveniadaParticular(txtCodigo.Text);
                if (Principal.dtPesq.Rows.Count != 0)
                {
                    txtNome.Text = Funcoes.ChecaCampoVazio(dtRecebConv.Rows[0]["CON_NOME"].ToString());
                    txtCodigo.Text = Funcoes.ChecaCampoVazio(dtRecebConv.Rows[0]["CON_CODIGO"].ToString());
                    dtInicial.Focus();
                    return true;
                }
                else
                {
                    txtCodigo.Text = "";
                    txtNome.Text = "";
                    txtCodigo.Focus();
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        

        private void txtNome_Validated(object sender, EventArgs e)
        {
            
        }

        public void Botao()
        {
            try
            {
                this.tsbRecebimentos.AutoSize = false;
                this.tsbRecebimentos.Image = Image.FromFile(Application.StartupPath + @"\Imagens\caixa.png");
                this.tsbRecebimentos.Size = new System.Drawing.Size(165, 20);
                this.tsbRecebimentos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRecebimentos);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRecebimentos);
                tsbRecebimentos.Click += delegate
                {
                    var recConveniadas = Application.OpenForms.OfType<frmCxRecebConv>().FirstOrDefault();
                    Util.BotoesGenericos();
                    recConveniadas.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Util.BotoesGenericos();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtRecebConv.Clear();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRecebimentos);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRecebimentos);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if (control.Name != "txtVTotal" && control.Name != "dtInicial" && control.Name != "dtFinal")
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        public void Ultimo() { }

        public void Primeiro() 
        {
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
        }

        public void Proximo() { }

        public void Anterior() { }

        public bool Incluir()
        { return true; }

        public bool Excluir()
        { return true; }

        public bool Atualiza()
        { return true; }

        public void ImprimirRelatorio() { }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (txtCodigo.Text.Trim() != "")
                {
                    if (LeConveniada().Equals(false))
                    {
                        MessageBox.Show("Código empresa conveniada não cadastrado.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodigo.Focus();
                        
                    }
                }
                else
                    txtNome.Focus();
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                try
                {
                    if (txtNome.Text.Trim() != "")
                    {
                        var dados = new Conveniada();
                        dados.ConNome = txtNome.Text;
                        dados.ConLiberado = "TODOS";
                        string retorno;
                        dtRecebConv = dados.BuscaDados(dados, out retorno);
                        if (dtRecebConv.Rows.Count == 1)
                        {
                            txtCodigo.Text = dtRecebConv.Rows[0]["CON_CODIGO"].ToString();
                            LeConveniada();
                        }
                        else
                            if (dtRecebConv.Rows.Count == 0)
                            {
                                MessageBox.Show("Não foi encontrado nenhuma empresa conveniada contendo essa descrição!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtNome.Text = "";
                                txtNome.Focus();
                            }
                            else
                            {
                                using (frmBuscaConv buscaConv = new frmBuscaConv())
                                {
                                    buscaConv.dgBuscaConv.DataSource = dtRecebConv;
                                    buscaConv.ShowDialog();
                                }
                                if (!String.IsNullOrEmpty(Principal.idCliFor))
                                {
                                    txtCodigo.Text = Principal.idCliFor;
                                    LeConveniada();
                                }
                                else
                                {
                                    txtNome.Text = "";
                                    txtNome.Focus();
                                }
                            }
                    }
                    else
                        dtInicial.Focus();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Erro: " + ex.Message, "Recebimento de Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        public void AtalhoGrade() { }

        public void AtalhoFicha() { }

        private void frmCxRecebConv_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnMarcar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dgConveniadas_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnDesmarcar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtVTotal_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnVPagar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dgPagamentos_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbEspecie_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtEValor_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dgEspecie_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnConfirmar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }

    }
}
