﻿using GestaoAdministrativa.Caixa.Relatorios;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Caixa
{
    public partial class frmMovimentoEspecie : Form, Botoes
    {
        private ToolStripButton tsbRelatorio = new ToolStripButton("Rel. Mov. Espécie");
        private ToolStripSeparator tssRelatorio = new ToolStripSeparator();

        public frmMovimentoEspecie(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;

        }

        public void Botao()
        {
            try
            {
                this.tsbRelatorio.AutoSize = false;
                this.tsbRelatorio.Image = Properties.Resources.relatorio;
                this.tsbRelatorio.Size = new System.Drawing.Size(125, 20);
                this.tsbRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssRelatorio);
                tsbRelatorio.Click += delegate
                {
                    var relatorio = Application.OpenForms.OfType<frmMovimentoEspecie>().FirstOrDefault();
                    Funcoes.BotoesCadastro(relatorio.Name);
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnLimpar.Enabled = false;
                    relatorio.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = false;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbRelatorio);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssRelatorio);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmMovimentoEspecie_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                lblEstabelecimento.Text = "Estabelecimento: " + Principal.estAtual + " - " + Principal.nomeAtual;
                dtInicial.Value = DateTime.Now;
                rdbAnalitico.Checked = true;
                BuscaUsuarios();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void BuscaUsuarios()
        {
            try
            {
                Usuario usuario = new Usuario();
                cmbUsuario.DataSource = usuario.BuscaTodosUsuario("S");
                cmbUsuario.DisplayMember = "LOGIN_ID";
                cmbUsuario.ValueMember = "LOGIN_ID";
                cmbUsuario.SelectedValue = Principal.usuario;
                dtInicial.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void btnVisualizar_Click(object sender, EventArgs e)
        {
            try
            {
                Principal.msgEstilo = MessageBoxButtons.OK;
                Principal.msgIcone = MessageBoxIcon.Information;
                Principal.msgTitulo = "Consistência";
                
                if (Convert.ToDateTime(dtInicial.Text) > Convert.ToDateTime(dtFinal.Text))
                {
                    Principal.mensagem = "A Data Inicial não pode ser maior que a Data Final!";
                    Funcoes.Avisa();
                    dtInicial.Focus();
                    return;
                }

                if (rdbAnalitico.Checked.Equals(true))
                {
                    frmRelatorioCaixaEspecieAnalitco analitico = new frmRelatorioCaixaEspecieAnalitco(Convert.ToDateTime(dtInicial.Text), Convert.ToDateTime(dtFinal.Text), cmbUsuario.Text);
                    analitico.Show();
                }
                else
                {
                    frmRelatorioCaixaEspecieSintetico sintetico = new frmRelatorioCaixaEspecieSintetico(Convert.ToDateTime(dtInicial.Text), Convert.ToDateTime(dtFinal.Text), cmbUsuario.Text);
                    sintetico.Show();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Relatório", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            throw new NotImplementedException();
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
