﻿namespace GestaoAdministrativa.Caixa
{
    partial class frmRelatorioResumoDiario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rpwResumoDiario = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rpwResumoDiario
            // 
            this.rpwResumoDiario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpwResumoDiario.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Caixa.Relatorios.RelatorioResumoDiarioMaster.rdlc";
            this.rpwResumoDiario.Location = new System.Drawing.Point(0, 0);
            this.rpwResumoDiario.Name = "rpwResumoDiario";
            this.rpwResumoDiario.Size = new System.Drawing.Size(994, 578);
            this.rpwResumoDiario.TabIndex = 0;
            // 
            // frmRelatorioResumoDiario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rpwResumoDiario);
            this.Name = "frmRelatorioResumoDiario";
            this.Text = "frmRelatorioResumoDiario";
            this.Load += new System.EventHandler(this.frmRelatorioResumoDiario_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpwResumoDiario;
    }
}