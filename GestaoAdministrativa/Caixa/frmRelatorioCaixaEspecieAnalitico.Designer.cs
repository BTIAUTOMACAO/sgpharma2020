﻿namespace GestaoAdministrativa.Caixa.Relatorios
{
    partial class frmRelatorioCaixaEspecieAnalitco
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rpwEspecieAnalitico = new Microsoft.Reporting.WinForms.ReportViewer();
            this.SuspendLayout();
            // 
            // rpwEspecieAnalitico
            // 
            this.rpwEspecieAnalitico.Dock = System.Windows.Forms.DockStyle.Fill;
            this.rpwEspecieAnalitico.LocalReport.ReportEmbeddedResource = "GestaoAdministrativa.Caixa.Relatorios.RelatorioCaixaEspecieAnaliticoMaster.rdlc";
            this.rpwEspecieAnalitico.Location = new System.Drawing.Point(0, 0);
            this.rpwEspecieAnalitico.Name = "rpwEspecieAnalitico";
            this.rpwEspecieAnalitico.Size = new System.Drawing.Size(994, 578);
            this.rpwEspecieAnalitico.TabIndex = 0;
            // 
            // frmRelatorioCaixaEspecieAnalitco
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.Controls.Add(this.rpwEspecieAnalitico);
            this.Name = "frmRelatorioCaixaEspecieAnalitco";
            this.Text = "frmRelatorioCaixaEspecieAnalitco";
            this.Load += new System.EventHandler(this.frmRelatorioCaixaEspecieAnalitco_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Microsoft.Reporting.WinForms.ReportViewer rpwEspecieAnalitico;
    }
}