﻿namespace GestaoAdministrativa.Caixa
{
    partial class frmCxRecebimentos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCxRecebimentos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcRecebimentos = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbClienteDebitos = new System.Windows.Forms.ComboBox();
            this.dgClientes = new System.Windows.Forms.DataGridView();
            this.CF_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_APELIDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_FONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_ENDER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_BAIRRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_UF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_OBSERVACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtBTelefone = new System.Windows.Forms.MaskedTextBox();
            this.txtBCodigo = new System.Windows.Forms.TextBox();
            this.txtBCpf = new System.Windows.Forms.MaskedTextBox();
            this.txtBNome = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.tcPagto = new System.Windows.Forms.TabControl();
            this.tpPagar = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.lblTotalSaldo = new System.Windows.Forms.Label();
            this.lblTotalJuros = new System.Windows.Forms.Label();
            this.lblDevedor = new System.Windows.Forms.Label();
            this.btnDesmarcar = new System.Windows.Forms.Button();
            this.btnMarcar = new System.Windows.Forms.Button();
            this.btnImprimir = new System.Windows.Forms.Button();
            this.btnVisualizar = new System.Windows.Forms.Button();
            this.btnConfirmar = new System.Windows.Forms.Button();
            this.gbEspecie = new System.Windows.Forms.GroupBox();
            this.lblTroco = new System.Windows.Forms.Label();
            this.lblTotal = new System.Windows.Forms.Label();
            this.dgEspecie = new System.Windows.Forms.DataGridView();
            this.ESP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESP_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ESP_VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEValor = new System.Windows.Forms.TextBox();
            this.cmbEspecie = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.gbValor = new System.Windows.Forms.GroupBox();
            this.txtTSJuros = new System.Windows.Forms.TextBox();
            this.btnPagar = new System.Windows.Forms.Button();
            this.rdbSJuros = new System.Windows.Forms.RadioButton();
            this.txtTCJuros = new System.Windows.Forms.TextBox();
            this.rdbCJuros = new System.Windows.Forms.RadioButton();
            this.dgRecebimentos = new System.Windows.Forms.DataGridView();
            this.ESCOLHA = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.COBRANCA_PARCELA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_PARCELA_VENCIMENTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_PARCELA_NUM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_PARCELA_VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_PARCELA_SALDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACRESCIMO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.VALOR_ATUALIZADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DT_CADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RD_PAGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.COBRANCA_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblEstab = new System.Windows.Forms.Label();
            this.tpPagos = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.lblCliente = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.dtFiltroFinal = new System.Windows.Forms.DateTimePicker();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.dgPagos = new System.Windows.Forms.DataGridView();
            this.ESCOLHECANCELAR = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.COB_PARCELA_VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DT_RECEBIMENTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnCBuscar = new System.Windows.Forms.Button();
            this.dtFiltroInicial = new System.Windows.Forms.DateTimePicker();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.btnVisualizarProdutosPagos = new System.Windows.Forms.Button();
            this.dtFinal = new System.Windows.Forms.DateTimePicker();
            this.btnCImprimir = new System.Windows.Forms.Button();
            this.dtInicial = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcRecebimentos.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgClientes)).BeginInit();
            this.tpFicha.SuspendLayout();
            this.tcPagto.SuspendLayout();
            this.tpPagar.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.gbEspecie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEspecie)).BeginInit();
            this.gbValor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRecebimentos)).BeginInit();
            this.tpPagos.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPagos)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcRecebimentos);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(974, 560);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(974, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(177, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Recebimento de Títulos";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 535);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(968, 22);
            this.statusStrip1.TabIndex = 41;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcRecebimentos
            // 
            this.tcRecebimentos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcRecebimentos.Controls.Add(this.tpGrade);
            this.tcRecebimentos.Controls.Add(this.tpFicha);
            this.tcRecebimentos.Location = new System.Drawing.Point(6, 33);
            this.tcRecebimentos.Name = "tcRecebimentos";
            this.tcRecebimentos.SelectedIndex = 0;
            this.tcRecebimentos.Size = new System.Drawing.Size(965, 499);
            this.tcRecebimentos.TabIndex = 40;
            this.tcRecebimentos.SelectedIndexChanged += new System.EventHandler(this.tcRecebimentos_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.Controls.Add(this.label8);
            this.tpGrade.Controls.Add(this.cmbClienteDebitos);
            this.tpGrade.Controls.Add(this.dgClientes);
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.txtBTelefone);
            this.tpGrade.Controls.Add(this.txtBCodigo);
            this.tpGrade.Controls.Add(this.txtBCpf);
            this.tpGrade.Controls.Add(this.txtBNome);
            this.tpGrade.Controls.Add(this.label2);
            this.tpGrade.Controls.Add(this.label1);
            this.tpGrade.Controls.Add(this.label13);
            this.tpGrade.Controls.Add(this.label15);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(957, 470);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            this.tpGrade.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Navy;
            this.label8.Location = new System.Drawing.Point(675, 417);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(135, 16);
            this.label8.TabIndex = 132;
            this.label8.Text = "Cliente com Débitos";
            // 
            // cmbClienteDebitos
            // 
            this.cmbClienteDebitos.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbClienteDebitos.FormattingEnabled = true;
            this.cmbClienteDebitos.Items.AddRange(new object[] {
            "TODOS",
            "SIM",
            "NÃO"});
            this.cmbClienteDebitos.Location = new System.Drawing.Point(678, 437);
            this.cmbClienteDebitos.Name = "cmbClienteDebitos";
            this.cmbClienteDebitos.Size = new System.Drawing.Size(132, 24);
            this.cmbClienteDebitos.TabIndex = 131;
            // 
            // dgClientes
            // 
            this.dgClientes.AllowUserToAddRows = false;
            this.dgClientes.AllowUserToDeleteRows = false;
            this.dgClientes.AllowUserToResizeColumns = false;
            this.dgClientes.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgClientes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgClientes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgClientes.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgClientes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgClientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgClientes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CF_ID,
            this.CF_NOME,
            this.CF_APELIDO,
            this.CF_FONE,
            this.CF_DOCTO,
            this.CF_ENDER,
            this.CF_BAIRRO,
            this.CF_CIDADE,
            this.CF_UF,
            this.CF_OBSERVACAO});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgClientes.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgClientes.GridColor = System.Drawing.Color.LightGray;
            this.dgClientes.Location = new System.Drawing.Point(9, 6);
            this.dgClientes.MultiSelect = false;
            this.dgClientes.Name = "dgClientes";
            this.dgClientes.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgClientes.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgClientes.RowHeadersVisible = false;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgClientes.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgClientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgClientes.Size = new System.Drawing.Size(942, 406);
            this.dgClientes.TabIndex = 130;
            this.dgClientes.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgClientes_CellMouseClick);
            this.dgClientes.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgClientes_CellMouseDoubleClick);
            this.dgClientes.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgClientes_KeyDown);
            // 
            // CF_ID
            // 
            this.CF_ID.DataPropertyName = "CF_ID";
            this.CF_ID.HeaderText = "Cliente ID";
            this.CF_ID.MaxInputLength = 18;
            this.CF_ID.Name = "CF_ID";
            this.CF_ID.ReadOnly = true;
            this.CF_ID.Width = 90;
            // 
            // CF_NOME
            // 
            this.CF_NOME.DataPropertyName = "CF_NOME";
            this.CF_NOME.HeaderText = "Nome";
            this.CF_NOME.MaxInputLength = 60;
            this.CF_NOME.Name = "CF_NOME";
            this.CF_NOME.ReadOnly = true;
            this.CF_NOME.Width = 300;
            // 
            // CF_APELIDO
            // 
            this.CF_APELIDO.DataPropertyName = "CF_APELIDO";
            this.CF_APELIDO.HeaderText = "Apelido";
            this.CF_APELIDO.MaxInputLength = 30;
            this.CF_APELIDO.Name = "CF_APELIDO";
            this.CF_APELIDO.ReadOnly = true;
            // 
            // CF_FONE
            // 
            this.CF_FONE.DataPropertyName = "CF_FONE";
            this.CF_FONE.HeaderText = "Telefone";
            this.CF_FONE.Name = "CF_FONE";
            this.CF_FONE.ReadOnly = true;
            // 
            // CF_DOCTO
            // 
            this.CF_DOCTO.DataPropertyName = "CF_DOCTO";
            this.CF_DOCTO.HeaderText = "CPF/CNPJ";
            this.CF_DOCTO.MaxInputLength = 14;
            this.CF_DOCTO.Name = "CF_DOCTO";
            this.CF_DOCTO.ReadOnly = true;
            // 
            // CF_ENDER
            // 
            this.CF_ENDER.DataPropertyName = "CF_ENDER";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.NullValue = null;
            this.CF_ENDER.DefaultCellStyle = dataGridViewCellStyle3;
            this.CF_ENDER.HeaderText = "Endereco";
            this.CF_ENDER.MaxInputLength = 50;
            this.CF_ENDER.Name = "CF_ENDER";
            this.CF_ENDER.ReadOnly = true;
            this.CF_ENDER.Width = 250;
            // 
            // CF_BAIRRO
            // 
            this.CF_BAIRRO.DataPropertyName = "CF_BAIRRO";
            this.CF_BAIRRO.HeaderText = "Bairro";
            this.CF_BAIRRO.MaxInputLength = 30;
            this.CF_BAIRRO.Name = "CF_BAIRRO";
            this.CF_BAIRRO.ReadOnly = true;
            this.CF_BAIRRO.Width = 150;
            // 
            // CF_CIDADE
            // 
            this.CF_CIDADE.DataPropertyName = "CF_CIDADE";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.NullValue = null;
            this.CF_CIDADE.DefaultCellStyle = dataGridViewCellStyle4;
            this.CF_CIDADE.HeaderText = "Cidade";
            this.CF_CIDADE.MaxInputLength = 30;
            this.CF_CIDADE.Name = "CF_CIDADE";
            this.CF_CIDADE.ReadOnly = true;
            // 
            // CF_UF
            // 
            this.CF_UF.DataPropertyName = "CF_UF";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            this.CF_UF.DefaultCellStyle = dataGridViewCellStyle5;
            this.CF_UF.HeaderText = "UF";
            this.CF_UF.MaxInputLength = 2;
            this.CF_UF.Name = "CF_UF";
            this.CF_UF.ReadOnly = true;
            this.CF_UF.Width = 50;
            // 
            // CF_OBSERVACAO
            // 
            this.CF_OBSERVACAO.DataPropertyName = "CF_OBSERVACAO";
            this.CF_OBSERVACAO.HeaderText = "Observação";
            this.CF_OBSERVACAO.Name = "CF_OBSERVACAO";
            this.CF_OBSERVACAO.ReadOnly = true;
            this.CF_OBSERVACAO.Width = 200;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(828, 423);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnBuscar.TabIndex = 129;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtBTelefone
            // 
            this.txtBTelefone.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBTelefone.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBTelefone.Location = new System.Drawing.Point(550, 437);
            this.txtBTelefone.Mask = "(99) 0000-0000";
            this.txtBTelefone.Name = "txtBTelefone";
            this.txtBTelefone.Size = new System.Drawing.Size(111, 22);
            this.txtBTelefone.TabIndex = 128;
            this.txtBTelefone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBTelefone_KeyPress);
            // 
            // txtBCodigo
            // 
            this.txtBCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBCodigo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBCodigo.Location = new System.Drawing.Point(440, 437);
            this.txtBCodigo.MaxLength = 60;
            this.txtBCodigo.Name = "txtBCodigo";
            this.txtBCodigo.Size = new System.Drawing.Size(95, 22);
            this.txtBCodigo.TabIndex = 127;
            this.txtBCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBCodigo_KeyPress);
            // 
            // txtBCpf
            // 
            this.txtBCpf.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBCpf.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBCpf.Location = new System.Drawing.Point(285, 437);
            this.txtBCpf.Name = "txtBCpf";
            this.txtBCpf.Size = new System.Drawing.Size(138, 22);
            this.txtBCpf.TabIndex = 126;
            this.txtBCpf.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBCpf_KeyPress);
            // 
            // txtBNome
            // 
            this.txtBNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtBNome.Location = new System.Drawing.Point(6, 437);
            this.txtBNome.MaxLength = 60;
            this.txtBNome.Name = "txtBNome";
            this.txtBNome.Size = new System.Drawing.Size(265, 22);
            this.txtBNome.TabIndex = 92;
            this.txtBNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBNome_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(547, 419);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 16);
            this.label2.TabIndex = 91;
            this.label2.Text = "Telefone";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(437, 419);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 84;
            this.label1.Text = "Código";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.ForeColor = System.Drawing.Color.Navy;
            this.label13.Location = new System.Drawing.Point(282, 419);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(72, 16);
            this.label13.TabIndex = 62;
            this.label13.Text = "CPF/CNPJ";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.ForeColor = System.Drawing.Color.Navy;
            this.label15.Location = new System.Drawing.Point(6, 419);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(45, 16);
            this.label15.TabIndex = 60;
            this.label15.Text = "Nome";
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.tcPagto);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(957, 470);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // tcPagto
            // 
            this.tcPagto.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcPagto.Controls.Add(this.tpPagar);
            this.tcPagto.Controls.Add(this.tpPagos);
            this.tcPagto.Location = new System.Drawing.Point(6, 6);
            this.tcPagto.Name = "tcPagto";
            this.tcPagto.SelectedIndex = 0;
            this.tcPagto.Size = new System.Drawing.Size(948, 458);
            this.tcPagto.TabIndex = 0;
            this.tcPagto.SelectedIndexChanged += new System.EventHandler(this.tcPagto_SelectedIndexChanged);
            // 
            // tpPagar
            // 
            this.tpPagar.Controls.Add(this.groupBox2);
            this.tpPagar.Location = new System.Drawing.Point(4, 25);
            this.tpPagar.Name = "tpPagar";
            this.tpPagar.Padding = new System.Windows.Forms.Padding(3);
            this.tpPagar.Size = new System.Drawing.Size(940, 429);
            this.tpPagar.TabIndex = 0;
            this.tpPagar.Text = "Pagar";
            this.tpPagar.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.CausesValidation = false;
            this.groupBox2.Controls.Add(this.lblTotalSaldo);
            this.groupBox2.Controls.Add(this.lblTotalJuros);
            this.groupBox2.Controls.Add(this.lblDevedor);
            this.groupBox2.Controls.Add(this.btnDesmarcar);
            this.groupBox2.Controls.Add(this.btnMarcar);
            this.groupBox2.Controls.Add(this.btnImprimir);
            this.groupBox2.Controls.Add(this.btnVisualizar);
            this.groupBox2.Controls.Add(this.btnConfirmar);
            this.groupBox2.Controls.Add(this.gbEspecie);
            this.groupBox2.Controls.Add(this.gbValor);
            this.groupBox2.Controls.Add(this.dgRecebimentos);
            this.groupBox2.Controls.Add(this.lblEstab);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(928, 417);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // lblTotalSaldo
            // 
            this.lblTotalSaldo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalSaldo.ForeColor = System.Drawing.Color.Black;
            this.lblTotalSaldo.Location = new System.Drawing.Point(621, 21);
            this.lblTotalSaldo.Name = "lblTotalSaldo";
            this.lblTotalSaldo.Size = new System.Drawing.Size(301, 16);
            this.lblTotalSaldo.TabIndex = 165;
            this.lblTotalSaldo.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotalJuros
            // 
            this.lblTotalJuros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalJuros.ForeColor = System.Drawing.Color.Black;
            this.lblTotalJuros.Location = new System.Drawing.Point(621, 44);
            this.lblTotalJuros.Name = "lblTotalJuros";
            this.lblTotalJuros.Size = new System.Drawing.Size(301, 16);
            this.lblTotalJuros.TabIndex = 164;
            this.lblTotalJuros.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblDevedor
            // 
            this.lblDevedor.AutoSize = true;
            this.lblDevedor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDevedor.ForeColor = System.Drawing.Color.Black;
            this.lblDevedor.Location = new System.Drawing.Point(6, 44);
            this.lblDevedor.Name = "lblDevedor";
            this.lblDevedor.Size = new System.Drawing.Size(53, 16);
            this.lblDevedor.TabIndex = 163;
            this.lblDevedor.Text = "Cliente";
            // 
            // btnDesmarcar
            // 
            this.btnDesmarcar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDesmarcar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDesmarcar.Image = ((System.Drawing.Image)(resources.GetObject("btnDesmarcar.Image")));
            this.btnDesmarcar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDesmarcar.Location = new System.Drawing.Point(195, 359);
            this.btnDesmarcar.Name = "btnDesmarcar";
            this.btnDesmarcar.Size = new System.Drawing.Size(168, 52);
            this.btnDesmarcar.TabIndex = 162;
            this.btnDesmarcar.Text = "        Desmarcar Todos         (F4)";
            this.btnDesmarcar.UseVisualStyleBackColor = true;
            this.btnDesmarcar.Visible = false;
            this.btnDesmarcar.Click += new System.EventHandler(this.btnDesmarcar_Click);
            this.btnDesmarcar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnDesmarcar_KeyDown);
            // 
            // btnMarcar
            // 
            this.btnMarcar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMarcar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarcar.Image = ((System.Drawing.Image)(resources.GetObject("btnMarcar.Image")));
            this.btnMarcar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnMarcar.Location = new System.Drawing.Point(9, 359);
            this.btnMarcar.Name = "btnMarcar";
            this.btnMarcar.Size = new System.Drawing.Size(170, 52);
            this.btnMarcar.TabIndex = 161;
            this.btnMarcar.Text = "            Marcar Todos           (F3)";
            this.btnMarcar.UseVisualStyleBackColor = true;
            this.btnMarcar.Visible = false;
            this.btnMarcar.Click += new System.EventHandler(this.btnMarcar_Click);
            this.btnMarcar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnMarcar_KeyDown);
            // 
            // btnImprimir
            // 
            this.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnImprimir.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnImprimir.Image = ((System.Drawing.Image)(resources.GetObject("btnImprimir.Image")));
            this.btnImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnImprimir.Location = new System.Drawing.Point(567, 359);
            this.btnImprimir.Name = "btnImprimir";
            this.btnImprimir.Size = new System.Drawing.Size(170, 52);
            this.btnImprimir.TabIndex = 124;
            this.btnImprimir.Text = "        Imprimir      Produtos (F9)";
            this.btnImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnImprimir.UseVisualStyleBackColor = true;
            this.btnImprimir.Visible = false;
            this.btnImprimir.Click += new System.EventHandler(this.btnImprimir_Click);
            this.btnImprimir.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnImprimir_KeyDown);
            // 
            // btnVisualizar
            // 
            this.btnVisualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVisualizar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVisualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnVisualizar.Image")));
            this.btnVisualizar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVisualizar.Location = new System.Drawing.Point(382, 359);
            this.btnVisualizar.Name = "btnVisualizar";
            this.btnVisualizar.Size = new System.Drawing.Size(170, 52);
            this.btnVisualizar.TabIndex = 123;
            this.btnVisualizar.Text = "           Visualizar     Produtos (F8)";
            this.btnVisualizar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVisualizar.UseVisualStyleBackColor = true;
            this.btnVisualizar.Visible = false;
            this.btnVisualizar.Click += new System.EventHandler(this.btnVisualizar_Click);
            this.btnVisualizar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnVisualizar_KeyDown);
            // 
            // btnConfirmar
            // 
            this.btnConfirmar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConfirmar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConfirmar.Image = ((System.Drawing.Image)(resources.GetObject("btnConfirmar.Image")));
            this.btnConfirmar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnConfirmar.Location = new System.Drawing.Point(752, 359);
            this.btnConfirmar.Name = "btnConfirmar";
            this.btnConfirmar.Size = new System.Drawing.Size(170, 52);
            this.btnConfirmar.TabIndex = 122;
            this.btnConfirmar.Text = "               Confirmar          Pagamento (F10)";
            this.btnConfirmar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConfirmar.UseVisualStyleBackColor = true;
            this.btnConfirmar.Visible = false;
            this.btnConfirmar.Click += new System.EventHandler(this.btnConfirmar_Click);
            this.btnConfirmar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnConfirmar_KeyDown);
            // 
            // gbEspecie
            // 
            this.gbEspecie.Controls.Add(this.lblTroco);
            this.gbEspecie.Controls.Add(this.lblTotal);
            this.gbEspecie.Controls.Add(this.dgEspecie);
            this.gbEspecie.Controls.Add(this.txtEValor);
            this.gbEspecie.Controls.Add(this.cmbEspecie);
            this.gbEspecie.Controls.Add(this.label4);
            this.gbEspecie.Controls.Add(this.label3);
            this.gbEspecie.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbEspecie.ForeColor = System.Drawing.Color.Navy;
            this.gbEspecie.Location = new System.Drawing.Point(280, 202);
            this.gbEspecie.Name = "gbEspecie";
            this.gbEspecie.Size = new System.Drawing.Size(642, 150);
            this.gbEspecie.TabIndex = 118;
            this.gbEspecie.TabStop = false;
            this.gbEspecie.Text = "Baixa";
            this.gbEspecie.Visible = false;
            this.gbEspecie.Enter += new System.EventHandler(this.gbEspecie_Enter);
            // 
            // lblTroco
            // 
            this.lblTroco.Location = new System.Drawing.Point(152, 127);
            this.lblTroco.Name = "lblTroco";
            this.lblTroco.Size = new System.Drawing.Size(180, 20);
            this.lblTroco.TabIndex = 120;
            this.lblTroco.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lblTotal
            // 
            this.lblTotal.Location = new System.Drawing.Point(301, 128);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(180, 20);
            this.lblTotal.TabIndex = 119;
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // dgEspecie
            // 
            this.dgEspecie.AllowUserToAddRows = false;
            this.dgEspecie.AllowUserToResizeColumns = false;
            this.dgEspecie.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEspecie.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgEspecie.BackgroundColor = System.Drawing.Color.White;
            this.dgEspecie.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.Disable;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEspecie.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgEspecie.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ESP_CODIGO,
            this.ESP_DESCR,
            this.ESP_VALOR});
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEspecie.DefaultCellStyle = dataGridViewCellStyle13;
            this.dgEspecie.GridColor = System.Drawing.Color.LightGray;
            this.dgEspecie.Location = new System.Drawing.Point(188, 39);
            this.dgEspecie.MultiSelect = false;
            this.dgEspecie.Name = "dgEspecie";
            this.dgEspecie.ReadOnly = true;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEspecie.RowHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgEspecie.RowHeadersWidth = 30;
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEspecie.RowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgEspecie.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEspecie.Size = new System.Drawing.Size(293, 85);
            this.dgEspecie.TabIndex = 118;
            this.dgEspecie.RowsRemoved += new System.Windows.Forms.DataGridViewRowsRemovedEventHandler(this.dgEspecie_RowsRemoved);
            this.dgEspecie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgEspecie_KeyDown);
            // 
            // ESP_CODIGO
            // 
            this.ESP_CODIGO.DataPropertyName = "ESP_CODIGO";
            dataGridViewCellStyle11.Format = "C2";
            dataGridViewCellStyle11.NullValue = null;
            this.ESP_CODIGO.DefaultCellStyle = dataGridViewCellStyle11;
            this.ESP_CODIGO.HeaderText = "Esp. Codigo";
            this.ESP_CODIGO.MaxInputLength = 18;
            this.ESP_CODIGO.Name = "ESP_CODIGO";
            this.ESP_CODIGO.ReadOnly = true;
            this.ESP_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ESP_CODIGO.Visible = false;
            // 
            // ESP_DESCR
            // 
            this.ESP_DESCR.DataPropertyName = "ESP_DESCRICAO";
            this.ESP_DESCR.HeaderText = "Espécie";
            this.ESP_DESCR.MaxInputLength = 20;
            this.ESP_DESCR.Name = "ESP_DESCR";
            this.ESP_DESCR.ReadOnly = true;
            this.ESP_DESCR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ESP_DESCR.Width = 135;
            // 
            // ESP_VALOR
            // 
            this.ESP_VALOR.DataPropertyName = "ESP_VALOR";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle12.Format = "C2";
            dataGridViewCellStyle12.NullValue = null;
            this.ESP_VALOR.DefaultCellStyle = dataGridViewCellStyle12;
            this.ESP_VALOR.HeaderText = "Valor";
            this.ESP_VALOR.MaxInputLength = 18;
            this.ESP_VALOR.Name = "ESP_VALOR";
            this.ESP_VALOR.ReadOnly = true;
            this.ESP_VALOR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ESP_VALOR.Width = 125;
            // 
            // txtEValor
            // 
            this.txtEValor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtEValor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEValor.Location = new System.Drawing.Point(12, 102);
            this.txtEValor.Name = "txtEValor";
            this.txtEValor.Size = new System.Drawing.Size(161, 22);
            this.txtEValor.TabIndex = 3;
            this.txtEValor.Text = "0,00";
            this.txtEValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtEValor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEValor_KeyDown);
            this.txtEValor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEValor_KeyPress);
            this.txtEValor.Validated += new System.EventHandler(this.txtEValor_Validated);
            // 
            // cmbEspecie
            // 
            this.cmbEspecie.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbEspecie.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbEspecie.FormattingEnabled = true;
            this.cmbEspecie.Location = new System.Drawing.Point(12, 39);
            this.cmbEspecie.Name = "cmbEspecie";
            this.cmbEspecie.Size = new System.Drawing.Size(161, 24);
            this.cmbEspecie.TabIndex = 2;
            this.cmbEspecie.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbEspecie_KeyDown);
            this.cmbEspecie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbEspecie_KeyPress);
            this.cmbEspecie.Validated += new System.EventHandler(this.cmbEspecie_Validated);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 84);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 16);
            this.label4.TabIndex = 1;
            this.label4.Text = "Valor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(9, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 16);
            this.label3.TabIndex = 0;
            this.label3.Text = "Espécie";
            // 
            // gbValor
            // 
            this.gbValor.Controls.Add(this.txtTSJuros);
            this.gbValor.Controls.Add(this.btnPagar);
            this.gbValor.Controls.Add(this.rdbSJuros);
            this.gbValor.Controls.Add(this.txtTCJuros);
            this.gbValor.Controls.Add(this.rdbCJuros);
            this.gbValor.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbValor.ForeColor = System.Drawing.Color.Navy;
            this.gbValor.Location = new System.Drawing.Point(9, 202);
            this.gbValor.Name = "gbValor";
            this.gbValor.Size = new System.Drawing.Size(265, 150);
            this.gbValor.TabIndex = 117;
            this.gbValor.TabStop = false;
            this.gbValor.Text = "Valor";
            // 
            // txtTSJuros
            // 
            this.txtTSJuros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTSJuros.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTSJuros.Location = new System.Drawing.Point(11, 111);
            this.txtTSJuros.Name = "txtTSJuros";
            this.txtTSJuros.Size = new System.Drawing.Size(129, 26);
            this.txtTSJuros.TabIndex = 117;
            this.txtTSJuros.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTSJuros.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTSJuros_KeyDown);
            this.txtTSJuros.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTSJuros_KeyPress);
            // 
            // btnPagar
            // 
            this.btnPagar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPagar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPagar.ForeColor = System.Drawing.Color.Black;
            this.btnPagar.Image = ((System.Drawing.Image)(resources.GetObject("btnPagar.Image")));
            this.btnPagar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPagar.Location = new System.Drawing.Point(157, 64);
            this.btnPagar.Name = "btnPagar";
            this.btnPagar.Size = new System.Drawing.Size(89, 56);
            this.btnPagar.TabIndex = 118;
            this.btnPagar.Text = "Pagar";
            this.btnPagar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnPagar.UseVisualStyleBackColor = true;
            this.btnPagar.Click += new System.EventHandler(this.btnPagar_Click);
            // 
            // rdbSJuros
            // 
            this.rdbSJuros.AutoSize = true;
            this.rdbSJuros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbSJuros.ForeColor = System.Drawing.Color.Navy;
            this.rdbSJuros.Location = new System.Drawing.Point(11, 89);
            this.rdbSJuros.Name = "rdbSJuros";
            this.rdbSJuros.Size = new System.Drawing.Size(127, 20);
            this.rdbSJuros.TabIndex = 116;
            this.rdbSJuros.TabStop = true;
            this.rdbSJuros.Text = "Valor sem Juros";
            this.rdbSJuros.UseVisualStyleBackColor = true;
            this.rdbSJuros.CheckedChanged += new System.EventHandler(this.rdbSJuros_CheckedChanged);
            this.rdbSJuros.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbSJuros_KeyDown);
            // 
            // txtTCJuros
            // 
            this.txtTCJuros.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtTCJuros.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTCJuros.Location = new System.Drawing.Point(12, 47);
            this.txtTCJuros.Name = "txtTCJuros";
            this.txtTCJuros.Size = new System.Drawing.Size(129, 26);
            this.txtTCJuros.TabIndex = 2;
            this.txtTCJuros.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtTCJuros.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtTCJuros_KeyDown);
            this.txtTCJuros.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTCJuros_KeyPress);
            // 
            // rdbCJuros
            // 
            this.rdbCJuros.AutoSize = true;
            this.rdbCJuros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rdbCJuros.ForeColor = System.Drawing.Color.Navy;
            this.rdbCJuros.Location = new System.Drawing.Point(12, 21);
            this.rdbCJuros.Name = "rdbCJuros";
            this.rdbCJuros.Size = new System.Drawing.Size(128, 20);
            this.rdbCJuros.TabIndex = 0;
            this.rdbCJuros.TabStop = true;
            this.rdbCJuros.Text = "Valor com Juros";
            this.rdbCJuros.UseVisualStyleBackColor = true;
            this.rdbCJuros.CheckedChanged += new System.EventHandler(this.rdbCJuros_CheckedChanged);
            this.rdbCJuros.KeyDown += new System.Windows.Forms.KeyEventHandler(this.rdbCJuros_KeyDown);
            // 
            // dgRecebimentos
            // 
            this.dgRecebimentos.AllowUserToAddRows = false;
            this.dgRecebimentos.AllowUserToDeleteRows = false;
            this.dgRecebimentos.AllowUserToResizeColumns = false;
            this.dgRecebimentos.AllowUserToResizeRows = false;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            this.dgRecebimentos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle16;
            this.dgRecebimentos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgRecebimentos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgRecebimentos.ColumnHeadersHeight = 25;
            this.dgRecebimentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgRecebimentos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ESCOLHA,
            this.COBRANCA_PARCELA_ID,
            this.COBRANCA_PARCELA_VENCIMENTO,
            this.COBRANCA_PARCELA_NUM,
            this.COBRANCA_PARCELA_VALOR,
            this.COBRANCA_PARCELA_SALDO,
            this.ACRESCIMO,
            this.VALOR_ATUALIZADO,
            this.DT_CADASTRO,
            this.RD_PAGO,
            this.DTALTERACAO,
            this.OPALTERACAO,
            this.COBRANCA_ID});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgRecebimentos.DefaultCellStyle = dataGridViewCellStyle22;
            this.dgRecebimentos.GridColor = System.Drawing.Color.LightGray;
            this.dgRecebimentos.Location = new System.Drawing.Point(9, 63);
            this.dgRecebimentos.Name = "dgRecebimentos";
            this.dgRecebimentos.ReadOnly = true;
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgRecebimentos.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.dgRecebimentos.RowHeadersVisible = false;
            this.dgRecebimentos.RowHeadersWidth = 33;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            this.dgRecebimentos.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.dgRecebimentos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgRecebimentos.Size = new System.Drawing.Size(913, 133);
            this.dgRecebimentos.TabIndex = 116;
            this.dgRecebimentos.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgRecebimentos_CellEndEdit);
            this.dgRecebimentos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgRecebimentos_KeyDown);
            this.dgRecebimentos.MouseClick += new System.Windows.Forms.MouseEventHandler(this.dgRecebimentos_MouseClick);
            // 
            // ESCOLHA
            // 
            this.ESCOLHA.Frozen = true;
            this.ESCOLHA.HeaderText = "";
            this.ESCOLHA.Name = "ESCOLHA";
            this.ESCOLHA.ReadOnly = true;
            this.ESCOLHA.TrueValue = "true";
            this.ESCOLHA.Visible = false;
            this.ESCOLHA.Width = 30;
            // 
            // COBRANCA_PARCELA_ID
            // 
            this.COBRANCA_PARCELA_ID.DataPropertyName = "COBRANCA_PARCELA_ID";
            this.COBRANCA_PARCELA_ID.HeaderText = "Código";
            this.COBRANCA_PARCELA_ID.MaxInputLength = 18;
            this.COBRANCA_PARCELA_ID.Name = "COBRANCA_PARCELA_ID";
            this.COBRANCA_PARCELA_ID.ReadOnly = true;
            this.COBRANCA_PARCELA_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.COBRANCA_PARCELA_ID.Visible = false;
            // 
            // COBRANCA_PARCELA_VENCIMENTO
            // 
            this.COBRANCA_PARCELA_VENCIMENTO.DataPropertyName = "COBRANCA_PARCELA_VENCIMENTO";
            this.COBRANCA_PARCELA_VENCIMENTO.Frozen = true;
            this.COBRANCA_PARCELA_VENCIMENTO.HeaderText = "Vencimento";
            this.COBRANCA_PARCELA_VENCIMENTO.MaxInputLength = 10;
            this.COBRANCA_PARCELA_VENCIMENTO.Name = "COBRANCA_PARCELA_VENCIMENTO";
            this.COBRANCA_PARCELA_VENCIMENTO.ReadOnly = true;
            this.COBRANCA_PARCELA_VENCIMENTO.Width = 105;
            // 
            // COBRANCA_PARCELA_NUM
            // 
            this.COBRANCA_PARCELA_NUM.DataPropertyName = "COBRANCA_PARCELA_NUM";
            this.COBRANCA_PARCELA_NUM.Frozen = true;
            this.COBRANCA_PARCELA_NUM.HeaderText = "Parcela";
            this.COBRANCA_PARCELA_NUM.MaxInputLength = 10;
            this.COBRANCA_PARCELA_NUM.Name = "COBRANCA_PARCELA_NUM";
            this.COBRANCA_PARCELA_NUM.ReadOnly = true;
            this.COBRANCA_PARCELA_NUM.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.COBRANCA_PARCELA_NUM.Width = 70;
            // 
            // COBRANCA_PARCELA_VALOR
            // 
            this.COBRANCA_PARCELA_VALOR.DataPropertyName = "COBRANCA_PARCELA_VALOR";
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle18.Format = "N2";
            dataGridViewCellStyle18.NullValue = null;
            this.COBRANCA_PARCELA_VALOR.DefaultCellStyle = dataGridViewCellStyle18;
            this.COBRANCA_PARCELA_VALOR.Frozen = true;
            this.COBRANCA_PARCELA_VALOR.HeaderText = "Valor Parcela";
            this.COBRANCA_PARCELA_VALOR.MaxInputLength = 18;
            this.COBRANCA_PARCELA_VALOR.Name = "COBRANCA_PARCELA_VALOR";
            this.COBRANCA_PARCELA_VALOR.ReadOnly = true;
            this.COBRANCA_PARCELA_VALOR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // COBRANCA_PARCELA_SALDO
            // 
            this.COBRANCA_PARCELA_SALDO.DataPropertyName = "COBRANCA_PARCELA_SALDO";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle19.Format = "N2";
            dataGridViewCellStyle19.NullValue = null;
            this.COBRANCA_PARCELA_SALDO.DefaultCellStyle = dataGridViewCellStyle19;
            this.COBRANCA_PARCELA_SALDO.Frozen = true;
            this.COBRANCA_PARCELA_SALDO.HeaderText = "Saldo";
            this.COBRANCA_PARCELA_SALDO.MaxInputLength = 18;
            this.COBRANCA_PARCELA_SALDO.Name = "COBRANCA_PARCELA_SALDO";
            this.COBRANCA_PARCELA_SALDO.ReadOnly = true;
            this.COBRANCA_PARCELA_SALDO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // ACRESCIMO
            // 
            this.ACRESCIMO.DataPropertyName = "ACRESCIMO";
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle20.Format = "N2";
            dataGridViewCellStyle20.NullValue = null;
            this.ACRESCIMO.DefaultCellStyle = dataGridViewCellStyle20;
            this.ACRESCIMO.Frozen = true;
            this.ACRESCIMO.HeaderText = "Acréscimo";
            this.ACRESCIMO.MaxInputLength = 18;
            this.ACRESCIMO.Name = "ACRESCIMO";
            this.ACRESCIMO.ReadOnly = true;
            this.ACRESCIMO.Width = 110;
            // 
            // VALOR_ATUALIZADO
            // 
            this.VALOR_ATUALIZADO.DataPropertyName = "VALOR_ATUALIZADO";
            dataGridViewCellStyle21.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle21.Format = "N2";
            dataGridViewCellStyle21.NullValue = null;
            this.VALOR_ATUALIZADO.DefaultCellStyle = dataGridViewCellStyle21;
            this.VALOR_ATUALIZADO.Frozen = true;
            this.VALOR_ATUALIZADO.HeaderText = "Valor Atualizado";
            this.VALOR_ATUALIZADO.MaxInputLength = 18;
            this.VALOR_ATUALIZADO.Name = "VALOR_ATUALIZADO";
            this.VALOR_ATUALIZADO.ReadOnly = true;
            this.VALOR_ATUALIZADO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.VALOR_ATUALIZADO.Width = 120;
            // 
            // DT_CADASTRO
            // 
            this.DT_CADASTRO.DataPropertyName = "DT_CADASTRO";
            this.DT_CADASTRO.Frozen = true;
            this.DT_CADASTRO.HeaderText = "Emissão";
            this.DT_CADASTRO.MaxInputLength = 10;
            this.DT_CADASTRO.Name = "DT_CADASTRO";
            this.DT_CADASTRO.ReadOnly = true;
            this.DT_CADASTRO.Width = 110;
            // 
            // RD_PAGO
            // 
            this.RD_PAGO.DataPropertyName = "RD_PAGO";
            this.RD_PAGO.HeaderText = "Pago";
            this.RD_PAGO.MaxInputLength = 1;
            this.RD_PAGO.Name = "RD_PAGO";
            this.RD_PAGO.ReadOnly = true;
            this.RD_PAGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.RD_PAGO.Visible = false;
            this.RD_PAGO.Width = 75;
            // 
            // DTALTERACAO
            // 
            this.DTALTERACAO.DataPropertyName = "DT_ALTERACAO";
            this.DTALTERACAO.HeaderText = "Dt. Alteração";
            this.DTALTERACAO.MaxInputLength = 10;
            this.DTALTERACAO.Name = "DTALTERACAO";
            this.DTALTERACAO.ReadOnly = true;
            this.DTALTERACAO.Visible = false;
            // 
            // OPALTERACAO
            // 
            this.OPALTERACAO.DataPropertyName = "OP_ALTERACAO";
            this.OPALTERACAO.HeaderText = "Usuário";
            this.OPALTERACAO.MaxInputLength = 18;
            this.OPALTERACAO.Name = "OPALTERACAO";
            this.OPALTERACAO.ReadOnly = true;
            this.OPALTERACAO.Visible = false;
            // 
            // COBRANCA_ID
            // 
            this.COBRANCA_ID.DataPropertyName = "COBRANCA_ID";
            this.COBRANCA_ID.HeaderText = "COBRANCA_ID";
            this.COBRANCA_ID.Name = "COBRANCA_ID";
            this.COBRANCA_ID.ReadOnly = true;
            this.COBRANCA_ID.Visible = false;
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(6, 21);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(113, 16);
            this.lblEstab.TabIndex = 109;
            this.lblEstab.Text = "Estabelecimento";
            // 
            // tpPagos
            // 
            this.tpPagos.Controls.Add(this.groupBox3);
            this.tpPagos.Location = new System.Drawing.Point(4, 25);
            this.tpPagos.Name = "tpPagos";
            this.tpPagos.Padding = new System.Windows.Forms.Padding(3);
            this.tpPagos.Size = new System.Drawing.Size(940, 429);
            this.tpPagos.TabIndex = 1;
            this.tpPagos.Text = "Pagos";
            this.tpPagos.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.lblCliente);
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.groupBox4);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(928, 417);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Principal";
            // 
            // lblCliente
            // 
            this.lblCliente.AutoSize = true;
            this.lblCliente.ForeColor = System.Drawing.Color.Black;
            this.lblCliente.Location = new System.Drawing.Point(12, 21);
            this.lblCliente.Name = "lblCliente";
            this.lblCliente.Size = new System.Drawing.Size(44, 16);
            this.lblCliente.TabIndex = 5;
            this.lblCliente.Text = "sdsad";
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Controls.Add(this.label9);
            this.groupBox5.Controls.Add(this.dtFiltroFinal);
            this.groupBox5.Controls.Add(this.btnCancelar);
            this.groupBox5.Controls.Add(this.dgPagos);
            this.groupBox5.Controls.Add(this.btnCBuscar);
            this.groupBox5.Controls.Add(this.dtFiltroInicial);
            this.groupBox5.ForeColor = System.Drawing.Color.Navy;
            this.groupBox5.Location = new System.Drawing.Point(13, 147);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(900, 253);
            this.groupBox5.TabIndex = 2;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Cancelar Pagamentos";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(151, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 16);
            this.label11.TabIndex = 126;
            this.label11.Text = "Data Final";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(13, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 16);
            this.label9.TabIndex = 126;
            this.label9.Text = "Data Inicial";
            // 
            // dtFiltroFinal
            // 
            this.dtFiltroFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFiltroFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFiltroFinal.Location = new System.Drawing.Point(153, 51);
            this.dtFiltroFinal.Name = "dtFiltroFinal";
            this.dtFiltroFinal.Size = new System.Drawing.Size(116, 22);
            this.dtFiltroFinal.TabIndex = 6;
            // 
            // btnCancelar
            // 
            this.btnCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancelar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.ForeColor = System.Drawing.Color.Black;
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCancelar.Location = new System.Drawing.Point(700, 31);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(180, 38);
            this.btnCancelar.TabIndex = 132;
            this.btnCancelar.Text = "Excluir Recebimento";
            this.btnCancelar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // dgPagos
            // 
            this.dgPagos.AllowUserToAddRows = false;
            this.dgPagos.AllowUserToDeleteRows = false;
            this.dgPagos.AllowUserToResizeColumns = false;
            this.dgPagos.AllowUserToResizeRows = false;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPagos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            this.dgPagos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPagos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.dgPagos.ColumnHeadersHeight = 25;
            this.dgPagos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ESCOLHECANCELAR,
            this.COB_PARCELA_VALOR,
            this.DT_RECEBIMENTO});
            dataGridViewCellStyle27.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle27.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle27.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPagos.DefaultCellStyle = dataGridViewCellStyle27;
            this.dgPagos.GridColor = System.Drawing.Color.LightGray;
            this.dgPagos.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.dgPagos.Location = new System.Drawing.Point(15, 85);
            this.dgPagos.MultiSelect = false;
            this.dgPagos.Name = "dgPagos";
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPagos.RowHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.dgPagos.RowHeadersVisible = false;
            this.dgPagos.RowHeadersWidth = 33;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPagos.RowsDefaultCellStyle = dataGridViewCellStyle29;
            this.dgPagos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPagos.Size = new System.Drawing.Size(865, 154);
            this.dgPagos.TabIndex = 131;
            this.dgPagos.BackgroundColorChanged += new System.EventHandler(this.dgPagos_BackgroundColorChanged);
            this.dgPagos.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPagos_CellEndEdit);
            // 
            // ESCOLHECANCELAR
            // 
            this.ESCOLHECANCELAR.Frozen = true;
            this.ESCOLHECANCELAR.HeaderText = "";
            this.ESCOLHECANCELAR.Name = "ESCOLHECANCELAR";
            this.ESCOLHECANCELAR.TrueValue = "true";
            this.ESCOLHECANCELAR.Width = 30;
            // 
            // COB_PARCELA_VALOR
            // 
            this.COB_PARCELA_VALOR.DataPropertyName = "COBRANCA_PARCELA_VALOR";
            this.COB_PARCELA_VALOR.HeaderText = "Valor ";
            this.COB_PARCELA_VALOR.Name = "COB_PARCELA_VALOR";
            this.COB_PARCELA_VALOR.Width = 70;
            // 
            // DT_RECEBIMENTO
            // 
            this.DT_RECEBIMENTO.DataPropertyName = "DT_RECEBIMENTO";
            this.DT_RECEBIMENTO.HeaderText = "Data Recebimento";
            this.DT_RECEBIMENTO.Name = "DT_RECEBIMENTO";
            this.DT_RECEBIMENTO.Width = 150;
            // 
            // btnCBuscar
            // 
            this.btnCBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCBuscar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnCBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnCBuscar.Image")));
            this.btnCBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCBuscar.Location = new System.Drawing.Point(295, 35);
            this.btnCBuscar.Name = "btnCBuscar";
            this.btnCBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnCBuscar.TabIndex = 130;
            this.btnCBuscar.Text = "Buscar";
            this.btnCBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCBuscar.UseVisualStyleBackColor = true;
            this.btnCBuscar.Click += new System.EventHandler(this.btnCBuscar_Click);
            // 
            // dtFiltroInicial
            // 
            this.dtFiltroInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFiltroInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFiltroInicial.Location = new System.Drawing.Point(15, 51);
            this.dtFiltroInicial.Name = "dtFiltroInicial";
            this.dtFiltroInicial.Size = new System.Drawing.Size(116, 22);
            this.dtFiltroInicial.TabIndex = 3;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.btnVisualizarProdutosPagos);
            this.groupBox4.Controls.Add(this.dtFinal);
            this.groupBox4.Controls.Add(this.btnCImprimir);
            this.groupBox4.Controls.Add(this.dtInicial);
            this.groupBox4.Controls.Add(this.label7);
            this.groupBox4.Controls.Add(this.label6);
            this.groupBox4.ForeColor = System.Drawing.Color.Navy;
            this.groupBox4.Location = new System.Drawing.Point(13, 45);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(900, 96);
            this.groupBox4.TabIndex = 1;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Período para Impressão do Comprovante";
            // 
            // btnVisualizarProdutosPagos
            // 
            this.btnVisualizarProdutosPagos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVisualizarProdutosPagos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVisualizarProdutosPagos.ForeColor = System.Drawing.Color.Black;
            this.btnVisualizarProdutosPagos.Image = ((System.Drawing.Image)(resources.GetObject("btnVisualizarProdutosPagos.Image")));
            this.btnVisualizarProdutosPagos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVisualizarProdutosPagos.Location = new System.Drawing.Point(427, 24);
            this.btnVisualizarProdutosPagos.Name = "btnVisualizarProdutosPagos";
            this.btnVisualizarProdutosPagos.Size = new System.Drawing.Size(171, 45);
            this.btnVisualizarProdutosPagos.TabIndex = 127;
            this.btnVisualizarProdutosPagos.Text = "Visualizar Produtos";
            this.btnVisualizarProdutosPagos.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVisualizarProdutosPagos.UseVisualStyleBackColor = true;
            this.btnVisualizarProdutosPagos.Click += new System.EventHandler(this.btnVisualizarProdutosPagos_Click);
            // 
            // dtFinal
            // 
            this.dtFinal.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtFinal.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtFinal.Location = new System.Drawing.Point(153, 43);
            this.dtFinal.Name = "dtFinal";
            this.dtFinal.Size = new System.Drawing.Size(116, 22);
            this.dtFinal.TabIndex = 126;
            // 
            // btnCImprimir
            // 
            this.btnCImprimir.Enabled = false;
            this.btnCImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCImprimir.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCImprimir.ForeColor = System.Drawing.Color.Black;
            this.btnCImprimir.Image = ((System.Drawing.Image)(resources.GetObject("btnCImprimir.Image")));
            this.btnCImprimir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCImprimir.Location = new System.Drawing.Point(284, 24);
            this.btnCImprimir.Name = "btnCImprimir";
            this.btnCImprimir.Size = new System.Drawing.Size(137, 45);
            this.btnCImprimir.TabIndex = 125;
            this.btnCImprimir.Text = "Imprimir Comprovante";
            this.btnCImprimir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCImprimir.UseVisualStyleBackColor = true;
            this.btnCImprimir.Click += new System.EventHandler(this.btnCImprimir_Click);
            // 
            // dtInicial
            // 
            this.dtInicial.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtInicial.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtInicial.Location = new System.Drawing.Point(16, 43);
            this.dtInicial.Name = "dtInicial";
            this.dtInicial.Size = new System.Drawing.Size(116, 22);
            this.dtInicial.TabIndex = 2;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(151, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 16);
            this.label7.TabIndex = 1;
            this.label7.Text = "Data Final";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 24);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 16);
            this.label6.TabIndex = 0;
            this.label6.Text = "Data Inicial";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(6, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(0, 16);
            this.label5.TabIndex = 0;
            // 
            // frmCxRecebimentos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmCxRecebimentos";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmCxRecebimentos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmCxRecebimentos_Load);
            this.Shown += new System.EventHandler(this.frmCxRecebimentos_Shown);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcRecebimentos.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgClientes)).EndInit();
            this.tpFicha.ResumeLayout(false);
            this.tcPagto.ResumeLayout(false);
            this.tpPagar.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.gbEspecie.ResumeLayout(false);
            this.gbEspecie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEspecie)).EndInit();
            this.gbValor.ResumeLayout(false);
            this.gbValor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRecebimentos)).EndInit();
            this.tpPagos.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPagos)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtBNome;
        private System.Windows.Forms.MaskedTextBox txtBTelefone;
        private System.Windows.Forms.MaskedTextBox txtBCpf;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgClientes;
        private System.Windows.Forms.DataGridView dgRecebimentos;
        private System.Windows.Forms.GroupBox gbValor;
        private System.Windows.Forms.TextBox txtTCJuros;
        private System.Windows.Forms.RadioButton rdbCJuros;
        private System.Windows.Forms.GroupBox gbEspecie;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.DataGridView dgEspecie;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ESP_VALOR;
        private System.Windows.Forms.TextBox txtEValor;
        private System.Windows.Forms.ComboBox cmbEspecie;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtTSJuros;
        private System.Windows.Forms.Button btnPagar;
        private System.Windows.Forms.RadioButton rdbSJuros;
        private System.Windows.Forms.Button btnConfirmar;
        private System.Windows.Forms.Label lblTroco;
        private System.Windows.Forms.Button btnImprimir;
        private System.Windows.Forms.Button btnVisualizar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.DateTimePicker dtInicial;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnCImprimir;
        private System.Windows.Forms.Button btnDesmarcar;
        private System.Windows.Forms.Button btnMarcar;
        public System.Windows.Forms.TabControl tcRecebimentos;
        public System.Windows.Forms.TabPage tpGrade;
        public System.Windows.Forms.TabPage tpFicha;
        public System.Windows.Forms.TextBox txtBCodigo;
        public System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblCliente;
        public System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.DataGridView dgPagos;
        public System.Windows.Forms.Button btnCBuscar;
        public System.Windows.Forms.TabControl tcPagto;
        public System.Windows.Forms.TabPage tpPagar;
        public System.Windows.Forms.TabPage tpPagos;
        public System.Windows.Forms.DateTimePicker dtFiltroInicial;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_APELIDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_FONE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_ENDER;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_BAIRRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_UF;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_OBSERVACAO;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbClienteDebitos;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.DateTimePicker dtFiltroFinal;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ESCOLHECANCELAR;
        private System.Windows.Forms.DataGridViewTextBoxColumn COB_PARCELA_VALOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn DT_RECEBIMENTO;
        private System.Windows.Forms.DateTimePicker dtFinal;
        private System.Windows.Forms.Label lblDevedor;
        private System.Windows.Forms.Label lblEstab;
        private System.Windows.Forms.Label lblTotalSaldo;
        private System.Windows.Forms.Label lblTotalJuros;
        private System.Windows.Forms.Button btnVisualizarProdutosPagos;
        private System.Windows.Forms.DataGridViewCheckBoxColumn ESCOLHA;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_VENCIMENTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_NUM;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_VALOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_PARCELA_SALDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACRESCIMO;
        private System.Windows.Forms.DataGridViewTextBoxColumn VALOR_ATUALIZADO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DT_CADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn RD_PAGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn COBRANCA_ID;
    }
}