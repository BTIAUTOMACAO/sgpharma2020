﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing.Printing;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa
{
    public partial class frmImpressoras : Form
    {
        public frmImpressoras()
        {
            InitializeComponent();
        }

        private void frmImpressoras_Load(object sender, EventArgs e)
        {
            Principal.impressora = "";
            foreach (string impressora in PrinterSettings.InstalledPrinters)
            {
                cmbImpressora.Items.Add(impressora);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Principal.impressora = cmbImpressora.Text;
            this.Close();
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            Principal.impressora = "";
            this.Close();
        }

        private void cmbImpressora_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnOK.PerformClick();
                
        }

    }
}
