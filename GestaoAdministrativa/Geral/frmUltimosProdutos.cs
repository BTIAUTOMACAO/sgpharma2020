﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Geral
{
    public partial class frmUltimosProdutos : Form
    {
        private Produto dadosProdutos = new Produto();
        private int clienteID;

        public frmUltimosProdutos(int cliID)
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
            clienteID = cliID;
        }

        private void frmUltimosProdutos_Load(object sender, EventArgs e)
        {
            try
            {
                dtInicial.Value = DateTime.Now.AddDays(-90);
                dtFinal.Value = DateTime.Now;

                dgProdutos.DataSource = dadosProdutos.UltimosProdutosPorCliente(Principal.estAtual, Principal.empAtual, dtInicial.Value, dtFinal.Value, clienteID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Últimos Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnFiltrar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnFiltrar, "Filtrar");
        }

        private void btnCancela_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCancela, "Fechar");
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dtFinal.Value < dtInicial.Value)
                {
                    MessageBox.Show("Data Final menor que a Data Inicial!", "Controle de Entrega", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtFinal.Focus();
                    return;
                }

                dgProdutos.DataSource = dadosProdutos.UltimosProdutosPorCliente(Principal.estAtual, Principal.empAtual, dtInicial.Value, dtFinal.Value, clienteID);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Últimos Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void dtInicial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void dtFinal_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnFiltrar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnCancela_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                this.Close();
            }
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
