﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Geral
{
    public partial class frmBuscaMedico : Form
    {
        public frmBuscaMedico()
        {
            InitializeComponent();
        }

        private void btnBusca_Click(object sender, EventArgs e)
        {
            BuscaMedicos();
        }

        private void SelecionaLinha()
        {
            int indiceGrid = dgMedicos.CurrentCell.RowIndex;

            Principal.medico = dgMedicos.Rows[indiceGrid].Cells["MED_NOME"].Value.ToString();
            Principal.uf = dgMedicos.Rows[indiceGrid].Cells["MED_UF"].Value.ToString();
            Principal.crm = dgMedicos.Rows[indiceGrid].Cells["MED_CODIGO"].Value.ToString();

            this.Close();
        }

        public void BuscaMedicos()
        {
            Cursor = Cursors.WaitCursor;
            Medico med = new Medico();
            DataTable dtMedico = med.BuscaDados(txtCrmCro.Text.Trim(), txtNome.Text.Trim());
            dgMedicos.DataSource = dtMedico;
            lblRegistros.Text = dtMedico.Rows.Count + " Registros Encontrados";
            dgMedicos.Focus();
            Cursor = Cursors.Default;
        }

        private void dgMedicos_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelecionaLinha();
        }

        private void dgMedicos_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                SelecionaLinha();
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                this.Close();
                return true;
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void txtCrmCro_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13)
            {
                BuscaMedicos();
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                BuscaMedicos();
            }
        }

        private void frmBuscaMedico_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            lblRegistros.Text = dgMedicos.Rows.Count + " Registros Encontrados";
        }
    }
}
