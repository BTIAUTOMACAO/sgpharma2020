﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa
{
    public partial class frmEstEscolha : Form
    {
        public frmEstEscolha()
        {
            InitializeComponent();
        }

        private void frmEmpEscolha_Load(object sender, EventArgs e)
        {
            try
            {
                cmbEmpresa.Items.Add("");
                cmbEmpresa.DataSource = Util.SelecionaRegistrosTodosOuEspecifico("ESTABELECIMENTOS", "", "EST_DESABILITADO", "'N'", false, "EST_CODIGO");
                cmbEmpresa.DisplayMember = "EST_FANTASIA";
                cmbEmpresa.ValueMember = "EST_CODIGO";

                cmbEmpresa.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbEmpresa_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (cmbEmpresa.SelectedIndex != -1)
            {
                Principal.estAtual = Convert.ToInt32(cmbEmpresa.SelectedValue);
                Principal.empAtual = Convert.ToInt32(cmbEmpresa.SelectedValue);
                this.Close();
            }
        }
    }
}
