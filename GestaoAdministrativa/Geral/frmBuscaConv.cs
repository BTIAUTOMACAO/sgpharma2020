﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Geral
{
    public partial class frmBuscaConv : Form
    {
        private int indiceGrid;
        public bool sValida;

        public frmBuscaConv()
        {
            InitializeComponent();
        }

        private void frmBuscaConv_Load(object sender, EventArgs e)
        {
            sValida = false;
            Principal.idCliFor = "";
            Principal.nomeCliFor = "";
            indiceGrid = 0;
        }

        private void dgBuscaConv_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                Principal.idCliFor = dgBuscaConv.Rows[e.RowIndex].Cells[0].Value.ToString();
                Principal.nomeCliFor = dgBuscaConv.Rows[indiceGrid].Cells[1].Value.ToString();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Pesquisa de Emp. Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgBuscaConv_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    Principal.idCliFor = dgBuscaConv.Rows[indiceGrid].Cells[0].Value.ToString();
                    Principal.nomeCliFor = dgBuscaConv.Rows[indiceGrid].Cells[1].Value.ToString();
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Pesquisa de Emp. Conveniadas", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgBuscaConv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indiceGrid = e.RowIndex;
        }

        private void dgBuscaConv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                Principal.nomeCliFor = "";
                Principal.idCliFor = "";
                indiceGrid = 0;
                this.Close();
            }
            int indice = dgBuscaConv.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && indiceGrid < indice)
            {
                indiceGrid = indiceGrid + 1;
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && indiceGrid <= indice)
                {
                    indiceGrid = indiceGrid - 1;
                }
        }

        private void frmBuscaConv_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                sValida = false;
                this.Close();
            }
        }
    }
}
