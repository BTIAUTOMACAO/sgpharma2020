﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa
{
    public partial class frmOcorrencia : Form
    {
        public frmOcorrencia()
        {
            InitializeComponent();
        }

        private void frmOcorrencia_Load(object sender, EventArgs e)
        {
            txtData.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtHora.Text = DateTime.Now.ToString("HH:mm:ss");
            txtNome.Focus();
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtMotivo.Focus();
            }
        }

        private void txtMotivo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnConfirma.PerformClick();
            }
        }

        private void btnConfirma_Click(object sender, EventArgs e)
        {
            if (txtNome.Text == "")
            {
                MessageBox.Show("Informe o nome do solicitante.", "Ocorrência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNome.Focus();
            }
            else
                if (txtMotivo.Text == "")
                {
                    MessageBox.Show("Informe o motivo da ocorrência.", "Ocorrência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMotivo.Focus();
                }
                else
                {
                    Principal.data = txtData.Text + " " + txtHora.Text;
                    Principal.motivo = "Solicitante: " + txtNome.Text + " - Motivo: " + txtMotivo.Text;
                    this.Close();
                }
        }
    }
}
