﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Geral
{
    public partial class frmParametros : Form, Botoes 
    {
        #region DECLARAÇÃO DAS VARIÁVEIS
        private DataTable dtGeral = new DataTable();
        private DataTable dtCadastro = new DataTable();
        private DataTable dtVendas = new DataTable();
        private DataTable dtSngpc = new DataTable();
        private DataTable dtCaixa = new DataTable();
        private DataTable dtEstoque = new DataTable();
        private DataTable dtPagar = new DataTable();
        private DataTable dtReceber = new DataTable();
        private DataTable dtImpressora = new DataTable();
        private DataTable dtSAT = new DataTable();
        private DataTable dtBeneficios = new DataTable();
        private ToolStripButton tsbParametros = new ToolStripButton("Parâmetros");
        private ToolStripSeparator tssParametros = new ToolStripSeparator();
        private Parametro dadosParametro = new Parametro();
        #endregion

        public frmParametros(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        private void frmParametros_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmParametros_Shown(object sender, EventArgs e)
        {
            CarregarGrid();

            CriaParametros.CriaOuAtualizaParametros();
        }

        public bool CarregarGrid()
        {
            try
            {
                var busca = new Parametro();

                //MODULO GERAL//
                dtGeral = busca.BuscarParametrosPorModulo("GERAL");
                dgGeral.DataSource = dtGeral;

                //MODULO CADASTRO//
                dtCadastro = busca.BuscarParametrosPorModulo("CADASTRO");
                dgCadastro.DataSource = dtCadastro;

                //MODULO VENDAS//
                dtVendas = busca.BuscarParametrosPorModulo("VENDAS");
                dgVendas.DataSource = dtVendas;

                //MODULO SNGPC//
                dtSngpc = busca.BuscarParametrosPorModulo("SNGPC");
                dgSngpc.DataSource = dtSngpc;

                //MODULO CAIXA//
                dtCaixa = busca.BuscarParametrosPorModulo("CAIXA");
                dgCaixa.DataSource = dtCaixa;

                //MODULO ESTOQUE//
                dtEstoque = busca.BuscarParametrosPorModulo("ESTOQUE");
                dgEstoque.DataSource = dtEstoque;

                //MODULO CONTAS A PAGAR//
                dtPagar = busca.BuscarParametrosPorModulo("PAGAR");
                dgPagar.DataSource = dtPagar;

                //MODULO CONTAS A RECEBER//
                dtReceber = busca.BuscarParametrosPorModulo("RECEBER");
                dgReceber.DataSource = dtReceber;

                //MODULO IMPRESSORA//
                dtImpressora = busca.BuscarParametrosPorModulo("IMPRESSORA");
                dgImpressora.DataSource = dtImpressora;

                //MODULO SAT//
                dtSAT = busca.BuscarParametrosPorModulo("SAT");
                dgSAT.DataSource = dtSAT;

                //MODULO BENEFICIOS//
                dtBeneficios = busca.BuscarParametrosPorModulo("BENEFICIOS");
                dgBeneficios.DataSource = dtBeneficios;

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void dgGeral_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Parametro dParametro = new Parametro()
                {
                    ParTabela = Convert.ToInt32(dgGeral.Rows[e.RowIndex].Cells[0].Value),
                    ParPosicao = dgGeral.Rows[e.RowIndex].Cells[1].Value.ToString(),
                    ParDescricao = dgGeral.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                    ParModulo = "GERAL",
                    ParEstacao = dgGeral.Rows[e.RowIndex].Cells[2].Value.ToString()
                };

                dadosParametro.AtualizaParDescricao(dParametro);

            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgCadastro_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Parametro dParametro = new Parametro()
                {
                    ParTabela = Convert.ToInt32(dgCadastro.Rows[e.RowIndex].Cells[0].Value),
                    ParPosicao = dgCadastro.Rows[e.RowIndex].Cells[1].Value.ToString(),
                    ParDescricao = dgCadastro.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                    ParModulo = "CADASTRO",
                    ParEstacao = dgCadastro.Rows[e.RowIndex].Cells[2].Value.ToString()
                };

                dadosParametro.AtualizaParDescricao(dParametro);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgVendas_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Parametro dParametro = new Parametro()
                {
                    ParTabela = Convert.ToInt32(dgVendas.Rows[e.RowIndex].Cells[0].Value),
                    ParPosicao = dgVendas.Rows[e.RowIndex].Cells[1].Value.ToString(),
                    ParDescricao = dgVendas.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                    ParModulo = "VENDAS",
                    ParEstacao = dgVendas.Rows[e.RowIndex].Cells[2].Value.ToString()
                };

                dadosParametro.AtualizaParDescricao(dParametro);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgSngpc_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Parametro dParametro = new Parametro()
                {
                    ParTabela = Convert.ToInt32(dgSngpc.Rows[e.RowIndex].Cells[0].Value),
                    ParPosicao = dgSngpc.Rows[e.RowIndex].Cells[1].Value.ToString(),
                    ParDescricao = dgSngpc.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                    ParModulo = "SNGPC",
                    ParEstacao = dgSngpc.Rows[e.RowIndex].Cells[2].Value.ToString()
                };

                dadosParametro.AtualizaParDescricao(dParametro);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgCaixa_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Parametro dParametro = new Parametro()
                {
                    ParTabela = Convert.ToInt32(dgCaixa.Rows[e.RowIndex].Cells[0].Value),
                    ParPosicao = dgCaixa.Rows[e.RowIndex].Cells[1].Value.ToString(),
                    ParDescricao = dgCaixa.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                    ParModulo = "CAIXA",
                    ParEstacao = dgCaixa.Rows[e.RowIndex].Cells[2].Value.ToString()
                };

                dadosParametro.AtualizaParDescricao(dParametro);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgEstoque_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Parametro dParametro = new Parametro()
                {
                    ParTabela = Convert.ToInt32(dgEstoque.Rows[e.RowIndex].Cells[0].Value),
                    ParPosicao = dgEstoque.Rows[e.RowIndex].Cells[1].Value.ToString(),
                    ParDescricao = dgEstoque.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                    ParModulo = "ESTOQUE",
                    ParEstacao = dgEstoque.Rows[e.RowIndex].Cells[2].Value.ToString()
                };

                dadosParametro.AtualizaParDescricao(dParametro);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgPagar_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Parametro dParametro = new Parametro()
                {
                    ParTabela = Convert.ToInt32(dgPagar.Rows[e.RowIndex].Cells[0].Value),
                    ParPosicao = dgPagar.Rows[e.RowIndex].Cells[1].Value.ToString(),
                    ParDescricao = dgPagar.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                    ParModulo = "PAGAR",
                    ParEstacao = dgPagar.Rows[e.RowIndex].Cells[2].Value.ToString()
                };

                dadosParametro.AtualizaParDescricao(dParametro);
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgReceber_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Parametro dParametro = new Parametro()
                {
                    ParTabela = Convert.ToInt32(dgReceber.Rows[e.RowIndex].Cells[0].Value),
                    ParPosicao = dgReceber.Rows[e.RowIndex].Cells[1].Value.ToString(),
                    ParDescricao = dgReceber.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                    ParModulo = "RECEBER",
                    ParEstacao = dgReceber.Rows[e.RowIndex].Cells[2].Value.ToString()
                };

                dadosParametro.AtualizaParDescricao(dParametro);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgImpressora_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Parametro dParametro = new Parametro()
                {
                    ParTabela = Convert.ToInt32(dgImpressora.Rows[e.RowIndex].Cells[0].Value),
                    ParPosicao = dgImpressora.Rows[e.RowIndex].Cells[1].Value.ToString(),
                    ParDescricao = dgImpressora.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                    ParModulo = "IMPRESSORA",
                    ParEstacao = dgImpressora.Rows[e.RowIndex].Cells[2].Value.ToString()
                };

                dadosParametro.AtualizaParDescricao(dParametro);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgSAT_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Parametro dParametro = new Parametro()
                {
                    ParTabela = Convert.ToInt32(dgSAT.Rows[e.RowIndex].Cells[0].Value),
                    ParPosicao = dgSAT.Rows[e.RowIndex].Cells[1].Value.ToString(),
                    ParDescricao = dgSAT.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                    ParEstacao = dgSAT.Rows[e.RowIndex].Cells[2].Value.ToString(),
                    ParModulo = "SAT"
                };

                dadosParametro.AtualizaParDescricao(dParametro);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgBeneficios_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Parametro dParametro = new Parametro()
                {
                    ParTabela = Convert.ToInt32(dgBeneficios.Rows[e.RowIndex].Cells[0].Value),
                    ParPosicao = dgBeneficios.Rows[e.RowIndex].Cells[1].Value.ToString(),
                    ParDescricao = dgBeneficios.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString(),
                    ParModulo = "BENEFICIOS",
                    ParEstacao = dgBeneficios.Rows[e.RowIndex].Cells[2].Value.ToString()
                };

                dadosParametro.AtualizaParDescricao(dParametro);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        
        public void Botao()
        {
            try
            {
                this.tsbParametros.AutoSize = false;
                this.tsbParametros.Image = Properties.Resources.parametros;
                this.tsbParametros.Size = new System.Drawing.Size(125, 20);
                this.tsbParametros.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbParametros);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssParametros);
                tsbParametros.Click += delegate
                {
                    var parametros = Application.OpenForms.OfType<frmParametros>().FirstOrDefault();
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");

                    parametros.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbParametros);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssParametros);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Parametrização do Sistema", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Primeiro() 
        {
            Principal.mdiPrincipal.btnExcluir.Enabled = false;
            Principal.mdiPrincipal.btnIncluir.Enabled = false;
            Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
        }

        public void Proximo() { }

        public void Ultimo() { }

        public void Anterior() { }

        public void ImprimirRelatorio() { }

        public bool Incluir()
        {
            return true;
        }

        public bool Atualiza()
        {
            return true;
        }

        public bool Excluir()
        {
            return true;
        }

        public void Limpar() { }

        public void AtalhoGrade() { }

        public void AtalhoFicha() { }

        public void Ajuda() { }

       
    }
}
