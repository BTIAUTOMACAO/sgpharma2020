﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Geral
{
    public partial class frmChamados : Form
    {
        public string caminho;

        public frmChamados()
        {
            InitializeComponent();
        }

        private void frmChamados_Load(object sender, EventArgs e)
        {
            Limpar();
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                txtTelefone.Focus();
            }
        }

        private void cmbSetor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                txtContato.Focus();
            }
        }

        private void txtContato_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                txtMensagem.Focus();
            }
        }

        private void txtEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void cmbSetor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void txtContato_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void txtMensagem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void btnAnexar_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void btnEnviarChamado_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void frmChamados_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void btnEnviarChamado_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtEmail.Text))
                {
                    MessageBox.Show("Necessário informar um E-mail", "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail.Focus();
                }
                else if (String.IsNullOrEmpty(Funcoes.RemoveCaracter(txtTelefone.Text)))
                {
                    MessageBox.Show("Necessário informar um Telefone", "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtEmail.Focus();
                }
                else if (cmbSetor.SelectedIndex == -1)
                {
                    MessageBox.Show("Necessário informar o Setor", "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbSetor.Focus();
                }
                else if (String.IsNullOrEmpty(txtMensagem.Text))
                {
                    MessageBox.Show("Necessário informar a Mensagem", "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMensagem.Focus();
                }
                else
                {
                    Cursor = Cursors.WaitCursor;

                    SmtpClient client = new SmtpClient("smtp.cdconectec.com.br");
                    client.Port = 587;
                    client.EnableSsl = false;
                    client.Timeout = 100000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;

                    client.Credentials = new System.Net.NetworkCredential("sgpharma@cdconectec.com.br", "Sat@#2019");
                    MailMessage msg = new MailMessage();
                    msg.To.Add(new MailAddress("ariane@drogabella.com.br"));
                    msg.To.Add(new MailAddress("lucia.bti@drogabella.com.br"));
                    msg.To.Add(new MailAddress("carolyne@drogabella.com.br"));
                    if (!String.IsNullOrEmpty(caminho))
                    {
                        Attachment anexar = new Attachment(caminho);
                        msg.Attachments.Add(anexar);
                    }

                    msg.From = new MailAddress("sgpharma@cdconectec.com.br");
                    msg.Subject = "Chamado sobre " + cmbSetor.Text + " - " + Principal.nomeAtual;
                    msg.Body = "Contato: " + txtContato.Text + "\nTelefone: " + txtTelefone.Text + "\nMensagem:" +  txtMensagem.Text + "\n\nEmail da Farmácia: " + txtEmail.Text;

                    client.Send(msg);

                    MessageBox.Show("E-mail enviado com sucesso! Aguarde contato da resolução.", "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpar();

                    if(MessageBox.Show("Deseja fechar a Tela de Chamado?","Chamado",MessageBoxButtons.YesNo,MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                    {
                        this.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void Limpar()
        {
            txtEmail.Text = "";
            txtContato.Text = "";
            cmbSetor.SelectedIndex = -1;
            txtMensagem.Text = "";
            lblCaminho.Text = "";
            caminho = "";
            txtEmail.Focus();
        }

        private void btnAnexar_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                caminho = openFileDialog1.FileName;
                lblCaminho.Text = caminho;
            }
            else
            {
                lblCaminho.Text = "";
            }
        }

        private void txtTelefone_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
                this.Close();
        }

        private void txtTelefone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                cmbSetor.Focus();
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}

