﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Geral
{
    public partial class frmConsultarEstoqueFranquias : Form
    {
        private string codBusca;
        private string descProduto;
        private string enderecoWebService;
        private string codLoja;
        private string codGrupo;
        private int indiceGrid;
        private int idReserva;

        public frmConsultarEstoqueFranquias(string codBarras, string descricao)
        {
            this.codBusca = codBarras;
            this.descProduto = descricao;
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        private void frmConsultarEstoqueFranquias_Load(object sender, EventArgs e)
        {
            indiceGrid = 0;
            codLoja = Funcoes.LeParametro(9, "52", true);
            codGrupo = Funcoes.LeParametro(9, "53", true);
            enderecoWebService = Funcoes.LeParametro(9, "54", true);

            if (!String.IsNullOrEmpty(codBusca))
            {
                txtCodigo.Text = codBusca;
                btnBuscar.PerformClick();
            }
            else
            {
                txtCodigo.Text = "";
                txtCodigo.Focus();
            }

        }

        private async void btnBuscar_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(txtCodigo.Text.Trim()))
            {
                dgProdutos.Rows.Clear();
                await ConsultarFiliais(codGrupo, codLoja, txtCodigo.Text.Trim());
            }
            else
            {
                MessageBox.Show("Código de Barras não pode ser em branco!", "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodigo.Focus();
            }
        }

        private async Task ConsultarFiliais(string grupoId, string codEstab, string codBarras)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Produtos/BuscaProduto?grupoId=" + grupoId + "&codBarras=" + codBarras + "&codEstab=" + codEstab);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    List<Negocio.EstoqueFilial> retorno = JsonConvert.DeserializeObject<List<Negocio.EstoqueFilial>>(responseBody);
                    if (retorno[0].codErro.Equals("00"))
                    {
                        dgProdutos.Rows.Clear();
                        for (int i = 0; i < retorno.Count; i++)
                        {
                            dgProdutos.Rows.Insert(dgProdutos.RowCount, new Object[] {
                                retorno[i].codEstabelecimento, retorno[i].codBarras, retorno[i].quantidade, retorno[i].precoCusto, retorno[i].precoVenda,
                                retorno[i].descricao, retorno[i].descEstab, retorno[i].telefone
                            });
                        }
                        dgProdutos.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Produto não encontrado em outras Filiais", "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodigo.Text = "";
                        txtCodigo.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Problema de conexão com o servidor, verifique o Conexão com Internet ou contate o Suporte\n\rErro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }

        private void txtCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dgProdutos.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && indiceGrid < indice)
            {
                indiceGrid = indiceGrid + 1;
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && indiceGrid < indice)
            {
                indiceGrid = indiceGrid - 1;
            }

            teclaAtalho(e);
        }

        private void frmConsultarEstoqueFranquias_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void frmConsultarEstoqueFranquias_Shown(object sender, EventArgs e)
        {
            Principal.dtPesq = Util.CarregarCombosPorEmpresa("COL_CODIGO", "COL_NOME", "COLABORADORES", true, "COL_DESABILITADO = 'N'");
            if (Principal.dtPesq.Rows.Count > 0)
            {
                cmbVendedor.DataSource = Principal.dtPesq;
                cmbVendedor.DisplayMember = "COL_NOME";
                cmbVendedor.ValueMember = "COL_CODIGO";
                cmbVendedor.SelectedIndex = -1;
            }
        }

        private void btnBuscar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnBuscar, "Consultar Filiais");
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void btnLimpar_Click(object sender, EventArgs e)
        {
            txtCodigo.Text = "";
            dgProdutos.Rows.Clear();
            txtCodigo.Focus();
        }

        private void btnReservar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgProdutos.RowCount > 0)
                {
                    bool achou = false;
                    if (!String.IsNullOrEmpty(dgProdutos.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString()))
                    {
                        for (int i = 0; i < dgReserva.RowCount; i++)
                        {
                            if (dgProdutos.Rows[indiceGrid].Cells["COD_LOJA"].Value.ToString() != dgReserva.Rows[i].Cells["ID_LOJA"].Value.ToString())
                            {
                                MessageBox.Show("Reserva somente para a mesma Filial dos demais Produtos", "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }

                            if (dgProdutos.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString() == dgReserva.Rows[i].Cells["COD_BARRAS"].Value.ToString())
                            {
                                dgReserva.Rows[i].Cells["QTDE"].Value = Convert.ToInt32(dgReserva.Rows[i].Cells["QTDE"].Value) + 1;
                                achou = true;
                            }
                        }

                        if (!achou)
                        {
                            dgReserva.Rows.Insert(dgReserva.RowCount, new Object[] { dgProdutos.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString(),
                            dgProdutos.Rows[indiceGrid].Cells["PROD_DESCR"].Value.ToString(),
                            1,
                            dgProdutos.Rows[indiceGrid].Cells["NOME_FILIAL"].Value.ToString(),
                            dgProdutos.Rows[indiceGrid].Cells["COD_LOJA"].Value.ToString() });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgReserva_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void dgProdutos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indiceGrid = e.RowIndex;
        }

        private async void btnConfirmarReserva_Click(object sender, EventArgs e)
        {
            try
            {
                if(dgReserva.Rows.Count > 0)
                {
                    if (cmbVendedor.SelectedIndex != -1)
                    {
                        if (MessageBox.Show("Confirma a Reserva de Produtos?", "Consulta Filiais", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            int id = Funcoes.IdentificaVerificaID("RESERVA", "CODIGO");
                            Cursor = Cursors.WaitCursor;

                            bool retorno = await InsereReserva();
                            
                            if(retorno)
                            {
                                var reserva = new Reserva();
                                reserva.EmpCodigo = Principal.empAtual;
                                reserva.EstCodigo = Principal.estAtual;
                                reserva.ID = id;
                                reserva.Codigo = idReserva;
                                reserva.Solicitante = cmbVendedor.Text;
                                reserva.LojaOrigem = Convert.ToInt32(Funcoes.LeParametro(9, "52", true));
                                reserva.LojaDestino = Convert.ToInt32(dgReserva.Rows[0].Cells["ID_LOJA"].Value);
                                reserva.DtCadastro = DateTime.Now;
                                reserva.OpCadastro = Principal.usuario;
                                reserva.Status = "P";

                                BancoDados.AbrirTrans();

                                if (reserva.InsereRegistros(reserva))
                                {
                                    var reservaItens = new ReservaItens();

                                    for(int i=0; i < dgReserva.RowCount; i++)
                                    {
                                        reservaItens.EmpCodigo = Principal.empAtual;
                                        reservaItens.EstCodigo = Principal.estAtual;
                                        reservaItens.ID = id;
                                        reservaItens.CodBarras = dgReserva.Rows[i].Cells["COD_BARRAS"].Value.ToString();
                                        reservaItens.Qtde = Convert.ToInt32(dgReserva.Rows[i].Cells["QTDE"].Value);

                                        if(!reservaItens.InsereRegistros(reservaItens))
                                        {
                                            BancoDados.ErroTrans();
                                            return;
                                        }
                                    }

                                    BancoDados.FecharTrans();

                                    MessageBox.Show("Reserva Solicitada com Sucesso", "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    this.Close();
                                }
                                else
                                    BancoDados.ErroTrans();
                            }
                        }
                        else
                            return;
                    }
                    else
                    {
                        MessageBox.Show("Selecione o Vendedor", "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbVendedor.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Necessário ao menos um produto para realizar a Reserva", "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodigo.Focus();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void cmbVendedor_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnConfirmarReserva_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        public async Task<bool> InsereReserva()
        {
            try
            {
                idReserva = await EnviaTransacao();

                if (idReserva != 0)
                {
                    for (int i = 0; i < dgReserva.RowCount; i++)
                    {
                        using (var client = new HttpClient())
                        {
                            Cursor = Cursors.WaitCursor;
                            client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                            HttpResponseMessage response = await client.GetAsync("Reserva/InsereItensReservaDeProdutos?id=" + idReserva
                                            + "&prodCodigo=" + dgReserva.Rows[i].Cells["COD_BARRAS"].Value.ToString()
                                            + "&qtde=" + Convert.ToInt32(dgReserva.Rows[i].Cells["QTDE"].Value));
                            if (!response.IsSuccessStatusCode)
                                return false;
                        }
                    }
                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Consulta Filiais", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<int> EnviaTransacao()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Reserva/InsereReservaDeProdutos?codEstab=" + Funcoes.LeParametro(9, "52", true)
                        + "&grupoID=" + Funcoes.LeParametro(9, "53", true) + "&estabDestino=" + dgReserva.Rows[0].Cells["ID_LOJA"].Value
                        + "&solicitante=" + cmbVendedor.Text + "&usuario=" + Principal.usuario);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    return Convert.ToInt32(responseBody);
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return 0;
            }
        }
    }
}
