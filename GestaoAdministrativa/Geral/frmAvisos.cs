﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Geral
{
    public partial class frmAvisos : Form
    {
        public frmAvisos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
        }

        private async void frmAvisos_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;

            Rectangle workingArea = Screen.GetWorkingArea(Principal.mdiPrincipal);
            this.Location = new Point((workingArea.Right - 10) - Size.Width,
                                      0);
            

            List<Avisos> dados = await BuscaAvisos();

            if (dados[0].CodErro == "00")
            {
                for (int i = 0; i < dados.Count; i++)
                {
                    txtMensagem.Text += (dados[i].Aviso) + "\r\n\r\n";

                    if (!string.IsNullOrEmpty(dados[i].Url))
                    {
                        WebClient ftpClient = new WebClient();
                        ftpClient.Credentials = new NetworkCredential("conectectecnologia", "Rpc@#2019Bella");

                        byte[] imageByte = ftpClient.DownloadData(dados[i].Url);

                        MemoryStream mStream = new MemoryStream();
                        byte[] pData = imageByte;
                        mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                        Bitmap bm = new Bitmap(mStream, false);
                        mStream.Dispose();

                        pictureBox1.Image = bm;
                        if (bm.Width > pictureBox1.ClientSize.Width || bm.Height > pictureBox1.ClientSize.Height)
                        {
                            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage; //if image is larger than picturebox
                        }
                        else
                        {
                            pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage; //'if image is smaller than picturebox
                        }
                    }
                }
            }
            else if (dados[0].CodErro == "01")
            {
                txtMensagem.Text += dados[0].Mensagem;
            }
        }

        public async Task<List<Avisos>> BuscaAvisos()
        {
            List<Avisos> allItems = new List<Avisos>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Chamados/BuscaAvisos?codEstabelecimento=" + Funcoes.LeParametro(9, "52", true));
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<Avisos>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }
    }
}
