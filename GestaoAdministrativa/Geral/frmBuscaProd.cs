﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Geral;
using System.Net.Http;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace GestaoAdministrativa
{
    public partial class frmBuscaProd : Form
    {
        private int indiceGrid;
        private string filtroProd;
        private DataTable dtBuscaProd = new DataTable();
        private string prodDesc, prodCod, prodPreco, prodFiltro, prodPri;
        private DataRow[] linha_resultado;
        private bool fVendas;

        public frmBuscaProd(bool vendas)
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
            fVendas = vendas;
        }

        private void frmBuscaProd_Load(object sender, EventArgs e)
        {
            try
            {
                this.StartPosition = FormStartPosition.CenterScreen;
                indiceGrid = 0;
                Principal.codBarra = "";

                cmbPriAtivo.DataSource = Util.CarregarCombosPorEmpresa("PRI_CODIGO", "PRI_DESCRICAO", "PRINCIPIO_ATIVO", true, "PRI_DESABILITADO = 'N'");
                cmbPriAtivo.DisplayMember = "PRI_DESCRICAO";
                cmbPriAtivo.ValueMember = "PRI_CODIGO";
                cmbPriAtivo.SelectedIndex = -1;


                if (String.IsNullOrEmpty(txtDescr.Text.Trim()) && txtPreco.Text.Equals("0,00"))
                {
                    txtDescr.Focus();
                }
                else
                {
                    btnBuscar.PerformClick();
                }

                if (Funcoes.LeParametro(9, "51", false).Equals("S"))
                {
                    btnConsultarLojas.Visible = true;
                }
                else
                    btnConsultarLojas.Visible = false;

                if(fVendas)
                {
                    btnCancela.Enabled = true;
                }
                else
                {
                    btnCancela.Enabled = false;
                }

                txtDescr.Focus();

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Pesquisa de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtCodigo.Text.Trim()))
                {
                    btnBuscar.PerformClick();
                }
                else
                    txtCodigo.Focus();
            }
        }

        private void txtDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (!String.IsNullOrEmpty(txtDescr.Text.Trim()))
                {
                    btnBuscar.PerformClick();
                }
                else
                    txtCodigo.Focus();
            }
        }

        private void cmbPriAtivo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbPriAtivo.SelectedIndex != -1)
                {
                    btnBuscar.PerformClick();
                }
                else
                    txtPreco.Focus();
            }
        }

        private void btnFiltros_Click(object sender, EventArgs e)
        {
            filtroProd = "";
            filtroProd = Funcoes.FiltraProdutos("B", "B", "B", "B");
            btnBuscar.PerformClick();
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            if (fVendas)
            {
                for (int i = 0; i < dgProd.RowCount; i++)
                {
                    if (dgProd.Rows[i].Selected == true)
                    {
                        Principal.codBarra += dgProd.Rows[i].Cells[0].Value.ToString() + ",";
                    }
                }
            }

            this.Close();
        }

        public bool BuscaProd(string descricao = "", bool verificaPromo = false, double valor = 0, string codBarra = "")
        {
            try
            {
                prodDesc = "";
                prodCod = "";
                prodPreco = "";
                prodPri = "";

                Application.DoEvents();
                Cursor = Cursors.WaitCursor;

                indiceGrid = 0;

                if (descricao != "" || txtDescr.Text.Trim() != "")
                {
                    if (txtDescr.Text.Trim() != "")
                    {
                        prodDesc = txtDescr.Text.Trim().ToUpper();
                    }
                    else if (descricao != "")
                    {
                        prodDesc = descricao;
                    }
                }
                if (valor != 0 || Convert.ToDecimal(txtPreco.Text) > 0)
                {
                    if (valor != 0)
                    {
                        prodPreco = Funcoes.BValor(valor);
                    }
                    else
                    {
                        prodPreco = Funcoes.BValor(Convert.ToDouble(txtPreco.Text));
                    }
                }
                if (txtCodigo.Text.Trim() != "" || codBarra != "")
                {
                    if (txtCodigo.Text.Trim() != "")
                    {
                        prodCod = txtCodigo.Text;
                    }
                    else if (codBarra != "")
                    {
                        prodCod = codBarra;
                    }
                }
                if (cmbPriAtivo.SelectedIndex != -1)
                {
                    prodPri = cmbPriAtivo.SelectedValue.ToString();
                }
                if (!String.IsNullOrEmpty(filtroProd))
                {
                    prodFiltro = filtroProd;
                }
                var buscaProduto = new Produto();

                dtBuscaProd = buscaProduto.BuscaDadosProdutosPesquisaDetalhada(Principal.estAtual, Principal.empAtual, prodDesc, prodCod, prodPreco, prodPri, prodFiltro);

                if (dtBuscaProd.Rows.Count != 0)
                {
                    if (verificaPromo == true)
                    {
                        for (int i = 0; i < dtBuscaProd.Rows.Count; i++)
                        {
                            Application.DoEvents();
                            #region VERIFICA SE PRODUTO ESTA EM PROMOCAO
                            Principal.dtRetorno = Funcoes.IdentificaProdutoEmPromocao(Principal.empAtual, Convert.ToInt32(dtBuscaProd.Rows[i]["PROD_ID"]),
                                dtBuscaProd.Rows[i]["DEP_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dtBuscaProd.Rows[i]["DEP_CODIGO"]),
                                dtBuscaProd.Rows[i]["CLAS_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dtBuscaProd.Rows[i]["CLAS_CODIGO"]),
                                dtBuscaProd.Rows[i]["SUB_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dtBuscaProd.Rows[i]["SUB_CODIGO"]));
                            if (Principal.dtRetorno.Rows.Count != 0)
                            {
                                //VERIFICA SE RETORNOU MAIS DE UMA CONDIÇÃO DE DESCONTO//
                                if (Principal.dtRetorno.Rows.Count == 1)
                                {
                                    if (Principal.dtRetorno.Rows[0]["PROMO_TIPO"].Equals("FIXO"))
                                    {
                                        if (DateTime.Now.Day >= Convert.ToInt32(Principal.dtRetorno.Rows[0]["DATA_INI"]) && DateTime.Now.Day <= Convert.ToInt32(Principal.dtRetorno.Rows[0]["DATA_FIM"]))
                                        {
                                            linha_resultado = dtBuscaProd.Select("COD_BARRA = '" + dtBuscaProd.Rows[i]["COD_BARRA"] + "'");
                                            linha_resultado[0].BeginEdit();
                                            linha_resultado[0]["PROD_PROMO"] = "S";
                                            if (Convert.ToDecimal(Principal.dtRetorno.Rows[0]["PROMO_PRECO"]) > 0)
                                            {
                                                linha_resultado[0]["PRE_VALOR"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(Principal.dtRetorno.Rows[0]["PROMO_PRECO"]), 2));
                                            }
                                            else if (Convert.ToDecimal(Principal.dtRetorno.Rows[0]["PROMO_PORC"]) > 0)
                                            {
                                                linha_resultado[0]["DESC_MAX"] = Principal.dtRetorno.Rows[0]["PROMO_PORC"].ToString();
                                            }
                                            linha_resultado[0].EndEdit();
                                        }
                                    }
                                    else if (Principal.dtRetorno.Rows[0]["PROMO_TIPO"].Equals("DINAMICO"))
                                    {
                                        if (DateTime.Now >= Convert.ToDateTime(Convert.ToDateTime(Principal.dtRetorno.Rows[0]["DATA_INI"]).ToString("dd/MM/yyyy 00:00:00"))
                                            && DateTime.Now <= Convert.ToDateTime(Convert.ToDateTime(Principal.dtRetorno.Rows[0]["DATA_FIM"]).ToString("dd/MM/yyyy 23:59:59")))
                                        {
                                            linha_resultado = dtBuscaProd.Select("PROD_CODIGO = '" + dtBuscaProd.Rows[i]["PROD_CODIGO"] + "'");
                                            linha_resultado[0].BeginEdit();
                                            linha_resultado[0]["PROD_PROMO"] = "S";
                                            if (Convert.ToDecimal(Principal.dtRetorno.Rows[0]["PROMO_PRECO"]) > 0)
                                            {
                                                linha_resultado[0]["PRE_VALOR"] = String.Format("{0:N}", Math.Round(Convert.ToDecimal(Principal.dtRetorno.Rows[0]["PROMO_PRECO"]), 2));
                                            }
                                            else if (Convert.ToDecimal(Principal.dtRetorno.Rows[0]["PROMO_PORC"]) > 0)
                                            {
                                                linha_resultado[0]["DESC_MAX"] = Principal.dtRetorno.Rows[0]["PROMO_PORC"].ToString();
                                            }
                                            linha_resultado[0].EndEdit();
                                        }
                                    }
                                }

                            }
                            #endregion

                            Principal.dtRetorno = Funcoes.VerificaProdDesconto(Principal.estAtual, Principal.wsConID, Convert.ToInt32(dtBuscaProd.Rows[i]["PROD_ID"]),
                                dtBuscaProd.Rows[i]["DEP_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dtBuscaProd.Rows[i]["DEP_CODIGO"]),
                                dtBuscaProd.Rows[i]["CLAS_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dtBuscaProd.Rows[i]["CLAS_CODIGO"]),
                                dtBuscaProd.Rows[i]["SUB_CODIGO"].ToString() == "" ? 0 : Convert.ToInt32(dtBuscaProd.Rows[i]["SUB_CODIGO"]));

                            if (Principal.dtRetorno.Rows.Count > 0)
                            {
                                linha_resultado = dtBuscaProd.Select("PROD_CODIGO = '" + dtBuscaProd.Rows[i]["PROD_CODIGO"] + "'");
                                linha_resultado[0].BeginEdit();
                                linha_resultado[0]["DESC_MIN"] = Principal.dtRetorno.Rows[0]["DESC_MIN"].ToString();
                                linha_resultado[0]["DESC_MAX"] = Principal.dtRetorno.Rows[0]["DESC_MAX"].ToString();
                                linha_resultado[0].EndEdit();
                            }
                        }

                        if (Funcoes.LeParametro(6, "347", true).Equals("N"))
                        {
                            dgProd.Columns[5].Visible = false;
                            dgProd.Columns[6].Visible = false;
                        }
                        else
                        {
                            dgProd.Columns[5].Visible = true;
                            dgProd.Columns[6].Visible = true;
                        }

                        if (Funcoes.LeParametro(6, "348", true).Equals("N"))
                        {
                            dgProd.Columns[3].Visible = false;
                        }
                        else
                        {
                            dgProd.Columns[3].Visible = true;
                        }
                    }
                    dgProd.DataSource = dtBuscaProd;

                    dgProd.Focus();
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Pesquisa de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgProd.DataSource = dtBuscaProd;
                    txtDescr.Text = "";
                    txtCodigo.Text = "";
                    cmbPriAtivo.SelectedIndex = -1;
                    filtroProd = "";
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Pesquisa de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (BuscaProd().Equals(true))
            {
                dgProd.Focus();
            }
            else
            {
                dgProd.Focus();
                SendKeys.Send("{TAB}");
            }
        }

        private void dgProd_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dgProd.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && indiceGrid < indice)
            {
                indiceGrid = indiceGrid + 1;
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && indiceGrid < indice)
            {
                indiceGrid = indiceGrid - 1;
            }

            teclaAtalho(e);
        }

        private void dgProd_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgProd.RowCount > 0)
                {
                    Principal.codBarra = dgProd.Rows[e.RowIndex].Cells[0].Value.ToString();
                    Principal.prodDescr = dgProd.Rows[e.RowIndex].Cells[1].Value.ToString();
                    this.Close();
                }
                else
                    txtDescr.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Pesquisa de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgProd_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {

                if (e.KeyChar == 13)
                {
                    if (dgProd.RowCount > 0)
                    {
                        Principal.codBarra = dgProd.Rows[indiceGrid].Cells[0].Value.ToString();
                        Principal.prodDescr = dgProd.Rows[indiceGrid].Cells[1].Value.ToString();
                        this.Close();
                    }
                    else
                        txtDescr.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Pesquisa de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgProd_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indiceGrid = e.RowIndex;
        }

        private void txtPreco_Validated(object sender, EventArgs e)
        {
            if (txtPreco.Text.Trim() != "")
            {
                txtPreco.Text = String.Format("{0:N}", Convert.ToDecimal(txtPreco.Text));
            }
            else
                txtPreco.Text = "0,00";
        }

        private void txtPreco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (Convert.ToDecimal(txtPreco.Text) > 0)
                {
                    btnBuscar.PerformClick();
                }
                else
                    btnFiltros.Focus();
            }
        }

        private void dgProd_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgProd.Rows[e.RowIndex].Cells[10].Value.ToString() == "S")
            {
                dgProd.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Green;
                dgProd.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Green;
            }
        }

        private void frmBuscaProd_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    Principal.codBarra = "";
                    Principal.prodDescr = "";
                    this.Close();
                    break;
                case Keys.F1:
                    btnConsultarLojas.PerformClick();
                    break;
            }
        }

        private void txtDescr_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void cmbPriAtivo_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void txtPreco_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnBuscar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnBuscar_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnBuscar, "Buscar");
        }

        private void btnCancela_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnCancela, "Vários Produtos");
        }

        private void btnFiltros_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnFiltros, "Filtrar");
        }

        private void btnConsultarLojas_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.btnConsultarLojas, "Consultar Estoque - F1");
        }

        private void btnConsultarLojas_Click(object sender, EventArgs e)
        {
            bool servidor;
            string codBarras = "";
            string descricao = "";
            servidor = Util.TesteIPServidorNuvem();
            if (servidor)
            {
                if (dgProd.RowCount > 0)
                {
                    if (dgProd.Rows[indiceGrid].Cells["PROD_CONTROLADO"].Value.Equals("S"))
                    {
                        MessageBox.Show("Produto Controlado, não é possível realizar Reserva!", "Estoque Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dgProd.Focus();
                        return;
                    }
                    else
                    {
                        codBarras = dgProd.Rows[indiceGrid].Cells[0].Value.ToString();
                        descricao = dgProd.Rows[indiceGrid].Cells[1].Value.ToString();
                    }
                }

                var consultarFranquias = new frmConsultarEstoqueFranquias(codBarras, descricao);
                consultarFranquias.ShowDialog();
            }
            else
            {
                MessageBox.Show("Servidor Fora! Aguarde alguns instantes!", "Estoque Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnConsultarLojas_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnFiltros_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void btnBuscarSimilar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(dgProd.Rows[indiceGrid].Cells["PRI_DESCRICAO"].Value.ToString()))
                {
                    cmbPriAtivo.Text = dgProd.Rows[indiceGrid].Cells["PRI_DESCRICAO"].Value.ToString();
                    txtCodigo.Text = "";
                    txtDescr.Text = "";
                    txtPreco.Text = "0,00";
                    btnBuscar.PerformClick();
                }
                else
                {
                    MessageBox.Show("Produto sem Princípio Ativo!", "Pesquisa de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    dgProd.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Pesquisa de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private async void lançarFaltaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                string bloqueado = Util.SelecionaCampoEspecificoDaTabela("PRODUTOS_DETALHE", "PROD_BLOQ_COMPRA", "PROD_CODIGO", dgProd.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString(),
                    true, true, true);

                if (String.IsNullOrEmpty(bloqueado) || bloqueado.Equals("N"))
                {
                    string qtde = Microsoft.VisualBasic.Interaction.InputBox("Digite a Quantidade", "Lote do Produto");
                    if (!String.IsNullOrEmpty(qtde))
                    {
                        if (Convert.ToInt32(qtde) > 0)
                        {
                            Cursor = Cursors.WaitCursor;
                            int id = Funcoes.IdentificaVerificaID("LANCAMENTO_FALTAS", "ID", 0, "", 0);
                            var faltas = new LancamentoFaltas(
                                Principal.empAtual,
                                Principal.estAtual,
                                id,
                                dgProd.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString(),
                                Convert.ToInt32(qtde),
                                "TELA DE BUSCA DE PRODUTO",
                                "FALTA DE MERCADORIA",
                                Principal.nomeEstacao,
                                "P",
                                Convert.ToInt32(Principal.codigoVendedor),
                                DateTime.Now,
                                Util.SelecionaCampoEspecificoDaTabela("COLABORADORES", "COL_NOME", "COL_CODIGO", Principal.codigoVendedor),
                                DateTime.Now,
                                Principal.usuario,
                                0
                                );

                            if (faltas.InsereRegistros(faltas))
                            {
                                await InsereFalta(faltas);

                                Cursor = Cursors.Default;
                                MessageBox.Show("Lançamento realizado com sucesso!", "Lançamento de Faltas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                Funcoes.GravaLogInclusao("ID", id.ToString(), Principal.usuario, "LACAMENTO_FALTAS", dgProd.Rows[indiceGrid].Cells["PROD_CODIGO"].Value.ToString(), Principal.estAtual, Principal.empAtual);

                                dgProd.Focus();
                            }
                            else
                                dgProd.Focus();
                        }
                        else
                        {
                            MessageBox.Show("Quatidade não pode ser Zero!", "Lançamento de Faltas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dgProd.Focus();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Informe a Quantidade de Produtos", "Faltas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dgProd.Focus();
                    }
                }
                else
                {
                    MessageBox.Show("Produto Bloqueado para Compra. Verifique o Cadastro!", "Faltas", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgProd.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Faltas de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public async Task<bool> InsereFalta(LancamentoFaltas falta)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Falteiro/InsereFalteiro?codEstab=" + Funcoes.LeParametro(9, "52", true)
                                    + "&grupoID=" + Funcoes.LeParametro(9, "53", true)
                                    + "&idDaLoja=" + falta.ID
                                    + "&prodCodigo=" + falta.ProdCodigo
                                    + "&qtde=" + falta.Qtde
                                    + "&observacao=" + falta.Observacao
                                    + "&usuario=" + falta.OpCadastro);
                    if (!response.IsSuccessStatusCode)
                    {
                        using (StreamWriter writer = new StreamWriter(@"C:\BTI\Falteiro.txt", true))
                        {
                            writer.WriteLine("COD. BARRAS: " + falta.ProdCodigo + " / QTDE: " + falta.Qtde);
                        }
                    }
                }


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Lançamento de Faltas de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private void btnCancela_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

    }
}
