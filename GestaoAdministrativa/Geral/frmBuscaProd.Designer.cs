﻿namespace GestaoAdministrativa
{
    partial class frmBuscaProd
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBuscaProd));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnConsultarLojas = new System.Windows.Forms.Button();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.btnCancela = new System.Windows.Forms.Button();
            this.btnFiltros = new System.Windows.Forms.Button();
            this.txtPreco = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.cmbPriAtivo = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCodigo = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDescr = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.dgProd = new System.Windows.Forms.DataGridView();
            this.PROD_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_ESTATUAL = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MED_PCO18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRE_VALOR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESC_MIN = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESC_MAX = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEP_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FAB_DESCRICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PRI_DESCRICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_PROMO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DEP_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CLAS_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SUB_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PROD_CONTROLADO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsPrincipioAtivo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.btnBuscarSimilar = new System.Windows.Forms.ToolStripMenuItem();
            this.lblEstab = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.lançarFaltaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProd)).BeginInit();
            this.cmsPrincipioAtivo.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.SlateGray;
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.Color.White;
            this.groupBox1.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.groupBox1.Location = new System.Drawing.Point(5, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(980, 560);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Filtros";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Controls.Add(this.dgProd);
            this.panel2.Controls.Add(this.lblEstab);
            this.panel2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel2.Location = new System.Drawing.Point(5, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(969, 536);
            this.panel2.TabIndex = 43;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnConsultarLojas);
            this.panel1.Controls.Add(this.btnBuscar);
            this.panel1.Controls.Add(this.btnCancela);
            this.panel1.Controls.Add(this.btnFiltros);
            this.panel1.Controls.Add(this.txtPreco);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.cmbPriAtivo);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.txtCodigo);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.txtDescr);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Location = new System.Drawing.Point(6, 8);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(955, 96);
            this.panel1.TabIndex = 158;
            // 
            // btnConsultarLojas
            // 
            this.btnConsultarLojas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnConsultarLojas.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnConsultarLojas.ForeColor = System.Drawing.Color.Black;
            this.btnConsultarLojas.Image = ((System.Drawing.Image)(resources.GetObject("btnConsultarLojas.Image")));
            this.btnConsultarLojas.Location = new System.Drawing.Point(746, 49);
            this.btnConsultarLojas.Name = "btnConsultarLojas";
            this.btnConsultarLojas.Size = new System.Drawing.Size(46, 41);
            this.btnConsultarLojas.TabIndex = 187;
            this.btnConsultarLojas.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnConsultarLojas.UseVisualStyleBackColor = true;
            this.btnConsultarLojas.Visible = false;
            this.btnConsultarLojas.Click += new System.EventHandler(this.btnConsultarLojas_Click);
            this.btnConsultarLojas.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnConsultarLojas_KeyDown);
            this.btnConsultarLojas.MouseHover += new System.EventHandler(this.btnConsultarLojas_MouseHover);
            // 
            // btnBuscar
            // 
            this.btnBuscar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.Location = new System.Drawing.Point(902, 49);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(46, 41);
            this.btnBuscar.TabIndex = 190;
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            this.btnBuscar.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnBuscar_KeyDown);
            this.btnBuscar.MouseHover += new System.EventHandler(this.btnBuscar_MouseHover);
            // 
            // btnCancela
            // 
            this.btnCancela.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancela.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancela.ForeColor = System.Drawing.Color.Black;
            this.btnCancela.Image = ((System.Drawing.Image)(resources.GetObject("btnCancela.Image")));
            this.btnCancela.Location = new System.Drawing.Point(850, 49);
            this.btnCancela.Name = "btnCancela";
            this.btnCancela.Size = new System.Drawing.Size(46, 41);
            this.btnCancela.TabIndex = 189;
            this.btnCancela.UseVisualStyleBackColor = true;
            this.btnCancela.Click += new System.EventHandler(this.btnCancela_Click);
            this.btnCancela.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnCancela_KeyDown);
            this.btnCancela.MouseHover += new System.EventHandler(this.btnCancela_MouseHover);
            // 
            // btnFiltros
            // 
            this.btnFiltros.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnFiltros.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFiltros.ForeColor = System.Drawing.Color.Black;
            this.btnFiltros.Image = ((System.Drawing.Image)(resources.GetObject("btnFiltros.Image")));
            this.btnFiltros.Location = new System.Drawing.Point(798, 49);
            this.btnFiltros.Name = "btnFiltros";
            this.btnFiltros.Size = new System.Drawing.Size(46, 41);
            this.btnFiltros.TabIndex = 188;
            this.btnFiltros.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnFiltros.UseVisualStyleBackColor = true;
            this.btnFiltros.Click += new System.EventHandler(this.btnFiltros_Click);
            this.btnFiltros.KeyDown += new System.Windows.Forms.KeyEventHandler(this.btnFiltros_KeyDown);
            this.btnFiltros.MouseHover += new System.EventHandler(this.btnFiltros_MouseHover);
            // 
            // txtPreco
            // 
            this.txtPreco.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPreco.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPreco.Location = new System.Drawing.Point(850, 21);
            this.txtPreco.MaxLength = 13;
            this.txtPreco.Name = "txtPreco";
            this.txtPreco.Size = new System.Drawing.Size(98, 22);
            this.txtPreco.TabIndex = 186;
            this.txtPreco.Text = "0,00";
            this.txtPreco.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtPreco.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtPreco_KeyDown);
            this.txtPreco.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPreco_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.Black;
            this.label8.Location = new System.Drawing.Point(847, 2);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(45, 16);
            this.label8.TabIndex = 185;
            this.label8.Text = "Preço";
            // 
            // cmbPriAtivo
            // 
            this.cmbPriAtivo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPriAtivo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPriAtivo.FormattingEnabled = true;
            this.cmbPriAtivo.Location = new System.Drawing.Point(473, 18);
            this.cmbPriAtivo.Name = "cmbPriAtivo";
            this.cmbPriAtivo.Size = new System.Drawing.Size(371, 24);
            this.cmbPriAtivo.TabIndex = 184;
            this.cmbPriAtivo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmbPriAtivo_KeyDown);
            this.cmbPriAtivo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbPriAtivo_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.ForeColor = System.Drawing.Color.Black;
            this.label7.Location = new System.Drawing.Point(470, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(101, 16);
            this.label7.TabIndex = 183;
            this.label7.Text = "Princípio Ativo";
            // 
            // txtCodigo
            // 
            this.txtCodigo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodigo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodigo.Location = new System.Drawing.Point(280, 20);
            this.txtCodigo.MaxLength = 13;
            this.txtCodigo.Name = "txtCodigo";
            this.txtCodigo.Size = new System.Drawing.Size(186, 22);
            this.txtCodigo.TabIndex = 182;
            this.txtCodigo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodigo_KeyDown);
            this.txtCodigo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodigo_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(277, 2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 16);
            this.label6.TabIndex = 181;
            this.label6.Text = "Cód. de Barra";
            // 
            // txtDescr
            // 
            this.txtDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescr.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescr.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescr.Location = new System.Drawing.Point(8, 21);
            this.txtDescr.MaxLength = 50;
            this.txtDescr.Name = "txtDescr";
            this.txtDescr.Size = new System.Drawing.Size(266, 22);
            this.txtDescr.TabIndex = 1;
            this.txtDescr.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtDescr_KeyDown);
            this.txtDescr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescr_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(5, 2);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 16);
            this.label5.TabIndex = 161;
            this.label5.Text = "Descrição";
            // 
            // dgProd
            // 
            this.dgProd.AllowUserToAddRows = false;
            this.dgProd.AllowUserToDeleteRows = false;
            this.dgProd.AllowUserToResizeColumns = false;
            this.dgProd.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Navy;
            this.dgProd.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgProd.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProd.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgProd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgProd.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PROD_CODIGO,
            this.PROD_DESCR,
            this.PROD_ESTATUAL,
            this.MED_PCO18,
            this.PRE_VALOR,
            this.DESC_MIN,
            this.DESC_MAX,
            this.DEP_DESCR,
            this.FAB_DESCRICAO,
            this.PRI_DESCRICAO,
            this.PROD_PROMO,
            this.DEP_CODIGO,
            this.CLAS_CODIGO,
            this.SUB_CODIGO,
            this.PROD_ID,
            this.PROD_CONTROLADO});
            this.dgProd.ContextMenuStrip = this.cmsPrincipioAtivo;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgProd.DefaultCellStyle = dataGridViewCellStyle8;
            this.dgProd.GridColor = System.Drawing.Color.LightGray;
            this.dgProd.Location = new System.Drawing.Point(6, 105);
            this.dgProd.Name = "dgProd";
            this.dgProd.ReadOnly = true;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgProd.RowHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgProd.RowHeadersVisible = false;
            this.dgProd.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Navy;
            this.dgProd.RowsDefaultCellStyle = dataGridViewCellStyle10;
            this.dgProd.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgProd.Size = new System.Drawing.Size(955, 426);
            this.dgProd.TabIndex = 1;
            this.dgProd.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgProd_CellClick);
            this.dgProd.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgProd_CellMouseDoubleClick);
            this.dgProd.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgProd_RowPrePaint);
            this.dgProd.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgProd_KeyDown);
            this.dgProd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgProd_KeyPress);
            // 
            // PROD_CODIGO
            // 
            this.PROD_CODIGO.DataPropertyName = "PROD_CODIGO";
            this.PROD_CODIGO.HeaderText = "Cód. de Barra";
            this.PROD_CODIGO.MaxInputLength = 15;
            this.PROD_CODIGO.Name = "PROD_CODIGO";
            this.PROD_CODIGO.ReadOnly = true;
            this.PROD_CODIGO.Width = 115;
            // 
            // PROD_DESCR
            // 
            this.PROD_DESCR.DataPropertyName = "PROD_DESCR";
            this.PROD_DESCR.HeaderText = "Descrição";
            this.PROD_DESCR.MaxInputLength = 50;
            this.PROD_DESCR.Name = "PROD_DESCR";
            this.PROD_DESCR.ReadOnly = true;
            this.PROD_DESCR.Width = 400;
            // 
            // PROD_ESTATUAL
            // 
            this.PROD_ESTATUAL.DataPropertyName = "PROD_ESTATUAL";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.PROD_ESTATUAL.DefaultCellStyle = dataGridViewCellStyle3;
            this.PROD_ESTATUAL.HeaderText = "Est.";
            this.PROD_ESTATUAL.MaxInputLength = 18;
            this.PROD_ESTATUAL.Name = "PROD_ESTATUAL";
            this.PROD_ESTATUAL.ReadOnly = true;
            this.PROD_ESTATUAL.Width = 60;
            // 
            // MED_PCO18
            // 
            this.MED_PCO18.DataPropertyName = "MED_PCO18";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle4.Format = "N2";
            dataGridViewCellStyle4.NullValue = null;
            this.MED_PCO18.DefaultCellStyle = dataGridViewCellStyle4;
            this.MED_PCO18.HeaderText = "PMC";
            this.MED_PCO18.Name = "MED_PCO18";
            this.MED_PCO18.ReadOnly = true;
            this.MED_PCO18.Visible = false;
            this.MED_PCO18.Width = 70;
            // 
            // PRE_VALOR
            // 
            this.PRE_VALOR.DataPropertyName = "PRE_VALOR";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle5.Format = "N2";
            dataGridViewCellStyle5.NullValue = null;
            this.PRE_VALOR.DefaultCellStyle = dataGridViewCellStyle5;
            this.PRE_VALOR.HeaderText = "Preço";
            this.PRE_VALOR.MaxInputLength = 18;
            this.PRE_VALOR.Name = "PRE_VALOR";
            this.PRE_VALOR.ReadOnly = true;
            this.PRE_VALOR.Width = 70;
            // 
            // DESC_MIN
            // 
            this.DESC_MIN.DataPropertyName = "DESC_MIN";
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle6.Format = "N2";
            dataGridViewCellStyle6.NullValue = null;
            this.DESC_MIN.DefaultCellStyle = dataGridViewCellStyle6;
            this.DESC_MIN.HeaderText = "% Min.";
            this.DESC_MIN.Name = "DESC_MIN";
            this.DESC_MIN.ReadOnly = true;
            this.DESC_MIN.Visible = false;
            this.DESC_MIN.Width = 70;
            // 
            // DESC_MAX
            // 
            this.DESC_MAX.DataPropertyName = "DESC_MAX";
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            dataGridViewCellStyle7.Format = "N2";
            dataGridViewCellStyle7.NullValue = null;
            this.DESC_MAX.DefaultCellStyle = dataGridViewCellStyle7;
            this.DESC_MAX.HeaderText = "% Max.";
            this.DESC_MAX.Name = "DESC_MAX";
            this.DESC_MAX.ReadOnly = true;
            this.DESC_MAX.Visible = false;
            this.DESC_MAX.Width = 70;
            // 
            // DEP_DESCR
            // 
            this.DEP_DESCR.DataPropertyName = "DEP_DESCR";
            this.DEP_DESCR.HeaderText = "Departamento";
            this.DEP_DESCR.MaxInputLength = 30;
            this.DEP_DESCR.Name = "DEP_DESCR";
            this.DEP_DESCR.ReadOnly = true;
            // 
            // FAB_DESCRICAO
            // 
            this.FAB_DESCRICAO.DataPropertyName = "FAB_DESCRICAO";
            this.FAB_DESCRICAO.HeaderText = "Fabricante";
            this.FAB_DESCRICAO.MaxInputLength = 30;
            this.FAB_DESCRICAO.Name = "FAB_DESCRICAO";
            this.FAB_DESCRICAO.ReadOnly = true;
            this.FAB_DESCRICAO.Width = 90;
            // 
            // PRI_DESCRICAO
            // 
            this.PRI_DESCRICAO.DataPropertyName = "PRI_DESCRICAO";
            this.PRI_DESCRICAO.HeaderText = "Princ. Ativo";
            this.PRI_DESCRICAO.MaxInputLength = 130;
            this.PRI_DESCRICAO.Name = "PRI_DESCRICAO";
            this.PRI_DESCRICAO.ReadOnly = true;
            // 
            // PROD_PROMO
            // 
            this.PROD_PROMO.DataPropertyName = "PROD_PROMO";
            this.PROD_PROMO.HeaderText = "Promocao";
            this.PROD_PROMO.Name = "PROD_PROMO";
            this.PROD_PROMO.ReadOnly = true;
            this.PROD_PROMO.Visible = false;
            // 
            // DEP_CODIGO
            // 
            this.DEP_CODIGO.DataPropertyName = "DEP_CODIGO";
            this.DEP_CODIGO.HeaderText = "Dep. Codigo";
            this.DEP_CODIGO.Name = "DEP_CODIGO";
            this.DEP_CODIGO.ReadOnly = true;
            this.DEP_CODIGO.Visible = false;
            // 
            // CLAS_CODIGO
            // 
            this.CLAS_CODIGO.DataPropertyName = "CLAS_CODIGO";
            this.CLAS_CODIGO.HeaderText = "Clas. Codigo";
            this.CLAS_CODIGO.Name = "CLAS_CODIGO";
            this.CLAS_CODIGO.ReadOnly = true;
            this.CLAS_CODIGO.Visible = false;
            // 
            // SUB_CODIGO
            // 
            this.SUB_CODIGO.DataPropertyName = "SUB_CODIGO";
            this.SUB_CODIGO.HeaderText = "Sub. Codigo";
            this.SUB_CODIGO.Name = "SUB_CODIGO";
            this.SUB_CODIGO.ReadOnly = true;
            this.SUB_CODIGO.Visible = false;
            // 
            // PROD_ID
            // 
            this.PROD_ID.DataPropertyName = "PROD_ID";
            this.PROD_ID.HeaderText = "Prod. ID";
            this.PROD_ID.Name = "PROD_ID";
            this.PROD_ID.ReadOnly = true;
            this.PROD_ID.Visible = false;
            // 
            // PROD_CONTROLADO
            // 
            this.PROD_CONTROLADO.DataPropertyName = "PROD_CONTROLADO";
            this.PROD_CONTROLADO.HeaderText = "Controlado";
            this.PROD_CONTROLADO.Name = "PROD_CONTROLADO";
            this.PROD_CONTROLADO.ReadOnly = true;
            this.PROD_CONTROLADO.Visible = false;
            // 
            // cmsPrincipioAtivo
            // 
            this.cmsPrincipioAtivo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnBuscarSimilar,
            this.lançarFaltaToolStripMenuItem});
            this.cmsPrincipioAtivo.Name = "cmsPrincipioAtivo";
            this.cmsPrincipioAtivo.Size = new System.Drawing.Size(161, 70);
            // 
            // btnBuscarSimilar
            // 
            this.btnBuscarSimilar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscarSimilar.Name = "btnBuscarSimilar";
            this.btnBuscarSimilar.Size = new System.Drawing.Size(160, 22);
            this.btnBuscarSimilar.Text = "Buscar Similar";
            this.btnBuscarSimilar.Click += new System.EventHandler(this.btnBuscarSimilar_Click);
            // 
            // lblEstab
            // 
            this.lblEstab.AutoSize = true;
            this.lblEstab.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstab.ForeColor = System.Drawing.Color.Black;
            this.lblEstab.Location = new System.Drawing.Point(6, 8);
            this.lblEstab.Name = "lblEstab";
            this.lblEstab.Size = new System.Drawing.Size(0, 16);
            this.lblEstab.TabIndex = 152;
            this.lblEstab.Tag = "";
            // 
            // lançarFaltaToolStripMenuItem
            // 
            this.lançarFaltaToolStripMenuItem.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lançarFaltaToolStripMenuItem.Name = "lançarFaltaToolStripMenuItem";
            this.lançarFaltaToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.lançarFaltaToolStripMenuItem.Text = "Lançar Falta";
            this.lançarFaltaToolStripMenuItem.Click += new System.EventHandler(this.lançarFaltaToolStripMenuItem_Click);
            // 
            // frmBuscaProd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SlateGray;
            this.ClientSize = new System.Drawing.Size(990, 562);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBuscaProd";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pesquisa de Produtos";
            this.Load += new System.EventHandler(this.frmBuscaProd_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmBuscaProd_KeyDown);
            this.groupBox1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgProd)).EndInit();
            this.cmsPrincipioAtivo.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblEstab;
        public System.Windows.Forms.DataGridView dgProd;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnConsultarLojas;
        public System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.Button btnCancela;
        private System.Windows.Forms.Button btnFiltros;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbPriAtivo;
        private System.Windows.Forms.Label label7;
        public System.Windows.Forms.TextBox txtCodigo;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox txtDescr;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ContextMenuStrip cmsPrincipioAtivo;
        private System.Windows.Forms.ToolStripMenuItem btnBuscarSimilar;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_ESTATUAL;
        private System.Windows.Forms.DataGridViewTextBoxColumn MED_PCO18;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRE_VALOR;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESC_MIN;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESC_MAX;
        private System.Windows.Forms.DataGridViewTextBoxColumn DEP_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn FAB_DESCRICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PRI_DESCRICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_PROMO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DEP_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CLAS_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn SUB_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PROD_CONTROLADO;
        public System.Windows.Forms.TextBox txtPreco;
        private System.Windows.Forms.ToolStripMenuItem lançarFaltaToolStripMenuItem;
    }
}