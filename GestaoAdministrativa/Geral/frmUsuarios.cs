﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace GestaoAdministrativa
{
    public partial class frmUsuarios : Form, Botoes
    {
        //DECLARACAO DAS VARIÁVEIS//
        private DataTable dtUsuarios = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbUsuarios = new ToolStripButton("Usuários");
        private ToolStripSeparator tssUsuarios = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;

        private string enderecoWebService = Funcoes.LeParametro(9, "54", true);
        private string grupoID = Funcoes.LeParametro(9, "53", true);
        private string codEstab = Funcoes.LeParametro(9, "52", true);

        public frmUsuarios(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmUsuarios_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            try
            {
                txtID.Text = dtUsuarios.Rows[linha]["ID"].ToString();
                txtNome.Text = dtUsuarios.Rows[linha]["LOGIN_ID"].ToString();
                txtSenha.Text = Funcoes.DescriptografaSenha(dtUsuarios.Rows[linha]["SENHA"].ToString());
                cmbGrupo.SelectedValue = dtUsuarios.Rows[linha]["GRUPO_ID"].ToString();
                if (dtUsuarios.Rows[linha]["LIBERADO"].ToString() == "S")
                {
                    chkLiberado.Checked = true;
                }
                else
                    chkLiberado.Checked = false;

                txtData.Text = Funcoes.ChecaCampoVazio(dtUsuarios.Rows[linha]["DTALTERACAO"].ToString());
                txtUsuario.Text = Funcoes.ChecaCampoVazio(dtUsuarios.Rows[linha]["OPALTERACAO"].ToString());

                //HABILITA BOTOES DE NAVEGACAO//
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtUsuarios, linha));
                txtNome.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void LimparGrade()
        {
            dtUsuarios.Clear();
            dgUsuarios.DataSource = dtUsuarios;
            Funcoes.LimpaFormularios(this);
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            cmbLiberado.SelectedIndex = 0;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            txtID.Text = "";
            txtNome.Text = "";
            txtSenha.Text = "";
            chkLiberado.Checked = false;
            cmbGrupo.SelectedIndex = -1;
            txtData.Text = "";
            txtUsuario.Text = "";
            txtNome.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcUsuarios.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                LimparFicha();

            BotoesHabilitados();
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgUsuarios.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgUsuarios.CurrentCell = dgUsuarios.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgUsuarios.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgUsuarios.CurrentCell = dgUsuarios.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Primeiro()
        {
            if (dtUsuarios.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgUsuarios.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgUsuarios.CurrentCell = dgUsuarios.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcUsuarios.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }

        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtUsuarios.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgUsuarios.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgUsuarios.CurrentCell = dgUsuarios.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos())
                {
                    //VERIFICA SE O GRUPO E TEM PERMISSAO DE ADMINISTRADOR//
                    Usuario usuario = new Usuario
                    {
                        ID = Convert.ToInt32(txtID.Text),
                        LoginID = txtNome.Text.Trim(),
                        Senha = Funcoes.CriptografaSenha(txtSenha.Text),
                        Administrador = Util.SelecionaCampoEspecificoDaTabela("GRUPO_USUARIOS", "ADMINISTRADOR", "GRUPO_USU_ID", cmbGrupo.SelectedValue.ToString()) == "S" ? "1" : "0",
                        GrupoID = int.Parse(cmbGrupo.SelectedValue.ToString()),
                        Liberado = chkLiberado.Checked == true ? "S" : "N",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                    };

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("USUARIOS", "ID", txtID.Text);

                    if (usuario.AtualizaDados(usuario, Principal.dtBusca))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Usuários");
                        Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                        dtUsuarios.Clear();
                        dgUsuarios.DataSource = dtUsuarios;
                        emGrade = false;
                        tslRegistros.Text = "";
                        LimparFicha();
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Excluir()
        {
            try
            {
                if (txtID.Text.Trim() != "" || txtNome.Text.Trim() != "")
                {
                    if (MessageBox.Show("Confirma a exclusão do usuário?", "Exclusão tabela Usuários", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                    {
                        using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }
                        var dados = new Usuario();
                        if (!dados.ExcluirDados(txtID.Text).Equals(-1))
                        {
                            Funcoes.GravaLogExclusao("ID", txtID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "USUARIOS", txtID.Text, Principal.motivo, Principal.estAtual, Principal.empAtual);
                            dtUsuarios.Clear();
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dgUsuarios.DataSource = dtUsuarios;
                            emGrade = false;
                            tcUsuarios.SelectedTab = tpGrade;
                            tslRegistros.Text = "";
                            Funcoes.LimpaFormularios(this);
                            txtBID.Focus();
                            cmbLiberado.SelectedIndex = 0;
                            return true;
                        }
                    }
                    return false;
                }
                else
                    MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        public async void InsereAPI()
        {
            await InsereOperador(Convert.ToInt32(txtID.Text), txtNome.Text.Trim());
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                if (ConsisteCampos())
                {
                    txtID.Text = Funcoes.IdentificaVerificaID("USUARIOS", "ID").ToString();
                    chkLiberado.Checked = true;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    //VERIFICA SE O GRUPO E TEM PERMISSAO DE ADMINISTRADOR//
                    Usuario usuario = new Usuario
                    {
                        ID = Convert.ToInt32(txtID.Text),
                        LoginID = txtNome.Text.Trim(),
                        Senha = Funcoes.CriptografaSenha(txtSenha.Text),
                        Administrador = Util.SelecionaCampoEspecificoDaTabela("GRUPO_USUARIOS", "ADMINISTRADOR", "GRUPO_USU_ID", cmbGrupo.SelectedValue.ToString()) == "S" ? "1" : "0",
                        GrupoID = int.Parse(cmbGrupo.SelectedValue.ToString()),
                        Liberado = chkLiberado.Checked == true ? "S" : "N",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                    };

                    Principal.dtBusca = Util.RegistrosPorEstabelecimento("USUARIOS", "ID", txtID.Text);
                    
                    if (Principal.dtBusca.Rows.Count == 0)
                    {
                        //VERIFICA SE USUARIO JA ESTA CADASTRADO//
                        Principal.dtPesq = Util.RegistrosPorEstabelecimento("USUARIOS", "LOGIN_ID", txtNome.Text.Trim(), false, true);
                        if (Principal.dtPesq.Rows.Count != 0)
                        {
                            Cursor = Cursors.Default;
                            Principal.mensagem = "Usuário já cadastrado.";
                            Funcoes.Avisa();
                            txtNome.Text = "";
                            txtNome.Focus();
                            return false;
                        }

                        //INCLUI NOVA LINHA NA TABELA//
                        if (usuario.InserirDados(usuario))
                        {
                            InsereAPI();
                            //GRAVA LOG DE ALTERAÇÃO NO BANCO DE DADOS//
                            Funcoes.GravaLogInclusao("ID", txtID.Text, Principal.usuario, "USUARIOS", txtNome.Text, Principal.estAtual, Principal.empAtual);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtUsuarios.Clear();
                            dgUsuarios.DataSource = dtUsuarios;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            cmbLiberado.SelectedIndex = 0;
                            return true;
                        }
                        else
                        {
                            txtNome.Focus();
                            return false;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public async Task<bool> InsereOperador(int operadorId, string nome)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.GetAsync("Operador/CadastraOperador?operadorId=" + operadorId + "&nome=" + nome + "&grupoID=" + grupoID + "&estabelecimentoId=" + codEstab + "&status=A");

                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        return Convert.ToBoolean(responseBody);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;

            }
        }


        public void ImprimirRelatorio()
        {
        }

        private void frmUsuarios_Shown(object sender, EventArgs e)
        {
            Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("GRUPO_USU_ID", "DESCRICAO", "GRUPO_USUARIOS");
            if (Principal.dtPesq.Rows.Count == 0)
            {
                MessageBox.Show("Necessário cadastrar pelo menos um Grupo de Usuário,\npara cadastrar um novo usuário.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
            else
            {
                cmbBGrupos.DataSource = Principal.dtPesq;
                cmbBGrupos.DisplayMember = "DESCRICAO";
                cmbBGrupos.ValueMember = "GRUPO_USU_ID";
                cmbBGrupos.SelectedIndex = -1;
            }
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";

            if (String.IsNullOrEmpty(txtNome.Text.Trim()))
            {
                Principal.mensagem = "Nome do usuário não pode ser em branco.";
                Funcoes.Avisa();
                txtNome.Focus();
                return false;
            }
            if (String.IsNullOrEmpty(txtSenha.Text.Trim()))
            {
                Principal.mensagem = "Senha não pode ser em branco.";
                Funcoes.Avisa();
                txtSenha.Focus();
                return false;
            }
            if (Equals(cmbGrupo.SelectedIndex, -1))
            {
                Principal.mensagem = "Selecione um grupo.";
                Funcoes.Avisa();
                cmbGrupo.Focus();
                return false;
            }
            return true;
        }

        private void tcUsuarios_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tcUsuarios.SelectedTab == tpGrade)
            {
                dgUsuarios.Focus();
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
            }
            else
                if (tcUsuarios.SelectedTab == tpFicha)
            {
                if (cmbGrupo.DataSource == null)
                {
                    Principal.dtPesq = Util.CarregarCombosPorEstabelecimento("GRUPO_USU_ID", "DESCRICAO", "GRUPO_USUARIOS", false, "LIBERADO = 'S'");
                    if (Principal.dtPesq.Rows.Count == 0)
                    {
                        MessageBox.Show("Necessário cadastrar pelo menos um Grupo de Usuário,\npara cadastrar um novo usuário.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        this.Close();
                    }
                    else
                    {
                        cmbGrupo.DataSource = Principal.dtPesq;
                        cmbGrupo.DisplayMember = "DESCRICAO";
                        cmbGrupo.ValueMember = "GRUPO_USU_ID";
                        cmbGrupo.SelectedIndex = -1;
                    }
                }
                Funcoes.BotoesCadastro("frmUsuarios");
                if (emGrade == true)
                {
                    CarregarDados(contRegistros);
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
                else
                {
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtUsuarios, contRegistros));
                    chkLiberado.Checked = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    txtNome.Focus();
                    emGrade = false;
                }
            }
        }

        private void dgUsuarios_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtUsuarios, contRegistros));
            emGrade = true;
        }

        private void dgUsuarios_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                tcUsuarios.SelectedTab = tpFicha;
                emGrade = true;
                Cursor = Cursors.Default;
            }
        }

        private void dgUsuarios_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtUsuarios.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtUsuarios.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtUsuarios.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void txtBID_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBLogin.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBLogin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBLogin.Text.Trim()))
                {
                    cmbBGrupos.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void cmbBGrupo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (cmbGrupo.SelectedIndex == -1)
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtSenha.Focus();
            }
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbGrupo.Focus();
            }
        }

        private void cmbGrupo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        //EXPORTA DADOS DO DATAGRIDVIEW PARA O EXCEL//
        private void tsmExportar_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgUsuarios.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgUsuarios);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgUsuarios.RowCount;
                    for (i = 0; i <= dgUsuarios.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgUsuarios[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;

                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgUsuarios.ColumnCount; k++)
                    {
                        if (dgUsuarios.Columns[k].Visible == true)
                        {
                            switch (dgUsuarios.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgUsuarios.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgUsuarios.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgUsuarios.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgUsuarios.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgUsuarios.ColumnCount : indice) + Convert.ToString(dgUsuarios.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmUsuario");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtUsuarios, contRegistros));
            if (tcUsuarios.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcUsuarios.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;
                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            try
            {
                this.tsbUsuarios.AutoSize = false;
                this.tsbUsuarios.Image = Properties.Resources.usuario;
                this.tsbUsuarios.Size = new System.Drawing.Size(100, 20);
                this.tsbUsuarios.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbUsuarios);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssUsuarios);
                tsbUsuarios.Click += delegate
                {
                    var cadUsuarios = Application.OpenForms.OfType<frmUsuarios>().FirstOrDefault();
                    BotoesHabilitados();
                    cadUsuarios.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                dtUsuarios.Dispose();
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbUsuarios);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssUsuarios);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                BotoesHabilitados();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                Usuario usuario = new Usuario
                {
                    ID = txtBID.Text.Trim() == "" ? 0 : Convert.ToInt32(txtBID.Text),
                    LoginID = txtBLogin.Text.Trim(),
                    GrupoID = cmbBGrupos.SelectedIndex == -1 ? -1 : Convert.ToInt32(cmbBGrupos.SelectedValue),
                    Liberado = cmbLiberado.Text
                };

                dtUsuarios = usuario.BuscarDados(usuario, out strOrdem);

                if (dtUsuarios.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtUsuarios.Rows.Count;
                    dgUsuarios.DataSource = dtUsuarios;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtUsuarios, 0));
                    dgUsuarios.Focus();
                    contRegistros = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgUsuarios.DataSource = dtUsuarios;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    Funcoes.LimpaFormularios(this);
                    emGrade = false;
                    tslRegistros.Text = "";
                    cmbBGrupos.SelectedIndex = -1;
                    cmbLiberado.SelectedIndex = -1;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgUsuarios_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            try
            {
                if (dgUsuarios.Rows.Count > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    if (decrescente == false)
                    {
                        if (dgUsuarios.Columns[e.ColumnIndex].Name == "ADM")
                        {
                            dtUsuarios = BancoDados.selecionarRegistros(strOrdem + " ORDER BY ADMINISTRADOR DESC");
                        }
                        else
                            if (dgUsuarios.Columns[e.ColumnIndex].Name == "LIB")
                        {
                            dtUsuarios = BancoDados.selecionarRegistros(strOrdem + " ORDER BY LIBERADO DESC");
                        }
                        else
                        {
                            dtUsuarios = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgUsuarios.Columns[e.ColumnIndex].Name + " DESC");
                        }
                        dgUsuarios.DataSource = dtUsuarios;
                        decrescente = true;
                    }
                    else
                    {
                        if (dgUsuarios.Columns[e.ColumnIndex].Name == "ADM")
                        {
                            dtUsuarios = BancoDados.selecionarRegistros(strOrdem + " ORDER BY ADMINISTRADOR ASC");
                        }
                        else
                            if (dgUsuarios.Columns[e.ColumnIndex].Name == "LIB")
                        {
                            dtUsuarios = BancoDados.selecionarRegistros(strOrdem + " ORDER BY LIBERADO ASC");
                        }
                        else
                        {
                            dtUsuarios = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgUsuarios.Columns[e.ColumnIndex].Name + " ASC");
                        }

                        dgUsuarios.DataSource = dtUsuarios;
                        decrescente = false;
                    }

                    contRegistros = 0;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtUsuarios, contRegistros));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void pictureBox2_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox2, "Campo Obrigatório");
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox4_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox4, "Campo Obrigatório");
        }

        private void dgUsuarios_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgUsuarios.Rows[e.RowIndex].Cells[3].Value.ToString() == "N")
            {
                dgUsuarios.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgUsuarios.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control.Name != "txtID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgUsuarios.ColumnCount; i++)
                {
                    if (dgUsuarios.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgUsuarios.ColumnCount];
                string[] coluna = new string[dgUsuarios.ColumnCount];

                for (int i = 0; i < dgUsuarios.ColumnCount; i++)
                {
                    grid[i] = dgUsuarios.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgUsuarios.ColumnCount; i++)
                {
                    if (dgUsuarios.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgUsuarios.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgUsuarios.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgUsuarios.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgUsuarios.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgUsuarios.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgUsuarios.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgUsuarios.ColumnCount; i++)
                        {
                            dgUsuarios.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcUsuarios.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcUsuarios.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
