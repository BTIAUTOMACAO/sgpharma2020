﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using System.Globalization;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa.Geral
{
    public partial class frmBuscaVProd : Form
    {
        private Produto buscaProduto = new Produto();
        private DataTable dtBuscaProd = new DataTable();

        public frmBuscaVProd()
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        private void txtCodigo_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtCodigo.Text.Trim() != "")
                {
                    Cursor = Cursors.WaitCursor;
                    dgProdutos.DataSource = buscaProduto.BuscaRapidaProdutos(Principal.estAtual, Principal.empAtual, txtCodigo.Text, "", 0);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Busca de Preço de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
                txtDescricao.Focus();
        }

        private void txtDescricao_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtDescricao.Text.Trim() != "")
                {
                    Cursor = Cursors.WaitCursor;
                    dgProdutos.DataSource = buscaProduto.BuscaRapidaProdutos(Principal.estAtual, Principal.empAtual, "", txtDescricao.Text, 0);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Busca de Preço de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtDescricao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtPreco.Focus();
        }

        private void txtPreco_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToDouble(txtPreco.Text) > 0)
                {
                    Cursor = Cursors.WaitCursor;
                    dgProdutos.DataSource = buscaProduto.BuscaRapidaProdutos(Principal.estAtual, Principal.empAtual,"", "", Convert.ToDouble(txtPreco.Text));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Busca de Preço de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void txtPreco_Validated(object sender, EventArgs e)
        {
            if (txtPreco.Text.Trim() != "")
            {
                txtPreco.Text = String.Format("{0:N}", Convert.ToDouble(txtPreco.Text));
            }
            else
                txtPreco.Text = "0,00";
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmBuscaVProd_Load(object sender, EventArgs e)
        {

        }

        private void txtCodigo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
                this.Close();
        }

        private void txtDescricao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
                this.Close();
        }

        private void txtPreco_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
                this.Close();
        }

        private void dgProdutos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
                this.Close();
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}
