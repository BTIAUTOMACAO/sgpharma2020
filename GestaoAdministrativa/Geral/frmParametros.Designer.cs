﻿namespace GestaoAdministrativa.Geral
{
    partial class frmParametros
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle64 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle65 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle66 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle63 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpBeneficios = new System.Windows.Forms.TabPage();
            this.dgBeneficios = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpCadastro = new System.Windows.Forms.TabPage();
            this.dgCadastro = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpCaixa = new System.Windows.Forms.TabPage();
            this.dgCaixa = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpEstoque = new System.Windows.Forms.TabPage();
            this.dgEstoque = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpGeral = new System.Windows.Forms.TabPage();
            this.dgGeral = new System.Windows.Forms.DataGridView();
            this.PAR_TABELA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAR_ESTACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAR_POSICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAR_COMENT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAR_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PAR_OBS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpImpressora = new System.Windows.Forms.TabPage();
            this.dgImpressora = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpPagar = new System.Windows.Forms.TabPage();
            this.dgPagar = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpReceber = new System.Windows.Forms.TabPage();
            this.dgReceber = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpSat = new System.Windows.Forms.TabPage();
            this.dgSAT = new System.Windows.Forms.DataGridView();
            this.tpSngpc = new System.Windows.Forms.TabPage();
            this.dgSngpc = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpVendas = new System.Windows.Forms.TabPage();
            this.dgVendas = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label10 = new System.Windows.Forms.Label();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpBeneficios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgBeneficios)).BeginInit();
            this.tpCadastro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCadastro)).BeginInit();
            this.tpCaixa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCaixa)).BeginInit();
            this.tpEstoque.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEstoque)).BeginInit();
            this.tpGeral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgGeral)).BeginInit();
            this.tpImpressora.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgImpressora)).BeginInit();
            this.tpPagar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPagar)).BeginInit();
            this.tpReceber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgReceber)).BeginInit();
            this.tpSat.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSAT)).BeginInit();
            this.tpSngpc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSngpc)).BeginInit();
            this.tpVendas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVendas)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.tabControl1);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(9, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(976, 560);
            this.groupBox1.TabIndex = 36;
            this.groupBox1.TabStop = false;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tpBeneficios);
            this.tabControl1.Controls.Add(this.tpCadastro);
            this.tabControl1.Controls.Add(this.tpCaixa);
            this.tabControl1.Controls.Add(this.tpEstoque);
            this.tabControl1.Controls.Add(this.tpGeral);
            this.tabControl1.Controls.Add(this.tpImpressora);
            this.tabControl1.Controls.Add(this.tpPagar);
            this.tabControl1.Controls.Add(this.tpReceber);
            this.tabControl1.Controls.Add(this.tpSat);
            this.tabControl1.Controls.Add(this.tpSngpc);
            this.tabControl1.Controls.Add(this.tpVendas);
            this.tabControl1.ItemSize = new System.Drawing.Size(60, 21);
            this.tabControl1.Location = new System.Drawing.Point(6, 33);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(965, 521);
            this.tabControl1.TabIndex = 0;
            // 
            // tpBeneficios
            // 
            this.tpBeneficios.Controls.Add(this.dgBeneficios);
            this.tpBeneficios.ForeColor = System.Drawing.Color.Black;
            this.tpBeneficios.Location = new System.Drawing.Point(4, 25);
            this.tpBeneficios.Name = "tpBeneficios";
            this.tpBeneficios.Padding = new System.Windows.Forms.Padding(3);
            this.tpBeneficios.Size = new System.Drawing.Size(957, 492);
            this.tpBeneficios.TabIndex = 10;
            this.tpBeneficios.Text = "Benefícios";
            this.tpBeneficios.UseVisualStyleBackColor = true;
            // 
            // dgBeneficios
            // 
            this.dgBeneficios.AllowUserToAddRows = false;
            this.dgBeneficios.AllowUserToDeleteRows = false;
            this.dgBeneficios.AllowUserToResizeColumns = false;
            this.dgBeneficios.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgBeneficios.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgBeneficios.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgBeneficios.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgBeneficios.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgBeneficios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBeneficios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn56,
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgBeneficios.DefaultCellStyle = dataGridViewCellStyle4;
            this.dgBeneficios.GridColor = System.Drawing.Color.LightGray;
            this.dgBeneficios.Location = new System.Drawing.Point(6, 6);
            this.dgBeneficios.MultiSelect = false;
            this.dgBeneficios.Name = "dgBeneficios";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgBeneficios.RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgBeneficios.RowHeadersVisible = false;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            this.dgBeneficios.RowsDefaultCellStyle = dataGridViewCellStyle6;
            this.dgBeneficios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgBeneficios.Size = new System.Drawing.Size(945, 480);
            this.dgBeneficios.TabIndex = 1;
            this.dgBeneficios.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBeneficios_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.DataPropertyName = "PAR_TABELA";
            dataGridViewCellStyle3.Format = "N0";
            dataGridViewCellStyle3.NullValue = null;
            this.dataGridViewTextBoxColumn55.DefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridViewTextBoxColumn55.HeaderText = "Tabela";
            this.dataGridViewTextBoxColumn55.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.ReadOnly = true;
            this.dataGridViewTextBoxColumn55.Width = 50;
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.DataPropertyName = "PAR_ESTACAO";
            this.dataGridViewTextBoxColumn56.HeaderText = "Estação";
            this.dataGridViewTextBoxColumn56.MaxInputLength = 30;
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.DataPropertyName = "PAR_POSICAO";
            this.dataGridViewTextBoxColumn57.HeaderText = "Posição";
            this.dataGridViewTextBoxColumn57.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.ReadOnly = true;
            this.dataGridViewTextBoxColumn57.Width = 60;
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.DataPropertyName = "PAR_COMENT";
            this.dataGridViewTextBoxColumn58.HeaderText = "Comentário";
            this.dataGridViewTextBoxColumn58.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.ReadOnly = true;
            this.dataGridViewTextBoxColumn58.Width = 300;
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.DataPropertyName = "PAR_DESCR";
            this.dataGridViewTextBoxColumn59.HeaderText = "Conteúdo";
            this.dataGridViewTextBoxColumn59.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.Width = 300;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.DataPropertyName = "PAR_OBS";
            this.dataGridViewTextBoxColumn60.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn60.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.ReadOnly = true;
            this.dataGridViewTextBoxColumn60.Width = 200;
            // 
            // tpCadastro
            // 
            this.tpCadastro.Controls.Add(this.dgCadastro);
            this.tpCadastro.ForeColor = System.Drawing.Color.Black;
            this.tpCadastro.Location = new System.Drawing.Point(4, 25);
            this.tpCadastro.Name = "tpCadastro";
            this.tpCadastro.Padding = new System.Windows.Forms.Padding(3);
            this.tpCadastro.Size = new System.Drawing.Size(957, 492);
            this.tpCadastro.TabIndex = 1;
            this.tpCadastro.Text = "Cadastro";
            this.tpCadastro.UseVisualStyleBackColor = true;
            // 
            // dgCadastro
            // 
            this.dgCadastro.AllowUserToAddRows = false;
            this.dgCadastro.AllowUserToDeleteRows = false;
            this.dgCadastro.AllowUserToResizeColumns = false;
            this.dgCadastro.AllowUserToResizeRows = false;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCadastro.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle7;
            this.dgCadastro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCadastro.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCadastro.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle8;
            this.dgCadastro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCadastro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6});
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCadastro.DefaultCellStyle = dataGridViewCellStyle10;
            this.dgCadastro.GridColor = System.Drawing.Color.LightGray;
            this.dgCadastro.Location = new System.Drawing.Point(6, 6);
            this.dgCadastro.MultiSelect = false;
            this.dgCadastro.Name = "dgCadastro";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCadastro.RowHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgCadastro.RowHeadersVisible = false;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCadastro.RowsDefaultCellStyle = dataGridViewCellStyle12;
            this.dgCadastro.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCadastro.Size = new System.Drawing.Size(945, 480);
            this.dgCadastro.TabIndex = 1;
            this.dgCadastro.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCadastro_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "PAR_TABELA";
            dataGridViewCellStyle9.Format = "N0";
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewTextBoxColumn1.HeaderText = "Tabela";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.ReadOnly = true;
            this.dataGridViewTextBoxColumn1.Width = 50;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "PAR_ESTACAO";
            this.dataGridViewTextBoxColumn2.HeaderText = "Estação";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 30;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "PAR_POSICAO";
            this.dataGridViewTextBoxColumn3.HeaderText = "Posição";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.ReadOnly = true;
            this.dataGridViewTextBoxColumn3.Width = 60;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "PAR_COMENT";
            this.dataGridViewTextBoxColumn4.HeaderText = "Comentário";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.ReadOnly = true;
            this.dataGridViewTextBoxColumn4.Width = 300;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "PAR_DESCR";
            this.dataGridViewTextBoxColumn5.HeaderText = "Conteúdo";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 300;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "PAR_OBS";
            this.dataGridViewTextBoxColumn6.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.ReadOnly = true;
            this.dataGridViewTextBoxColumn6.Width = 200;
            // 
            // tpCaixa
            // 
            this.tpCaixa.Controls.Add(this.dgCaixa);
            this.tpCaixa.ForeColor = System.Drawing.Color.Black;
            this.tpCaixa.Location = new System.Drawing.Point(4, 25);
            this.tpCaixa.Name = "tpCaixa";
            this.tpCaixa.Size = new System.Drawing.Size(957, 492);
            this.tpCaixa.TabIndex = 4;
            this.tpCaixa.Text = "Caixa";
            this.tpCaixa.UseVisualStyleBackColor = true;
            // 
            // dgCaixa
            // 
            this.dgCaixa.AllowUserToAddRows = false;
            this.dgCaixa.AllowUserToDeleteRows = false;
            this.dgCaixa.AllowUserToResizeColumns = false;
            this.dgCaixa.AllowUserToResizeRows = false;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCaixa.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle13;
            this.dgCaixa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCaixa.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCaixa.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle14;
            this.dgCaixa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCaixa.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24});
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCaixa.DefaultCellStyle = dataGridViewCellStyle16;
            this.dgCaixa.GridColor = System.Drawing.Color.LightGray;
            this.dgCaixa.Location = new System.Drawing.Point(6, 6);
            this.dgCaixa.MultiSelect = false;
            this.dgCaixa.Name = "dgCaixa";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle17.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle17.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle17.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCaixa.RowHeadersDefaultCellStyle = dataGridViewCellStyle17;
            this.dgCaixa.RowHeadersVisible = false;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCaixa.RowsDefaultCellStyle = dataGridViewCellStyle18;
            this.dgCaixa.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCaixa.Size = new System.Drawing.Size(945, 480);
            this.dgCaixa.TabIndex = 1;
            this.dgCaixa.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCaixa_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "PAR_TABELA";
            dataGridViewCellStyle15.Format = "N0";
            this.dataGridViewTextBoxColumn19.DefaultCellStyle = dataGridViewCellStyle15;
            this.dataGridViewTextBoxColumn19.HeaderText = "Tabela";
            this.dataGridViewTextBoxColumn19.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.ReadOnly = true;
            this.dataGridViewTextBoxColumn19.Width = 50;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "PAR_ESTACAO";
            this.dataGridViewTextBoxColumn20.HeaderText = "Estação";
            this.dataGridViewTextBoxColumn20.MaxInputLength = 30;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "PAR_POSICAO";
            this.dataGridViewTextBoxColumn21.HeaderText = "Posição";
            this.dataGridViewTextBoxColumn21.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.ReadOnly = true;
            this.dataGridViewTextBoxColumn21.Width = 60;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "PAR_COMENT";
            this.dataGridViewTextBoxColumn22.HeaderText = "Comentário";
            this.dataGridViewTextBoxColumn22.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.ReadOnly = true;
            this.dataGridViewTextBoxColumn22.Width = 300;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "PAR_DESCR";
            this.dataGridViewTextBoxColumn23.HeaderText = "Conteúdo";
            this.dataGridViewTextBoxColumn23.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.Width = 300;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "PAR_OBS";
            this.dataGridViewTextBoxColumn24.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn24.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.ReadOnly = true;
            this.dataGridViewTextBoxColumn24.Width = 200;
            // 
            // tpEstoque
            // 
            this.tpEstoque.Controls.Add(this.dgEstoque);
            this.tpEstoque.ForeColor = System.Drawing.Color.Black;
            this.tpEstoque.Location = new System.Drawing.Point(4, 25);
            this.tpEstoque.Name = "tpEstoque";
            this.tpEstoque.Size = new System.Drawing.Size(957, 492);
            this.tpEstoque.TabIndex = 5;
            this.tpEstoque.Text = "Estoque";
            this.tpEstoque.UseVisualStyleBackColor = true;
            // 
            // dgEstoque
            // 
            this.dgEstoque.AllowUserToAddRows = false;
            this.dgEstoque.AllowUserToDeleteRows = false;
            this.dgEstoque.AllowUserToResizeColumns = false;
            this.dgEstoque.AllowUserToResizeRows = false;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEstoque.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
            this.dgEstoque.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgEstoque.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEstoque.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
            this.dgEstoque.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEstoque.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30});
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEstoque.DefaultCellStyle = dataGridViewCellStyle22;
            this.dgEstoque.GridColor = System.Drawing.Color.LightGray;
            this.dgEstoque.Location = new System.Drawing.Point(6, 6);
            this.dgEstoque.MultiSelect = false;
            this.dgEstoque.Name = "dgEstoque";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle23.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle23.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle23.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle23.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle23.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle23.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEstoque.RowHeadersDefaultCellStyle = dataGridViewCellStyle23;
            this.dgEstoque.RowHeadersVisible = false;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEstoque.RowsDefaultCellStyle = dataGridViewCellStyle24;
            this.dgEstoque.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgEstoque.Size = new System.Drawing.Size(945, 480);
            this.dgEstoque.TabIndex = 1;
            this.dgEstoque.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEstoque_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "PAR_TABELA";
            dataGridViewCellStyle21.Format = "N0";
            this.dataGridViewTextBoxColumn25.DefaultCellStyle = dataGridViewCellStyle21;
            this.dataGridViewTextBoxColumn25.HeaderText = "Tabela";
            this.dataGridViewTextBoxColumn25.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.ReadOnly = true;
            this.dataGridViewTextBoxColumn25.Width = 50;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "PAR_ESTACAO";
            this.dataGridViewTextBoxColumn26.HeaderText = "Estação";
            this.dataGridViewTextBoxColumn26.MaxInputLength = 30;
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "PAR_POSICAO";
            this.dataGridViewTextBoxColumn27.HeaderText = "Posição";
            this.dataGridViewTextBoxColumn27.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.ReadOnly = true;
            this.dataGridViewTextBoxColumn27.Width = 60;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "PAR_COMENT";
            this.dataGridViewTextBoxColumn28.HeaderText = "Comentário";
            this.dataGridViewTextBoxColumn28.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.ReadOnly = true;
            this.dataGridViewTextBoxColumn28.Width = 300;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "PAR_DESCR";
            this.dataGridViewTextBoxColumn29.HeaderText = "Conteúdo";
            this.dataGridViewTextBoxColumn29.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.Width = 300;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "PAR_OBS";
            this.dataGridViewTextBoxColumn30.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn30.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.ReadOnly = true;
            this.dataGridViewTextBoxColumn30.Width = 200;
            // 
            // tpGeral
            // 
            this.tpGeral.Controls.Add(this.dgGeral);
            this.tpGeral.ForeColor = System.Drawing.Color.Black;
            this.tpGeral.Location = new System.Drawing.Point(4, 25);
            this.tpGeral.Name = "tpGeral";
            this.tpGeral.Padding = new System.Windows.Forms.Padding(3);
            this.tpGeral.Size = new System.Drawing.Size(957, 492);
            this.tpGeral.TabIndex = 0;
            this.tpGeral.Text = "Geral";
            this.tpGeral.UseVisualStyleBackColor = true;
            // 
            // dgGeral
            // 
            this.dgGeral.AllowUserToAddRows = false;
            this.dgGeral.AllowUserToDeleteRows = false;
            this.dgGeral.AllowUserToResizeColumns = false;
            this.dgGeral.AllowUserToResizeRows = false;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            this.dgGeral.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle25;
            this.dgGeral.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgGeral.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgGeral.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle26;
            this.dgGeral.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgGeral.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.PAR_TABELA,
            this.PAR_ESTACAO,
            this.PAR_POSICAO,
            this.PAR_COMENT,
            this.PAR_DESCR,
            this.PAR_OBS});
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgGeral.DefaultCellStyle = dataGridViewCellStyle28;
            this.dgGeral.GridColor = System.Drawing.Color.LightGray;
            this.dgGeral.Location = new System.Drawing.Point(6, 6);
            this.dgGeral.MultiSelect = false;
            this.dgGeral.Name = "dgGeral";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle29.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle29.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle29.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle29.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle29.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle29.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgGeral.RowHeadersDefaultCellStyle = dataGridViewCellStyle29;
            this.dgGeral.RowHeadersVisible = false;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black;
            this.dgGeral.RowsDefaultCellStyle = dataGridViewCellStyle30;
            this.dgGeral.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgGeral.Size = new System.Drawing.Size(945, 480);
            this.dgGeral.TabIndex = 0;
            this.dgGeral.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgGeral_CellEndEdit);
            // 
            // PAR_TABELA
            // 
            this.PAR_TABELA.DataPropertyName = "PAR_TABELA";
            dataGridViewCellStyle27.Format = "N0";
            this.PAR_TABELA.DefaultCellStyle = dataGridViewCellStyle27;
            this.PAR_TABELA.HeaderText = "Tabela";
            this.PAR_TABELA.MaxInputLength = 18;
            this.PAR_TABELA.Name = "PAR_TABELA";
            this.PAR_TABELA.ReadOnly = true;
            this.PAR_TABELA.Width = 50;
            // 
            // PAR_ESTACAO
            // 
            this.PAR_ESTACAO.DataPropertyName = "PAR_ESTACAO";
            this.PAR_ESTACAO.HeaderText = "Estação";
            this.PAR_ESTACAO.MaxInputLength = 30;
            this.PAR_ESTACAO.Name = "PAR_ESTACAO";
            this.PAR_ESTACAO.ReadOnly = true;
            // 
            // PAR_POSICAO
            // 
            this.PAR_POSICAO.DataPropertyName = "PAR_POSICAO";
            this.PAR_POSICAO.HeaderText = "Posição";
            this.PAR_POSICAO.MaxInputLength = 10;
            this.PAR_POSICAO.Name = "PAR_POSICAO";
            this.PAR_POSICAO.ReadOnly = true;
            this.PAR_POSICAO.Width = 60;
            // 
            // PAR_COMENT
            // 
            this.PAR_COMENT.DataPropertyName = "PAR_COMENT";
            this.PAR_COMENT.HeaderText = "Comentário";
            this.PAR_COMENT.MaxInputLength = 100;
            this.PAR_COMENT.Name = "PAR_COMENT";
            this.PAR_COMENT.ReadOnly = true;
            this.PAR_COMENT.Width = 300;
            // 
            // PAR_DESCR
            // 
            this.PAR_DESCR.DataPropertyName = "PAR_DESCR";
            this.PAR_DESCR.HeaderText = "Conteúdo";
            this.PAR_DESCR.MaxInputLength = 100;
            this.PAR_DESCR.Name = "PAR_DESCR";
            this.PAR_DESCR.Width = 300;
            // 
            // PAR_OBS
            // 
            this.PAR_OBS.DataPropertyName = "PAR_OBS";
            this.PAR_OBS.HeaderText = "Descrição";
            this.PAR_OBS.MaxInputLength = 100;
            this.PAR_OBS.Name = "PAR_OBS";
            this.PAR_OBS.ReadOnly = true;
            this.PAR_OBS.Width = 200;
            // 
            // tpImpressora
            // 
            this.tpImpressora.Controls.Add(this.dgImpressora);
            this.tpImpressora.ForeColor = System.Drawing.Color.Black;
            this.tpImpressora.Location = new System.Drawing.Point(4, 25);
            this.tpImpressora.Name = "tpImpressora";
            this.tpImpressora.Size = new System.Drawing.Size(957, 492);
            this.tpImpressora.TabIndex = 8;
            this.tpImpressora.Text = "Impressora";
            this.tpImpressora.UseVisualStyleBackColor = true;
            // 
            // dgImpressora
            // 
            this.dgImpressora.AllowUserToAddRows = false;
            this.dgImpressora.AllowUserToDeleteRows = false;
            this.dgImpressora.AllowUserToResizeColumns = false;
            this.dgImpressora.AllowUserToResizeRows = false;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.Black;
            this.dgImpressora.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle31;
            this.dgImpressora.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgImpressora.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle32.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgImpressora.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle32;
            this.dgImpressora.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgImpressora.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48});
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgImpressora.DefaultCellStyle = dataGridViewCellStyle34;
            this.dgImpressora.GridColor = System.Drawing.Color.LightGray;
            this.dgImpressora.Location = new System.Drawing.Point(6, 6);
            this.dgImpressora.MultiSelect = false;
            this.dgImpressora.Name = "dgImpressora";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle35.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle35.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle35.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle35.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle35.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle35.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgImpressora.RowHeadersDefaultCellStyle = dataGridViewCellStyle35;
            this.dgImpressora.RowHeadersVisible = false;
            dataGridViewCellStyle36.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black;
            this.dgImpressora.RowsDefaultCellStyle = dataGridViewCellStyle36;
            this.dgImpressora.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgImpressora.Size = new System.Drawing.Size(945, 480);
            this.dgImpressora.TabIndex = 1;
            this.dgImpressora.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgImpressora_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.DataPropertyName = "PAR_TABELA";
            dataGridViewCellStyle33.Format = "N0";
            this.dataGridViewTextBoxColumn43.DefaultCellStyle = dataGridViewCellStyle33;
            this.dataGridViewTextBoxColumn43.HeaderText = "Tabela";
            this.dataGridViewTextBoxColumn43.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.ReadOnly = true;
            this.dataGridViewTextBoxColumn43.Width = 70;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.DataPropertyName = "PAR_ESTACAO";
            this.dataGridViewTextBoxColumn44.HeaderText = "Estação";
            this.dataGridViewTextBoxColumn44.MaxInputLength = 30;
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.ReadOnly = true;
            this.dataGridViewTextBoxColumn44.Width = 80;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.DataPropertyName = "PAR_POSICAO";
            this.dataGridViewTextBoxColumn45.HeaderText = "Posição";
            this.dataGridViewTextBoxColumn45.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.ReadOnly = true;
            this.dataGridViewTextBoxColumn45.Width = 80;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.DataPropertyName = "PAR_COMENT";
            this.dataGridViewTextBoxColumn46.HeaderText = "Comentário";
            this.dataGridViewTextBoxColumn46.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.ReadOnly = true;
            this.dataGridViewTextBoxColumn46.Width = 300;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.DataPropertyName = "PAR_DESCR";
            this.dataGridViewTextBoxColumn47.HeaderText = "Conteúdo";
            this.dataGridViewTextBoxColumn47.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.Width = 300;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.DataPropertyName = "PAR_OBS";
            this.dataGridViewTextBoxColumn48.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn48.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.ReadOnly = true;
            this.dataGridViewTextBoxColumn48.Width = 200;
            // 
            // tpPagar
            // 
            this.tpPagar.Controls.Add(this.dgPagar);
            this.tpPagar.ForeColor = System.Drawing.Color.Black;
            this.tpPagar.Location = new System.Drawing.Point(4, 25);
            this.tpPagar.Name = "tpPagar";
            this.tpPagar.Size = new System.Drawing.Size(957, 492);
            this.tpPagar.TabIndex = 6;
            this.tpPagar.Text = "Contas à Pagar";
            this.tpPagar.UseVisualStyleBackColor = true;
            // 
            // dgPagar
            // 
            this.dgPagar.AllowUserToAddRows = false;
            this.dgPagar.AllowUserToDeleteRows = false;
            this.dgPagar.AllowUserToResizeColumns = false;
            this.dgPagar.AllowUserToResizeRows = false;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPagar.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle37;
            this.dgPagar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPagar.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPagar.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle38;
            this.dgPagar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPagar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32,
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36});
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPagar.DefaultCellStyle = dataGridViewCellStyle40;
            this.dgPagar.GridColor = System.Drawing.Color.LightGray;
            this.dgPagar.Location = new System.Drawing.Point(6, 6);
            this.dgPagar.MultiSelect = false;
            this.dgPagar.Name = "dgPagar";
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle41.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle41.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle41.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle41.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle41.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle41.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPagar.RowHeadersDefaultCellStyle = dataGridViewCellStyle41;
            this.dgPagar.RowHeadersVisible = false;
            dataGridViewCellStyle42.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPagar.RowsDefaultCellStyle = dataGridViewCellStyle42;
            this.dgPagar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgPagar.Size = new System.Drawing.Size(945, 480);
            this.dgPagar.TabIndex = 1;
            this.dgPagar.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPagar_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "PAR_TABELA";
            dataGridViewCellStyle39.Format = "N0";
            this.dataGridViewTextBoxColumn31.DefaultCellStyle = dataGridViewCellStyle39;
            this.dataGridViewTextBoxColumn31.HeaderText = "Tabela";
            this.dataGridViewTextBoxColumn31.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.ReadOnly = true;
            this.dataGridViewTextBoxColumn31.Width = 50;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "PAR_ESTACAO";
            this.dataGridViewTextBoxColumn32.HeaderText = "Estação";
            this.dataGridViewTextBoxColumn32.MaxInputLength = 30;
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "PAR_POSICAO";
            this.dataGridViewTextBoxColumn33.HeaderText = "Posição";
            this.dataGridViewTextBoxColumn33.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.ReadOnly = true;
            this.dataGridViewTextBoxColumn33.Width = 60;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "PAR_COMENT";
            this.dataGridViewTextBoxColumn34.HeaderText = "Comentário";
            this.dataGridViewTextBoxColumn34.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.ReadOnly = true;
            this.dataGridViewTextBoxColumn34.Width = 300;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "PAR_DESCR";
            this.dataGridViewTextBoxColumn35.HeaderText = "Conteúdo";
            this.dataGridViewTextBoxColumn35.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.Width = 300;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "PAR_OBS";
            this.dataGridViewTextBoxColumn36.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn36.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.ReadOnly = true;
            this.dataGridViewTextBoxColumn36.Width = 200;
            // 
            // tpReceber
            // 
            this.tpReceber.Controls.Add(this.dgReceber);
            this.tpReceber.ForeColor = System.Drawing.Color.Black;
            this.tpReceber.Location = new System.Drawing.Point(4, 25);
            this.tpReceber.Name = "tpReceber";
            this.tpReceber.Size = new System.Drawing.Size(957, 492);
            this.tpReceber.TabIndex = 7;
            this.tpReceber.Text = "Contas à Receber";
            this.tpReceber.UseVisualStyleBackColor = true;
            // 
            // dgReceber
            // 
            this.dgReceber.AllowUserToAddRows = false;
            this.dgReceber.AllowUserToDeleteRows = false;
            this.dgReceber.AllowUserToResizeColumns = false;
            this.dgReceber.AllowUserToResizeRows = false;
            dataGridViewCellStyle43.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle43.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.Black;
            this.dgReceber.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle43;
            this.dgReceber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgReceber.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle44.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle44.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle44.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle44.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgReceber.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle44;
            this.dgReceber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgReceber.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40,
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42});
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle46.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle46.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle46.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle46.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle46.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgReceber.DefaultCellStyle = dataGridViewCellStyle46;
            this.dgReceber.GridColor = System.Drawing.Color.LightGray;
            this.dgReceber.Location = new System.Drawing.Point(6, 6);
            this.dgReceber.MultiSelect = false;
            this.dgReceber.Name = "dgReceber";
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle47.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle47.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle47.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle47.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle47.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle47.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgReceber.RowHeadersDefaultCellStyle = dataGridViewCellStyle47;
            this.dgReceber.RowHeadersVisible = false;
            dataGridViewCellStyle48.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle48.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.Color.Black;
            this.dgReceber.RowsDefaultCellStyle = dataGridViewCellStyle48;
            this.dgReceber.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgReceber.Size = new System.Drawing.Size(945, 480);
            this.dgReceber.TabIndex = 1;
            this.dgReceber.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgReceber_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "PAR_TABELA";
            dataGridViewCellStyle45.Format = "N0";
            this.dataGridViewTextBoxColumn37.DefaultCellStyle = dataGridViewCellStyle45;
            this.dataGridViewTextBoxColumn37.HeaderText = "Tabela";
            this.dataGridViewTextBoxColumn37.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.ReadOnly = true;
            this.dataGridViewTextBoxColumn37.Width = 50;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "PAR_ESTACAO";
            this.dataGridViewTextBoxColumn38.HeaderText = "Estação";
            this.dataGridViewTextBoxColumn38.MaxInputLength = 30;
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "PAR_POSICAO";
            this.dataGridViewTextBoxColumn39.HeaderText = "Posição";
            this.dataGridViewTextBoxColumn39.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.ReadOnly = true;
            this.dataGridViewTextBoxColumn39.Width = 60;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.DataPropertyName = "PAR_COMENT";
            this.dataGridViewTextBoxColumn40.HeaderText = "Comentário";
            this.dataGridViewTextBoxColumn40.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.ReadOnly = true;
            this.dataGridViewTextBoxColumn40.Width = 300;
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.DataPropertyName = "PAR_DESCR";
            this.dataGridViewTextBoxColumn41.HeaderText = "Conteúdo";
            this.dataGridViewTextBoxColumn41.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.Width = 300;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.DataPropertyName = "PAR_OBS";
            this.dataGridViewTextBoxColumn42.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn42.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.ReadOnly = true;
            this.dataGridViewTextBoxColumn42.Width = 200;
            // 
            // tpSat
            // 
            this.tpSat.Controls.Add(this.dgSAT);
            this.tpSat.ForeColor = System.Drawing.Color.Black;
            this.tpSat.Location = new System.Drawing.Point(4, 25);
            this.tpSat.Name = "tpSat";
            this.tpSat.Padding = new System.Windows.Forms.Padding(3);
            this.tpSat.Size = new System.Drawing.Size(957, 492);
            this.tpSat.TabIndex = 9;
            this.tpSat.Text = "SAT";
            this.tpSat.UseVisualStyleBackColor = true;
            // 
            // dgSAT
            // 
            this.dgSAT.AllowUserToAddRows = false;
            this.dgSAT.AllowUserToDeleteRows = false;
            this.dgSAT.AllowUserToResizeColumns = false;
            this.dgSAT.AllowUserToResizeRows = false;
            dataGridViewCellStyle49.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle49.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle49.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle49.SelectionForeColor = System.Drawing.Color.Black;
            this.dgSAT.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle49;
            this.dgSAT.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSAT.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle50.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle50.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle50.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle50.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle50.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle50.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSAT.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle50;
            this.dgSAT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSAT.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn54});
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle52.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle52.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle52.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle52.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle52.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle52.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgSAT.DefaultCellStyle = dataGridViewCellStyle52;
            this.dgSAT.GridColor = System.Drawing.Color.LightGray;
            this.dgSAT.Location = new System.Drawing.Point(6, 6);
            this.dgSAT.MultiSelect = false;
            this.dgSAT.Name = "dgSAT";
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle53.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle53.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle53.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle53.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle53.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle53.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSAT.RowHeadersDefaultCellStyle = dataGridViewCellStyle53;
            this.dgSAT.RowHeadersVisible = false;
            dataGridViewCellStyle54.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle54.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.Color.Black;
            this.dgSAT.RowsDefaultCellStyle = dataGridViewCellStyle54;
            this.dgSAT.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSAT.Size = new System.Drawing.Size(945, 480);
            this.dgSAT.TabIndex = 1;
            this.dgSAT.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSAT_CellEndEdit);
            // 
            // tpSngpc
            // 
            this.tpSngpc.Controls.Add(this.dgSngpc);
            this.tpSngpc.ForeColor = System.Drawing.Color.Black;
            this.tpSngpc.Location = new System.Drawing.Point(4, 25);
            this.tpSngpc.Name = "tpSngpc";
            this.tpSngpc.Size = new System.Drawing.Size(957, 492);
            this.tpSngpc.TabIndex = 3;
            this.tpSngpc.Text = "SNGPC";
            this.tpSngpc.UseVisualStyleBackColor = true;
            // 
            // dgSngpc
            // 
            this.dgSngpc.AllowUserToAddRows = false;
            this.dgSngpc.AllowUserToDeleteRows = false;
            this.dgSngpc.AllowUserToResizeColumns = false;
            this.dgSngpc.AllowUserToResizeRows = false;
            dataGridViewCellStyle55.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle55.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.Color.Black;
            this.dgSngpc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle55;
            this.dgSngpc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSngpc.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle56.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle56.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle56.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle56.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle56.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle56.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle56.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSngpc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle56;
            this.dgSngpc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSngpc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18});
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle58.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle58.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle58.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle58.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle58.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle58.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgSngpc.DefaultCellStyle = dataGridViewCellStyle58;
            this.dgSngpc.GridColor = System.Drawing.Color.LightGray;
            this.dgSngpc.Location = new System.Drawing.Point(6, 6);
            this.dgSngpc.MultiSelect = false;
            this.dgSngpc.Name = "dgSngpc";
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle59.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle59.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle59.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle59.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle59.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle59.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSngpc.RowHeadersDefaultCellStyle = dataGridViewCellStyle59;
            this.dgSngpc.RowHeadersVisible = false;
            dataGridViewCellStyle60.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle60.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle60.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle60.SelectionForeColor = System.Drawing.Color.Black;
            this.dgSngpc.RowsDefaultCellStyle = dataGridViewCellStyle60;
            this.dgSngpc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgSngpc.Size = new System.Drawing.Size(945, 480);
            this.dgSngpc.TabIndex = 1;
            this.dgSngpc.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSngpc_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "PAR_TABELA";
            dataGridViewCellStyle57.Format = "N0";
            this.dataGridViewTextBoxColumn13.DefaultCellStyle = dataGridViewCellStyle57;
            this.dataGridViewTextBoxColumn13.HeaderText = "Tabela";
            this.dataGridViewTextBoxColumn13.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.ReadOnly = true;
            this.dataGridViewTextBoxColumn13.Width = 50;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "PAR_ESTACAO";
            this.dataGridViewTextBoxColumn14.HeaderText = "Estação";
            this.dataGridViewTextBoxColumn14.MaxInputLength = 30;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "PAR_POSICAO";
            this.dataGridViewTextBoxColumn15.HeaderText = "Posição";
            this.dataGridViewTextBoxColumn15.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.ReadOnly = true;
            this.dataGridViewTextBoxColumn15.Width = 60;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "PAR_COMENT";
            this.dataGridViewTextBoxColumn16.HeaderText = "Comentário";
            this.dataGridViewTextBoxColumn16.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.ReadOnly = true;
            this.dataGridViewTextBoxColumn16.Width = 300;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "PAR_DESCR";
            this.dataGridViewTextBoxColumn17.HeaderText = "Conteúdo";
            this.dataGridViewTextBoxColumn17.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.Width = 300;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "PAR_OBS";
            this.dataGridViewTextBoxColumn18.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn18.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.ReadOnly = true;
            this.dataGridViewTextBoxColumn18.Width = 200;
            // 
            // tpVendas
            // 
            this.tpVendas.Controls.Add(this.dgVendas);
            this.tpVendas.ForeColor = System.Drawing.Color.Black;
            this.tpVendas.Location = new System.Drawing.Point(4, 25);
            this.tpVendas.Name = "tpVendas";
            this.tpVendas.Size = new System.Drawing.Size(957, 492);
            this.tpVendas.TabIndex = 2;
            this.tpVendas.Text = "Vendas";
            this.tpVendas.UseVisualStyleBackColor = true;
            // 
            // dgVendas
            // 
            this.dgVendas.AllowUserToAddRows = false;
            this.dgVendas.AllowUserToDeleteRows = false;
            this.dgVendas.AllowUserToResizeColumns = false;
            this.dgVendas.AllowUserToResizeRows = false;
            dataGridViewCellStyle61.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle61.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle61.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle61.SelectionForeColor = System.Drawing.Color.Black;
            this.dgVendas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle61;
            this.dgVendas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgVendas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle62.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle62.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle62.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle62.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle62.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgVendas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle62;
            this.dgVendas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVendas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12});
            dataGridViewCellStyle64.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle64.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle64.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle64.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle64.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle64.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle64.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgVendas.DefaultCellStyle = dataGridViewCellStyle64;
            this.dgVendas.GridColor = System.Drawing.Color.LightGray;
            this.dgVendas.Location = new System.Drawing.Point(6, 6);
            this.dgVendas.MultiSelect = false;
            this.dgVendas.Name = "dgVendas";
            dataGridViewCellStyle65.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle65.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle65.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle65.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle65.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle65.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle65.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgVendas.RowHeadersDefaultCellStyle = dataGridViewCellStyle65;
            this.dgVendas.RowHeadersVisible = false;
            dataGridViewCellStyle66.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle66.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle66.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle66.SelectionForeColor = System.Drawing.Color.Black;
            this.dgVendas.RowsDefaultCellStyle = dataGridViewCellStyle66;
            this.dgVendas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgVendas.Size = new System.Drawing.Size(945, 480);
            this.dgVendas.TabIndex = 1;
            this.dgVendas.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgVendas_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "PAR_TABELA";
            dataGridViewCellStyle63.Format = "N0";
            this.dataGridViewTextBoxColumn7.DefaultCellStyle = dataGridViewCellStyle63;
            this.dataGridViewTextBoxColumn7.HeaderText = "Tabela";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.ReadOnly = true;
            this.dataGridViewTextBoxColumn7.Width = 50;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "PAR_ESTACAO";
            this.dataGridViewTextBoxColumn8.HeaderText = "Estação";
            this.dataGridViewTextBoxColumn8.MaxInputLength = 30;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "PAR_POSICAO";
            this.dataGridViewTextBoxColumn9.HeaderText = "Posição";
            this.dataGridViewTextBoxColumn9.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.ReadOnly = true;
            this.dataGridViewTextBoxColumn9.Width = 60;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "PAR_COMENT";
            this.dataGridViewTextBoxColumn10.HeaderText = "Comentário";
            this.dataGridViewTextBoxColumn10.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.ReadOnly = true;
            this.dataGridViewTextBoxColumn10.Width = 300;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "PAR_DESCR";
            this.dataGridViewTextBoxColumn11.HeaderText = "Conteúdo";
            this.dataGridViewTextBoxColumn11.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.Width = 300;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "PAR_OBS";
            this.dataGridViewTextBoxColumn12.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn12.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.ReadOnly = true;
            this.dataGridViewTextBoxColumn12.Width = 200;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label10);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(976, 24);
            this.panel1.TabIndex = 42;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.White;
            this.label10.Location = new System.Drawing.Point(7, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(199, 18);
            this.label10.TabIndex = 39;
            this.label10.Text = "Parametrização do Sistema";
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.DataPropertyName = "PAR_TABELA";
            dataGridViewCellStyle51.Format = "N0";
            this.dataGridViewTextBoxColumn49.DefaultCellStyle = dataGridViewCellStyle51;
            this.dataGridViewTextBoxColumn49.HeaderText = "Tabela";
            this.dataGridViewTextBoxColumn49.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.ReadOnly = true;
            this.dataGridViewTextBoxColumn49.Width = 50;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.DataPropertyName = "PAR_ESTACAO";
            this.dataGridViewTextBoxColumn50.HeaderText = "Estação";
            this.dataGridViewTextBoxColumn50.MaxInputLength = 30;
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.ReadOnly = true;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.DataPropertyName = "PAR_POSICAO";
            this.dataGridViewTextBoxColumn51.HeaderText = "Posição";
            this.dataGridViewTextBoxColumn51.MaxInputLength = 10;
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.ReadOnly = true;
            this.dataGridViewTextBoxColumn51.Width = 60;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.DataPropertyName = "PAR_COMENT";
            this.dataGridViewTextBoxColumn52.HeaderText = "Comentário";
            this.dataGridViewTextBoxColumn52.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.ReadOnly = true;
            this.dataGridViewTextBoxColumn52.Width = 300;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.DataPropertyName = "PAR_DESCR";
            this.dataGridViewTextBoxColumn53.HeaderText = "Conteúdo";
            this.dataGridViewTextBoxColumn53.MaxInputLength = 350;
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.Width = 300;
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.DataPropertyName = "PAR_OBS";
            this.dataGridViewTextBoxColumn54.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn54.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.ReadOnly = true;
            this.dataGridViewTextBoxColumn54.Width = 200;
            // 
            // frmParametros
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(994, 578);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "frmParametros";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmParametros";
            this.Load += new System.EventHandler(this.frmParametros_Load);
            this.Shown += new System.EventHandler(this.frmParametros_Shown);
            this.groupBox1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tpBeneficios.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgBeneficios)).EndInit();
            this.tpCadastro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCadastro)).EndInit();
            this.tpCaixa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCaixa)).EndInit();
            this.tpEstoque.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgEstoque)).EndInit();
            this.tpGeral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgGeral)).EndInit();
            this.tpImpressora.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgImpressora)).EndInit();
            this.tpPagar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPagar)).EndInit();
            this.tpReceber.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgReceber)).EndInit();
            this.tpSat.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSAT)).EndInit();
            this.tpSngpc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSngpc)).EndInit();
            this.tpVendas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgVendas)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpGeral;
        private System.Windows.Forms.TabPage tpCadastro;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tpVendas;
        private System.Windows.Forms.TabPage tpSngpc;
        private System.Windows.Forms.TabPage tpCaixa;
        private System.Windows.Forms.TabPage tpEstoque;
        private System.Windows.Forms.TabPage tpPagar;
        private System.Windows.Forms.TabPage tpReceber;
        private System.Windows.Forms.TabPage tpImpressora;
        private System.Windows.Forms.DataGridView dgGeral;
        private System.Windows.Forms.DataGridView dgCadastro;
        private System.Windows.Forms.DataGridView dgVendas;
        private System.Windows.Forms.DataGridView dgSngpc;
        private System.Windows.Forms.DataGridView dgCaixa;
        private System.Windows.Forms.DataGridView dgEstoque;
        private System.Windows.Forms.DataGridView dgPagar;
        private System.Windows.Forms.DataGridView dgReceber;
        private System.Windows.Forms.DataGridView dgImpressora;
        private System.Windows.Forms.TabPage tpSat;
        private System.Windows.Forms.TabPage tpBeneficios;
        private System.Windows.Forms.DataGridView dgSAT;
        private System.Windows.Forms.DataGridView dgBeneficios;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_TABELA;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_ESTACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_POSICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_COMENT;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn PAR_OBS;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
    }
}