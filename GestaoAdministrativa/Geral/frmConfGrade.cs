﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa.Geral
{
    public partial class frmConfGrade : Form
    {
        string[] mostrar;
        string[] colunas;
        int tamanho;

        public frmConfGrade(string[] formulario, int indice, string[] visivel)
        {
            InitializeComponent();
            colunas = new string[indice];
            mostrar = new string[indice];
            mostrar = visivel;
            colunas = formulario;
            tamanho = indice;
            Principal.matriz = new string[tamanho];
        }

        private void frmConfGrade_Load(object sender, EventArgs e)
        {
            try
            {
                for (int i = 0; i < tamanho; i++)
                {
                    if (mostrar[i] == "True")
                    {
                        dgColuna.Rows.Insert(dgColuna.RowCount, new Object[] { "True", colunas[i] });
                    }
                    else
                    {
                        dgColuna.Rows.Insert(dgColuna.RowCount, new Object[] { "False", colunas[i] });
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conf. Grade", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                int qtde = 0;

                for (int i = 0; i < dgColuna.RowCount; i++)
                {
                    if (dgColuna.Rows[i].Cells[0].Value.ToString() == "True")
                    {
                        Principal.matriz[qtde] = dgColuna.Rows[i].Cells[1].Value.ToString();
                        qtde = qtde + 1;
                    }
                }

                if (qtde == 0)
                {
                    Principal.matriz[qtde] = "Desmarcar";
                }

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Conf. Grade", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnMarcar_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgColuna.RowCount; i++)
            {
                dgColuna.Rows[i].Cells[0].Value = "True";
            }
        }

        private void btnDesmarcar_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < dgColuna.RowCount; i++)
            {
                dgColuna.Rows[i].Cells[0].Value = "False";
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Array.Clear(Principal.matriz, 0, tamanho);
            this.Close();
        }

        private void dgColuna_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnMarcar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnDesmarcar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnOk_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void btnCancelar_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        private void frmConfGrade_KeyDown(object sender, KeyEventArgs e)
        {
            teclaAtalho(e);
        }

        public void teclaAtalho(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
            }
        }
    }
}
