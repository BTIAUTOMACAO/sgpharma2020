﻿using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Negocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Geral
{
    public partial class frmSenha : Form, Botoes
    {

        public frmSenha()
        {
            InitializeComponent();

        }
        #region BOTOES
        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void FormularioFoco()
        {
            try
            {
                Funcoes.BotoesCadastro("frmSenha");

                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Estabelecimentos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            throw new NotImplementedException();
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Sair()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        #endregion

        private void frmSenha_Load(object sender, EventArgs e)
        {
            BuscaUsuario();
        }
        public void BuscaUsuario()
        {
            DataTable dtPesq = Util.CarregarCombosPorEstabelecimento("LOGIN_ID", "SENHA", "USUARIOS");
            cmbUsuario.DataSource = dtPesq;
            cmbUsuario.DisplayMember = "LOGIN_ID";
            cmbUsuario.ValueMember = "SENHA";
            cmbUsuario.SelectedIndex = 0;
            cmbUsuario.Focus();
        }

        private void cmbUsuario_SelectedValueChanged(object sender, EventArgs e)
        {
            txtSenha.Text = Funcoes.DescriptografaSenha(Convert.ToString(cmbUsuario.SelectedValue));
        }
    }
}
