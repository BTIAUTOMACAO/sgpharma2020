﻿using GestaoAdministrativa.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestaoAdministrativa.Geral
{
    public partial class frmFormularioChamado : Form, Botoes
    {
        public string caminho;
        private ToolStripButton tsbChamados = new ToolStripButton("Chamados");
        private ToolStripSeparator tssChamados = new ToolStripSeparator();

        public frmFormularioChamado(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        public void Botao()
        {
            try
            {
                this.tsbChamados.AutoSize = false;
                this.tsbChamados.Image = Properties.Resources.grupo;
                this.tsbChamados.Size = new System.Drawing.Size(125, 20);
                this.tsbChamados.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                Principal.mdiPrincipal.stsBotoes.Items.Add(tsbChamados);
                Principal.mdiPrincipal.stsBotoes.Items.Add(tssChamados);
                tsbChamados.Click += delegate
                {
                    var utilECF = Application.OpenForms.OfType<frmFormularioChamado>().FirstOrDefault();
                    Funcoes.BotoesCadastro(utilECF.Name);
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    utilECF.Focus();
                };
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void FormularioFoco()
        {
            try
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Sair()
        {
            try
            {
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbChamados);
                Principal.mdiPrincipal.stsBotoes.Items.Remove(tssChamados);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void frmFormularioChamado_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                txtAssunto.Focus();
            }
        }

        private void txtAssunto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(e.KeyChar == 13)
            {
                txtMensagem.Focus();
            }
        }

        private void btnAnexar_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                caminho = openFileDialog1.FileName;
                lblCaminho.Text = caminho;
            }
            else
            {
                lblCaminho.Text = "";
            }
        }

        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                if (String.IsNullOrEmpty(txtEmail.Text))
                {
                    MessageBox.Show("Necessário informar um E-mail", "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail.Focus();
                }
                else if (String.IsNullOrEmpty(txtAssunto.Text))
                {
                    MessageBox.Show("Necessário informar o Assunto", "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEmail.Focus();
                }
                else if (String.IsNullOrEmpty(txtMensagem.Text))
                {
                    MessageBox.Show("Necessário informar a Mensagem", "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtMensagem.Focus();
                }
                else
                {
                    Cursor = Cursors.WaitCursor;
                    
                    SmtpClient client = new SmtpClient("smtp.cdconectec.com.br");
                    client.Port = 587;
                    client.EnableSsl = false;
                    client.Timeout = 100000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;

                    client.Credentials = new System.Net.NetworkCredential("sgpharma@cdconectec.com.br", "Sat@#2019");
                    MailMessage msg = new MailMessage();
                    msg.To.Add(new MailAddress("ariane@drogabella.com.br"));
                    msg.To.Add(new MailAddress("lucia.bti@drogabella.com.br"));
                    if (!String.IsNullOrEmpty(caminho))
                    {
                        Attachment anexar = new Attachment(caminho);
                        msg.Attachments.Add(anexar);
                    }

                    msg.From = new MailAddress("sgpharma@cdconectec.com.br");
                    msg.Subject = txtAssunto.Text + " - " + Principal.nomeAtual;
                    msg.Body = txtMensagem.Text + "\n\nEmail da Farmácia: " + txtEmail.Text;

                    client.Send(msg);

                    MessageBox.Show("E-mail enviado com sucesso! Aguarde contato da resolução.", "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Limpar();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Chamados", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void btnZap_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            ProcessStartInfo sInfo = new ProcessStartInfo("https://api.whatsapp.com/send?phone=5512997678747&text=Suporte%20SGPharma");
            Process.Start(sInfo);
            Cursor = Cursors.Default;
        }

        public void Primeiro()
        {
            throw new NotImplementedException();
        }

        public void Ultimo()
        {
            throw new NotImplementedException();
        }

        public void Proximo()
        {
            throw new NotImplementedException();
        }

        public void Anterior()
        {
            throw new NotImplementedException();
        }

        public bool Atualiza()
        {
            throw new NotImplementedException();
        }

        public bool Incluir()
        {
            throw new NotImplementedException();
        }

        public bool Excluir()
        {
            throw new NotImplementedException();
        }

        public void Limpar()
        {
            txtEmail.Text = "";
            txtAssunto.Text = "";
            txtMensagem.Text = "";
            lblCaminho.Text = "";
            caminho = "";
            txtEmail.Focus();
        }
        
        public void ImprimirRelatorio()
        {
            throw new NotImplementedException();
        }

        public void AtalhoGrade()
        {
            throw new NotImplementedException();
        }

        public void AtalhoFicha()
        {
            throw new NotImplementedException();
        }

        public void Ajuda()
        {
            throw new NotImplementedException();
        }
    }
}
