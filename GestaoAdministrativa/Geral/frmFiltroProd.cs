﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;

namespace GestaoAdministrativa
{
    public partial class frmFiltroProd : Form
    {

        private string camposFiltro;
        public string filtro;
        private bool passou;
        public string idDepto;
        public string idClasse;
        public string  idSubClasse;
        public string idFab;

        public frmFiltroProd()
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        private void frmFiltroProd_Load(object sender, EventArgs e)
        {
            try
            {
                
                dgDepto.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgDepto.ForeColor = System.Drawing.Color.Black;
                dgDepto.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgDepto.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;

                dgClasses.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgClasses.ForeColor = System.Drawing.Color.Black;
                dgClasses.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgClasses.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;

                dgFabricantes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgFabricantes.ForeColor = System.Drawing.Color.Black;
                dgFabricantes.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgFabricantes.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;

                dgSubClasses.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgSubClasses.ForeColor = System.Drawing.Color.Black;
                dgSubClasses.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgSubClasses.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;

                rdbDesmarcar.Checked = false;
                rdbMarcar.Checked = false;
                txtDepto.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Filtro de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmFiltroProd_Shown(object sender, EventArgs e)
        {
            CarregaGrids();
        }

        public bool CarregaGrids()
        {
            try
            {
                //CARREGA DEPARTAMENTOS//
                Principal.dtPesq = Util.CarregarCombosPorEmpresa("DEP_CODIGO", "DEP_DESCR", "DEPARTAMENTOS", true, "DEP_DESABILITADO = 'N'");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um Departameto,\npara escolher um filtro.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    return false;
                }
                else
                {
                    dgDepto.DataSource = Principal.dtPesq;
                }

                //CARREGA CLASSES//
                Principal.dtPesq = Util.CarregarCombosPorEmpresa("CLAS_CODIGO", "CLAS_DESCR", "CLASSES", true, "CLAS_DESABILITADO = 'N'");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma Classe,\npara escolher um filtro.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    return false;
                }
                else
                {
                    dgClasses.DataSource = Principal.dtPesq;
                }

                //CARREGA SUBCLASSES//
                Principal.dtPesq = Util.CarregarCombosPorEmpresa("SUB_CODIGO", "SUB_DESCR", "SUBCLASSES", true, "SUB_DESABILITADO = 'N'");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos uma SubClasse,\npara realizadar um filtro.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    return false;
                }
                else
                {
                    dgSubClasses.DataSource = Principal.dtPesq;
                }

                //CARREGA FABRICANTES//
                Principal.dtPesq = Util.CarregarCombosPorEmpresa("FAB_CODIGO", "FAB_DESCRICAO", "FABRICANTES", true, "FAB_DESABILITADO = 'N'");
                if (Principal.dtPesq.Rows.Count == 0)
                {
                    MessageBox.Show("Necessário cadastrar pelo menos um Fabricante,\npara realizadar um filtro.", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                    return false;
                }
                else
                {
                    dgFabricantes.DataSource = Principal.dtPesq;
                }

                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Filtro de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

        }

        private void txtDepto_TextChanged(object sender, EventArgs e)
        {
            Util.FiltrarDados(Principal.estAtual, "DEPARTAMENTOS", "DEP_CODIGO", "DEP_DESCR", "DEP_DESABILITADO", dgDepto, txtDepto);
        }

        private void txtClasse_TextChanged(object sender, EventArgs e)
        {
            Util.FiltrarDados(Principal.estAtual, "CLASSES", "CLAS_CODIGO", "CLAS_DESCR", "CLAS_DESABILITADO", dgClasses, txtClasse);
        }

        private void txtSClasse_TextChanged(object sender, EventArgs e)
        {
            Util.FiltrarDados(Principal.estAtual, "SUBCLASSES", "SUB_CODIGO", "SUB_DESCR", "SUB_DESABILITADO", dgSubClasses, txtSClasse);
        }

        private void txtFabricantes_TextChanged(object sender, EventArgs e)
        {
            Util.FiltrarDados(Principal.estAtual, "FABRICANTES", "FAB_CODIGO", "FAB_DESCRICAO", "FAB_DESABILITADO", dgFabricantes, txtFabricantes);
        }

        private void txtDepto_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dgDepto.Focus();
        }

        private void txtClasse_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dgClasses.Focus();
        }

        private void txtSClasse_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dgSubClasses.Focus();
        }

        private void txtFabricantes_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                dgFabricantes.Focus();
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            try
            {
                camposFiltro = "";
                passou = false;

                for (int i = 0; i < dgDepto.RowCount; i++)
                {
                    if (Convert.ToString(dgDepto.Rows[i].Cells[0].Value).Equals("True"))
                    {
                        camposFiltro = camposFiltro + Convert.ToInt32(dgDepto.Rows[i].Cells[1].Value).ToString() + " ,";
                    }
                }

                if (!String.IsNullOrEmpty(camposFiltro))
                {
                    camposFiltro = camposFiltro.Substring(0, camposFiltro.Length - 1);
                    filtro = " " + idDepto + ".DEP_CODIGO IN (" + camposFiltro + ")";
                    passou = true;
                }
          
                camposFiltro = "";

                for (int i = 0; i < dgClasses.RowCount; i++)
                {
                    if (Convert.ToString(dgClasses.Rows[i].Cells[0].Value).Equals("True"))
                    {
                        camposFiltro = camposFiltro + Convert.ToInt32(dgClasses.Rows[i].Cells[1].Value).ToString() + " ,";
                    }
                }

                if (!String.IsNullOrEmpty(camposFiltro))
                {
                    camposFiltro = camposFiltro.Substring(0, camposFiltro.Length - 1);
                    if (passou == true)
                    {
                        filtro = filtro + " AND " + idClasse + ".CLAS_CODIGO IN (" + camposFiltro + ")";
                    }
                    else
                        filtro = " " + idClasse + ".CLAS_CODIGO IN (" + camposFiltro + ")";

                    passou = true;
                }

                camposFiltro = "";

                for (int i = 0; i < dgSubClasses.RowCount; i++)
                {
                    if (Convert.ToString(dgSubClasses.Rows[i].Cells[0].Value).Equals("True"))
                    {
                        camposFiltro = camposFiltro + Convert.ToInt32(dgSubClasses.Rows[i].Cells[1].Value).ToString() + " ,";
                    }
                }

                if (!String.IsNullOrEmpty(camposFiltro))
                {
                    camposFiltro = camposFiltro.Substring(0, camposFiltro.Length - 1);
                    if (passou == true)
                    {
                        filtro = filtro + " AND " + idSubClasse + ".SUB_CODIGO IN (" + camposFiltro + ")";
                    }
                    else
                        filtro = " " + idSubClasse + ".SUB_CODIGO IN (" + camposFiltro + ")";

                    passou = true;
                }
            
                camposFiltro = "";

                for (int i = 0; i < dgFabricantes.RowCount; i++)
                {
                    if (Convert.ToString(dgFabricantes.Rows[i].Cells[0].Value).Equals("True"))
                    {
                        camposFiltro = camposFiltro + Convert.ToInt32(dgFabricantes.Rows[i].Cells[1].Value).ToString() + " ,";
                    }
                }

                if (!String.IsNullOrEmpty(camposFiltro))
                {
                    camposFiltro = camposFiltro.Substring(0, camposFiltro.Length - 1);
                    if (passou == true)
                    {
                        filtro = filtro + " AND " + idFab + ".FAB_CODIGO IN (" + camposFiltro + ")";
                    }
                    else
                        filtro = " " + idFab + ".FAB_CODIGO IN (" + camposFiltro + ")";

                    passou = true;
                }

                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Filtro de Produtos", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            filtro = "";
            idDepto = "";
            idClasse = "";
            idSubClasse = "";
            idFab = "";
            passou = false;
            this.Close();
        }

        private void rdbMarcar_CheckedChanged(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            int i;

            for (i = 0; i < dgDepto.RowCount; i++)
            {
                dgDepto.Rows[i].Cells[0].Value = "True";
            }
            for (i = 0; i < dgClasses.RowCount; i++)
            {
                dgClasses.Rows[i].Cells[0].Value = "True";
            }

            for (i = 0; i < dgSubClasses.RowCount; i++)
            {
                dgSubClasses.Rows[i].Cells[0].Value = "True";
            }

            for (i = 0; i < dgFabricantes.RowCount; i++)
            {
                dgFabricantes.Rows[i].Cells[0].Value = "True";
            }
            Cursor = Cursors.Default;
        }

        private void rdbDesmarcar_CheckedChanged(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            int i;

            for (i = 0; i < dgDepto.RowCount; i++)
            {
                dgDepto.Rows[i].Cells[0].Value = "False";
            }
            for (i = 0; i < dgClasses.RowCount; i++)
            {
                dgClasses.Rows[i].Cells[0].Value = "False";
            }

            for (i = 0; i < dgSubClasses.RowCount; i++)
            {
                dgSubClasses.Rows[i].Cells[0].Value = "False";
            }

            for (i = 0; i < dgFabricantes.RowCount; i++)
            {
                dgFabricantes.Rows[i].Cells[0].Value = "False";
            }
            Cursor = Cursors.Default;
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {

            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);

            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

    }
}
