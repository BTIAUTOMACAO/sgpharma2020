﻿namespace GestaoAdministrativa
{
    partial class frmGrupos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGrupos));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle18 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle19 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle20 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle21 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle22 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle24 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle25 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle26 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle23 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle27 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle28 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle30 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle31 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle32 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle29 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle33 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle34 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle36 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle37 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle38 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle35 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle39 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle40 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle42 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle43 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle44 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle41 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle45 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle46 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle48 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle49 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle50 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle47 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle51 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle52 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle54 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle55 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle56 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle53 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle57 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle58 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle60 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle61 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle62 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle59 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label11 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tslRegistros = new System.Windows.Forms.ToolStripStatusLabel();
            this.tcGrupos = new System.Windows.Forms.TabControl();
            this.tpGrade = new System.Windows.Forms.TabPage();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.cmbLiberado = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtBDescr = new System.Windows.Forms.TextBox();
            this.txtBID = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dgGrupos = new System.Windows.Forms.DataGridView();
            this.GRUPO_USU_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DESCRICAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ADM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LIB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPALTERACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DTCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.OPCADASTRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmsGrupo = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmConfigurar = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmExcel = new System.Windows.Forms.ToolStripMenuItem();
            this.tpFicha = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.txtData = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.chkLiberado = new System.Windows.Forms.CheckBox();
            this.chkAdmin = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtDescricao = new System.Windows.Forms.TextBox();
            this.tpPermissoes = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tpGeral = new System.Windows.Forms.TabPage();
            this.dgPermissoes = new System.Windows.Forms.DataGridView();
            this.MODULO_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MOD_DESCR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ACESSA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.INCLUI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ALTERA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EXCLUI = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GRUPO_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PERMISSOES_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpCadastro = new System.Windows.Forms.TabPage();
            this.dgCadastro = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpCaixa = new System.Windows.Forms.TabPage();
            this.dgCaixa = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpContasAPagar = new System.Windows.Forms.TabPage();
            this.dgContasAPagar = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn22 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn23 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn24 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpReceber = new System.Windows.Forms.TabPage();
            this.dgContasAReceber = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn25 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn26 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn27 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn28 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn29 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn30 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn31 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn32 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpEstoque = new System.Windows.Forms.TabPage();
            this.dgEstoque = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn33 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn34 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn35 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn36 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn37 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn38 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn39 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn40 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpSngpc = new System.Windows.Forms.TabPage();
            this.dgSngpc = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn41 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn42 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn43 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn44 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn45 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn46 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn47 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn48 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpVendas = new System.Windows.Forms.TabPage();
            this.dgVendas = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn49 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn50 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn51 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn52 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn53 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn54 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn55 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn56 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpRelatorio = new System.Windows.Forms.TabPage();
            this.dgRelatorios = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn57 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn58 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn59 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn60 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn61 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn62 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn63 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn64 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.tcGrupos.SuspendLayout();
            this.tpGrade.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgGrupos)).BeginInit();
            this.cmsGrupo.SuspendLayout();
            this.tpFicha.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.tpPermissoes.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tpGeral.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgPermissoes)).BeginInit();
            this.tpCadastro.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCadastro)).BeginInit();
            this.tpCaixa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgCaixa)).BeginInit();
            this.tpContasAPagar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgContasAPagar)).BeginInit();
            this.tpReceber.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgContasAReceber)).BeginInit();
            this.tpEstoque.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgEstoque)).BeginInit();
            this.tpSngpc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgSngpc)).BeginInit();
            this.tpVendas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgVendas)).BeginInit();
            this.tpRelatorio.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRelatorios)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.statusStrip1);
            this.groupBox1.Controls.Add(this.tcGrupos);
            this.groupBox1.Location = new System.Drawing.Point(10, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(959, 545);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.RoyalBlue;
            this.panel1.Controls.Add(this.label11);
            this.panel1.Location = new System.Drawing.Point(-1, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(960, 24);
            this.panel1.TabIndex = 44;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.Color.Transparent;
            this.label11.Font = new System.Drawing.Font("Arial", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(7, 2);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(232, 18);
            this.label11.TabIndex = 39;
            this.label11.Text = "Cadastro de Grupo de Usuários";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tslRegistros});
            this.statusStrip1.Location = new System.Drawing.Point(3, 520);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(953, 22);
            this.statusStrip1.TabIndex = 18;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tslRegistros
            // 
            this.tslRegistros.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tslRegistros.Name = "tslRegistros";
            this.tslRegistros.Size = new System.Drawing.Size(0, 17);
            // 
            // tcGrupos
            // 
            this.tcGrupos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcGrupos.Controls.Add(this.tpGrade);
            this.tcGrupos.Controls.Add(this.tpFicha);
            this.tcGrupos.Controls.Add(this.tpPermissoes);
            this.tcGrupos.Location = new System.Drawing.Point(6, 33);
            this.tcGrupos.Name = "tcGrupos";
            this.tcGrupos.SelectedIndex = 0;
            this.tcGrupos.Size = new System.Drawing.Size(929, 484);
            this.tcGrupos.TabIndex = 12;
            this.tcGrupos.SelectedIndexChanged += new System.EventHandler(this.tcGrupos_SelectedIndexChanged);
            // 
            // tpGrade
            // 
            this.tpGrade.BackColor = System.Drawing.Color.White;
            this.tpGrade.Controls.Add(this.btnBuscar);
            this.tpGrade.Controls.Add(this.cmbLiberado);
            this.tpGrade.Controls.Add(this.label3);
            this.tpGrade.Controls.Add(this.txtBDescr);
            this.tpGrade.Controls.Add(this.txtBID);
            this.tpGrade.Controls.Add(this.label5);
            this.tpGrade.Controls.Add(this.label4);
            this.tpGrade.Controls.Add(this.dgGrupos);
            this.tpGrade.Location = new System.Drawing.Point(4, 25);
            this.tpGrade.Name = "tpGrade";
            this.tpGrade.Padding = new System.Windows.Forms.Padding(3);
            this.tpGrade.Size = new System.Drawing.Size(921, 455);
            this.tpGrade.TabIndex = 0;
            this.tpGrade.Text = "Em Grade (F1)";
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.ForeColor = System.Drawing.Color.Black;
            this.btnBuscar.Image = ((System.Drawing.Image)(resources.GetObject("btnBuscar.Image")));
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBuscar.Location = new System.Drawing.Point(604, 404);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(94, 38);
            this.btnBuscar.TabIndex = 88;
            this.btnBuscar.Text = "Buscar";
            this.btnBuscar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // cmbLiberado
            // 
            this.cmbLiberado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLiberado.FormattingEnabled = true;
            this.cmbLiberado.Items.AddRange(new object[] {
            "TODOS",
            "S",
            "N"});
            this.cmbLiberado.Location = new System.Drawing.Point(452, 418);
            this.cmbLiberado.Name = "cmbLiberado";
            this.cmbLiberado.Size = new System.Drawing.Size(130, 24);
            this.cmbLiberado.TabIndex = 87;
            this.cmbLiberado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbLiberado_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Navy;
            this.label3.Location = new System.Drawing.Point(449, 399);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 16);
            this.label3.TabIndex = 86;
            this.label3.Text = "Liberado";
            // 
            // txtBDescr
            // 
            this.txtBDescr.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBDescr.Location = new System.Drawing.Point(124, 420);
            this.txtBDescr.Name = "txtBDescr";
            this.txtBDescr.Size = new System.Drawing.Size(313, 22);
            this.txtBDescr.TabIndex = 23;
            this.txtBDescr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBDescr_KeyPress);
            // 
            // txtBID
            // 
            this.txtBID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtBID.Location = new System.Drawing.Point(9, 420);
            this.txtBID.Name = "txtBID";
            this.txtBID.Size = new System.Drawing.Size(100, 22);
            this.txtBID.TabIndex = 22;
            this.txtBID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBCodigo_KeyPress);
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Navy;
            this.label5.Location = new System.Drawing.Point(121, 401);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 16);
            this.label5.TabIndex = 21;
            this.label5.Text = "Descrição";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Navy;
            this.label4.Location = new System.Drawing.Point(6, 401);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 16);
            this.label4.TabIndex = 20;
            this.label4.Text = "Grupo ID";
            // 
            // dgGrupos
            // 
            this.dgGrupos.AllowUserToAddRows = false;
            this.dgGrupos.AllowUserToDeleteRows = false;
            this.dgGrupos.AllowUserToOrderColumns = true;
            this.dgGrupos.AllowUserToResizeColumns = false;
            this.dgGrupos.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgGrupos.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgGrupos.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgGrupos.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgGrupos.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgGrupos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgGrupos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.GRUPO_USU_ID,
            this.DESCRICAO,
            this.ADM,
            this.LIB,
            this.DTALTERACAO,
            this.OPALTERACAO,
            this.DTCADASTRO,
            this.OPCADASTRO});
            this.dgGrupos.ContextMenuStrip = this.cmsGrupo;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgGrupos.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgGrupos.GridColor = System.Drawing.Color.LightGray;
            this.dgGrupos.Location = new System.Drawing.Point(6, 6);
            this.dgGrupos.MultiSelect = false;
            this.dgGrupos.Name = "dgGrupos";
            this.dgGrupos.ReadOnly = true;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgGrupos.RowHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgGrupos.RowHeadersVisible = false;
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black;
            this.dgGrupos.RowsDefaultCellStyle = dataGridViewCellStyle8;
            this.dgGrupos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgGrupos.Size = new System.Drawing.Size(930, 392);
            this.dgGrupos.TabIndex = 0;
            this.dgGrupos.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgGrupos_CellMouseClick);
            this.dgGrupos.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgGrupos_CellMouseDoubleClick);
            this.dgGrupos.ColumnHeaderMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgGrupos_ColumnHeaderMouseClick);
            this.dgGrupos.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgGrupos_RowPrePaint);
            this.dgGrupos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgGrupos_KeyDown);
            // 
            // GRUPO_USU_ID
            // 
            this.GRUPO_USU_ID.DataPropertyName = "GRUPO_USU_ID";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.GRUPO_USU_ID.DefaultCellStyle = dataGridViewCellStyle3;
            this.GRUPO_USU_ID.HeaderText = "Grupo ID";
            this.GRUPO_USU_ID.MaxInputLength = 18;
            this.GRUPO_USU_ID.Name = "GRUPO_USU_ID";
            this.GRUPO_USU_ID.ReadOnly = true;
            this.GRUPO_USU_ID.Width = 90;
            // 
            // DESCRICAO
            // 
            this.DESCRICAO.DataPropertyName = "DESCRICAO";
            this.DESCRICAO.HeaderText = "Descrição";
            this.DESCRICAO.MaxInputLength = 25;
            this.DESCRICAO.Name = "DESCRICAO";
            this.DESCRICAO.ReadOnly = true;
            this.DESCRICAO.Width = 200;
            // 
            // ADM
            // 
            this.ADM.DataPropertyName = "ADMINISTRADOR";
            this.ADM.HeaderText = "Administrador";
            this.ADM.MaxInputLength = 1;
            this.ADM.Name = "ADM";
            this.ADM.ReadOnly = true;
            // 
            // LIB
            // 
            this.LIB.DataPropertyName = "LIBERADO";
            this.LIB.HeaderText = "Liberado";
            this.LIB.MaxInputLength = 1;
            this.LIB.Name = "LIB";
            this.LIB.ReadOnly = true;
            // 
            // DTALTERACAO
            // 
            this.DTALTERACAO.DataPropertyName = "DTALTERACAO";
            dataGridViewCellStyle4.Format = "G";
            this.DTALTERACAO.DefaultCellStyle = dataGridViewCellStyle4;
            this.DTALTERACAO.HeaderText = "Data Alteração";
            this.DTALTERACAO.MaxInputLength = 30;
            this.DTALTERACAO.Name = "DTALTERACAO";
            this.DTALTERACAO.ReadOnly = true;
            this.DTALTERACAO.Width = 150;
            // 
            // OPALTERACAO
            // 
            this.OPALTERACAO.DataPropertyName = "OPALTERACAO";
            this.OPALTERACAO.HeaderText = "Usuário Alteração";
            this.OPALTERACAO.MaxInputLength = 25;
            this.OPALTERACAO.Name = "OPALTERACAO";
            this.OPALTERACAO.ReadOnly = true;
            this.OPALTERACAO.Width = 150;
            // 
            // DTCADASTRO
            // 
            this.DTCADASTRO.DataPropertyName = "DTCADASTRO";
            dataGridViewCellStyle5.Format = "G";
            this.DTCADASTRO.DefaultCellStyle = dataGridViewCellStyle5;
            this.DTCADASTRO.HeaderText = "Data do Cadastro";
            this.DTCADASTRO.MaxInputLength = 30;
            this.DTCADASTRO.Name = "DTCADASTRO";
            this.DTCADASTRO.ReadOnly = true;
            this.DTCADASTRO.Width = 150;
            // 
            // OPCADASTRO
            // 
            this.OPCADASTRO.DataPropertyName = "OPCADASTRO";
            this.OPCADASTRO.HeaderText = "Usuário Cadastro";
            this.OPCADASTRO.MaxInputLength = 25;
            this.OPCADASTRO.Name = "OPCADASTRO";
            this.OPCADASTRO.ReadOnly = true;
            this.OPCADASTRO.Width = 150;
            // 
            // cmsGrupo
            // 
            this.cmsGrupo.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmsGrupo.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmConfigurar,
            this.tsmExcel});
            this.cmsGrupo.Name = "cmsGrupo";
            this.cmsGrupo.Size = new System.Drawing.Size(192, 48);
            // 
            // tsmConfigurar
            // 
            this.tsmConfigurar.Name = "tsmConfigurar";
            this.tsmConfigurar.Size = new System.Drawing.Size(191, 22);
            this.tsmConfigurar.Text = "Configurar Grade";
            this.tsmConfigurar.Click += new System.EventHandler(this.tsmConfigurar_Click);
            // 
            // tsmExcel
            // 
            this.tsmExcel.Name = "tsmExcel";
            this.tsmExcel.Size = new System.Drawing.Size(191, 22);
            this.tsmExcel.Text = "Exportar para Excel";
            this.tsmExcel.Click += new System.EventHandler(this.tsmExcel_Click);
            // 
            // tpFicha
            // 
            this.tpFicha.Controls.Add(this.groupBox3);
            this.tpFicha.Controls.Add(this.groupBox2);
            this.tpFicha.Location = new System.Drawing.Point(4, 25);
            this.tpFicha.Name = "tpFicha";
            this.tpFicha.Padding = new System.Windows.Forms.Padding(3);
            this.tpFicha.Size = new System.Drawing.Size(921, 455);
            this.tpFicha.TabIndex = 1;
            this.tpFicha.Text = "Em Ficha (F2)";
            this.tpFicha.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.groupBox5);
            this.groupBox3.Controls.Add(this.txtUsuario);
            this.groupBox3.Controls.Add(this.txtData);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.ForeColor = System.Drawing.Color.Black;
            this.groupBox3.Location = new System.Drawing.Point(6, 355);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(930, 92);
            this.groupBox3.TabIndex = 25;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Dados da Última Alteração";
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.pictureBox9);
            this.groupBox5.Controls.Add(this.label45);
            this.groupBox5.Controls.Add(this.label44);
            this.groupBox5.Controls.Add(this.pictureBox10);
            this.groupBox5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(574, 23);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(350, 46);
            this.groupBox5.TabIndex = 195;
            this.groupBox5.TabStop = false;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.pictureBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox9.Location = new System.Drawing.Point(173, 21);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(16, 16);
            this.pictureBox9.TabIndex = 195;
            this.pictureBox9.TabStop = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.ForeColor = System.Drawing.Color.Navy;
            this.label45.Location = new System.Drawing.Point(191, 21);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 16);
            this.label45.TabIndex = 194;
            this.label45.Text = "Campo não Editável";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.ForeColor = System.Drawing.Color.Navy;
            this.label44.Location = new System.Drawing.Point(34, 21);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(129, 16);
            this.label44.TabIndex = 192;
            this.label44.Text = "Campo Obrigatório";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox10.Image")));
            this.pictureBox10.Location = new System.Drawing.Point(16, 21);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(16, 16);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox10.TabIndex = 191;
            this.pictureBox10.TabStop = false;
            // 
            // txtUsuario
            // 
            this.txtUsuario.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtUsuario.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtUsuario.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtUsuario.Location = new System.Drawing.Point(228, 47);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.ReadOnly = true;
            this.txtUsuario.Size = new System.Drawing.Size(165, 22);
            this.txtUsuario.TabIndex = 21;
            // 
            // txtData
            // 
            this.txtData.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtData.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtData.Location = new System.Drawing.Point(9, 47);
            this.txtData.Name = "txtData";
            this.txtData.ReadOnly = true;
            this.txtData.Size = new System.Drawing.Size(195, 22);
            this.txtData.TabIndex = 20;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Navy;
            this.label6.Location = new System.Drawing.Point(6, 28);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(165, 16);
            this.label6.TabIndex = 18;
            this.label6.Text = "Data da última alteração";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Navy;
            this.label7.Location = new System.Drawing.Point(225, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 16);
            this.label7.TabIndex = 19;
            this.label7.Text = "Usuário";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.chkLiberado);
            this.groupBox2.Controls.Add(this.chkAdmin);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtID);
            this.groupBox2.Controls.Add(this.txtDescricao);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.ForeColor = System.Drawing.Color.Black;
            this.groupBox2.Location = new System.Drawing.Point(6, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(930, 140);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Principal";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(218, 27);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(16, 16);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 194;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(84, 27);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(16, 16);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox3.TabIndex = 193;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // chkLiberado
            // 
            this.chkLiberado.AutoSize = true;
            this.chkLiberado.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkLiberado.ForeColor = System.Drawing.Color.Navy;
            this.chkLiberado.Location = new System.Drawing.Point(16, 109);
            this.chkLiberado.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkLiberado.Name = "chkLiberado";
            this.chkLiberado.Size = new System.Drawing.Size(84, 20);
            this.chkLiberado.TabIndex = 21;
            this.chkLiberado.Text = "Liberado";
            this.chkLiberado.UseVisualStyleBackColor = true;
            // 
            // chkAdmin
            // 
            this.chkAdmin.AutoSize = true;
            this.chkAdmin.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAdmin.ForeColor = System.Drawing.Color.Navy;
            this.chkAdmin.Location = new System.Drawing.Point(16, 78);
            this.chkAdmin.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.chkAdmin.Name = "chkAdmin";
            this.chkAdmin.Size = new System.Drawing.Size(112, 20);
            this.chkAdmin.TabIndex = 20;
            this.chkAdmin.Text = "Adminstrador";
            this.chkAdmin.UseVisualStyleBackColor = true;
            this.chkAdmin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.chkAdmin_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(13, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 18;
            this.label1.Text = "Grupo ID";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Navy;
            this.label2.Location = new System.Drawing.Point(142, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 16);
            this.label2.TabIndex = 19;
            this.label2.Text = "Descrição";
            // 
            // txtID
            // 
            this.txtID.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtID.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtID.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.Location = new System.Drawing.Point(16, 45);
            this.txtID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtID.Name = "txtID";
            this.txtID.ReadOnly = true;
            this.txtID.Size = new System.Drawing.Size(110, 22);
            this.txtID.TabIndex = 22;
            this.txtID.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtID.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtID_KeyPress);
            // 
            // txtDescricao
            // 
            this.txtDescricao.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtDescricao.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtDescricao.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDescricao.Location = new System.Drawing.Point(145, 45);
            this.txtDescricao.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtDescricao.MaxLength = 25;
            this.txtDescricao.Name = "txtDescricao";
            this.txtDescricao.Size = new System.Drawing.Size(421, 22);
            this.txtDescricao.TabIndex = 23;
            this.txtDescricao.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDescricao_KeyPress);
            // 
            // tpPermissoes
            // 
            this.tpPermissoes.Controls.Add(this.groupBox4);
            this.tpPermissoes.Location = new System.Drawing.Point(4, 25);
            this.tpPermissoes.Name = "tpPermissoes";
            this.tpPermissoes.Size = new System.Drawing.Size(921, 455);
            this.tpPermissoes.TabIndex = 2;
            this.tpPermissoes.Text = "Permissões";
            this.tpPermissoes.UseVisualStyleBackColor = true;
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.tabControl1);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.ForeColor = System.Drawing.Color.Navy;
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(906, 445);
            this.groupBox4.TabIndex = 3;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Permissões para o grupo";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tpGeral);
            this.tabControl1.Controls.Add(this.tpCadastro);
            this.tabControl1.Controls.Add(this.tpCaixa);
            this.tabControl1.Controls.Add(this.tpContasAPagar);
            this.tabControl1.Controls.Add(this.tpReceber);
            this.tabControl1.Controls.Add(this.tpEstoque);
            this.tabControl1.Controls.Add(this.tpSngpc);
            this.tabControl1.Controls.Add(this.tpVendas);
            this.tabControl1.Controls.Add(this.tpRelatorio);
            this.tabControl1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(9, 21);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(891, 419);
            this.tabControl1.TabIndex = 2;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tpGeral
            // 
            this.tpGeral.Controls.Add(this.dgPermissoes);
            this.tpGeral.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpGeral.Location = new System.Drawing.Point(4, 25);
            this.tpGeral.Name = "tpGeral";
            this.tpGeral.Padding = new System.Windows.Forms.Padding(3);
            this.tpGeral.Size = new System.Drawing.Size(883, 390);
            this.tpGeral.TabIndex = 0;
            this.tpGeral.Text = "Geral";
            this.tpGeral.UseVisualStyleBackColor = true;
            // 
            // dgPermissoes
            // 
            this.dgPermissoes.AllowUserToAddRows = false;
            this.dgPermissoes.AllowUserToDeleteRows = false;
            this.dgPermissoes.AllowUserToResizeColumns = false;
            this.dgPermissoes.AllowUserToResizeRows = false;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPermissoes.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle9;
            this.dgPermissoes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgPermissoes.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPermissoes.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle10;
            this.dgPermissoes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgPermissoes.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MODULO_ID,
            this.MOD_DESCR,
            this.ACESSA,
            this.INCLUI,
            this.ALTERA,
            this.EXCLUI,
            this.GRUPO_ID,
            this.PERMISSOES_ID});
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgPermissoes.DefaultCellStyle = dataGridViewCellStyle12;
            this.dgPermissoes.GridColor = System.Drawing.Color.LightGray;
            this.dgPermissoes.Location = new System.Drawing.Point(6, 6);
            this.dgPermissoes.Name = "dgPermissoes";
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgPermissoes.RowHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgPermissoes.RowHeadersVisible = false;
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle14.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle14.SelectionForeColor = System.Drawing.Color.Black;
            this.dgPermissoes.RowsDefaultCellStyle = dataGridViewCellStyle14;
            this.dgPermissoes.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgPermissoes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgPermissoes.Size = new System.Drawing.Size(869, 378);
            this.dgPermissoes.TabIndex = 1;
            this.dgPermissoes.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPermissoes_CellContentClick);
            this.dgPermissoes.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPermissoes_CellEndEdit);
            this.dgPermissoes.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgPermissoes_CellValidated);
            // 
            // MODULO_ID
            // 
            this.MODULO_ID.DataPropertyName = "MODULO_ID";
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.MODULO_ID.DefaultCellStyle = dataGridViewCellStyle11;
            this.MODULO_ID.HeaderText = "Modulo ID";
            this.MODULO_ID.MaxInputLength = 18;
            this.MODULO_ID.Name = "MODULO_ID";
            this.MODULO_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MODULO_ID.Visible = false;
            // 
            // MOD_DESCR
            // 
            this.MOD_DESCR.DataPropertyName = "DESCRICAO";
            this.MOD_DESCR.HeaderText = "Descrição";
            this.MOD_DESCR.MaxInputLength = 100;
            this.MOD_DESCR.Name = "MOD_DESCR";
            this.MOD_DESCR.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.MOD_DESCR.Width = 400;
            // 
            // ACESSA
            // 
            this.ACESSA.DataPropertyName = "ACESSA";
            this.ACESSA.HeaderText = "Acessa";
            this.ACESSA.MaxInputLength = 1;
            this.ACESSA.Name = "ACESSA";
            this.ACESSA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ACESSA.Width = 75;
            // 
            // INCLUI
            // 
            this.INCLUI.DataPropertyName = "INCLUI";
            this.INCLUI.HeaderText = "Inclui";
            this.INCLUI.MaxInputLength = 1;
            this.INCLUI.Name = "INCLUI";
            this.INCLUI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.INCLUI.Width = 75;
            // 
            // ALTERA
            // 
            this.ALTERA.DataPropertyName = "ALTERA";
            this.ALTERA.HeaderText = "Altera";
            this.ALTERA.MaxInputLength = 1;
            this.ALTERA.Name = "ALTERA";
            this.ALTERA.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.ALTERA.Width = 75;
            // 
            // EXCLUI
            // 
            this.EXCLUI.DataPropertyName = "EXCLUI";
            this.EXCLUI.HeaderText = "Exclui";
            this.EXCLUI.MaxInputLength = 1;
            this.EXCLUI.Name = "EXCLUI";
            this.EXCLUI.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.EXCLUI.Width = 75;
            // 
            // GRUPO_ID
            // 
            this.GRUPO_ID.DataPropertyName = "GRUPO_ID";
            this.GRUPO_ID.HeaderText = "Grupo ID";
            this.GRUPO_ID.MaxInputLength = 18;
            this.GRUPO_ID.Name = "GRUPO_ID";
            this.GRUPO_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.GRUPO_ID.Visible = false;
            // 
            // PERMISSOES_ID
            // 
            this.PERMISSOES_ID.DataPropertyName = "PERMISSOES_ID";
            this.PERMISSOES_ID.HeaderText = "Permissoes ID";
            this.PERMISSOES_ID.MaxInputLength = 18;
            this.PERMISSOES_ID.Name = "PERMISSOES_ID";
            this.PERMISSOES_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.PERMISSOES_ID.Visible = false;
            // 
            // tpCadastro
            // 
            this.tpCadastro.Controls.Add(this.dgCadastro);
            this.tpCadastro.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpCadastro.Location = new System.Drawing.Point(4, 25);
            this.tpCadastro.Name = "tpCadastro";
            this.tpCadastro.Padding = new System.Windows.Forms.Padding(3);
            this.tpCadastro.Size = new System.Drawing.Size(883, 390);
            this.tpCadastro.TabIndex = 1;
            this.tpCadastro.Text = "Cadastro";
            this.tpCadastro.UseVisualStyleBackColor = true;
            // 
            // dgCadastro
            // 
            this.dgCadastro.AllowUserToAddRows = false;
            this.dgCadastro.AllowUserToDeleteRows = false;
            this.dgCadastro.AllowUserToResizeColumns = false;
            this.dgCadastro.AllowUserToResizeRows = false;
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle15.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle15.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCadastro.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle15;
            this.dgCadastro.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCadastro.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle16.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle16.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCadastro.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle16;
            this.dgCadastro.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCadastro.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7,
            this.dataGridViewTextBoxColumn8});
            dataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle18.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle18.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle18.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle18.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCadastro.DefaultCellStyle = dataGridViewCellStyle18;
            this.dgCadastro.GridColor = System.Drawing.Color.LightGray;
            this.dgCadastro.Location = new System.Drawing.Point(7, 6);
            this.dgCadastro.Name = "dgCadastro";
            dataGridViewCellStyle19.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle19.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle19.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle19.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle19.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle19.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCadastro.RowHeadersDefaultCellStyle = dataGridViewCellStyle19;
            this.dgCadastro.RowHeadersVisible = false;
            dataGridViewCellStyle20.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle20.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle20.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle20.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle20.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCadastro.RowsDefaultCellStyle = dataGridViewCellStyle20;
            this.dgCadastro.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgCadastro.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgCadastro.Size = new System.Drawing.Size(869, 378);
            this.dgCadastro.TabIndex = 2;
            this.dgCadastro.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCadastro_CellEndEdit);
            this.dgCadastro.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCadastro_CellValidated);
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.DataPropertyName = "MODULO_ID";
            dataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle17;
            this.dataGridViewTextBoxColumn1.HeaderText = "Modulo ID";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn1.Visible = false;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.DataPropertyName = "DESCRICAO";
            this.dataGridViewTextBoxColumn2.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn2.Width = 400;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.DataPropertyName = "ACESSA";
            this.dataGridViewTextBoxColumn3.HeaderText = "Acessa";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn3.Width = 75;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.DataPropertyName = "INCLUI";
            this.dataGridViewTextBoxColumn4.HeaderText = "Inclui";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn4.Width = 75;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.DataPropertyName = "ALTERA";
            this.dataGridViewTextBoxColumn5.HeaderText = "Altera";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn5.Width = 75;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.DataPropertyName = "EXCLUI";
            this.dataGridViewTextBoxColumn6.HeaderText = "Exclui";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn6.Width = 75;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.DataPropertyName = "GRUPO_ID";
            this.dataGridViewTextBoxColumn7.HeaderText = "Grupo ID";
            this.dataGridViewTextBoxColumn7.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn7.Visible = false;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.DataPropertyName = "PERMISSOES_ID";
            this.dataGridViewTextBoxColumn8.HeaderText = "Permissoes ID";
            this.dataGridViewTextBoxColumn8.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            this.dataGridViewTextBoxColumn8.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn8.Visible = false;
            // 
            // tpCaixa
            // 
            this.tpCaixa.Controls.Add(this.dgCaixa);
            this.tpCaixa.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpCaixa.Location = new System.Drawing.Point(4, 25);
            this.tpCaixa.Name = "tpCaixa";
            this.tpCaixa.Size = new System.Drawing.Size(883, 390);
            this.tpCaixa.TabIndex = 2;
            this.tpCaixa.Text = "Caixa";
            this.tpCaixa.UseVisualStyleBackColor = true;
            // 
            // dgCaixa
            // 
            this.dgCaixa.AllowUserToAddRows = false;
            this.dgCaixa.AllowUserToDeleteRows = false;
            this.dgCaixa.AllowUserToResizeColumns = false;
            this.dgCaixa.AllowUserToResizeRows = false;
            dataGridViewCellStyle21.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle21.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle21.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCaixa.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle21;
            this.dgCaixa.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgCaixa.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle22.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle22.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle22.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle22.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle22.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle22.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCaixa.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle22;
            this.dgCaixa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgCaixa.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14,
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewTextBoxColumn16});
            dataGridViewCellStyle24.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle24.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle24.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle24.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle24.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle24.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle24.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCaixa.DefaultCellStyle = dataGridViewCellStyle24;
            this.dgCaixa.GridColor = System.Drawing.Color.LightGray;
            this.dgCaixa.Location = new System.Drawing.Point(7, 6);
            this.dgCaixa.Name = "dgCaixa";
            dataGridViewCellStyle25.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle25.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle25.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle25.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle25.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle25.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCaixa.RowHeadersDefaultCellStyle = dataGridViewCellStyle25;
            this.dgCaixa.RowHeadersVisible = false;
            dataGridViewCellStyle26.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle26.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle26.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle26.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle26.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCaixa.RowsDefaultCellStyle = dataGridViewCellStyle26;
            this.dgCaixa.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgCaixa.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgCaixa.Size = new System.Drawing.Size(869, 378);
            this.dgCaixa.TabIndex = 2;
            this.dgCaixa.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCaixa_CellEndEdit);
            this.dgCaixa.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCaixa_CellValidated);
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.DataPropertyName = "MODULO_ID";
            dataGridViewCellStyle23.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn9.DefaultCellStyle = dataGridViewCellStyle23;
            this.dataGridViewTextBoxColumn9.HeaderText = "Modulo ID";
            this.dataGridViewTextBoxColumn9.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn9.Visible = false;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.DataPropertyName = "DESCRICAO";
            this.dataGridViewTextBoxColumn10.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn10.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            this.dataGridViewTextBoxColumn10.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn10.Width = 400;
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.DataPropertyName = "ACESSA";
            this.dataGridViewTextBoxColumn11.HeaderText = "Acessa";
            this.dataGridViewTextBoxColumn11.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            this.dataGridViewTextBoxColumn11.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn11.Width = 75;
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.DataPropertyName = "INCLUI";
            this.dataGridViewTextBoxColumn12.HeaderText = "Inclui";
            this.dataGridViewTextBoxColumn12.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            this.dataGridViewTextBoxColumn12.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn12.Width = 75;
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.DataPropertyName = "ALTERA";
            this.dataGridViewTextBoxColumn13.HeaderText = "Altera";
            this.dataGridViewTextBoxColumn13.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            this.dataGridViewTextBoxColumn13.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn13.Width = 75;
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.DataPropertyName = "EXCLUI";
            this.dataGridViewTextBoxColumn14.HeaderText = "Exclui";
            this.dataGridViewTextBoxColumn14.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            this.dataGridViewTextBoxColumn14.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn14.Width = 75;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.DataPropertyName = "GRUPO_ID";
            this.dataGridViewTextBoxColumn15.HeaderText = "Grupo ID";
            this.dataGridViewTextBoxColumn15.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            this.dataGridViewTextBoxColumn15.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn15.Visible = false;
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.DataPropertyName = "PERMISSOES_ID";
            this.dataGridViewTextBoxColumn16.HeaderText = "Permissoes ID";
            this.dataGridViewTextBoxColumn16.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn16.Visible = false;
            // 
            // tpContasAPagar
            // 
            this.tpContasAPagar.Controls.Add(this.dgContasAPagar);
            this.tpContasAPagar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpContasAPagar.Location = new System.Drawing.Point(4, 25);
            this.tpContasAPagar.Name = "tpContasAPagar";
            this.tpContasAPagar.Size = new System.Drawing.Size(883, 390);
            this.tpContasAPagar.TabIndex = 3;
            this.tpContasAPagar.Text = "Contas a Pagar";
            this.tpContasAPagar.UseVisualStyleBackColor = true;
            // 
            // dgContasAPagar
            // 
            this.dgContasAPagar.AllowUserToAddRows = false;
            this.dgContasAPagar.AllowUserToDeleteRows = false;
            this.dgContasAPagar.AllowUserToResizeColumns = false;
            this.dgContasAPagar.AllowUserToResizeRows = false;
            dataGridViewCellStyle27.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle27.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle27.SelectionForeColor = System.Drawing.Color.Black;
            this.dgContasAPagar.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle27;
            this.dgContasAPagar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgContasAPagar.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle28.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle28.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle28.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle28.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle28.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle28.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgContasAPagar.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle28;
            this.dgContasAPagar.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgContasAPagar.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21,
            this.dataGridViewTextBoxColumn22,
            this.dataGridViewTextBoxColumn23,
            this.dataGridViewTextBoxColumn24});
            dataGridViewCellStyle30.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle30.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle30.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle30.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle30.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle30.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgContasAPagar.DefaultCellStyle = dataGridViewCellStyle30;
            this.dgContasAPagar.GridColor = System.Drawing.Color.LightGray;
            this.dgContasAPagar.Location = new System.Drawing.Point(7, 6);
            this.dgContasAPagar.Name = "dgContasAPagar";
            dataGridViewCellStyle31.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle31.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle31.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle31.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle31.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle31.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgContasAPagar.RowHeadersDefaultCellStyle = dataGridViewCellStyle31;
            this.dgContasAPagar.RowHeadersVisible = false;
            dataGridViewCellStyle32.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle32.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle32.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle32.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle32.SelectionForeColor = System.Drawing.Color.Black;
            this.dgContasAPagar.RowsDefaultCellStyle = dataGridViewCellStyle32;
            this.dgContasAPagar.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgContasAPagar.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgContasAPagar.Size = new System.Drawing.Size(869, 378);
            this.dgContasAPagar.TabIndex = 2;
            this.dgContasAPagar.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgContasAPagar_CellEndEdit);
            this.dgContasAPagar.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgContasAPagar_CellValidated);
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.DataPropertyName = "MODULO_ID";
            dataGridViewCellStyle29.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn17.DefaultCellStyle = dataGridViewCellStyle29;
            this.dataGridViewTextBoxColumn17.HeaderText = "Modulo ID";
            this.dataGridViewTextBoxColumn17.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            this.dataGridViewTextBoxColumn17.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn17.Visible = false;
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.DataPropertyName = "DESCRICAO";
            this.dataGridViewTextBoxColumn18.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn18.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            this.dataGridViewTextBoxColumn18.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn18.Width = 400;
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.DataPropertyName = "ACESSA";
            this.dataGridViewTextBoxColumn19.HeaderText = "Acessa";
            this.dataGridViewTextBoxColumn19.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            this.dataGridViewTextBoxColumn19.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn19.Width = 75;
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.DataPropertyName = "INCLUI";
            this.dataGridViewTextBoxColumn20.HeaderText = "Inclui";
            this.dataGridViewTextBoxColumn20.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            this.dataGridViewTextBoxColumn20.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn20.Width = 75;
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.DataPropertyName = "ALTERA";
            this.dataGridViewTextBoxColumn21.HeaderText = "Altera";
            this.dataGridViewTextBoxColumn21.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            this.dataGridViewTextBoxColumn21.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn21.Width = 75;
            // 
            // dataGridViewTextBoxColumn22
            // 
            this.dataGridViewTextBoxColumn22.DataPropertyName = "EXCLUI";
            this.dataGridViewTextBoxColumn22.HeaderText = "Exclui";
            this.dataGridViewTextBoxColumn22.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn22.Name = "dataGridViewTextBoxColumn22";
            this.dataGridViewTextBoxColumn22.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn22.Width = 75;
            // 
            // dataGridViewTextBoxColumn23
            // 
            this.dataGridViewTextBoxColumn23.DataPropertyName = "GRUPO_ID";
            this.dataGridViewTextBoxColumn23.HeaderText = "Grupo ID";
            this.dataGridViewTextBoxColumn23.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn23.Name = "dataGridViewTextBoxColumn23";
            this.dataGridViewTextBoxColumn23.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn23.Visible = false;
            // 
            // dataGridViewTextBoxColumn24
            // 
            this.dataGridViewTextBoxColumn24.DataPropertyName = "PERMISSOES_ID";
            this.dataGridViewTextBoxColumn24.HeaderText = "Permissoes ID";
            this.dataGridViewTextBoxColumn24.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn24.Name = "dataGridViewTextBoxColumn24";
            this.dataGridViewTextBoxColumn24.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn24.Visible = false;
            // 
            // tpReceber
            // 
            this.tpReceber.Controls.Add(this.dgContasAReceber);
            this.tpReceber.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpReceber.Location = new System.Drawing.Point(4, 25);
            this.tpReceber.Name = "tpReceber";
            this.tpReceber.Size = new System.Drawing.Size(883, 390);
            this.tpReceber.TabIndex = 4;
            this.tpReceber.Text = "Contas a Receber";
            this.tpReceber.UseVisualStyleBackColor = true;
            // 
            // dgContasAReceber
            // 
            this.dgContasAReceber.AllowUserToAddRows = false;
            this.dgContasAReceber.AllowUserToDeleteRows = false;
            this.dgContasAReceber.AllowUserToResizeColumns = false;
            this.dgContasAReceber.AllowUserToResizeRows = false;
            dataGridViewCellStyle33.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle33.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle33.SelectionForeColor = System.Drawing.Color.Black;
            this.dgContasAReceber.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle33;
            this.dgContasAReceber.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgContasAReceber.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle34.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle34.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle34.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle34.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle34.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle34.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgContasAReceber.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle34;
            this.dgContasAReceber.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgContasAReceber.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn25,
            this.dataGridViewTextBoxColumn26,
            this.dataGridViewTextBoxColumn27,
            this.dataGridViewTextBoxColumn28,
            this.dataGridViewTextBoxColumn29,
            this.dataGridViewTextBoxColumn30,
            this.dataGridViewTextBoxColumn31,
            this.dataGridViewTextBoxColumn32});
            dataGridViewCellStyle36.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle36.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle36.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle36.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle36.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle36.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle36.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgContasAReceber.DefaultCellStyle = dataGridViewCellStyle36;
            this.dgContasAReceber.GridColor = System.Drawing.Color.LightGray;
            this.dgContasAReceber.Location = new System.Drawing.Point(7, 6);
            this.dgContasAReceber.Name = "dgContasAReceber";
            dataGridViewCellStyle37.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle37.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle37.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle37.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle37.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle37.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgContasAReceber.RowHeadersDefaultCellStyle = dataGridViewCellStyle37;
            this.dgContasAReceber.RowHeadersVisible = false;
            dataGridViewCellStyle38.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle38.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle38.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle38.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle38.SelectionForeColor = System.Drawing.Color.Black;
            this.dgContasAReceber.RowsDefaultCellStyle = dataGridViewCellStyle38;
            this.dgContasAReceber.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgContasAReceber.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgContasAReceber.Size = new System.Drawing.Size(869, 378);
            this.dgContasAReceber.TabIndex = 2;
            this.dgContasAReceber.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgContasAReceber_CellEndEdit);
            this.dgContasAReceber.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgContasAReceber_CellValidated);
            // 
            // dataGridViewTextBoxColumn25
            // 
            this.dataGridViewTextBoxColumn25.DataPropertyName = "MODULO_ID";
            dataGridViewCellStyle35.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn25.DefaultCellStyle = dataGridViewCellStyle35;
            this.dataGridViewTextBoxColumn25.HeaderText = "Modulo ID";
            this.dataGridViewTextBoxColumn25.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn25.Name = "dataGridViewTextBoxColumn25";
            this.dataGridViewTextBoxColumn25.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn25.Visible = false;
            // 
            // dataGridViewTextBoxColumn26
            // 
            this.dataGridViewTextBoxColumn26.DataPropertyName = "DESCRICAO";
            this.dataGridViewTextBoxColumn26.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn26.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn26.Name = "dataGridViewTextBoxColumn26";
            this.dataGridViewTextBoxColumn26.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn26.Width = 400;
            // 
            // dataGridViewTextBoxColumn27
            // 
            this.dataGridViewTextBoxColumn27.DataPropertyName = "ACESSA";
            this.dataGridViewTextBoxColumn27.HeaderText = "Acessa";
            this.dataGridViewTextBoxColumn27.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn27.Name = "dataGridViewTextBoxColumn27";
            this.dataGridViewTextBoxColumn27.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn27.Width = 75;
            // 
            // dataGridViewTextBoxColumn28
            // 
            this.dataGridViewTextBoxColumn28.DataPropertyName = "INCLUI";
            this.dataGridViewTextBoxColumn28.HeaderText = "Inclui";
            this.dataGridViewTextBoxColumn28.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn28.Name = "dataGridViewTextBoxColumn28";
            this.dataGridViewTextBoxColumn28.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn28.Width = 75;
            // 
            // dataGridViewTextBoxColumn29
            // 
            this.dataGridViewTextBoxColumn29.DataPropertyName = "ALTERA";
            this.dataGridViewTextBoxColumn29.HeaderText = "Altera";
            this.dataGridViewTextBoxColumn29.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn29.Name = "dataGridViewTextBoxColumn29";
            this.dataGridViewTextBoxColumn29.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn29.Width = 75;
            // 
            // dataGridViewTextBoxColumn30
            // 
            this.dataGridViewTextBoxColumn30.DataPropertyName = "EXCLUI";
            this.dataGridViewTextBoxColumn30.HeaderText = "Exclui";
            this.dataGridViewTextBoxColumn30.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn30.Name = "dataGridViewTextBoxColumn30";
            this.dataGridViewTextBoxColumn30.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn30.Width = 75;
            // 
            // dataGridViewTextBoxColumn31
            // 
            this.dataGridViewTextBoxColumn31.DataPropertyName = "GRUPO_ID";
            this.dataGridViewTextBoxColumn31.HeaderText = "Grupo ID";
            this.dataGridViewTextBoxColumn31.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn31.Name = "dataGridViewTextBoxColumn31";
            this.dataGridViewTextBoxColumn31.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn31.Visible = false;
            // 
            // dataGridViewTextBoxColumn32
            // 
            this.dataGridViewTextBoxColumn32.DataPropertyName = "PERMISSOES_ID";
            this.dataGridViewTextBoxColumn32.HeaderText = "Permissoes ID";
            this.dataGridViewTextBoxColumn32.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn32.Name = "dataGridViewTextBoxColumn32";
            this.dataGridViewTextBoxColumn32.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn32.Visible = false;
            // 
            // tpEstoque
            // 
            this.tpEstoque.Controls.Add(this.dgEstoque);
            this.tpEstoque.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpEstoque.Location = new System.Drawing.Point(4, 25);
            this.tpEstoque.Name = "tpEstoque";
            this.tpEstoque.Size = new System.Drawing.Size(883, 390);
            this.tpEstoque.TabIndex = 5;
            this.tpEstoque.Text = "Estoque";
            this.tpEstoque.UseVisualStyleBackColor = true;
            // 
            // dgEstoque
            // 
            this.dgEstoque.AllowUserToAddRows = false;
            this.dgEstoque.AllowUserToDeleteRows = false;
            this.dgEstoque.AllowUserToResizeColumns = false;
            this.dgEstoque.AllowUserToResizeRows = false;
            dataGridViewCellStyle39.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle39.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle39.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEstoque.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle39;
            this.dgEstoque.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgEstoque.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle40.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle40.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle40.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle40.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle40.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle40.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEstoque.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle40;
            this.dgEstoque.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgEstoque.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn33,
            this.dataGridViewTextBoxColumn34,
            this.dataGridViewTextBoxColumn35,
            this.dataGridViewTextBoxColumn36,
            this.dataGridViewTextBoxColumn37,
            this.dataGridViewTextBoxColumn38,
            this.dataGridViewTextBoxColumn39,
            this.dataGridViewTextBoxColumn40});
            dataGridViewCellStyle42.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle42.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle42.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle42.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle42.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle42.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle42.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgEstoque.DefaultCellStyle = dataGridViewCellStyle42;
            this.dgEstoque.GridColor = System.Drawing.Color.LightGray;
            this.dgEstoque.Location = new System.Drawing.Point(7, 6);
            this.dgEstoque.Name = "dgEstoque";
            dataGridViewCellStyle43.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle43.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle43.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle43.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle43.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle43.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgEstoque.RowHeadersDefaultCellStyle = dataGridViewCellStyle43;
            this.dgEstoque.RowHeadersVisible = false;
            dataGridViewCellStyle44.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle44.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle44.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle44.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle44.SelectionForeColor = System.Drawing.Color.Black;
            this.dgEstoque.RowsDefaultCellStyle = dataGridViewCellStyle44;
            this.dgEstoque.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgEstoque.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgEstoque.Size = new System.Drawing.Size(869, 378);
            this.dgEstoque.TabIndex = 2;
            this.dgEstoque.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEstoque_CellEndEdit);
            this.dgEstoque.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgEstoque_CellValidated);
            // 
            // dataGridViewTextBoxColumn33
            // 
            this.dataGridViewTextBoxColumn33.DataPropertyName = "MODULO_ID";
            dataGridViewCellStyle41.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn33.DefaultCellStyle = dataGridViewCellStyle41;
            this.dataGridViewTextBoxColumn33.HeaderText = "Modulo ID";
            this.dataGridViewTextBoxColumn33.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn33.Name = "dataGridViewTextBoxColumn33";
            this.dataGridViewTextBoxColumn33.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn33.Visible = false;
            // 
            // dataGridViewTextBoxColumn34
            // 
            this.dataGridViewTextBoxColumn34.DataPropertyName = "DESCRICAO";
            this.dataGridViewTextBoxColumn34.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn34.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn34.Name = "dataGridViewTextBoxColumn34";
            this.dataGridViewTextBoxColumn34.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn34.Width = 400;
            // 
            // dataGridViewTextBoxColumn35
            // 
            this.dataGridViewTextBoxColumn35.DataPropertyName = "ACESSA";
            this.dataGridViewTextBoxColumn35.HeaderText = "Acessa";
            this.dataGridViewTextBoxColumn35.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn35.Name = "dataGridViewTextBoxColumn35";
            this.dataGridViewTextBoxColumn35.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn35.Width = 75;
            // 
            // dataGridViewTextBoxColumn36
            // 
            this.dataGridViewTextBoxColumn36.DataPropertyName = "INCLUI";
            this.dataGridViewTextBoxColumn36.HeaderText = "Inclui";
            this.dataGridViewTextBoxColumn36.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn36.Name = "dataGridViewTextBoxColumn36";
            this.dataGridViewTextBoxColumn36.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn36.Width = 75;
            // 
            // dataGridViewTextBoxColumn37
            // 
            this.dataGridViewTextBoxColumn37.DataPropertyName = "ALTERA";
            this.dataGridViewTextBoxColumn37.HeaderText = "Altera";
            this.dataGridViewTextBoxColumn37.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn37.Name = "dataGridViewTextBoxColumn37";
            this.dataGridViewTextBoxColumn37.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn37.Width = 75;
            // 
            // dataGridViewTextBoxColumn38
            // 
            this.dataGridViewTextBoxColumn38.DataPropertyName = "EXCLUI";
            this.dataGridViewTextBoxColumn38.HeaderText = "Exclui";
            this.dataGridViewTextBoxColumn38.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn38.Name = "dataGridViewTextBoxColumn38";
            this.dataGridViewTextBoxColumn38.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn38.Width = 75;
            // 
            // dataGridViewTextBoxColumn39
            // 
            this.dataGridViewTextBoxColumn39.DataPropertyName = "GRUPO_ID";
            this.dataGridViewTextBoxColumn39.HeaderText = "Grupo ID";
            this.dataGridViewTextBoxColumn39.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn39.Name = "dataGridViewTextBoxColumn39";
            this.dataGridViewTextBoxColumn39.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn39.Visible = false;
            // 
            // dataGridViewTextBoxColumn40
            // 
            this.dataGridViewTextBoxColumn40.DataPropertyName = "PERMISSOES_ID";
            this.dataGridViewTextBoxColumn40.HeaderText = "Permissoes ID";
            this.dataGridViewTextBoxColumn40.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn40.Name = "dataGridViewTextBoxColumn40";
            this.dataGridViewTextBoxColumn40.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn40.Visible = false;
            // 
            // tpSngpc
            // 
            this.tpSngpc.Controls.Add(this.dgSngpc);
            this.tpSngpc.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpSngpc.Location = new System.Drawing.Point(4, 25);
            this.tpSngpc.Name = "tpSngpc";
            this.tpSngpc.Size = new System.Drawing.Size(883, 390);
            this.tpSngpc.TabIndex = 6;
            this.tpSngpc.Text = "SNGPC";
            this.tpSngpc.UseVisualStyleBackColor = true;
            // 
            // dgSngpc
            // 
            this.dgSngpc.AllowUserToAddRows = false;
            this.dgSngpc.AllowUserToDeleteRows = false;
            this.dgSngpc.AllowUserToResizeColumns = false;
            this.dgSngpc.AllowUserToResizeRows = false;
            dataGridViewCellStyle45.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle45.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle45.SelectionForeColor = System.Drawing.Color.Black;
            this.dgSngpc.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle45;
            this.dgSngpc.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgSngpc.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle46.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle46.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle46.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle46.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle46.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle46.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSngpc.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle46;
            this.dgSngpc.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgSngpc.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn41,
            this.dataGridViewTextBoxColumn42,
            this.dataGridViewTextBoxColumn43,
            this.dataGridViewTextBoxColumn44,
            this.dataGridViewTextBoxColumn45,
            this.dataGridViewTextBoxColumn46,
            this.dataGridViewTextBoxColumn47,
            this.dataGridViewTextBoxColumn48});
            dataGridViewCellStyle48.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle48.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle48.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle48.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle48.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle48.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle48.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgSngpc.DefaultCellStyle = dataGridViewCellStyle48;
            this.dgSngpc.GridColor = System.Drawing.Color.LightGray;
            this.dgSngpc.Location = new System.Drawing.Point(7, 6);
            this.dgSngpc.Name = "dgSngpc";
            dataGridViewCellStyle49.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle49.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle49.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle49.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle49.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle49.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle49.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgSngpc.RowHeadersDefaultCellStyle = dataGridViewCellStyle49;
            this.dgSngpc.RowHeadersVisible = false;
            dataGridViewCellStyle50.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle50.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle50.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle50.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle50.SelectionForeColor = System.Drawing.Color.Black;
            this.dgSngpc.RowsDefaultCellStyle = dataGridViewCellStyle50;
            this.dgSngpc.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgSngpc.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgSngpc.Size = new System.Drawing.Size(869, 378);
            this.dgSngpc.TabIndex = 2;
            this.dgSngpc.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSngpc_CellEndEdit);
            this.dgSngpc.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgSngpc_CellValidated);
            // 
            // dataGridViewTextBoxColumn41
            // 
            this.dataGridViewTextBoxColumn41.DataPropertyName = "MODULO_ID";
            dataGridViewCellStyle47.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn41.DefaultCellStyle = dataGridViewCellStyle47;
            this.dataGridViewTextBoxColumn41.HeaderText = "Modulo ID";
            this.dataGridViewTextBoxColumn41.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn41.Name = "dataGridViewTextBoxColumn41";
            this.dataGridViewTextBoxColumn41.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn41.Visible = false;
            // 
            // dataGridViewTextBoxColumn42
            // 
            this.dataGridViewTextBoxColumn42.DataPropertyName = "DESCRICAO";
            this.dataGridViewTextBoxColumn42.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn42.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn42.Name = "dataGridViewTextBoxColumn42";
            this.dataGridViewTextBoxColumn42.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn42.Width = 400;
            // 
            // dataGridViewTextBoxColumn43
            // 
            this.dataGridViewTextBoxColumn43.DataPropertyName = "ACESSA";
            this.dataGridViewTextBoxColumn43.HeaderText = "Acessa";
            this.dataGridViewTextBoxColumn43.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn43.Name = "dataGridViewTextBoxColumn43";
            this.dataGridViewTextBoxColumn43.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn43.Width = 75;
            // 
            // dataGridViewTextBoxColumn44
            // 
            this.dataGridViewTextBoxColumn44.DataPropertyName = "INCLUI";
            this.dataGridViewTextBoxColumn44.HeaderText = "Inclui";
            this.dataGridViewTextBoxColumn44.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn44.Name = "dataGridViewTextBoxColumn44";
            this.dataGridViewTextBoxColumn44.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn44.Width = 75;
            // 
            // dataGridViewTextBoxColumn45
            // 
            this.dataGridViewTextBoxColumn45.DataPropertyName = "ALTERA";
            this.dataGridViewTextBoxColumn45.HeaderText = "Altera";
            this.dataGridViewTextBoxColumn45.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn45.Name = "dataGridViewTextBoxColumn45";
            this.dataGridViewTextBoxColumn45.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn45.Width = 75;
            // 
            // dataGridViewTextBoxColumn46
            // 
            this.dataGridViewTextBoxColumn46.DataPropertyName = "EXCLUI";
            this.dataGridViewTextBoxColumn46.HeaderText = "Exclui";
            this.dataGridViewTextBoxColumn46.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn46.Name = "dataGridViewTextBoxColumn46";
            this.dataGridViewTextBoxColumn46.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn46.Width = 75;
            // 
            // dataGridViewTextBoxColumn47
            // 
            this.dataGridViewTextBoxColumn47.DataPropertyName = "GRUPO_ID";
            this.dataGridViewTextBoxColumn47.HeaderText = "Grupo ID";
            this.dataGridViewTextBoxColumn47.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn47.Name = "dataGridViewTextBoxColumn47";
            this.dataGridViewTextBoxColumn47.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn47.Visible = false;
            // 
            // dataGridViewTextBoxColumn48
            // 
            this.dataGridViewTextBoxColumn48.DataPropertyName = "PERMISSOES_ID";
            this.dataGridViewTextBoxColumn48.HeaderText = "Permissoes ID";
            this.dataGridViewTextBoxColumn48.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn48.Name = "dataGridViewTextBoxColumn48";
            this.dataGridViewTextBoxColumn48.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn48.Visible = false;
            // 
            // tpVendas
            // 
            this.tpVendas.Controls.Add(this.dgVendas);
            this.tpVendas.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpVendas.Location = new System.Drawing.Point(4, 25);
            this.tpVendas.Name = "tpVendas";
            this.tpVendas.Size = new System.Drawing.Size(883, 390);
            this.tpVendas.TabIndex = 7;
            this.tpVendas.Text = "Vendas";
            this.tpVendas.UseVisualStyleBackColor = true;
            // 
            // dgVendas
            // 
            this.dgVendas.AllowUserToAddRows = false;
            this.dgVendas.AllowUserToDeleteRows = false;
            this.dgVendas.AllowUserToResizeColumns = false;
            this.dgVendas.AllowUserToResizeRows = false;
            dataGridViewCellStyle51.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle51.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle51.SelectionForeColor = System.Drawing.Color.Black;
            this.dgVendas.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle51;
            this.dgVendas.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgVendas.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle52.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle52.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle52.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle52.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle52.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle52.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgVendas.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle52;
            this.dgVendas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgVendas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn49,
            this.dataGridViewTextBoxColumn50,
            this.dataGridViewTextBoxColumn51,
            this.dataGridViewTextBoxColumn52,
            this.dataGridViewTextBoxColumn53,
            this.dataGridViewTextBoxColumn54,
            this.dataGridViewTextBoxColumn55,
            this.dataGridViewTextBoxColumn56});
            dataGridViewCellStyle54.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle54.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle54.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle54.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle54.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle54.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle54.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgVendas.DefaultCellStyle = dataGridViewCellStyle54;
            this.dgVendas.GridColor = System.Drawing.Color.LightGray;
            this.dgVendas.Location = new System.Drawing.Point(7, 6);
            this.dgVendas.Name = "dgVendas";
            dataGridViewCellStyle55.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle55.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle55.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle55.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle55.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle55.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle55.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgVendas.RowHeadersDefaultCellStyle = dataGridViewCellStyle55;
            this.dgVendas.RowHeadersVisible = false;
            dataGridViewCellStyle56.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle56.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle56.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle56.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle56.SelectionForeColor = System.Drawing.Color.Black;
            this.dgVendas.RowsDefaultCellStyle = dataGridViewCellStyle56;
            this.dgVendas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgVendas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgVendas.Size = new System.Drawing.Size(869, 378);
            this.dgVendas.TabIndex = 2;
            this.dgVendas.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgVendas_CellEndEdit);
            this.dgVendas.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgVendas_CellValidated);
            // 
            // dataGridViewTextBoxColumn49
            // 
            this.dataGridViewTextBoxColumn49.DataPropertyName = "MODULO_ID";
            dataGridViewCellStyle53.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn49.DefaultCellStyle = dataGridViewCellStyle53;
            this.dataGridViewTextBoxColumn49.HeaderText = "Modulo ID";
            this.dataGridViewTextBoxColumn49.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn49.Name = "dataGridViewTextBoxColumn49";
            this.dataGridViewTextBoxColumn49.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn49.Visible = false;
            // 
            // dataGridViewTextBoxColumn50
            // 
            this.dataGridViewTextBoxColumn50.DataPropertyName = "DESCRICAO";
            this.dataGridViewTextBoxColumn50.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn50.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn50.Name = "dataGridViewTextBoxColumn50";
            this.dataGridViewTextBoxColumn50.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn50.Width = 400;
            // 
            // dataGridViewTextBoxColumn51
            // 
            this.dataGridViewTextBoxColumn51.DataPropertyName = "ACESSA";
            this.dataGridViewTextBoxColumn51.HeaderText = "Acessa";
            this.dataGridViewTextBoxColumn51.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn51.Name = "dataGridViewTextBoxColumn51";
            this.dataGridViewTextBoxColumn51.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn51.Width = 75;
            // 
            // dataGridViewTextBoxColumn52
            // 
            this.dataGridViewTextBoxColumn52.DataPropertyName = "INCLUI";
            this.dataGridViewTextBoxColumn52.HeaderText = "Inclui";
            this.dataGridViewTextBoxColumn52.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn52.Name = "dataGridViewTextBoxColumn52";
            this.dataGridViewTextBoxColumn52.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn52.Width = 75;
            // 
            // dataGridViewTextBoxColumn53
            // 
            this.dataGridViewTextBoxColumn53.DataPropertyName = "ALTERA";
            this.dataGridViewTextBoxColumn53.HeaderText = "Altera";
            this.dataGridViewTextBoxColumn53.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn53.Name = "dataGridViewTextBoxColumn53";
            this.dataGridViewTextBoxColumn53.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn53.Width = 75;
            // 
            // dataGridViewTextBoxColumn54
            // 
            this.dataGridViewTextBoxColumn54.DataPropertyName = "EXCLUI";
            this.dataGridViewTextBoxColumn54.HeaderText = "Exclui";
            this.dataGridViewTextBoxColumn54.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn54.Name = "dataGridViewTextBoxColumn54";
            this.dataGridViewTextBoxColumn54.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn54.Width = 75;
            // 
            // dataGridViewTextBoxColumn55
            // 
            this.dataGridViewTextBoxColumn55.DataPropertyName = "GRUPO_ID";
            this.dataGridViewTextBoxColumn55.HeaderText = "Grupo ID";
            this.dataGridViewTextBoxColumn55.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn55.Name = "dataGridViewTextBoxColumn55";
            this.dataGridViewTextBoxColumn55.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn55.Visible = false;
            // 
            // dataGridViewTextBoxColumn56
            // 
            this.dataGridViewTextBoxColumn56.DataPropertyName = "PERMISSOES_ID";
            this.dataGridViewTextBoxColumn56.HeaderText = "Permissoes ID";
            this.dataGridViewTextBoxColumn56.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn56.Name = "dataGridViewTextBoxColumn56";
            this.dataGridViewTextBoxColumn56.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn56.Visible = false;
            // 
            // tpRelatorio
            // 
            this.tpRelatorio.Controls.Add(this.dgRelatorios);
            this.tpRelatorio.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpRelatorio.Location = new System.Drawing.Point(4, 25);
            this.tpRelatorio.Name = "tpRelatorio";
            this.tpRelatorio.Size = new System.Drawing.Size(883, 390);
            this.tpRelatorio.TabIndex = 8;
            this.tpRelatorio.Text = "Relatórios";
            this.tpRelatorio.UseVisualStyleBackColor = true;
            // 
            // dgRelatorios
            // 
            this.dgRelatorios.AllowUserToAddRows = false;
            this.dgRelatorios.AllowUserToDeleteRows = false;
            this.dgRelatorios.AllowUserToResizeColumns = false;
            this.dgRelatorios.AllowUserToResizeRows = false;
            dataGridViewCellStyle57.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle57.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle57.SelectionForeColor = System.Drawing.Color.Black;
            this.dgRelatorios.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle57;
            this.dgRelatorios.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgRelatorios.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle58.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle58.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle58.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle58.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle58.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle58.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgRelatorios.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle58;
            this.dgRelatorios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRelatorios.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn57,
            this.dataGridViewTextBoxColumn58,
            this.dataGridViewTextBoxColumn59,
            this.dataGridViewTextBoxColumn60,
            this.dataGridViewTextBoxColumn61,
            this.dataGridViewTextBoxColumn62,
            this.dataGridViewTextBoxColumn63,
            this.dataGridViewTextBoxColumn64});
            dataGridViewCellStyle60.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle60.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle60.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle60.ForeColor = System.Drawing.Color.Navy;
            dataGridViewCellStyle60.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle60.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle60.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgRelatorios.DefaultCellStyle = dataGridViewCellStyle60;
            this.dgRelatorios.GridColor = System.Drawing.Color.LightGray;
            this.dgRelatorios.Location = new System.Drawing.Point(7, 6);
            this.dgRelatorios.Name = "dgRelatorios";
            dataGridViewCellStyle61.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle61.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle61.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle61.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle61.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle61.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle61.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgRelatorios.RowHeadersDefaultCellStyle = dataGridViewCellStyle61;
            this.dgRelatorios.RowHeadersVisible = false;
            dataGridViewCellStyle62.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle62.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle62.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle62.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle62.SelectionForeColor = System.Drawing.Color.Black;
            this.dgRelatorios.RowsDefaultCellStyle = dataGridViewCellStyle62;
            this.dgRelatorios.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgRelatorios.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgRelatorios.Size = new System.Drawing.Size(869, 378);
            this.dgRelatorios.TabIndex = 2;
            this.dgRelatorios.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgRelatorios_CellEndEdit);
            this.dgRelatorios.CellValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgRelatorios_CellValidated);
            // 
            // dataGridViewTextBoxColumn57
            // 
            this.dataGridViewTextBoxColumn57.DataPropertyName = "MODULO_ID";
            dataGridViewCellStyle59.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.dataGridViewTextBoxColumn57.DefaultCellStyle = dataGridViewCellStyle59;
            this.dataGridViewTextBoxColumn57.HeaderText = "Modulo ID";
            this.dataGridViewTextBoxColumn57.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn57.Name = "dataGridViewTextBoxColumn57";
            this.dataGridViewTextBoxColumn57.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn57.Visible = false;
            // 
            // dataGridViewTextBoxColumn58
            // 
            this.dataGridViewTextBoxColumn58.DataPropertyName = "DESCRICAO";
            this.dataGridViewTextBoxColumn58.HeaderText = "Descrição";
            this.dataGridViewTextBoxColumn58.MaxInputLength = 100;
            this.dataGridViewTextBoxColumn58.Name = "dataGridViewTextBoxColumn58";
            this.dataGridViewTextBoxColumn58.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn58.Width = 400;
            // 
            // dataGridViewTextBoxColumn59
            // 
            this.dataGridViewTextBoxColumn59.DataPropertyName = "ACESSA";
            this.dataGridViewTextBoxColumn59.HeaderText = "Acessa";
            this.dataGridViewTextBoxColumn59.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn59.Name = "dataGridViewTextBoxColumn59";
            this.dataGridViewTextBoxColumn59.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn59.Width = 75;
            // 
            // dataGridViewTextBoxColumn60
            // 
            this.dataGridViewTextBoxColumn60.DataPropertyName = "INCLUI";
            this.dataGridViewTextBoxColumn60.HeaderText = "Inclui";
            this.dataGridViewTextBoxColumn60.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn60.Name = "dataGridViewTextBoxColumn60";
            this.dataGridViewTextBoxColumn60.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn60.Width = 75;
            // 
            // dataGridViewTextBoxColumn61
            // 
            this.dataGridViewTextBoxColumn61.DataPropertyName = "ALTERA";
            this.dataGridViewTextBoxColumn61.HeaderText = "Altera";
            this.dataGridViewTextBoxColumn61.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn61.Name = "dataGridViewTextBoxColumn61";
            this.dataGridViewTextBoxColumn61.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn61.Width = 75;
            // 
            // dataGridViewTextBoxColumn62
            // 
            this.dataGridViewTextBoxColumn62.DataPropertyName = "EXCLUI";
            this.dataGridViewTextBoxColumn62.HeaderText = "Exclui";
            this.dataGridViewTextBoxColumn62.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn62.Name = "dataGridViewTextBoxColumn62";
            this.dataGridViewTextBoxColumn62.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn62.Width = 75;
            // 
            // dataGridViewTextBoxColumn63
            // 
            this.dataGridViewTextBoxColumn63.DataPropertyName = "GRUPO_ID";
            this.dataGridViewTextBoxColumn63.HeaderText = "Grupo ID";
            this.dataGridViewTextBoxColumn63.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn63.Name = "dataGridViewTextBoxColumn63";
            this.dataGridViewTextBoxColumn63.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn63.Visible = false;
            // 
            // dataGridViewTextBoxColumn64
            // 
            this.dataGridViewTextBoxColumn64.DataPropertyName = "PERMISSOES_ID";
            this.dataGridViewTextBoxColumn64.HeaderText = "Permissoes ID";
            this.dataGridViewTextBoxColumn64.MaxInputLength = 18;
            this.dataGridViewTextBoxColumn64.Name = "dataGridViewTextBoxColumn64";
            this.dataGridViewTextBoxColumn64.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.dataGridViewTextBoxColumn64.Visible = false;
            // 
            // frmGrupos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(973, 558);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MinimizeBox = false;
            this.Name = "frmGrupos";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "frmGrupos";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.frmGrupos_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tcGrupos.ResumeLayout(false);
            this.tpGrade.ResumeLayout(false);
            this.tpGrade.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgGrupos)).EndInit();
            this.cmsGrupo.ResumeLayout(false);
            this.tpFicha.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.tpPermissoes.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tpGeral.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgPermissoes)).EndInit();
            this.tpCadastro.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCadastro)).EndInit();
            this.tpCaixa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgCaixa)).EndInit();
            this.tpContasAPagar.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgContasAPagar)).EndInit();
            this.tpReceber.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgContasAReceber)).EndInit();
            this.tpEstoque.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgEstoque)).EndInit();
            this.tpSngpc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgSngpc)).EndInit();
            this.tpVendas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgVendas)).EndInit();
            this.tpRelatorio.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgRelatorios)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabControl tcGrupos;
        private System.Windows.Forms.TabPage tpGrade;
        private System.Windows.Forms.TabPage tpFicha;
        private System.Windows.Forms.CheckBox chkAdmin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDescricao;
        private System.Windows.Forms.CheckBox chkLiberado;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dgGrupos;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tslRegistros;
        private System.Windows.Forms.TextBox txtBDescr;
        private System.Windows.Forms.TextBox txtBID;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.TextBox txtData;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ContextMenuStrip cmsGrupo;
        private System.Windows.Forms.ToolStripMenuItem tsmExcel;
        private System.Windows.Forms.TabPage tpPermissoes;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.ComboBox cmbLiberado;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dgPermissoes;
        private System.Windows.Forms.DataGridViewTextBoxColumn MODULO_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn MOD_DESCR;
        private System.Windows.Forms.DataGridViewTextBoxColumn ACESSA;
        private System.Windows.Forms.DataGridViewTextBoxColumn INCLUI;
        private System.Windows.Forms.DataGridViewTextBoxColumn ALTERA;
        private System.Windows.Forms.DataGridViewTextBoxColumn EXCLUI;
        private System.Windows.Forms.DataGridViewTextBoxColumn GRUPO_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn PERMISSOES_ID;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.ToolStripMenuItem tsmConfigurar;
        private System.Windows.Forms.DataGridViewTextBoxColumn GRUPO_USU_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn DESCRICAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn ADM;
        private System.Windows.Forms.DataGridViewTextBoxColumn LIB;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPALTERACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn DTCADASTRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn OPCADASTRO;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tpGeral;
        private System.Windows.Forms.TabPage tpCadastro;
        private System.Windows.Forms.TabPage tpCaixa;
        private System.Windows.Forms.TabPage tpContasAPagar;
        private System.Windows.Forms.TabPage tpReceber;
        private System.Windows.Forms.TabPage tpEstoque;
        private System.Windows.Forms.TabPage tpSngpc;
        private System.Windows.Forms.TabPage tpVendas;
        private System.Windows.Forms.DataGridView dgCadastro;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridView dgCaixa;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridView dgContasAPagar;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn22;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn23;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn24;
        private System.Windows.Forms.DataGridView dgContasAReceber;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn25;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn26;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn27;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn28;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn29;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn30;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn31;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn32;
        private System.Windows.Forms.DataGridView dgEstoque;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn33;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn34;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn35;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn36;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn37;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn38;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn39;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn40;
        private System.Windows.Forms.DataGridView dgSngpc;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn41;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn42;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn43;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn44;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn45;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn46;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn47;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn48;
        private System.Windows.Forms.DataGridView dgVendas;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn49;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn50;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn51;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn52;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn53;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn54;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn55;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn56;
        private System.Windows.Forms.TabPage tpRelatorio;
        private System.Windows.Forms.DataGridView dgRelatorios;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn57;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn58;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn59;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn60;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn61;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn62;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn63;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn64;
    }
}