﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.SNGPC;
using GestaoAdministrativa.Negocio;
using GestaoAdministrativa.Vendas;

namespace GestaoAdministrativa
{
    public partial class frmBuscaForn : Form
    {
        private int indiceGrid;
        private string tBusca;
        public int cfCodigo;
        public string cfNome;
        public string cfDocto;
        public string conCodigo;
        public int conId;
        public string conWeb;
        public string cfCidade;
        public int cfStatus;
        public string conCodConv;
        public int cfID;
        public double conDesconto;

        public frmBuscaForn(string tipoBusca)
        {
            InitializeComponent();
            tBusca = tipoBusca;
        }

        private void frmBuscaForn_Load(object sender, EventArgs e)
        {
            if (tBusca.Equals("cliente"))
            {
                dgCliFor.Columns[4].Visible = true;
            }
            else
            {
                dgCliFor.Columns[4].Visible = false;
            }
            cfCodigo = 0;
            cfNome = "";
            cfDocto = "";
            conCodigo = "";
            conId = 0;
            conWeb = "";
            indiceGrid = 0;
            cfCidade = "";
            cfStatus = 0;
            conCodConv = "";
            cfID = 0;
            conDesconto = 0;
            Cursor = Cursors.Default;
        }
        public void SelecionaLinha()
        {
            try
            {
                if (tBusca.Equals("fornecedor"))
                {
                    Principal.cnpjCliFor = dgCliFor.Rows[indiceGrid].Cells["CF_DOCTO"].Value.ToString();
                    if(dgCliFor.Rows[indiceGrid].Cells["CF_CIDADE"].Value == null)
                    {
                        Principal.cidadeCliFor = "";
                    }
                    else 
                        Principal.cidadeCliFor = dgCliFor.Rows[indiceGrid].Cells["CF_CIDADE"].Value.ToString() + " / " + dgCliFor.Rows[indiceGrid].Cells["CF_UF"].Value.ToString();

                    if (dgCliFor.Rows[indiceGrid].Cells["CF_NOME"].Value == null)
                    {
                        Principal.nomeCliFor = "";
                    }
                    else
                        Principal.nomeCliFor = dgCliFor.Rows[indiceGrid].Cells["CF_NOME"].Value.ToString();
                }
                else
                {
                    if (String.IsNullOrEmpty(dgCliFor.Rows[indiceGrid].Cells["CF_CODIGO"].Value.ToString()) || Funcoes.IdentificaSeContemSomenteNumeros(dgCliFor.Rows[indiceGrid].Cells["CF_CODIGO"].Value.ToString()))
                    {
                        cfCodigo = dgCliFor.Rows[indiceGrid].Cells["CF_CODIGO"].Value.ToString() == "" ? 0 : Convert.ToInt32(dgCliFor.Rows[indiceGrid].Cells["CF_CODIGO"].Value);
                        cfNome = dgCliFor.Rows[indiceGrid].Cells["CF_NOME"].Value.ToString() == "" ? "" : dgCliFor.Rows[indiceGrid].Cells["CF_NOME"].Value.ToString();
                        cfDocto = dgCliFor.Rows[indiceGrid].Cells["CF_DOCTO"].Value.ToString();
                        cfCidade = dgCliFor.Rows[indiceGrid].Cells["CF_CIDADE"].Value.ToString();
                        cfID = dgCliFor.Rows[indiceGrid].Cells["CF_ID"].Value.ToString() == "" ? 0 : Convert.ToInt32(dgCliFor.Rows[indiceGrid].Cells["CF_ID"].Value);
                        if (!String.IsNullOrEmpty(dgCliFor.Rows[indiceGrid].Cells["CON_CODIGO"].Value.ToString()))
                        {
                            conCodigo = dgCliFor.Rows[indiceGrid].Cells["CON_CODIGO"].Value.ToString();
                            conCodConv = dgCliFor.Rows[indiceGrid].Cells["CON_COD_CONV"].Value.ToString();
                            conId = Convert.ToInt32(dgCliFor.Rows[indiceGrid].Cells["CON_ID"].Value);
                            conWeb = dgCliFor.Rows[indiceGrid].Cells["CON_WEB"].Value.ToString();
                        }
                        cfStatus = Convert.ToInt32(dgCliFor.Rows[indiceGrid].Cells["CF_STATUS"].Value);
                        conDesconto = Convert.ToDouble(dgCliFor.Rows[indiceGrid].Cells["CON_DESCONTO"].Value);
                    }
                    else
                    {
                        MessageBox.Show("Verifique o Cadastro do Cliente Campo Cód. do Cliente, só pode conter números ou estar em branco", "Busca Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Pesquisa de Forn. e Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgCliFor_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            SelecionaLinha();
        }

        private void dgCliFor_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    SelecionaLinha();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Pesquisa de Forn. e Clientes", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgCliFor_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            indiceGrid = e.RowIndex;
        }

        private void dgCliFor_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue.Equals(27))
            {
                cfCodigo = 0;
                cfNome = "";
                cfDocto = "";
                conCodigo = "";
                conId = 0;
                conWeb = "";
                indiceGrid = 0;
                cfCidade = "";
                indiceGrid = 0;
                cfStatus = 0;
                conCodConv = "";
                this.Close();
            }
            int indice = dgCliFor.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && indiceGrid < indice)
            {
                indiceGrid = indiceGrid + 1;
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && indiceGrid < indice)
            {
                indiceGrid = indiceGrid - 1;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            DataTable dtRetorno = new DataTable();
            string filtro;
            if (!String.IsNullOrEmpty(txtNome.Text))
            {
                if (tBusca.Equals("fornecedor"))
                {
                    var buscaFornecedor = new Cliente();
                    List<Cliente> cliente = new List<Cliente>();
                    cliente = buscaFornecedor.BuscaDadosFornecedor(1, txtNome.Text.Trim().ToUpper(), 1);
                    if(cliente.Count > 0)
                    {
                        for (int i = 0; i < cliente.Count; i++)
                        {
                            dgCliFor.Rows.Insert(dgCliFor.RowCount, new Object[] { cliente[i].CodigoConveniada, "", cliente[i].CfCodigo, cliente[i].CfNome, cliente[i].CfApelido, cliente[i].CfObservacao,
                                    cliente[i].CfDocto, cliente[i].CfTelefone, cliente[i].CfEndereco, cliente[i].CfBairro, cliente[i].CfCidade, cliente[i].CfUF, cliente[i].CfId, "0", "0", "0", cliente[i].CfStatus });
                        }
                    }
                    else
                    {
                        MessageBox.Show("Nenhum registro encontrado", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtNome.Focus();
                    }
                }
                else
                {
                    var dCliente = new Cliente();
                    dtRetorno = dCliente.DadosClienteTelaBusca(dgCliFor.Rows[0].Cells["CON_CODIGO"].Value.ToString(), txtNome.Text.ToUpper(), out filtro);
                    if(dtRetorno.Rows.Count > 0)
                    {
                        dgCliFor.DataSource = dtRetorno;
                        dgCliFor.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Nenhum registro encontrado", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtNome.Focus();
                    }

                }
            }
            else
            {
                MessageBox.Show("Busca não pode ser em branco.", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNome.Focus();
            }
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnBuscar.PerformClick();
        }

        private void txtNome_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyValue.Equals(27))
            {
                cfCodigo = 0;
                cfNome = "";
                cfDocto = "";
                conCodigo = "";
                conId = 0;
                conWeb = "";
                indiceGrid = 0;
                cfCidade = "";
                indiceGrid = 0;
                cfStatus = 0;
                conCodConv = "";
                this.Close();
            }
        }

        private void dgCliFor_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgCliFor.Rows[e.RowIndex].Cells["CON_WEB"].Value.ToString() == "4")
            {
                dgCliFor.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Maroon;
                dgCliFor.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Maroon;
            }
            else if (dgCliFor.Rows[e.RowIndex].Cells["CON_WEB"].Value.ToString() == "0")
            {
                dgCliFor.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.DarkGreen;
                dgCliFor.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.DarkGreen;
            }
        }
    }
}
