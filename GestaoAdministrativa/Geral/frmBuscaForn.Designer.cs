﻿namespace GestaoAdministrativa
{
    partial class frmBuscaForn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBuscaForn));
            this.dgCliFor = new System.Windows.Forms.DataGridView();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnBuscar = new System.Windows.Forms.Button();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.CON_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_APELIDO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_OBSERVACAO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_DOCTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_FONE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_ENDER = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_BAIRRO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_CIDADE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_UF = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_COD_CONV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_WEB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CF_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_DESCONTO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgCliFor)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dgCliFor
            // 
            this.dgCliFor.AllowUserToAddRows = false;
            this.dgCliFor.AllowUserToDeleteRows = false;
            this.dgCliFor.AllowUserToResizeColumns = false;
            this.dgCliFor.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCliFor.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgCliFor.BackgroundColor = System.Drawing.Color.White;
            this.dgCliFor.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCliFor.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgCliFor.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgCliFor.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CON_CODIGO,
            this.CON_NOME,
            this.CF_CODIGO,
            this.CF_NOME,
            this.CF_APELIDO,
            this.CF_OBSERVACAO,
            this.CF_DOCTO,
            this.CF_FONE,
            this.CF_ENDER,
            this.CF_BAIRRO,
            this.CF_CIDADE,
            this.CF_UF,
            this.CF_ID,
            this.CON_COD_CONV,
            this.CON_ID,
            this.CON_WEB,
            this.CF_STATUS,
            this.CON_DESCONTO});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgCliFor.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgCliFor.GridColor = System.Drawing.Color.LightGray;
            this.dgCliFor.Location = new System.Drawing.Point(0, 38);
            this.dgCliFor.Name = "dgCliFor";
            this.dgCliFor.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgCliFor.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgCliFor.RowHeadersVisible = false;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.LightGray;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgCliFor.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgCliFor.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgCliFor.Size = new System.Drawing.Size(690, 230);
            this.dgCliFor.TabIndex = 0;
            this.dgCliFor.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgCliFor_CellClick);
            this.dgCliFor.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgCliFor_CellMouseDoubleClick);
            this.dgCliFor.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgCliFor_RowPrePaint);
            this.dgCliFor.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgCliFor_KeyDown);
            this.dgCliFor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgCliFor_KeyPress);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.btnBuscar);
            this.panel1.Controls.Add(this.txtNome);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(690, 32);
            this.panel1.TabIndex = 1;
            // 
            // btnBuscar
            // 
            this.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBuscar.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBuscar.ForeColor = System.Drawing.Color.Navy;
            this.btnBuscar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBuscar.Location = new System.Drawing.Point(482, 3);
            this.btnBuscar.Name = "btnBuscar";
            this.btnBuscar.Size = new System.Drawing.Size(64, 25);
            this.btnBuscar.TabIndex = 3;
            this.btnBuscar.Text = "Busca";
            this.btnBuscar.UseVisualStyleBackColor = true;
            this.btnBuscar.Click += new System.EventHandler(this.btnBuscar_Click);
            // 
            // txtNome
            // 
            this.txtNome.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtNome.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txtNome.ForeColor = System.Drawing.Color.Black;
            this.txtNome.Location = new System.Drawing.Point(210, 5);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(266, 22);
            this.txtNome.TabIndex = 2;
            this.txtNome.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtNome_KeyDown);
            this.txtNome.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNome_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Navy;
            this.label1.Location = new System.Drawing.Point(3, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(201, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Busca Por Razão Social/Nome";
            // 
            // CON_CODIGO
            // 
            this.CON_CODIGO.DataPropertyName = "CON_CODIGO";
            this.CON_CODIGO.HeaderText = "Cód. Conv";
            this.CON_CODIGO.Name = "CON_CODIGO";
            this.CON_CODIGO.ReadOnly = true;
            this.CON_CODIGO.Width = 80;
            // 
            // CON_NOME
            // 
            this.CON_NOME.DataPropertyName = "CON_NOME";
            this.CON_NOME.HeaderText = "Conveniada";
            this.CON_NOME.Name = "CON_NOME";
            this.CON_NOME.ReadOnly = true;
            this.CON_NOME.Width = 130;
            // 
            // CF_CODIGO
            // 
            this.CF_CODIGO.DataPropertyName = "CF_CODIGO";
            this.CF_CODIGO.HeaderText = "Código";
            this.CF_CODIGO.MaxInputLength = 18;
            this.CF_CODIGO.Name = "CF_CODIGO";
            this.CF_CODIGO.ReadOnly = true;
            this.CF_CODIGO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_CODIGO.Visible = false;
            this.CF_CODIGO.Width = 80;
            // 
            // CF_NOME
            // 
            this.CF_NOME.DataPropertyName = "CF_NOME";
            this.CF_NOME.HeaderText = "Razão Social/Nome";
            this.CF_NOME.MaxInputLength = 60;
            this.CF_NOME.Name = "CF_NOME";
            this.CF_NOME.ReadOnly = true;
            this.CF_NOME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_NOME.Width = 250;
            // 
            // CF_APELIDO
            // 
            this.CF_APELIDO.DataPropertyName = "CF_APELIDO";
            this.CF_APELIDO.HeaderText = "Fantasia/Apelido";
            this.CF_APELIDO.MaxInputLength = 30;
            this.CF_APELIDO.Name = "CF_APELIDO";
            this.CF_APELIDO.ReadOnly = true;
            this.CF_APELIDO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_APELIDO.Width = 150;
            // 
            // CF_OBSERVACAO
            // 
            this.CF_OBSERVACAO.DataPropertyName = "CF_OBSERVACAO";
            this.CF_OBSERVACAO.HeaderText = "Observação";
            this.CF_OBSERVACAO.MaxInputLength = 500;
            this.CF_OBSERVACAO.Name = "CF_OBSERVACAO";
            this.CF_OBSERVACAO.ReadOnly = true;
            this.CF_OBSERVACAO.Visible = false;
            this.CF_OBSERVACAO.Width = 250;
            // 
            // CF_DOCTO
            // 
            this.CF_DOCTO.DataPropertyName = "CF_DOCTO";
            this.CF_DOCTO.HeaderText = "CNPJ/CPF";
            this.CF_DOCTO.MaxInputLength = 18;
            this.CF_DOCTO.Name = "CF_DOCTO";
            this.CF_DOCTO.ReadOnly = true;
            this.CF_DOCTO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_DOCTO.Width = 125;
            // 
            // CF_FONE
            // 
            this.CF_FONE.DataPropertyName = "CF_FONE";
            this.CF_FONE.HeaderText = "Telefone";
            this.CF_FONE.Name = "CF_FONE";
            this.CF_FONE.ReadOnly = true;
            // 
            // CF_ENDER
            // 
            this.CF_ENDER.DataPropertyName = "CF_ENDER";
            this.CF_ENDER.HeaderText = "Endereço";
            this.CF_ENDER.MaxInputLength = 60;
            this.CF_ENDER.Name = "CF_ENDER";
            this.CF_ENDER.ReadOnly = true;
            this.CF_ENDER.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_ENDER.Width = 200;
            // 
            // CF_BAIRRO
            // 
            this.CF_BAIRRO.DataPropertyName = "CF_BAIRRO";
            this.CF_BAIRRO.HeaderText = "Bairro";
            this.CF_BAIRRO.MaxInputLength = 20;
            this.CF_BAIRRO.Name = "CF_BAIRRO";
            this.CF_BAIRRO.ReadOnly = true;
            this.CF_BAIRRO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_BAIRRO.Width = 125;
            // 
            // CF_CIDADE
            // 
            this.CF_CIDADE.DataPropertyName = "CF_CIDADE";
            this.CF_CIDADE.HeaderText = "Cidade";
            this.CF_CIDADE.MaxInputLength = 30;
            this.CF_CIDADE.Name = "CF_CIDADE";
            this.CF_CIDADE.ReadOnly = true;
            this.CF_CIDADE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_CIDADE.Width = 150;
            // 
            // CF_UF
            // 
            this.CF_UF.DataPropertyName = "CF_UF";
            this.CF_UF.HeaderText = "UF";
            this.CF_UF.MaxInputLength = 2;
            this.CF_UF.Name = "CF_UF";
            this.CF_UF.ReadOnly = true;
            this.CF_UF.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.CF_UF.Width = 50;
            // 
            // CF_ID
            // 
            this.CF_ID.DataPropertyName = "CF_ID";
            this.CF_ID.HeaderText = "ID";
            this.CF_ID.Name = "CF_ID";
            this.CF_ID.ReadOnly = true;
            this.CF_ID.Visible = false;
            // 
            // CON_COD_CONV
            // 
            this.CON_COD_CONV.DataPropertyName = "CON_COD_CONV";
            this.CON_COD_CONV.HeaderText = "Cod. Conv";
            this.CON_COD_CONV.Name = "CON_COD_CONV";
            this.CON_COD_CONV.ReadOnly = true;
            this.CON_COD_CONV.Visible = false;
            // 
            // CON_ID
            // 
            this.CON_ID.DataPropertyName = "CON_ID";
            this.CON_ID.HeaderText = "Con. ID";
            this.CON_ID.Name = "CON_ID";
            this.CON_ID.ReadOnly = true;
            this.CON_ID.Visible = false;
            // 
            // CON_WEB
            // 
            this.CON_WEB.DataPropertyName = "CON_WEB";
            this.CON_WEB.HeaderText = "Con. Web";
            this.CON_WEB.Name = "CON_WEB";
            this.CON_WEB.ReadOnly = true;
            this.CON_WEB.Visible = false;
            // 
            // CF_STATUS
            // 
            this.CF_STATUS.DataPropertyName = "CF_STATUS";
            this.CF_STATUS.HeaderText = "Status";
            this.CF_STATUS.Name = "CF_STATUS";
            this.CF_STATUS.ReadOnly = true;
            this.CF_STATUS.Visible = false;
            // 
            // CON_DESCONTO
            // 
            this.CON_DESCONTO.DataPropertyName = "CON_DESCONTO";
            this.CON_DESCONTO.HeaderText = "Desconto";
            this.CON_DESCONTO.Name = "CON_DESCONTO";
            this.CON_DESCONTO.ReadOnly = true;
            this.CON_DESCONTO.Visible = false;
            // 
            // frmBuscaForn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(690, 268);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgCliFor);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBuscaForn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pesquisa de Fornecedores e Clientes";
            this.Load += new System.EventHandler(this.frmBuscaForn_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgCliFor)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView dgCliFor;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnBuscar;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_APELIDO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_OBSERVACAO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_DOCTO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_FONE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_ENDER;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_BAIRRO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_CIDADE;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_UF;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_COD_CONV;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_WEB;
        private System.Windows.Forms.DataGridViewTextBoxColumn CF_STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_DESCONTO;
    }
}