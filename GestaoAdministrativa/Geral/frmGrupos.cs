﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using GestaoAdministrativa.Cadastros;
using Excel = Microsoft.Office.Interop.Excel;
using GestaoAdministrativa.Geral;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa
{
    public partial class frmGrupos : Form, Botoes
    {
        private DataTable dtGrupos = new DataTable();
        private DataTable dtPermissoes = new DataTable();
        private int contRegistros;
        private bool emGrade;
        private ToolStripButton tsbGrupos = new ToolStripButton("Grupos de Usuários");
        private ToolStripSeparator tssGrupos = new ToolStripSeparator();
        private bool decrescente;
        private string strOrdem;
        private string tipo;
        private Permissao atualizaPermissao = new Permissao();
        public frmGrupos(MDIPrincipal menu)
        {
            InitializeComponent();
            Principal.mdiPrincipal = menu;
            RegisterFocusEvents(this.Controls);
        }

        private void frmGrupos_Load(object sender, EventArgs e)
        {
            try
            {
                this.WindowState = FormWindowState.Maximized;
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                cmbLiberado.SelectedIndex = 0;
                txtBID.Focus();

                dgPermissoes.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgPermissoes.ForeColor = System.Drawing.Color.Black;
                dgPermissoes.DefaultCellStyle.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                dgPermissoes.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void CarregarDados(int linha)
        {
            txtID.Text = Funcoes.ChecaCampoVazio(dtGrupos.Rows[linha]["GRUPO_USU_ID"].ToString());
            txtDescricao.Text = Funcoes.ChecaCampoVazio(dtGrupos.Rows[linha]["DESCRICAO"].ToString());
            if (dtGrupos.Rows[linha]["ADMINISTRADOR"].ToString() == "S")
            {
                chkAdmin.Checked = true;
            }
            else
                chkAdmin.Checked = false;

            if (dtGrupos.Rows[linha]["LIBERADO"].ToString() == "S")
            {
                chkLiberado.Checked = true;
            }
            else
                chkLiberado.Checked = false;

            txtData.Text = Funcoes.ChecaCampoVazio(dtGrupos.Rows[linha]["DTALTERACAO"].ToString());
            txtUsuario.Text = Funcoes.ChecaCampoVazio(dtGrupos.Rows[linha]["OPALTERACAO"].ToString());

            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupos, linha));

        }

        public void LimparGrade()
        {
            dtPermissoes.Clear();
            dgPermissoes.DataSource = dtPermissoes;
            dtGrupos.Clear();
            dgGrupos.DataSource = dtGrupos;
            contRegistros = 0;
            tslRegistros.Text = "";
            emGrade = false;
            Funcoes.LimpaFormularios(this);
            cmbLiberado.SelectedIndex = 0;
            txtBID.Focus();
        }

        public void LimparFicha()
        {
            dtPermissoes.Clear();
            dgPermissoes.DataSource = dtPermissoes;
            txtID.Text = "";
            txtDescricao.Text = "";
            chkAdmin.Checked = false;
            chkLiberado.Checked = false;
            txtData.Text = "";
            txtUsuario.Text = "";
            cmbLiberado.SelectedIndex = 0;
            txtDescricao.Focus();
        }

        public void Limpar()
        {
            Cursor = Cursors.Default;
            if (tcGrupos.SelectedTab == tpGrade)
            {
                LimparGrade();
            }
            else
                if (tcGrupos.SelectedTab == tpFicha)
            {
                LimparFicha();
            }

            BotoesHabilitados();
        }

        public void Primeiro()
        {
            if (dtGrupos.Rows.Count != 0)
            {
                Principal.indice = 0;
                Principal.bNaveg = false;
                contRegistros = 0;
                while (Principal.bNaveg == false)
                {
                    if (Convert.ToString(dgGrupos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                    {
                        dgGrupos.CurrentCell = dgGrupos.Rows[contRegistros].Cells[Principal.indice];
                        Principal.bNaveg = true;
                    }
                    Principal.indice = Principal.indice + 1;
                }
                CarregarDados(contRegistros);
            }
            else
            {
                Principal.mdiPrincipal.HabilitaBotoes("NNNN");
            }
            if (tcGrupos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
            }
        }

        public void Ultimo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = dtGrupos.Rows.Count - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgGrupos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgGrupos.CurrentCell = dgGrupos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Proximo()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros + 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgGrupos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgGrupos.CurrentCell = dgGrupos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public void Anterior()
        {
            Principal.indice = 0;
            Principal.bNaveg = false;
            contRegistros = contRegistros - 1;
            while (Principal.bNaveg == false)
            {
                if (Convert.ToString(dgGrupos.Rows[contRegistros].Cells[Principal.indice].Visible).Equals("True"))
                {
                    dgGrupos.CurrentCell = dgGrupos.Rows[contRegistros].Cells[Principal.indice];
                    Principal.bNaveg = true;
                }
                Principal.indice = Principal.indice + 1;
            }
            CarregarDados(contRegistros);
        }

        public bool ConsisteCampos()
        {
            Principal.msgEstilo = MessageBoxButtons.OK;
            Principal.msgIcone = MessageBoxIcon.Information;
            Principal.msgTitulo = "Consistência";
            
            if (String.IsNullOrEmpty(txtDescricao.Text.Trim()))
            {
                Principal.mensagem = "Descrição não pode ser em branco.";
                Funcoes.Avisa();
                txtDescricao.Focus();
            }
            return true;
        }

        public bool Atualiza()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                DataTable registroGrupoDeUsuario = new DataTable();
                if (ConsisteCampos().Equals(true))
                {
                    var gUsuarios = new GrupoDeUsuario
                    {
                        GrupoUsuId = int.Parse(txtID.Text),
                        Descricao = txtDescricao.Text.Trim().ToUpper(),
                        Administrador = chkAdmin.Checked == true ? "S" : "N",
                        Liberado = chkLiberado.Checked == true ? "S" : "N",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                        DtAlteracao = DateTime.Now,
                        OpAlteracao = Principal.usuario,
                    };
                    
                    registroGrupoDeUsuario = gUsuarios.IdentificaGrupoDeUsuarioCadastradoPorId(gUsuarios);

                    if (gUsuarios.AtualizaDados(gUsuarios, registroGrupoDeUsuario).Equals(true))
                    {
                        MessageBox.Show("Atualização realizada com sucesso!", "Grupo de Usuários");
                    }

                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    dtGrupos.Clear();
                    dgGrupos.DataSource = dtGrupos;
                    emGrade = false;
                    tslRegistros.Text = "";
                    LimparFicha();
                    return true;

                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public bool Excluir()
        {
            if (txtID.Text.Trim() != "" || txtDescricao.Text.Trim() != "")
            {
                if (MessageBox.Show("Confirma a exclusão do grupo?", "Exclusão tabela Grupo de Usuários", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)
                {
                    if (RegrasCadastro.ExcluirGrupos(txtID.Text).Equals(true))
                    {
                        using (frmOcorrencia ocorrencia = new frmOcorrencia())
                        {
                            ocorrencia.ShowDialog();
                        }
                        var excluirGrupo = new GrupoDeUsuario
                        {
                            GrupoUsuId = Convert.ToInt32(txtID.Text),
                        };
                        if (excluirGrupo.ExcluirGrupoDeUsuario(excluirGrupo).Equals(1))
                        {
                            Funcoes.GravaLogExclusao("GRUPO_USU_ID", txtID.Text, Principal.usuario, Convert.ToDateTime(Principal.data), "GRUPO_USUARIOS", txtID.Text, Principal.motivo, Principal.estAtual);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtGrupos.Clear();
                            dgGrupos.DataSource = dtGrupos;
                            emGrade = false;
                            LimparFicha();
                            tcGrupos.SelectedTab = tpGrade;
                            tslRegistros.Text = "";
                            Funcoes.LimpaFormularios(this);
                            cmbLiberado.SelectedIndex = 0;
                            txtBID.Focus();
                            return true;
                        }

                    }
                    return false;
                }
                else
                    return false;
            }
            else
            {
                MessageBox.Show("Campos em branco, sem registro para ser excluído.", "Grupo de Usuários", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
        }

        public bool Incluir()
        {
            try
            {
                Cursor = Cursors.WaitCursor;
                DataTable registroGrupoDeUsuario = new DataTable();
                if (ConsisteCampos().Equals(true))
                {
                    txtID.Text = Funcoes.IdentificaVerificaID("GRUPO_USUARIOS", "GRUPO_USU_ID").ToString();
                    chkLiberado.Checked = true;

                    var gUsuarios = new GrupoDeUsuario
                    {
                        GrupoUsuId = int.Parse(txtID.Text),
                        Descricao = txtDescricao.Text.Trim().ToUpper(),
                        Administrador = chkAdmin.Checked == true ? "S" : "N",
                        Liberado = chkLiberado.Checked == true ? "S" : "N",
                        DtCadastro = DateTime.Now,
                        OpCadastro = Principal.usuario,
                        DtAlteracao = DateTime.Now,
                        OpAlteracao = Principal.usuario,
                    };


                    registroGrupoDeUsuario = gUsuarios.IdentificaGrupoDeUsuarioCadastradoPorId(gUsuarios);
                    if (registroGrupoDeUsuario.Rows.Count == 0)
                    {
                        Principal.dtPesq = Util.RegistrosPorEstabelecimento("GRUPO_USUARIOS", "DESCRICAO", txtDescricao.Text.ToUpper(), false, true);
                        if (!String.IsNullOrEmpty(gUsuarios.IdentificaGrupoDeUsuarioCadastradoPorDescricao(gUsuarios)))
                        {
                            Cursor = Cursors.Default;
                            Principal.mensagem = "Grupo de Usuário já cadastrado.";
                            Funcoes.Avisa();
                            txtDescricao.Text = "";
                            txtDescricao.Focus();
                            return false;
                        }

                        if (gUsuarios.InsereRegistroGrupoUsuario(gUsuarios).Equals(true))
                        {
                            var permissoes = new Permissao();
                            //dtPermissoes = permissoes.BuscaPermissoesPorGrupoDeUsuario(Convert.ToInt32(txtID.Text));

                            DataTable dtModulos = new DataTable();
                            dtModulos = Util.SelecionaRegistrosTodosOuEspecifico("MODULO_MENU", "MODULO_ID");
                            for (int i = 0; i < dtModulos.Rows.Count; i++)
                            {
                                permissoes.PermissoesID = Convert.ToInt32(Funcoes.IdentificaVerificaID("PERMISSOES", "PERMISSOES_ID"));
                                permissoes.GrupoID = int.Parse(txtID.Text);
                                permissoes.ModuloID = int.Parse(dtModulos.Rows[i]["MODULO_ID"].ToString());
                                if (chkAdmin.Checked == true)
                                {
                                    permissoes.Acessa = 'S';
                                    permissoes.Inclui = 'S';
                                    permissoes.Altera = 'S';
                                    permissoes.Exclui = 'S';
                                }
                                else
                                {
                                    permissoes.Acessa = 'N';
                                    permissoes.Inclui = 'N';
                                    permissoes.Altera = 'N';
                                    permissoes.Exclui = 'N';
                                }
                                permissoes.DtCadastro = DateTime.Now;
                                permissoes.OpCadastro = Principal.usuario;

                                if (permissoes.InsereRegistroPermissao(permissoes).Equals(false))
                                {
                                    return false;
                                }
                            }

                            Funcoes.GravaLogInclusao("GRUPO_USU_ID", txtID.Text, Principal.usuario, "GRUPO_USUARIOS", txtDescricao.Text.ToUpper(), Principal.estAtual, Principal.empAtual);
                            Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                            dtGrupos.Clear();
                            dgGrupos.DataSource = dtGrupos;
                            emGrade = false;
                            tslRegistros.Text = "";
                            Limpar();
                            return true;
                        }
                        else
                        {
                            txtDescricao.Focus();
                            return false;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void ImprimirRelatorio()
        {
        }

        private void dgGrupos_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (e.RowIndex != -1)
            {
                Cursor = Cursors.WaitCursor;
                contRegistros = e.RowIndex;
                txtDescricao.Focus();
                emGrade = true;
                tcGrupos.SelectedTab = tpFicha;
                Cursor = Cursors.Default;
            }
        }

        private void tcGrupos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (tcGrupos.SelectedTab == tpGrade)
                {
                    dgGrupos.Focus();
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupos, contRegistros));
                }
                else
                    if (tcGrupos.SelectedTab == tpFicha)
                {
                    Funcoes.BotoesCadastro("frmGrupos");
                    if (emGrade == true)
                    {
                        CarregarDados(contRegistros);
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    }
                    else
                    {
                        Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupos, contRegistros));
                        Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                        Principal.mdiPrincipal.btnExcluir.Enabled = false;
                        chkLiberado.Checked = false;
                        txtID.Focus();
                        emGrade = false;
                    }
                }
                else
                        if (tcGrupos.SelectedTab == tpPermissoes)
                {
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnIncluir.Enabled = false;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    if (dgGrupos.RowCount != 0 && txtID.Text != "")
                    {
                        BuscaPermissoes();
                    }

                    dgPermissoes.Focus();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }


        public bool BuscaPermissoes()
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                var buscaPermissoes = new Permissao
                {
                    GrupoID = Convert.ToInt32(dgGrupos.Rows[contRegistros].Cells[0].Value)
                };
                
                //GERAL//
                dgPermissoes.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MGeral','tsmSuporte','tsmSuporte'");

                //CADASTRO//
                dgCadastro.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MCadastro', 'tsmFarmaciaPopular','tsmCVendas'");

                //CAIXA//
                dgCaixa.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MCaixa','tsmCRelatorios'");

                //CONTAS A PAGAR//
                dgContasAPagar.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MContPagar','tsmFinRelatorio'");

                //CONTAS A RECEBER//
                dgContasAReceber.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MContReceber','tsmRecRelatorio'");

                //ESTOQUE//
                dgEstoque.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MEstoque','tsmERelatorios'");

                //SNGPC//
                dgSngpc.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MSngpc','tsmInventario','tsmRelatoriosSNGPC'");

                //VENDAS//
                dgVendas.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MVendas','tsmVenRelatorios'");

                //RELATORIOS//
                dgRelatorios.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MRelatorios', 'tsmRelEntregasAberto'");

                tpGeral.Focus();
                dgPermissoes.Focus();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }


        private void txtBCodigo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Funcoes.SomenteNumeros(e);
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBID.Text.Trim()))
                {
                    txtBDescr.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void txtBDescr_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtBDescr.Text.Trim()))
                {
                    cmbLiberado.Focus();
                }
                else
                    btnBuscar.PerformClick();
            }
        }

        private void dgGrupos_KeyDown(object sender, KeyEventArgs e)
        {
            int indice = dtGrupos.Rows.Count - 1;
            //SETA PARA BAIXO//
            if (e.KeyCode == Keys.Down && dtGrupos.Rows.Count != 1 && contRegistros < indice)
            {
                contRegistros = contRegistros + 1;
                CarregarDados(contRegistros);
            }
            else
                //SETA PARA CIMA//
                if (e.KeyCode == Keys.Up && dtGrupos.Rows.Count != 1 && contRegistros > 0)
            {
                contRegistros = contRegistros - 1;
                CarregarDados(contRegistros);
            }
        }

        private void dgGrupos_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            contRegistros = e.RowIndex;
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupos, contRegistros));
            emGrade = true;
        }

        private void tsmExcel_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgGrupos.Rows.Count != 0)
                {
                    Cursor = Cursors.WaitCursor;
                    tslRegistros.Text = "Exportando para Excel. Aguarde...";

                    Excel.Application arquivoExcel = null;
                    Excel.Workbook arquivo = null;
                    Excel.Worksheet planilha = null;
                    Object oMissing = System.Reflection.Missing.Value;

                    arquivoExcel = new Excel.Application();

                    arquivoExcel.Visible = true;

                    arquivoExcel.UserControl = true;

                    arquivo = arquivoExcel.Workbooks.Add(oMissing);

                    planilha = (Excel.Worksheet)arquivo.Worksheets.get_Item(1);
                    int i = 0;
                    int j = 0;
                    int k = 0;
                    int x = 0;

                    List<DataGridViewColumn> colunas = Funcoes.OrdenaColunas(dgGrupos);

                    //INSERE AS COLUNAS NA PASTA DO EXCEL//
                    for (k = 0; k < colunas.Count; k++)
                    {
                        if (colunas[k].Visible == true)
                        {
                            planilha.Cells[1, x + 1] = colunas[k].HeaderText;
                            x = x + 1;
                        }
                    }

                    k = 1;
                    Principal.mdiPrincipal.pbMenu.Value = 0;
                    Principal.mdiPrincipal.pbMenu.Maximum = dgGrupos.RowCount;
                    for (i = 0; i <= dgGrupos.RowCount - 1; i++)
                    {
                        x = 0;
                        for (j = 0; j <= colunas.Count - 1; j++)
                        {
                            if (colunas[j].Visible == true)
                            {
                                DataGridViewCell coluna = dgGrupos[colunas[j].Index, i];
                                planilha.Cells[k + 1, x + 1] = coluna.Value;
                                x = x + 1;
                            }
                        }
                        k = k + 1;
                        Application.DoEvents();
                        Principal.mdiPrincipal.pbMenu.Value = i;
                    }

                    Excel.Range celulas;
                    string numCel;
                    int indice = 0;
                    for (k = 0; k < dgGrupos.ColumnCount; k++)
                    {
                        if (dgGrupos.Columns[k].Visible == true)
                        {
                            switch (dgGrupos.Columns[k].HeaderText)
                            {
                                case "Data Alteração":
                                    numCel = Principal.GetColunaExcel(dgGrupos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgGrupos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                                case "Data do Cadastro":
                                    numCel = Principal.GetColunaExcel(dgGrupos.Columns[indice + 1].Index);
                                    celulas = planilha.get_Range(numCel + "2", numCel + Convert.ToString(dgGrupos.RowCount + 1));
                                    celulas.NumberFormat = "dd/MM/yyyy HH:mm:ss";
                                    break;
                            }
                            indice += 1;
                        }
                    }

                    //BORDA NA PLANILHA//
                    celulas = planilha.get_Range("A1", Principal.GetColunaExcel(indice == 0 ? dgGrupos.ColumnCount : indice) + Convert.ToString(dgGrupos.RowCount + 1));
                    celulas.Borders.LineStyle = Excel.XlLineStyle.xlContinuous;

                    //FORMATA TAMANHO DA COLUNA//
                    planilha.Columns.AutoFit();

                    arquivo = null;

                    arquivoExcel = null;

                    GC.Collect();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Exportar", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                tslRegistros.Text = "";
                Principal.mdiPrincipal.pbMenu.Value = 0;
                Cursor = Cursors.Default;
            }
        }

        private void txtID_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtDescricao.Focus();
            }
        }

        private void txtDescricao_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkAdmin.Focus();
            }
        }

        private void chkAdmin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                chkLiberado.Focus();
            }
        }

        public void BotoesHabilitados()
        {
            Funcoes.BotoesCadastro("frmGrupos");
            Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupos, contRegistros));
            if (tcGrupos.SelectedTab == tpGrade)
            {
                Principal.mdiPrincipal.btnExcluir.Enabled = false;
                Principal.mdiPrincipal.btnIncluir.Enabled = false;
                Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                Principal.mdiPrincipal.btnLimpar.Enabled = true;
            }
            else if (tcGrupos.SelectedTab == tpFicha)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
            else if (tcGrupos.SelectedTab == tpPermissoes)
            {
                Principal.mdiPrincipal.btnLimpar.Enabled = true;

                if (String.IsNullOrEmpty(txtID.Text))
                {
                    Principal.mdiPrincipal.btnExcluir.Enabled = false;
                    Principal.mdiPrincipal.btnAtualiza.Enabled = false;
                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = true;

                }
                else
                {
                    if (Principal.mdiPrincipal.btnExcluir.Enabled)
                        Principal.mdiPrincipal.btnExcluir.Enabled = true;

                    if (Principal.mdiPrincipal.btnAtualiza.Enabled)
                        Principal.mdiPrincipal.btnAtualiza.Enabled = true;

                    if (Principal.mdiPrincipal.btnIncluir.Enabled)
                        Principal.mdiPrincipal.btnIncluir.Enabled = false;
                }
            }
        }

        public void Botao()
        {
            this.tsbGrupos.AutoSize = false;
            this.tsbGrupos.Image = Properties.Resources.grupo;
            this.tsbGrupos.Size = new System.Drawing.Size(150, 20);
            this.tsbGrupos.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            Principal.mdiPrincipal.stsBotoes.Items.Add(tsbGrupos);
            Principal.mdiPrincipal.stsBotoes.Items.Add(tssGrupos);
            tsbGrupos.Click += delegate
            {
                var cadGrupos = Application.OpenForms.OfType<frmGrupos>().FirstOrDefault();
                BotoesHabilitados();
                cadGrupos.Focus();
            };
        }

        public void FormularioFoco()
        {
            BotoesHabilitados();
        }

        public void Sair()
        {
            dtGrupos.Dispose();
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tsbGrupos);
            Principal.mdiPrincipal.stsBotoes.Items.Remove(tssGrupos);

        }

        private void cmbLiberado_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnBuscar.PerformClick();
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                var gUsuarios = new GrupoDeUsuario
                {
                    GrupoUsuId = txtBID.Text.Trim() == "" ? 0 : int.Parse(txtBID.Text.Trim()),
                    Descricao = txtBDescr.Text.Trim().ToUpper(),
                    Liberado = cmbLiberado.Text
                };

                dtGrupos = gUsuarios.BuscaGrupoDeUsuarios(gUsuarios, out strOrdem);
                if (dtGrupos.Rows.Count > 0)
                {
                    tslRegistros.Text = "Registros encontrados: " + dtGrupos.Rows.Count;
                    dgGrupos.DataSource = dtGrupos;
                    Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupos, 0));
                    dgGrupos.Focus();
                    contRegistros = 0;
                    cmbLiberado.SelectedIndex = 0;
                    emGrade = true;
                }
                else
                {
                    MessageBox.Show("Nenhum registro encontrado.", "Grupos", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgGrupos.DataSource = dtGrupos;
                    Principal.mdiPrincipal.HabilitaBotoes("NNNN");
                    txtBID.Focus();
                    tslRegistros.Text = "";
                    Funcoes.LimpaFormularios(this);
                    emGrade = false;
                    cmbLiberado.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgPermissoes_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";
                if (atualizaPermissao.AtualizaPermissoes(tipo, dgPermissoes.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper(), 
                    Convert.ToInt32(dgPermissoes.Rows[e.RowIndex].Cells[6].Value), Convert.ToInt32(dgPermissoes.Rows[e.RowIndex].Cells[0].Value)).Equals(true))
                {
                    if (tipo.Equals("ACESSA"))
                    {
                        atualizaPermissao.AtualizaPermissoesGeral(Convert.ToInt32(dgPermissoes.Rows[e.RowIndex].Cells[6].Value),
                            Convert.ToInt32(dgPermissoes.Rows[e.RowIndex].Cells[0].Value));

                        if (dgPermissoes.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("S"))
                        {
                            //Cadastra S em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgPermissoes.Rows[e.RowIndex].Cells[i].Value = 'S';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgPermissoes.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgPermissoes.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                        else
                        {
                            //Cadastra N em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgPermissoes.Rows[e.RowIndex].Cells[i].Value = 'N';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgPermissoes.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgPermissoes.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                    }
                    else
                    {
                        atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgPermissoes.Rows[e.RowIndex].Cells[7].Value), Principal.usuario,
                            tipo, dgPermissoes.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper());
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Permissões", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgGrupos_ColumnHeaderMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dgGrupos.Rows.Count > 0)
            {
                Cursor = Cursors.WaitCursor;
                if (decrescente == false)
                {
                    if (dgGrupos.Columns[e.ColumnIndex].Name == "ADM")
                    {
                        dtGrupos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY ADMINISTRADOR DESC");
                    }
                    else
                        if (dgGrupos.Columns[e.ColumnIndex].Name == "LIB")
                    {
                        dtGrupos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY LIBERADO DESC");
                    }
                    else
                    {
                        dtGrupos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgGrupos.Columns[e.ColumnIndex].Name + " DESC");
                    }
                    dgGrupos.DataSource = dtGrupos;
                    decrescente = true;
                }
                else
                {
                    if (dgGrupos.Columns[e.ColumnIndex].Name == "ADM")
                    {
                        dtGrupos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY ADMINISTRADOR ASC");
                    }
                    else
                        if (dgGrupos.Columns[e.ColumnIndex].Name == "LIB")
                    {
                        dtGrupos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY LIBERADO ASC");
                    }
                    else
                    {
                        dtGrupos = BancoDados.selecionarRegistros(strOrdem + " ORDER BY " + dgGrupos.Columns[e.ColumnIndex].Name + " ASC");
                    }

                    dgGrupos.DataSource = dtGrupos;
                    decrescente = false;
                }

                contRegistros = 0;
                Principal.mdiPrincipal.HabilitaBotoes(Funcoes.BotoesNavegacao(dtGrupos, contRegistros));
            }
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox3, "Campo Obrigatório");
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(this.pictureBox1, "Campo Obrigatório");
        }

        private void dgGrupos_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            if (dgGrupos.Rows[e.RowIndex].Cells[3].Value.ToString() == "N")
            {
                dgGrupos.Rows[e.RowIndex].DefaultCellStyle.ForeColor = Color.Red;
                dgGrupos.Rows[e.RowIndex].DefaultCellStyle.SelectionForeColor = Color.Red;
            }
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control.Name != "txtID") && (control.Name != "txtData") && (control.Name != "txtUsuario"))
                {
                    if ((control is TextBox) ||
                    (control is RichTextBox) ||
                    (control is MaskedTextBox))
                    {
                        control.Enter += new EventHandler(controlFocus_Enter);
                        control.Leave += new EventHandler(controlFocus_Leave);
                    }

                    RegisterFocusEvents(control.Controls);
                }
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

        private void dgPermissoes_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";

            if (tipo == "ACESSA")
            {
                if (Convert.ToString(dgPermissoes.Rows[e.RowIndex].Cells[2].Value) != "S" && Convert.ToString(dgPermissoes.Rows[e.RowIndex].Cells[2].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgPermissoes.Rows[e.RowIndex].Cells[2].Value = "N";
                }
            }
            else if (tipo == "INCLUI")
            {
                if (Convert.ToString(dgPermissoes.Rows[e.RowIndex].Cells[3].Value) != "S" && Convert.ToString(dgPermissoes.Rows[e.RowIndex].Cells[3].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgPermissoes.Rows[e.RowIndex].Cells[3].Value = "N";
                }
            }
            else if (tipo == "ALTERA")
            {
                if (Convert.ToString(dgPermissoes.Rows[e.RowIndex].Cells[4].Value) != "S" && Convert.ToString(dgPermissoes.Rows[e.RowIndex].Cells[4].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgPermissoes.Rows[e.RowIndex].Cells[4].Value = "N";
                }
            }
            else if (tipo == "EXCLUI")
            {
                if (Convert.ToString(dgPermissoes.Rows[e.RowIndex].Cells[5].Value) != "S" && Convert.ToString(dgPermissoes.Rows[e.RowIndex].Cells[5].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgPermissoes.Rows[e.RowIndex].Cells[5].Value = "N";
                }
            }
        }

        private void tsmConfigurar_Click(object sender, EventArgs e)
        {
            try
            {
                int colunas = 1;

                for (int i = 0; i < dgGrupos.ColumnCount; i++)
                {
                    if (dgGrupos.Columns[i].Visible == true)
                    {
                        colunas = colunas + 1;
                    }
                }
                string[] grid = new string[dgGrupos.ColumnCount];
                string[] coluna = new string[dgGrupos.ColumnCount];

                for (int i = 0; i < dgGrupos.ColumnCount; i++)
                {
                    grid[i] = dgGrupos.Columns[i].HeaderText;
                }

                for (int i = 0; i < dgGrupos.ColumnCount; i++)
                {
                    if (dgGrupos.Columns[i].Visible == true)
                    {
                        coluna[i] = "True";
                    }
                    else
                    {
                        coluna[i] = "False";
                    }
                }
                using (frmConfGrade grade = new frmConfGrade(grid, dgGrupos.ColumnCount, coluna))
                {
                    grade.ShowDialog();
                }

                if (Principal.matriz[0] != null)
                {
                    if (Principal.matriz[0] != "Desmarcar")
                    {
                        for (int i = 0; i < dgGrupos.ColumnCount; i++)
                        {
                            bool valida = false;
                            int x = 0;
                            while (valida == false)
                            {
                                if (dgGrupos.Columns[i].HeaderText == Principal.matriz[x])
                                {
                                    dgGrupos.Columns[i].Visible = true;
                                    valida = true;
                                }
                                else
                                {
                                    dgGrupos.Columns[i].Visible = false;
                                    valida = false;
                                }
                                x = x + 1;
                                if (x == dgGrupos.ColumnCount)
                                {
                                    valida = true;
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < dgGrupos.ColumnCount; i++)
                        {
                            dgGrupos.Columns[i].Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void AtalhoGrade()
        {
            tcGrupos.SelectedTab = tpGrade;
        }

        public void AtalhoFicha()
        {
            tcGrupos.SelectedTab = tpFicha;
        }


        public void Ajuda()
        {
            throw new NotImplementedException();
        }

        private void dgCadastro_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";
                if (atualizaPermissao.AtualizaPermissoes(tipo, dgCadastro.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper(),
                    Convert.ToInt32(dgCadastro.Rows[e.RowIndex].Cells[6].Value), Convert.ToInt32(dgCadastro.Rows[e.RowIndex].Cells[0].Value)).Equals(true))
                {
                    if (tipo.Equals("ACESSA"))
                    {
                        atualizaPermissao.AtualizaPermissoesGeral(Convert.ToInt32(dgCadastro.Rows[e.RowIndex].Cells[6].Value),
                            Convert.ToInt32(dgCadastro.Rows[e.RowIndex].Cells[0].Value));

                        if (dgCadastro.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("S"))
                        {
                            //Cadastra S em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgCadastro.Rows[e.RowIndex].Cells[i].Value = 'S';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgCadastro.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgCadastro.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                        else
                        {
                            //Cadastra N em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgCadastro.Rows[e.RowIndex].Cells[i].Value = 'N';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgCadastro.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgCadastro.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                    }
                    else
                    {
                        atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgCadastro.Rows[e.RowIndex].Cells[7].Value), Principal.usuario,
                            tipo, dgCadastro.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper());
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Permissões", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgCaixa_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";
                if (atualizaPermissao.AtualizaPermissoes(tipo, dgCaixa.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper(),
                    Convert.ToInt32(dgCaixa.Rows[e.RowIndex].Cells[6].Value), Convert.ToInt32(dgCaixa.Rows[e.RowIndex].Cells[0].Value)).Equals(true))
                {
                    if (tipo.Equals("ACESSA"))
                    {
                        atualizaPermissao.AtualizaPermissoesGeral(Convert.ToInt32(dgCaixa.Rows[e.RowIndex].Cells[6].Value),
                            Convert.ToInt32(dgCaixa.Rows[e.RowIndex].Cells[0].Value));

                        if (dgCaixa.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("S"))
                        {
                            //Cadastra S em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgCaixa.Rows[e.RowIndex].Cells[i].Value = 'S';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgCaixa.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgCaixa.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                        else
                        {
                            //Cadastra N em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgCaixa.Rows[e.RowIndex].Cells[i].Value = 'N';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgCaixa.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgCaixa.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                    }
                    else
                    {
                        atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgCaixa.Rows[e.RowIndex].Cells[7].Value), Principal.usuario,
                            tipo, dgCaixa.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper());
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Permissões", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgContasAPagar_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";
                if (atualizaPermissao.AtualizaPermissoes(tipo, dgContasAPagar.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper(), 
                    Convert.ToInt32(dgContasAPagar.Rows[e.RowIndex].Cells[6].Value), Convert.ToInt32(dgContasAPagar.Rows[e.RowIndex].Cells[0].Value)).Equals(true))
                {
                    if (tipo.Equals("ACESSA"))
                    {
                        atualizaPermissao.AtualizaPermissoesGeral(Convert.ToInt32(dgContasAPagar.Rows[e.RowIndex].Cells[6].Value),
                            Convert.ToInt32(dgContasAPagar.Rows[e.RowIndex].Cells[0].Value));

                        if (dgContasAPagar.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("S"))
                        {
                            //Cadastra S em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgContasAPagar.Rows[e.RowIndex].Cells[i].Value = 'S';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgContasAPagar.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgContasAPagar.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                        else
                        {
                            //Cadastra N em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgContasAPagar.Rows[e.RowIndex].Cells[i].Value = 'N';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgContasAPagar.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgContasAPagar.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                    }
                    else
                    {
                        atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgContasAPagar.Rows[e.RowIndex].Cells[7].Value), Principal.usuario,
                            tipo, dgContasAPagar.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper());
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Permissões", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgContasAReceber_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";
                if (atualizaPermissao.AtualizaPermissoes(tipo, dgContasAReceber.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper(), 
                    Convert.ToInt32(dgContasAReceber.Rows[e.RowIndex].Cells[6].Value), Convert.ToInt32(dgContasAReceber.Rows[e.RowIndex].Cells[0].Value)).Equals(true))
                {
                    if (tipo.Equals("ACESSA"))
                    {
                        atualizaPermissao.AtualizaPermissoesGeral(Convert.ToInt32(dgContasAReceber.Rows[e.RowIndex].Cells[6].Value),
                            Convert.ToInt32(dgContasAReceber.Rows[e.RowIndex].Cells[0].Value));

                        if (dgContasAReceber.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("S"))
                        {
                            //Cadastra S em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgContasAReceber.Rows[e.RowIndex].Cells[i].Value = 'S';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgContasAReceber.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgContasAReceber.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                        else
                        {
                            //Cadastra N em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgContasAReceber.Rows[e.RowIndex].Cells[i].Value = 'N';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgContasAReceber.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgContasAReceber.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                    }
                    else
                    {
                        atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgContasAReceber.Rows[e.RowIndex].Cells[7].Value), Principal.usuario,
                            tipo, dgContasAReceber.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper());
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Permissões", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgEstoque_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";
                if (atualizaPermissao.AtualizaPermissoes(tipo, dgEstoque.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper(), 
                    Convert.ToInt32(dgEstoque.Rows[e.RowIndex].Cells[6].Value), Convert.ToInt32(dgEstoque.Rows[e.RowIndex].Cells[0].Value)).Equals(true))
                {
                    if (tipo.Equals("ACESSA"))
                    {
                        atualizaPermissao.AtualizaPermissoesGeral(Convert.ToInt32(dgEstoque.Rows[e.RowIndex].Cells[6].Value),
                            Convert.ToInt32(dgEstoque.Rows[e.RowIndex].Cells[0].Value));

                        if (dgEstoque.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("S"))
                        {
                            //Cadastra S em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgEstoque.Rows[e.RowIndex].Cells[i].Value = 'S';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgEstoque.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgEstoque.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                        else
                        {
                            //Cadastra N em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgEstoque.Rows[e.RowIndex].Cells[i].Value = 'N';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgEstoque.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgEstoque.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                    }
                    else
                    {
                        atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgEstoque.Rows[e.RowIndex].Cells[7].Value), Principal.usuario,
                            tipo, dgEstoque.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper());
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Permissões", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgSngpc_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";
                if (atualizaPermissao.AtualizaPermissoes(tipo, dgSngpc.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper(), 
                    Convert.ToInt32(dgSngpc.Rows[e.RowIndex].Cells[6].Value), Convert.ToInt32(dgSngpc.Rows[e.RowIndex].Cells[0].Value)).Equals(true))
                {
                    if (tipo.Equals("ACESSA"))
                    {
                        atualizaPermissao.AtualizaPermissoesGeral(Convert.ToInt32(dgSngpc.Rows[e.RowIndex].Cells[6].Value),
                            Convert.ToInt32(dgSngpc.Rows[e.RowIndex].Cells[0].Value));

                        if (dgSngpc.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("S"))
                        {
                            //Cadastra S em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgSngpc.Rows[e.RowIndex].Cells[i].Value = 'S';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgSngpc.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgSngpc.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                        else
                        {
                            //Cadastra N em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgSngpc.Rows[e.RowIndex].Cells[i].Value = 'N';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgSngpc.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgSngpc.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                    }
                    else
                    {
                        atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgSngpc.Rows[e.RowIndex].Cells[7].Value), Principal.usuario,
                            tipo, dgSngpc.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper());
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Permissões", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgVendas_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";
                if (atualizaPermissao.AtualizaPermissoes(tipo, dgVendas.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper(),
                    Convert.ToInt32(dgVendas.Rows[e.RowIndex].Cells[6].Value), Convert.ToInt32(dgVendas.Rows[e.RowIndex].Cells[0].Value)).Equals(true))
                {
                    if (tipo.Equals("ACESSA"))
                    {
                        atualizaPermissao.AtualizaPermissoesGeral(Convert.ToInt32(dgVendas.Rows[e.RowIndex].Cells[6].Value),
                            Convert.ToInt32(dgVendas.Rows[e.RowIndex].Cells[0].Value));

                        if (dgVendas.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("S"))
                        {
                            //Cadastra S em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgVendas.Rows[e.RowIndex].Cells[i].Value = 'S';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgVendas.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgVendas.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                        else
                        {
                            //Cadastra N em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgVendas.Rows[e.RowIndex].Cells[i].Value = 'N';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgVendas.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgVendas.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                    }
                    else
                    {
                        atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgVendas.Rows[e.RowIndex].Cells[7].Value), Principal.usuario,
                            tipo, dgVendas.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper());
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Permissões", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgRelatorios_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";
                if (atualizaPermissao.AtualizaPermissoes(tipo, dgRelatorios.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper(),
                    Convert.ToInt32(dgRelatorios.Rows[e.RowIndex].Cells[6].Value), Convert.ToInt32(dgRelatorios.Rows[e.RowIndex].Cells[0].Value)).Equals(true))
                {
                    if (tipo.Equals("ACESSA"))
                    {
                        atualizaPermissao.AtualizaPermissoesGeral(Convert.ToInt32(dgRelatorios.Rows[e.RowIndex].Cells[6].Value),
                            Convert.ToInt32(dgRelatorios.Rows[e.RowIndex].Cells[0].Value));

                        if (dgRelatorios.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().Equals("S"))
                        {
                            //Cadastra S em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgRelatorios.Rows[e.RowIndex].Cells[i].Value = 'S';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgRelatorios.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgRelatorios.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                        else
                        {
                            //Cadastra N em todas as Colunas
                            for (int i = 2; i < 6; i++)
                            {
                                dgRelatorios.Rows[e.RowIndex].Cells[i].Value = 'N';

                                atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgRelatorios.Rows[e.RowIndex].Cells[7].Value), Principal.usuario, tipo, dgRelatorios.Rows[e.RowIndex].Cells[i].Value.ToString().ToUpper());
                            }
                        }
                    }
                    else
                    {
                        atualizaPermissao.GravaLogPermissoes(Convert.ToInt32(dgRelatorios.Rows[e.RowIndex].Cells[7].Value), Principal.usuario,
                            tipo, dgRelatorios.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString().ToUpper());
                    }
                }
            }

            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Permissões", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgCadastro_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";

            if (tipo == "ACESSA")
            {
                if (Convert.ToString(dgCadastro.Rows[e.RowIndex].Cells[2].Value) != "S" && Convert.ToString(dgCadastro.Rows[e.RowIndex].Cells[2].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgCadastro.Rows[e.RowIndex].Cells[2].Value = "N";
                }
            }
            else if (tipo == "INCLUI")
            {
                if (Convert.ToString(dgCadastro.Rows[e.RowIndex].Cells[3].Value) != "S" && Convert.ToString(dgCadastro.Rows[e.RowIndex].Cells[3].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgCadastro.Rows[e.RowIndex].Cells[3].Value = "N";
                }
            }
            else if (tipo == "ALTERA")
            {
                if (Convert.ToString(dgCadastro.Rows[e.RowIndex].Cells[4].Value) != "S" && Convert.ToString(dgCadastro.Rows[e.RowIndex].Cells[4].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgCadastro.Rows[e.RowIndex].Cells[4].Value = "N";
                }
            }
            else if (tipo == "EXCLUI")
            {
                if (Convert.ToString(dgCadastro.Rows[e.RowIndex].Cells[5].Value) != "S" && Convert.ToString(dgCadastro.Rows[e.RowIndex].Cells[5].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgCadastro.Rows[e.RowIndex].Cells[5].Value = "N";
                }
            }
        }

        private void dgCaixa_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";

            if (tipo == "ACESSA")
            {
                if (Convert.ToString(dgCaixa.Rows[e.RowIndex].Cells[2].Value) != "S" && Convert.ToString(dgCaixa.Rows[e.RowIndex].Cells[2].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgCaixa.Rows[e.RowIndex].Cells[2].Value = "N";
                }
            }
            else if (tipo == "INCLUI")
            {
                if (Convert.ToString(dgCaixa.Rows[e.RowIndex].Cells[3].Value) != "S" && Convert.ToString(dgCaixa.Rows[e.RowIndex].Cells[3].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgCaixa.Rows[e.RowIndex].Cells[3].Value = "N";
                }
            }
            else if (tipo == "ALTERA")
            {
                if (Convert.ToString(dgCaixa.Rows[e.RowIndex].Cells[4].Value) != "S" && Convert.ToString(dgCaixa.Rows[e.RowIndex].Cells[4].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    tipo = "N";
                }
            }
            else if (tipo == "EXCLUI")
            {
                if (Convert.ToString(dgCaixa.Rows[e.RowIndex].Cells[5].Value) != "S" && Convert.ToString(dgCaixa.Rows[e.RowIndex].Cells[5].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgCaixa.Rows[e.RowIndex].Cells[5].Value = "N";
                }
            }
        }

        private void dgContasAPagar_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";

            if (tipo == "ACESSA")
            {
                if (Convert.ToString(dgContasAPagar.Rows[e.RowIndex].Cells[2].Value) != "S" && Convert.ToString(dgContasAPagar.Rows[e.RowIndex].Cells[2].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgContasAPagar.Rows[e.RowIndex].Cells[2].Value = "N";
                }
            }
            else if (tipo == "INCLUI")
            {
                if (Convert.ToString(dgContasAPagar.Rows[e.RowIndex].Cells[3].Value) != "S" && Convert.ToString(dgContasAPagar.Rows[e.RowIndex].Cells[3].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgContasAPagar.Rows[e.RowIndex].Cells[3].Value = "N";
                }
            }
            else if (tipo == "ALTERA")
            {
                if (Convert.ToString(dgContasAPagar.Rows[e.RowIndex].Cells[4].Value) != "S" && Convert.ToString(dgContasAPagar.Rows[e.RowIndex].Cells[4].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgContasAPagar.Rows[e.RowIndex].Cells[4].Value = "N";
                }
            }
            else if (tipo == "EXCLUI")
            {
                if (Convert.ToString(dgContasAPagar.Rows[e.RowIndex].Cells[5].Value) != "S" && Convert.ToString(dgContasAPagar.Rows[e.RowIndex].Cells[5].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgContasAPagar.Rows[e.RowIndex].Cells[5].Value = "N";
                }
            }
        }

        private void dgContasAReceber_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";

            if (tipo == "ACESSA")
            {
                if (Convert.ToString(dgContasAReceber.Rows[e.RowIndex].Cells[2].Value) != "S" && Convert.ToString(dgContasAReceber.Rows[e.RowIndex].Cells[2].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgContasAReceber.Rows[e.RowIndex].Cells[2].Value = "N";
                }
            }
            else if (tipo == "INCLUI")
            {
                if (Convert.ToString(dgContasAReceber.Rows[e.RowIndex].Cells[3].Value) != "S" && Convert.ToString(dgContasAReceber.Rows[e.RowIndex].Cells[3].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgContasAReceber.Rows[e.RowIndex].Cells[3].Value = "N";
                }
            }
            else if (tipo == "ALTERA")
            {
                if (Convert.ToString(dgContasAReceber.Rows[e.RowIndex].Cells[4].Value) != "S" && Convert.ToString(dgContasAReceber.Rows[e.RowIndex].Cells[4].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgContasAReceber.Rows[e.RowIndex].Cells[4].Value = "N";
                }
            }
            else if (tipo == "EXCLUI")
            {
                if (Convert.ToString(dgContasAReceber.Rows[e.RowIndex].Cells[5].Value) != "S" && Convert.ToString(dgContasAReceber.Rows[e.RowIndex].Cells[5].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgContasAReceber.Rows[e.RowIndex].Cells[5].Value = "N";
                }
            }
        }

        private void dgEstoque_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";

            if (tipo == "ACESSA")
            {
                if (Convert.ToString(dgEstoque.Rows[e.RowIndex].Cells[2].Value) != "S" && Convert.ToString(dgEstoque.Rows[e.RowIndex].Cells[2].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgEstoque.Rows[e.RowIndex].Cells[2].Value = "N";
                }
            }
            else if (tipo == "INCLUI")
            {
                if (Convert.ToString(dgEstoque.Rows[e.RowIndex].Cells[3].Value) != "S" && Convert.ToString(dgEstoque.Rows[e.RowIndex].Cells[3].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgEstoque.Rows[e.RowIndex].Cells[3].Value = "N";
                }
            }
            else if (tipo == "ALTERA")
            {
                if (Convert.ToString(dgEstoque.Rows[e.RowIndex].Cells[4].Value) != "S" && Convert.ToString(dgEstoque.Rows[e.RowIndex].Cells[4].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgEstoque.Rows[e.RowIndex].Cells[4].Value = "N";
                }
            }
            else if (tipo == "EXCLUI")
            {
                if (Convert.ToString(dgEstoque.Rows[e.RowIndex].Cells[5].Value) != "S" && Convert.ToString(dgEstoque.Rows[e.RowIndex].Cells[5].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgEstoque.Rows[e.RowIndex].Cells[5].Value = "N";
                }
            }
        }

        private void dgSngpc_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";

            if (tipo == "ACESSA")
            {
                if (Convert.ToString(dgSngpc.Rows[e.RowIndex].Cells[2].Value) != "S" && Convert.ToString(dgSngpc.Rows[e.RowIndex].Cells[2].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgSngpc.Rows[e.RowIndex].Cells[2].Value = "N";
                }
            }
            else if (tipo == "INCLUI")
            {
                if (Convert.ToString(dgSngpc.Rows[e.RowIndex].Cells[3].Value) != "S" && Convert.ToString(dgSngpc.Rows[e.RowIndex].Cells[3].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgSngpc.Rows[e.RowIndex].Cells[3].Value = "N";
                }
            }
            else if (tipo == "ALTERA")
            {
                if (Convert.ToString(dgSngpc.Rows[e.RowIndex].Cells[4].Value) != "S" && Convert.ToString(dgSngpc.Rows[e.RowIndex].Cells[4].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgSngpc.Rows[e.RowIndex].Cells[4].Value = "N";
                }
            }
            else if (tipo == "EXCLUI")
            {
                if (Convert.ToString(dgSngpc.Rows[e.RowIndex].Cells[5].Value) != "S" && Convert.ToString(dgSngpc.Rows[e.RowIndex].Cells[5].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgSngpc.Rows[e.RowIndex].Cells[5].Value = "N";
                }
            }
        }

        private void dgVendas_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";

            if (tipo == "ACESSA")
            {
                if (Convert.ToString(dgVendas.Rows[e.RowIndex].Cells[2].Value) != "S" && Convert.ToString(dgVendas.Rows[e.RowIndex].Cells[2].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgVendas.Rows[e.RowIndex].Cells[2].Value = "N";
                }
            }
            else if (tipo == "INCLUI")
            {
                if (Convert.ToString(dgVendas.Rows[e.RowIndex].Cells[3].Value) != "S" && Convert.ToString(dgVendas.Rows[e.RowIndex].Cells[3].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgVendas.Rows[e.RowIndex].Cells[3].Value = "N";
                }
            }
            else if (tipo == "ALTERA")
            {
                if (Convert.ToString(dgVendas.Rows[e.RowIndex].Cells[4].Value) != "S" && Convert.ToString(dgVendas.Rows[e.RowIndex].Cells[4].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgVendas.Rows[e.RowIndex].Cells[4].Value = "N";
                }
            }
            else if (tipo == "EXCLUI")
            {
                if (Convert.ToString(dgVendas.Rows[e.RowIndex].Cells[5].Value) != "S" && Convert.ToString(dgVendas.Rows[e.RowIndex].Cells[5].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgVendas.Rows[e.RowIndex].Cells[5].Value = "N";
                }
            }
        }

        private void dgRelatorios_CellValidated(object sender, DataGridViewCellEventArgs e)
        {
            tipo = e.ColumnIndex == 2 ? "ACESSA" : e.ColumnIndex == 3 ? "INCLUI" : e.ColumnIndex == 4 ? "ALTERA" : "EXCLUI";

            if (tipo == "ACESSA")
            {
                if (Convert.ToString(dgRelatorios.Rows[e.RowIndex].Cells[2].Value) != "S" && Convert.ToString(dgRelatorios.Rows[e.RowIndex].Cells[2].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgRelatorios.Rows[e.RowIndex].Cells[2].Value = "N";
                }
            }
            else if (tipo == "INCLUI")
            {
                if (Convert.ToString(dgRelatorios.Rows[e.RowIndex].Cells[3].Value) != "S" && Convert.ToString(dgRelatorios.Rows[e.RowIndex].Cells[3].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgRelatorios.Rows[e.RowIndex].Cells[3].Value = "N";
                }
            }
            else if (tipo == "ALTERA")
            {
                if (Convert.ToString(dgRelatorios.Rows[e.RowIndex].Cells[4].Value) != "S" && Convert.ToString(dgRelatorios.Rows[e.RowIndex].Cells[4].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgRelatorios.Rows[e.RowIndex].Cells[4].Value = "N";
                }
            }
            else if (tipo == "EXCLUI")
            {
                if (Convert.ToString(dgRelatorios.Rows[e.RowIndex].Cells[5].Value) != "S" && Convert.ToString(dgRelatorios.Rows[e.RowIndex].Cells[5].Value) != "N")
                {
                    MessageBox.Show("Caracter Inválido!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dgRelatorios.Rows[e.RowIndex].Cells[5].Value = "N";
                }
            }
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                var buscaPermissoes = new Permissao
                {
                    GrupoID = Convert.ToInt32(dgGrupos.Rows[contRegistros].Cells[0].Value)
                };
                
                if (tabControl1.SelectedTab == tpGeral)
                {
                    //GERAL//
                    dgPermissoes.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MGeral','tsmSuporte','tsmSuporte'");
                }
                else if (tabControl1.SelectedTab == tpCadastro)
                {
                    //CADASTRO//
                    dgCadastro.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MCadastro', 'tsmFarmaciaPopular','tsmCVendas'");
                }
                else if (tabControl1.SelectedTab == tpCaixa)
                {
                    //CAIXA//
                    dgCaixa.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MCaixa','tsmCRelatorios'");
                }
                else if (tabControl1.SelectedTab == tpContasAPagar)
                {
                    //CONTAS A PAGAR//
                    dgContasAPagar.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MContPagar','tsmFinRelatorio'");
                }
                else if (tabControl1.SelectedTab == tpReceber)
                {
                    //CONTAS A RECEBER//
                    dgContasAReceber.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MContReceber','tsmRecRelatorio'");
                }
                else if (tabControl1.SelectedTab == tpEstoque)
                {
                    //ESTOQUE//
                    dgEstoque.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MEstoque','tsmERelatorios','tsmManutencaoDeFilial'");
                }
                else if (tabControl1.SelectedTab == tpSngpc)
                {
                    //SNGPC//
                    dgSngpc.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MSngpc','tsmInventario','tsmRelatoriosSNGPC'");
                }
                else if (tabControl1.SelectedTab == tpVendas)
                {
                    //VENDAS//
                    dgVendas.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MVendas','tsmVenRelatorios'");
                }
                else if (tabControl1.SelectedTab == tpRelatorio)
                {
                    //RELATORIOS//
                    dgRelatorios.DataSource = buscaPermissoes.BuscaPermissoesEModuloPorMenu(buscaPermissoes, "'MRelatorios', 'tsmRelEntregasAberto'");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Grupo de Usuários", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void dgPermissoes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
