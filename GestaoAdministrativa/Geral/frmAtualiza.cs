﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using System.Data.OleDb;
using System.Data.Odbc;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Net.Sockets;
using System.Net;
using GestaoAdministrativa.Negocio;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using GestaoAdministrativa.Classes.API;
using Microsoft.VisualBasic;
using GestaoAdministrativa.Impressao;
using GestaoAdministrativa.SAT;
using System.Net.NetworkInformation;

namespace GestaoAdministrativa
{
    public partial class frmAtualiza : Form
    {
        //DECLARAÇÃO DAS VARIÁVEIS//
        string arquivoDownload;
        private Pos pos = new Pos();
        string enderecoWebService, codEstab, grupoID;
        List<AtualizacaoGeral> dados = new List<Negocio.AtualizacaoGeral>();
        List<EstabelecimentoFilial> dadosEstabelecimento = new List<EstabelecimentoFilial>();
        List<AtualizacaoEstacao> identificaAtualizacao = new List<AtualizacaoEstacao>();
        bool bloqueio;
        //bool servidor;

        public frmAtualiza()
        {
            InitializeComponent();
        }

        private void frmAtualiza_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            //VERIFICA SE APLICAÇÃO JÁ ESTÁ EM USO//
            if (System.Diagnostics.Process.GetProcessesByName("SG Pharma").Length > 1)
            {
                MessageBox.Show("Atenção, esta aplicação já esta em uso, observe a barra de aplicações ou aguarde o processo de atualização ser finalizado.", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Exit();
            }
        }

        private async void frmAtualiza_Shown(object sender, EventArgs e)
        {
            Refresh();
            Cursor = Cursors.WaitCursor;
            Principal.nomeEstacao = System.Windows.Forms.SystemInformation.ComputerName;
            BancoDados.verificarBanco();
            if (BancoDados.conectar() == true)
            {
                if (Util.TesteIPServidorNuvem())
                {
                    Funcoes.GravaParametro(9, "54", "http://ts1.drogabella.com/webApiEstoqueFilial/api/", "GERAL", "Endereço da API para consultar estoque em outras filiais", "ESTOQUE", "Identifica o endereço da API para consultar estoque em outras filiais", false);

                    var verificaAtualizacao = new AtualizacaoTabela();
                    Application.DoEvents();
                    lblMsg.Text = "Atualizando Script";
                    lblMsg.Refresh();

                    if (String.IsNullOrEmpty(Funcoes.LeParametro(9, "52", false)))
                    {
                        verificaAtualizacao.ScriptAlteracaoBancoDeDados(this);
                    }

                    CriaParametros.CriaOuAtualizaParametros();

                    ParametrosBanco.ParametrosCriaouAltera();

                    if (VerificaConfiguracao().Equals(false))
                    {
                        PainelConfiguracao();
                    }
                    else
                    {
                        await VerificaAtualizacao();

                        if (bloqueio)
                        {
                            this.Close();
                        }

                        VerificaApi();

                        ParametrosBanco.ScriptBancoDeDados();
                    }
                }
                else
                {
                    CriaParametros.CriaOuAtualizaParametros();

                    ParametrosBanco.ParametrosCriaouAltera();

                    ParametrosBanco.ScriptBancoDeDados();

                    MessageBox.Show("Servidor Fora! As operções entre filiais e atualização de Estoque na Nuvem serão afetadas!", "Estoque Filial", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    Continua();

                    Application.Exit();
                }
            }
            else
            {
                this.Close();
            }
        }

        #region METODOS DE ATUALIZACAO
        public async Task<List<AtualizacaoGeral>> BuscaAtualizacao()
        {
            List<AtualizacaoGeral> allItems = new List<AtualizacaoGeral>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("AtualizacaoGeral/IdentificaAtualizacao?aplicativo=" + Application.ProductName);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<AtualizacaoGeral>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async Task<List<EstabelecimentoFilial>> BuscaDadosEstabelecimento()
        {
            List<EstabelecimentoFilial> allItems = new List<EstabelecimentoFilial>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Estabelecimento/BuscaDadosDoEstabelecimentos?codEstab=" + Funcoes.LeParametro(9, "52", false)
                                + "&grupoID=" + Funcoes.LeParametro(9, "53", false));
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<EstabelecimentoFilial>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }


        public async Task<List<AtualizacaoEstacao>> BuscaAtualizacaoPorEstacao()
        {
            List<AtualizacaoEstacao> allItems = new List<AtualizacaoEstacao>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("AtualizacaoEstacao/IdentificaAtualizacaoEstacao?aplicativo=" + Application.ProductName
                                + "&estacao=" + Principal.nomeEstacao + "&codEstab=" + Funcoes.LeParametro(9, "52", false)
                                + "&grupoID=" + Funcoes.LeParametro(9, "53", false));
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<AtualizacaoEstacao>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async Task<bool> VerificaAtualizacao()
        {
            try
            {
                bloqueio = false;
                lblMsg.Text = "Verificando Atualização!";
                lblMsg.Refresh();

                Version version = Assembly.GetEntryAssembly().GetName().Version;
                Principal.verSistema = version.Major + "." + version.Minor;

                dados = await BuscaAtualizacao();

                if (dados.Count > 0)
                {
                    if (dados[0].CodErro == "00")
                    {
                        arquivoDownload = dados[0].EnderecoAtualizacao;

                        Principal.enderecoFp = dados[0].EnderecoFp;
                        Principal.exeDna = dados[0].GeradorDNA;

                        if (Principal.verSistema != dados[0].Versao)
                        {
                            MessageBox.Show("Atualização Necessária!\nReferente à: " + dados[0].Mensagem, "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            FileStream ArquivoAtu = new FileStream(Application.StartupPath + @"\update\atualiza.dat", FileMode.Create);

                            byte[] byteCaminho = null;
                            byte[] byteArquivo = null;

                            byteCaminho = Encoding.ASCII.GetBytes(arquivoDownload + "\r");
                            byteArquivo = Encoding.ASCII.GetBytes(Application.ProductName + ".exe");
                            ArquivoAtu.Write(byteCaminho, 0, byteCaminho.Length);
                            ArquivoAtu.Write(byteArquivo, 0, byteArquivo.Length);
                            ArquivoAtu.Close();

                            Process Atualizador = System.Diagnostics.Process.Start(Application.StartupPath + @"\Atualiza\Atualizador.exe");

                            Application.Exit();

                            return false;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Erro ao acessar os dados, contate o suporte!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                else
                    return false;

                dadosEstabelecimento = await BuscaDadosEstabelecimento();
                if (dadosEstabelecimento[0].CodErro == "00")
                {
                    if (dadosEstabelecimento[0].Liberado.Equals("N"))
                    {
                        MessageBox.Show("Farmácia BLOQUEADA. Contate o Suporte!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        bloqueio = true;
                        return false;
                    }

                    if (dadosEstabelecimento[0].ExibeMensagem.Equals("S"))
                    {
                        MessageBox.Show("AVISO!\n" + dadosEstabelecimento[0].MensagemCliente, "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        await AtualizaStatusMensagem();
                    }

                    identificaAtualizacao = await BuscaAtualizacaoPorEstacao();
                    if (identificaAtualizacao.Count > 0)
                    {
                        if (identificaAtualizacao[0].CodErro == "00")
                        {
                            if (identificaAtualizacao[0].Atualiza == "S")
                            {
                                arquivoDownload = identificaAtualizacao[0].Endereco;

                                MessageBox.Show("Atualização Necessária!\nReferente à: " + identificaAtualizacao[0].MensagemCliente, "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);

                                FileStream ArquivoAtu = new FileStream(Application.StartupPath + @"\update\atualiza.dat", FileMode.Create);

                                byte[] byteCaminho = null;
                                byte[] byteArquivo = null;

                                byteCaminho = Encoding.ASCII.GetBytes(arquivoDownload + "\r");
                                byteArquivo = Encoding.ASCII.GetBytes(Application.ProductName + ".exe");
                                ArquivoAtu.Write(byteCaminho, 0, byteCaminho.Length);
                                ArquivoAtu.Write(byteArquivo, 0, byteArquivo.Length);
                                ArquivoAtu.Close();

                                await AtualizaStatusAtualizacao();

                                Process Atualizador = System.Diagnostics.Process.Start(Application.StartupPath + @"\Atualiza\Atualizador.exe");

                                Application.Exit();

                                return false;
                            }
                        }
                        else if (identificaAtualizacao[0].CodErro == "01")
                        {
                            await InsereNovaEstacao();
                        }
                        else
                        {
                            MessageBox.Show("Erro " + identificaAtualizacao[0].CodErro + ": " + identificaAtualizacao[0].MensagemErro, "Cadastro Nova Estação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                    #region MODULO_MENU
                    if (dadosEstabelecimento[0].AtualizaFormulario.Equals("S"))
                    {
                        bool erro = false;
                        List<ModuloMenu> dtFormularios = await BuscaFormularios();
                        if (dtFormularios.Count > 0)
                        {
                            var modulo = new ModuloMenu();
                            for (int i = 0; i < dtFormularios.Count; i++)
                            {
                                if (String.IsNullOrEmpty(modulo.BuscaSeExisteModulo(dtFormularios[i].ModuloID)))
                                {
                                    if (!modulo.InsereRegistros(dtFormularios[i]))
                                    {
                                        erro = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (!erro)
                        {
                            AtualizaTabelaPorCampo("ATUALIZA_FORMULARIO");
                        }
                    }
                    #endregion

                    #region INSERE GRUPOS E SUAS PERMISSOES
                    if (BancoDados.selecionarRegistros("SELECT * FROM GRUPO_USUARIOS").Rows.Count.Equals(1))
                    {
                        var dadosGrupoUsuario = new GrupoDeUsuario();

                        dadosGrupoUsuario.GrupoUsuId = 2;
                        dadosGrupoUsuario.Descricao = "CAIXA";
                        dadosGrupoUsuario.Administrador = "N";
                        dadosGrupoUsuario.Liberado = "S";
                        dadosGrupoUsuario.OpCadastro = Principal.usuario;
                        dadosGrupoUsuario.DtCadastro = DateTime.Now;

                        dadosGrupoUsuario.InsereRegistroGrupoUsuario(dadosGrupoUsuario);

                        InserirPermissoes(2, "11,13,42,43,45,46,47,48,49,96,97,99,100,101,103");

                        dadosGrupoUsuario.GrupoUsuId = 3;
                        dadosGrupoUsuario.Descricao = "BALCAO";
                        dadosGrupoUsuario.Administrador = "N";
                        dadosGrupoUsuario.Liberado = "S";
                        dadosGrupoUsuario.OpCadastro = Principal.usuario;
                        dadosGrupoUsuario.DtCadastro = DateTime.Now;

                        dadosGrupoUsuario.InsereRegistroGrupoUsuario(dadosGrupoUsuario);

                        InserirPermissoes(3, "11,13,29,40,41,96,97,118,101,116,123,126,109");

                        dadosGrupoUsuario.GrupoUsuId = 4;
                        dadosGrupoUsuario.Descricao = "SNGPC";
                        dadosGrupoUsuario.Administrador = "N";
                        dadosGrupoUsuario.Liberado = "S";
                        dadosGrupoUsuario.OpCadastro = Principal.usuario;
                        dadosGrupoUsuario.DtCadastro = DateTime.Now;

                        dadosGrupoUsuario.InsereRegistroGrupoUsuario(dadosGrupoUsuario);

                        InserirPermissoes(4, "11,13,29,82,83,84,85,86,87,88,89,90,91,92,93,94,95,105,150,96,97,128,137");
                    }
                    #endregion

                    #region ABCFARMA
                    if (dadosEstabelecimento[0].AtualizaABCFarma.Equals("S"))
                    {
                        if (MessageBox.Show("Está disponivel Nova Atualização ABCFARMA. Deseja Atualiza?", "ABCFARMA", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            bool valida = await AtualizaABCFARMA();
                            if (valida)
                            {
                                AtualizaTabelaPorCampo("ATUALIZA_ABCFARMA");
                                Funcoes.GravaParametro(9, "57", dadosEstabelecimento[0].VersaoAbcFarma);
                            }
                        }
                    }
                    #endregion

                    #region EnviaDadosDaVenda
                    lblMsg.Text = "Vendas WEB. Aguarde...";
                    lblMsg.Refresh();
                    if (dadosEstabelecimento[0].EnviaDadosVenda.Equals("S"))
                    {
                        VendasDados vendas = new VendasDados();
                        Principal.dtBusca = vendas.RelatorioDeVendasMensalAPI(Util.PrimeiroDiadoMes(DateAndTime.Now), Util.UltimoDiadoMes(DateAndTime.Now));

                        if (Principal.dtBusca.Rows.Count > 0)
                        {
                            AtualizaTabelaPorCampo("ENVIA_DADOS_VENDAS");

                            for (int i = 0; i < Principal.dtBusca.Rows.Count; i++)
                            {
                                await InsereRelatorioVendasMensal(Convert.ToInt32(Principal.dtBusca.Rows[i]["QTDE"]), Principal.dtBusca.Rows[i]["PROD_CODIGO"].ToString(), Principal.dtBusca.Rows[i]["PROD_DESCR"].ToString(), Principal.dtBusca.Rows[i]["DEP_DESCR"].ToString());
                            }
                        }
                    }
                    #endregion

                }
                else
                {
                    MessageBox.Show("Farmácia não Cadastrada. Contate o Suporte!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    bloqueio = true;
                    return false;
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        public void InserirPermissoes(int idGrupo, string codigos)
        {
            var permissoes = new Permissao();
            DataTable dtModulos = new DataTable();
            dtModulos = BancoDados.selecionarRegistros("SELECT * FROM MODULO_MENU WHERE MODULO_ID IN (" + codigos + ")");
            for (int i = 0; i < dtModulos.Rows.Count; i++)
            {
                permissoes.PermissoesID = Convert.ToInt32(Funcoes.IdentificaVerificaID("PERMISSOES", "PERMISSOES_ID"));
                permissoes.GrupoID = idGrupo;
                permissoes.ModuloID = int.Parse(dtModulos.Rows[i]["MODULO_ID"].ToString());
                permissoes.Acessa = 'S';
                permissoes.Inclui = 'S';
                permissoes.Altera = 'S';
                permissoes.Exclui = 'S';
                permissoes.DtCadastro = DateTime.Now;
                permissoes.OpCadastro = Principal.usuario;

                permissoes.InsereRegistroPermissao(permissoes);
            }
        }

        public async Task<bool> AtualizaABCFARMA()
        {
            try
            {
                lblMsg.Text = "Atualizando Preço ABCFarma";
                lblMsg.Refresh();
                Cursor = Cursors.WaitCursor;
                List<TabelaAbcFarma> prodAbc = await BuscaProdutos();
                if (prodAbc.Count > 0)
                {
                    if (prodAbc[0].CodErro == "00")
                    {
                        Application.DoEvents();

                        var produtosAbc = new ProdutosAbcFarma();
                        Principal.dtBusca = produtosAbc.BuscaDados();

                        //OBTEM TODOS OS PRE_VALOR E CODIGO DE BARRA//
                        var precos = new Preco();
                        var produtosDetalhe = new ProdutoDetalhe();
                        var produtos = new Produto();
                        Principal.dtRetorno = produtos.BuscaProdutosParaAbcFarma(1, 1);

                        for (int i = 0; i < prodAbc.Count; i++)
                        {
                            DataRow[] linha_produtos_abc = Principal.dtBusca.Select("PROD_CODIGO = '" + prodAbc[i].MedBarra + "'");

                            produtosAbc.ProdCodigo = prodAbc[i].MedBarra;
                            produtosAbc.ProdDescricao = prodAbc[i].MedDescricao;
                            produtosAbc.MedPco18 = prodAbc[i].MedPco;
                            produtosAbc.MedPla18 = prodAbc[i].MedPla;
                            produtosAbc.MedFra18 = prodAbc[i].MedFra;
                            produtosAbc.CodAbcFarma = prodAbc[i].MedAbc;
                            produtosAbc.DtCadastro = DateTime.Now;
                            produtosAbc.OpCadastro = Principal.usuario;

                            if (linha_produtos_abc.Length == 0)
                            {
                                produtosAbc.InserirDados(produtosAbc);
                            }
                            else
                            {
                                produtosAbc.AtualizaDados(produtosAbc);
                            }

                            DataRow[] linha_precos = Principal.dtRetorno.Select("PROD_CODIGO = '" + prodAbc[i].MedBarra + "'");
                            if (linha_precos.Length > 0)
                            {
                                if (Convert.ToDouble(linha_precos[0]["PRE_VALOR"]) != prodAbc[i].MedPco)
                                {
                                    precos.EmpCodigo = 1;
                                    precos.EstCodigo = 1;
                                    precos.PreValor = prodAbc[i].MedPco;
                                    precos.ProdPMC = prodAbc[i].MedPco;
                                    precos.ProdCodigo = prodAbc[i].MedBarra;
                                    precos.DtAlteracao = DateTime.Now;
                                    precos.OpAlteracao = Principal.usuario;

                                    if (!precos.AtualizaPrecoAbcFarmaPorProdCodigo(precos, prodAbc[i].MedPco))
                                        return false;


                                    produtos.ProdCodBarras = prodAbc[i].MedBarra;
                                    produtos.ProdNCM = prodAbc[i].MedNcm;
                                    produtos.ProdRegistroMS = prodAbc[i].MedRegistroMS;
                                    produtos.OpAlteracao = Principal.usuario;
                                    produtos.DtAlteracao = DateTime.Now;

                                    if (!produtos.AtualizaNcmRegistroMSAbcFarma(produtos))
                                        return false;

                                    produtosDetalhe.EmpCodigo = 1;
                                    produtosDetalhe.EstCodigo = 1;
                                    produtosDetalhe.ProdCodigo = prodAbc[i].MedBarra;
                                    produtosDetalhe.ProdLista = prodAbc[i].MedLista == "+" ? "P" : prodAbc[i].MedLista == "-" ? "N" : "U";
                                    produtosDetalhe.ProdCest = prodAbc[i].MedCest;
                                    produtosDetalhe.ProdDCB = prodAbc[i].MedDCB;
                                    produtosDetalhe.OpAlteracao = Principal.usuario;
                                    produtosDetalhe.DtAlteracao = DateTime.Now;

                                    if (!produtosDetalhe.AtualizaProdListaAbcFarma(produtosDetalhe))
                                        return false;
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            finally
            {
                lblMsg.Text = "";
                lblMsg.Refresh();
                Cursor = Cursors.Default;
            }

        }

        public async Task<List<TabelaAbcFarma>> BuscaProdutos()
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("TabelaAbcFarma/BuscaProdutosAbcFarma");
                response.EnsureSuccessStatusCode();
                string responseBody = await response.Content.ReadAsStringAsync();

                List<TabelaAbcFarma> allItems = JsonConvert.DeserializeObject<List<TabelaAbcFarma>>(responseBody);

                return allItems;
            }
        }

        public async Task<List<ModuloMenu>> BuscaFormularios()
        {
            List<ModuloMenu> allItems = new List<ModuloMenu>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Tabela/BuscaFormulario");
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<ModuloMenu>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async Task<bool> InsereNovaEstacao()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("AtualizacaoEstacao/NovaEstacao?aplicativo=" + Application.ProductName + "&estacao=" + Principal.nomeEstacao
                        + "&codEstab=" + codEstab + "&grupoID=" + grupoID);
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao Inserir Nova Estação. Contate o Suporte!", "Cadastro Nova Estação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Cadastro Nova Estação", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async void VerificaApi()
        {
            lblMsg.Text = "Verifica atualização de tabelas";
            lblMsg.Refresh();
            bool retorno = await VerificaAtualizaTabela();
            if (retorno)
            {
                lblMsg.Text = "Atualiza Tabela Espécie";
                lblMsg.Refresh();
                retorno = await AtualizaTabelaEspecie();

                if (retorno)
                {
                    lblMsg.Text = "Atualiza Tabela Forma de Pagamento";
                    lblMsg.Refresh();
                    retorno = await AtualizaTabelaFormaPagamento();

                    if (retorno)
                    {
                        lblMsg.Text = "Atualiza Tabela de Despesas";
                        lblMsg.Refresh();

                        retorno = await AtualizaTabelaDespesa();

                        if (retorno)
                        {
                            lblMsg.Text = "Atualizando Usuários";
                            lblMsg.Refresh();

                            Principal.dtBusca = BancoDados.selecionarRegistros("SELECT * FROM USUARIOS WHERE LOGIN_ID <> 'gadm' AND (LIBERADO = 'S' OR LIBERADO IS NULL)");

                            for (int i = 0; i < Principal.dtBusca.Rows.Count; i++)
                            {
                                InsereAPI(Convert.ToInt32(Principal.dtBusca.Rows[i]["ID"]), Principal.dtBusca.Rows[i]["LOGIN_ID"].ToString());
                            }

                            AtualizaStatusTabela();
                        }
                    }
                }
            }

            if (Funcoes.LeParametro(2, "31", true, Principal.nomeEstacao).Equals("S") && Convert.ToDateTime(Funcoes.LeParametro(2, "32", true)).ToString("dd/MM/yyyy") != DateTime.Now.ToString("dd/MM/yyyy"))
            {
                lblMsg.Text = "Buscando Entregas Pendentes";
                lblMsg.Refresh();
                var entregas = new ControleEntrega();
                DataTable dt = entregas.BuscaEntregasPendentes();
                if (dt.Rows.Count > 0)
                {
                    ProtocoloEntrega(dt);
                }

                Funcoes.GravaParametro(2, "32", DateTime.Now.ToString("dd/MM/yyyy"), "GERAL", "Data da ultima impressão", "IMPRESSORA", "Identifica a data da última impressão");
            }

            #region DadosParaNuvem
            if (Funcoes.LeParametro(6, "365", true, Principal.nomeEstacao).Equals("S"))
            {
                List<ControleDeDados> identificaAtualizacao = await IdentificaAtualizacao();
                if (identificaAtualizacao[0].CodErro == "01")
                {
                    lblMsg.Text = "Enviando Dados para Nuvem. Aguarde...";
                    lblMsg.Refresh();

                    Principal.estAtual = Convert.ToInt32(Funcoes.LeParametro(6, "366", false));
                    Principal.empAtual = Convert.ToInt32(Funcoes.LeParametro(6, "367", false));

                    Principal.dtRetorno = DadosNuvem.BuscaSomatorioDiario(DateTime.Now.AddDays(-1), Principal.empAtual, Principal.estAtual);
                    for (int i = 0; i < Principal.dtRetorno.Rows.Count; i++)
                    {
                        retorno = await InsereRelatorioVendasDiario(Convert.ToDouble(Principal.dtRetorno.Rows[i]["TOTAL"]), Convert.ToInt32(Principal.dtRetorno.Rows[i]["VENDAS"]), DateTime.Now.AddDays(-1));
                        if (retorno.Equals(false))
                        {
                            MessageBox.Show("Erro ao importar os dados para nuvem", "Conectec Tecnologia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                    }

                    Principal.dtRetorno = DadosNuvem.BuscaVendaPorFormaDePagamento(DateTime.Now.AddDays(-1), Principal.empAtual, Principal.estAtual);
                    for (int i = 0; i < Principal.dtRetorno.Rows.Count; i++)
                    {
                        retorno = await InsereRelatorioVendasFormaPagto(Convert.ToDouble(Principal.dtRetorno.Rows[i]["VENDA"]), Convert.ToInt32(Principal.dtRetorno.Rows[i]["VENDA_FORMA_ID"]), DateTime.Now.AddDays(-1));
                        if (retorno.Equals(false))
                        {
                            MessageBox.Show("Erro ao importar os dados para nuvem", "Conectec Tecnologia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                    }

                    Principal.dtRetorno = DadosNuvem.BuscaVendaPorEspecie(DateTime.Now.AddDays(-1), Principal.empAtual, Principal.estAtual);
                    for (int i = 0; i < Principal.dtRetorno.Rows.Count; i++)
                    {
                        retorno = await InsereRelatorioEspecie(Convert.ToDouble(Principal.dtRetorno.Rows[i]["VENDA"]), Convert.ToInt32(Principal.dtRetorno.Rows[i]["ESP_CODIGO"]), DateTime.Now.AddDays(-1));
                        if (retorno.Equals(false))
                        {
                            MessageBox.Show("Erro ao importar os dados para nuvem", "Conectec Tecnologia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                    }

                    //SINCRONIZA AS CONTAS A PAGAR
                    List<Pagar> dadosPagar = await EnviaOuRecebeDadosMysql.BuscaContasPendentes();
                    if (dadosPagar.Count > 0)
                    {
                        if (dadosPagar[0].CodErro.Equals("00"))
                        {
                            for (int i = 0; i < dadosPagar.Count; i++)
                            {
                                BancoDados.AbrirTrans();

                                int id = Funcoes.IdentificaVerificaID("PAGAR", "PAG_CODIGO", 0, "", Principal.empAtual);

                                Pagar pgto = new Pagar
                                {
                                    EmpCodigo = Principal.empAtual,
                                    PagCodigo = id,
                                    CfDocto = dadosPagar[i].CfDocto,
                                    PagDocto = dadosPagar[i].PagDocto,
                                    PagData = dadosPagar[i].PagData,
                                    PagValorPrincipal = dadosPagar[i].PagValorPrincipal,
                                    TipoCodigo = Convert.ToInt32(Convert.ToDouble(Util.SelecionaCampoEspecificoDaTabela("TIPO_DOCTO", "TIP_CODIGO", "TIP_DESCRICAO", "BOLETO", false, true)).ToString()),
                                    PagComprador = Convert.ToInt32(Funcoes.LeParametro(7, "01", true)),
                                    DespCodigo = dadosPagar[i].DespCodigo,
                                    Historico = "IMPORTAÇÃO WEB",
                                    PagStatus = dadosPagar[i].PagStatus,
                                    PagOrigem = dadosPagar[i].PagOrigem,
                                    DtCadastro = DateTime.Now,
                                    OpCadastro = Principal.usuario,
                                    ID = dadosPagar[i].ID,
                                };

                                Principal.dtBusca = Util.RegistrosPorEmpresa("PAGAR", "PAG_DOCTO", dadosPagar[i].PagDocto, true, true);
                                if (Principal.dtBusca.Rows.Count == 0)
                                {
                                    if (pgto.InsereRegistroFatura(pgto))
                                    {
                                        List<PagarDetalhe> dadosDetalhe = await EnviaOuRecebeDadosMysql.BuscaParcelasPorID(pgto.ID);
                                        if (dadosDetalhe.Count > 0)
                                        {
                                            if (dadosDetalhe[0].CodErro.Equals("00"))
                                            {
                                                for (int x = 0; x < dadosDetalhe.Count; x++)
                                                {
                                                    PagarDetalhe pgtoDt = new PagarDetalhe
                                                    {
                                                        EmpCodigo = Principal.empAtual,
                                                        PagCodigo = id,
                                                        PagDocto = pgto.PagDocto,
                                                        CfDocto = pgto.CfDocto,
                                                        PdParcela = dadosDetalhe[x].PdParcela,
                                                        PdValor = dadosDetalhe[x].PdValor,
                                                        PdVencimento = dadosDetalhe[x].PdVencimento,
                                                        PdSaldo = dadosDetalhe[x].PdSaldo,
                                                        PdStatus = dadosDetalhe[x].PdStatus,
                                                        DtCadastro = DateTime.Now,
                                                        OpCadastro = Principal.usuario
                                                    };

                                                    if (!pgtoDt.InsereRegistroFaturaDetalhe(pgtoDt))
                                                    {
                                                        BancoDados.ErroTrans();
                                                        break;
                                                    }
                                                }

                                                await EnviaOuRecebeDadosMysql.AtualizaEnvioEPagCodigo('S', pgto.ID, pgto.PagCodigo);
                                            }
                                            else
                                            {
                                                BancoDados.ErroTrans();
                                                break;
                                            }
                                        }
                                    }
                                    else
                                        BancoDados.ErroTrans();
                                }

                                BancoDados.FecharTrans();
                            }
                        }
                    }

                    //REPOSICAO DE PRODUTOS
                    Principal.dtRetorno = DadosNuvem.BuscaProdutosParaReposicao(DateTime.Now.AddDays(-1), Principal.empAtual, Principal.estAtual);
                    for (int i = 0; i < Principal.dtRetorno.Rows.Count; i++)
                    {
                        retorno = await InsereRelatorioReposicao(Principal.dtRetorno.Rows[i]["PROD_CODIGO"].ToString(),
                            Convert.ToInt32(Principal.dtRetorno.Rows[i]["QTDE_VENDIDA"]), Convert.ToDouble(Principal.dtRetorno.Rows[i]["PROD_CUSTO"]),
                            Convert.ToInt32(Principal.dtRetorno.Rows[i]["SUGESTAO"]), Convert.ToInt32(Principal.dtRetorno.Rows[i]["PROD_ESTATUAL"]));
                        if (retorno.Equals(false))
                        {
                            MessageBox.Show("Erro ao importar os dados para nuvem", "Conectec Tecnologia", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                    }

                    //ATUALIZA STATUS DA ENTREGA//
                    Principal.dtRetorno = DadosNuvem.BuscaEntregaFilial();
                    for (int i = 0; i < Principal.dtRetorno.Rows.Count; i++)
                    {
                        List<EntregaFilial> dados = await BuscaStatusEntrega(Convert.ToInt32(Principal.dtRetorno.Rows[i]["ID"]));
                        if (dados.Count > 0)
                        {
                            if (dados[0].CodErro == "00")
                            {
                                if (dados[0].Status != "P")
                                {
                                    BancoDados.ExecuteNoQuery("UPDATE ENTREGA_FILIAL SET STATUS = '" + dados[0].Status + "' WHERE ID = " + dados[0].ID, null);
                                }
                            }
                        }
                    }

                    await StatusAtualizacao();
                }
            }
            #endregion

            Continua();

            Application.Exit();
        }

        public async void InsereAPI(int id, string nome)
        {
            await InsereOperador(id, nome);
        }

        public async Task<bool> InsereOperador(int operadorId, string nome)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = await client.GetAsync("Operador/CadastraOperador?operadorId=" + operadorId + "&nome=" + nome + "&grupoID=" + grupoID + "&estabelecimentoId=" + codEstab + "&status=A");

                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        return Convert.ToBoolean(responseBody);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;

            }
        }

        #region ATUALIZA TABELA 

        public async Task<bool> VerificaAtualizaTabela()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Tabela/VeridicaAtualizaTabela?codEstab=" + codEstab + "&grupoID=" + grupoID);
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        return Convert.ToBoolean(responseBody);
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<bool> AtualizaTabelaEspecie()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Tabela/BuscaEspecie");
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        Especie esp = new Especie();

                        List<EspecieFilial> especie = JsonConvert.DeserializeObject<List<EspecieFilial>>(responseBody);

                        for (int i = 0; i < especie.Count; ++i)
                        {
                            string descricaoEspecie = esp.BuscaDescricao(Convert.ToInt32(especie[i].especieID));
                            if (String.IsNullOrEmpty(descricaoEspecie))
                            {
                                esp.EspCodigo = Convert.ToInt32(especie[i].especieID);
                                esp.EspDescricao = especie[i].descricao;
                                esp.EspCheque = especie[i].cheque.ToString();
                                esp.EspCaixa = especie[i].caixa.ToString();
                                esp.EspLiberado = especie[i].desabilitado.ToString();
                                esp.EspVinculado = especie[i].vinculado.ToString();
                                esp.EspSAT = especie[i].sat;
                                esp.DtCadastro = DateTime.Now;
                                esp.OpCadastro = Principal.usuario;

                                if (esp.InserirDados(esp).Equals(false))
                                {
                                    return false;
                                };
                            }
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização Espécie", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<bool> AtualizaTabelaFormaPagamento()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Tabela/BuscaFormaPagamento");
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        FormasPagamento forma = new FormasPagamento();

                        List<FormaPagamentoFilial> pag = JsonConvert.DeserializeObject<List<FormaPagamentoFilial>>(responseBody);

                        for (int i = 0; i < pag.Count; ++i)
                        {
                            string descricao = forma.IdentificaFormaDePagamento(Convert.ToInt32(pag[i].formaID), Principal.empAtual);
                            if (String.IsNullOrEmpty(descricao))
                            {
                                forma.EmpCodigo = 1;
                                forma.FormaID = pag[i].formaID;
                                forma.FormaDescricao = pag[i].descricao;
                                forma.VenctoDiaFixo = pag[i].venctoDiaFixo.ToString();
                                forma.QtdeParcelas = pag[i].qtdeParcelas;
                                forma.DiaVenctoFixo = pag[i].diaVenctoFixo;
                                forma.QtdeVias = pag[i].qtdeVias;
                                forma.OperacaoCaixa = pag[i].operacaoCaixa.ToString();
                                forma.ConvBeneficio = pag[i].convBeneficio.ToString();
                                forma.FormaLiberado = pag[i].formaLiberado.ToString();

                                if (forma.InsereRegistros(forma).Equals(false))
                                {
                                    return false;

                                };
                            }
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização Forma de Pagamento", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<bool> AtualizaTabelaDespesa()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Tabela/BuscaDespesa");
                    if (response.IsSuccessStatusCode)
                    {
                        string responseBody = await response.Content.ReadAsStringAsync();
                        Despesa despesa = new Despesa();

                        List<Despesa> desp = JsonConvert.DeserializeObject<List<Despesa>>(responseBody);

                        for (int i = 0; i < desp.Count; ++i)
                        {
                            string descricao = despesa.IdentificaDespesa(Convert.ToInt32(desp[i].DespCodigo), 1);
                            if (String.IsNullOrEmpty(descricao))
                            {
                                despesa.DespCodigo = desp[i].DespCodigo;
                                despesa.DespDescr = desp[i].DespDescr;
                                despesa.DespLiberado = desp[i].DespLiberado;
                                despesa.DtCadastro = DateTime.Now;
                                despesa.OpCadastro = "gadm";

                                if (despesa.InsereRegistros(despesa).Equals(false))
                                {
                                    return false;

                                };
                            }
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização Despesa", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async void AtualizaStatusTabela()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Tabela/AtualizaStatusTabela?codEstab=" + codEstab + "&grupoID=" + grupoID + "&atualizaTabela=N");
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao atualizar status Estabelecimento coluna Atualiza_Tabela", "Verifica Atualização Tabela", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização Tabela", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public async Task<bool> AtualizaStatusMensagem()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Tabela/AtualizaStatusMensagem?codEstab=" + codEstab + "&grupoID=" + grupoID + "&exibeMensagem=N");
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao atualizar status Exibe Mensagem.", "Verifica Atualização Mensagem", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização Mensagem", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<bool> AtualizaStatusAtualizacao()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("AtualizacaoEstacao/AtualizaStatusDaAtualizacao?codEstab=" + codEstab + "&grupoID=" + grupoID + "&atualiza=N"
                        + "&aplicativo=" + Application.ProductName + "&estacao=" + Principal.nomeEstacao);
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao atualizar status Atualiza.", "Verifica Atualização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        #endregion

        public void Continua()
        {
            try
            {
                this.Hide();
                var login = new frmLogin();
                login.Text = "Login - [" + BancoDados.LeINI("Conexao", "Banco") + "] - Versão " + Principal.verSistema;
                login.ShowDialog();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        private async void btnConfirma_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(txtCodLoja.Text))
            {
                MessageBox.Show("Cod. da Loja não pode ser em branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodLoja.Focus();
            }
            else if (String.IsNullOrEmpty(txtCodGrupo.Text))
            {
                MessageBox.Show("Cod. do Grupo não pode ser em branco!", "Consistência", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodGrupo.Focus();
            }
            else
            {
                Funcoes.GravaParametro(9, "52", txtCodLoja.Text, "GERAL", "Código da Loja", "ESTOQUE", "Identifica o código da loja para consulta de estoque em outras filiais", true);

                Funcoes.GravaParametro(9, "53", txtCodGrupo.Text, "GERAL", "Código do Grupo das Filiais", "ESTOQUE", "Identifica o código do grupo das filiais", true);
                
                if (VerificaConfiguracao().Equals(false))
                {
                    VerificaConfiguracao();
                }
                else
                {
                    await VerificaAtualizacao();

                    if (bloqueio)
                    {
                        this.Close();
                    }

                    VerificaApi();
                }
            }
        }

        private void txtEndereco_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodLoja.Focus();
        }

        private void txtCodLoja_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                txtCodGrupo.Focus();
        }

        private void txtCodGrupo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                btnConfirma.PerformClick();
        }

        #region CONFIGURA DADOS DA LOJA
        public bool VerificaConfiguracao()
        {
            if (String.IsNullOrEmpty(Funcoes.LeParametro(9, "54", true)))
            {
                return false;
            }
            if (Funcoes.LeParametro(9, "52", true).Equals("0"))
            {
                return false;
            }
            if (Funcoes.LeParametro(9, "53", true).Equals("0"))
            {
                return false;
            }
            else
            {
                codEstab = Funcoes.LeParametro(9, "52", true);
                grupoID = Funcoes.LeParametro(9, "53", true);
                enderecoWebService = Funcoes.LeParametro(9, "54", true);
                pnlConfiguracao.Visible = false;
                return true;
            }
        }
        public void PainelConfiguracao()
        {
            lblMsg.Text = "Aguardando digitação dos dados";
            lblMsg.Refresh();

            pnlConfiguracao.Visible = true;
            txtCodLoja.Text = Funcoes.LeParametro(9, "52", true).Equals("0") ? String.Empty : Funcoes.LeParametro(9, "52", true);
            txtCodGrupo.Text = Funcoes.LeParametro(9, "53", true).Equals("0") ? String.Empty : Funcoes.LeParametro(9, "53", true);
        }
        #endregion

        public async void AtualizaTabelaPorCampo(string campo)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Tabela/AtualizaStatus?codEstab=" + Funcoes.LeParametro(9, "52", false)
                                + "&grupoID=" + Funcoes.LeParametro(9, "53", false) + "&atualiza=N&campo=" + campo);
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao atualizar status Estabelecimento", "Verifica Atualização Tabela", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização Tabela", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region ENTREGAS PENDENTES 
        public void ProtocoloEntrega(DataTable dt)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                string protocolo = "__________________________________________" + "\n";
                protocolo += Funcoes.CentralizaTexto("ENTREGAS PENDENTES/TRANSITO", 43) + "\n";
                protocolo += "__________________________________________" + "\n";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    protocolo += "VENDA ID: " + dt.Rows[i]["VENDA_ID"] + " VENDEDOR: " + dt.Rows[i]["VENDEDOR"] + "\n";
                    protocolo += "DATA....: " + Convert.ToDateTime(dt.Rows[i]["ENT_CONTROLE_DATA"]).ToString("dd/MM/yyyy") + " / HORA: " + dt.Rows[i]["HORAS"] + "\n";
                    protocolo += "STATUS..: " + dt.Rows[i]["ENT_CONTROLE_STATUS"] + "\n";
                    protocolo += "F. PAGTO: " + dt.Rows[i]["FORMA_DESCRICAO"] + " TOTAL...: " + String.Format("{0:N}", Convert.ToDouble(dt.Rows[i]["VENDA_TOTAL"])) + "\n";
                    protocolo += "CLIENTE.: " + dt.Rows[i]["CF_NOME"] + "\n";

                    protocolo += "==========================================" + "\n";
                }
                protocolo += "\n\n\n";
                if (!Funcoes.LeParametro(15, "01", true, Principal.nomeEstacao).Equals("DARUMADLL"))
                {
                    if (Funcoes.LeParametro(15, "09", true, Principal.nomeEstacao).Equals("BEMATECH") && Funcoes.LeParametro(6, "353", true, Principal.nomeEstacao).Equals("F")
                         && Funcoes.LeParametro(6, "355", true, Principal.nomeEstacao).Equals("S"))
                    {
                        int iRetorno;
                        string s_cmdTX = "\n";
                        if (Funcoes.LeParametro(15, "11", true, Principal.nomeEstacao).Equals("MP2500TH"))
                        {
                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(8);
                        }
                        else
                        {
                            iRetorno = BematechImpressora.ConfiguraModeloImpressora(7);
                        }

                        iRetorno = BematechImpressora.IniciaPorta("USB");

                        iRetorno = BematechImpressora.FormataTX(protocolo, 3, 0, 0, 0, 0);

                        s_cmdTX = "\r\n";
                        iRetorno = BematechImpressora.ComandoTX(s_cmdTX, s_cmdTX.Length);

                        iRetorno = BematechImpressora.AcionaGuilhotina(0);

                        iRetorno = BematechImpressora.FechaPorta();
                    }
                    else
                    {
                        Impressao.Impressao.Imprimir(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao), protocolo);
                        if (Funcoes.LeParametro(2, "30", true).Equals("S"))
                        {
                            Impressao.Impressao.Guilhotina(Funcoes.LeParametro(2, "18", false, Principal.nomeEstacao));
                        }
                    }
                }
                else
                {
                    //IMPRIME COMPROVANTE
                    int iRetorno = DarumaDLL.iImprimirTexto_DUAL_DarumaFramework(protocolo, 0);
                    if (iRetorno != 1)
                    {
                        MessageBox.Show("Retorno: " + DarumaDLL.TrataRetorno(iRetorno), "Impressão", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "Entregas Pendentes/Trânsito", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }
        #endregion

        #region DADOS PARA NUVEM
        public async Task<List<ControleDeDados>> IdentificaAtualizacao()
        {
            List<ControleDeDados> allItems = new List<ControleDeDados>();
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("ControleDados/IdentificaAtualizacaoDiaria?codGrupo=" + Funcoes.LeParametro(9, "53", false) + "&codEstab=" + Funcoes.LeParametro(9, "52", false) + "&data=" + DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy"));
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<ControleDeDados>>(responseBody);

                    return allItems;
                }
            }
            catch (Exception)
            {
                return allItems;
            }
        }

        public async Task<bool> InsereRelatorioVendasDiario(double total, int numeroVendas, DateTime data)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatorioVendas/InserirVendaDiaria?grupoID=" + Funcoes.LeParametro(9, "53", false) + "&codEstab=" + Funcoes.LeParametro(9, "52", false) + "&total="
                        + total.ToString().Replace(",", ".") + "&numeroVendas=" + numeroVendas.ToString() + "&data=" + data.ToString("dd/MM/yyyy"));
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao Inserir Registros", "Relatório Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Relatório Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<bool> InsereRelatorioVendasFormaPagto(double total, int formaId, DateTime data)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatorioVendas/InserirVendaFormaPagto?grupoID=" + Funcoes.LeParametro(9, "53", false) + "&codEstab=" + Funcoes.LeParametro(9, "52", false) + "&total="
                        + total.ToString().Replace(",", ".") + "&formaID=" + formaId.ToString() + "&data=" + data.ToString("dd/MM/yyyy"));
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao Inserir Registros", "Relatório Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Relatório Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<bool> InsereRelatorioEspecie(double total, int espId, DateTime data)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatorioVendas/InserirVendaEspecie?grupoID=" + Funcoes.LeParametro(9, "53", false) + "&codEstab=" + Funcoes.LeParametro(9, "52", false) + "&total="
                        + total.ToString().Replace(",", ".") + "&espID=" + espId.ToString() + "&data=" + data.ToString("dd/MM/yyyy"));
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao Inserir Registros", "Relatório Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Relatório Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<bool> StatusAtualizacao()
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("ControleDados/InsereControleDados?codEstab=" + Funcoes.LeParametro(9, "52", false) + "&grupoID=" + Funcoes.LeParametro(9, "53", false) + "&atualiza=S"
                        + "&data=" + DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy"));
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao atualizar status Atualiza.", "Verifica Atualização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Verifica Atualização", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public async Task<bool> InsereRelatorioReposicao(string prodCodigo, int qtdeVendida, double custo, int sugestao, int estoqueAtual)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatorioVendas/InserirReposicaoEstoque?grupoID=" + Funcoes.LeParametro(9, "53", false) + "&codEstab=" + Funcoes.LeParametro(9, "52", false)
                        + "&data=" + DateTime.Now.AddDays(-1).ToString("dd/MM/yyyy") + "&prodCodigo=" + prodCodigo + "&qtdeVendida=" + qtdeVendida + "&custo=" + custo.ToString().Replace(",", ".")
                        + "&sugestao=" + sugestao + "&estoqueAtual=" + estoqueAtual);
                    if (!response.IsSuccessStatusCode)
                    {
                        MessageBox.Show("Erro ao Inserir Registros", "Relatório Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Relatório Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtEndereco_TextChanged(object sender, EventArgs e)
        {

        }

        public async Task<List<EntregaFilial>> BuscaStatusEntrega(int id)
        {
            List<EntregaFilial> allItems = new List<EntregaFilial>();

            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Funcoes.LeParametro(9, "54", true));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("Entrega/BuscaEntregaPorID?ID=" + id);
                    response.EnsureSuccessStatusCode();
                    string responseBody = await response.Content.ReadAsStringAsync();

                    allItems = JsonConvert.DeserializeObject<List<EntregaFilial>>(responseBody);

                    return allItems;
                }
            }
            catch
            {
                MessageBox.Show("Erro ao conectar ao servidor, operações com filiais serão afetadas. Contate o Suporte Técnico!", "CONECTEC", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return allItems;
            }
        }

        public async Task<bool> InsereRelatorioVendasMensal(int qtde, string prodCodigo, string prodDescricao, string departamento)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    Cursor = Cursors.WaitCursor;
                    client.BaseAddress = new Uri(enderecoWebService);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = await client.GetAsync("RelatorioVendas/InserirVendaMensal?grupoID=" + Funcoes.LeParametro(9, "53", false) + "&codEstab=" + Funcoes.LeParametro(9, "52", false) + "&codBarras="
                        + prodCodigo + "&descricao=" + prodDescricao + "&data=" + DateAndTime.Now.ToString("dd/MM/yyyy") + "&qtde=" + qtde + "&departamento=" + departamento);
                    if (!response.IsSuccessStatusCode)
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro : " + ex.Message, "Relatório Vendas", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        #endregion
    }
}
