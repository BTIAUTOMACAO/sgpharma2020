﻿namespace GestaoAdministrativa.Geral
{
    partial class frmBuscaConv
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBuscaConv));
            this.dgBuscaConv = new System.Windows.Forms.DataGridView();
            this.CON_CODIGO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_NOME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CON_WEB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgBuscaConv)).BeginInit();
            this.SuspendLayout();
            // 
            // dgBuscaConv
            // 
            this.dgBuscaConv.AllowUserToAddRows = false;
            this.dgBuscaConv.AllowUserToDeleteRows = false;
            this.dgBuscaConv.AllowUserToResizeColumns = false;
            this.dgBuscaConv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black;
            this.dgBuscaConv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgBuscaConv.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgBuscaConv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgBuscaConv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgBuscaConv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.CON_CODIGO,
            this.CON_NOME,
            this.CON_ID,
            this.CON_WEB});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgBuscaConv.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgBuscaConv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgBuscaConv.GridColor = System.Drawing.Color.LightGray;
            this.dgBuscaConv.Location = new System.Drawing.Point(0, 0);
            this.dgBuscaConv.Name = "dgBuscaConv";
            this.dgBuscaConv.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgBuscaConv.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgBuscaConv.RowHeadersVisible = false;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Gainsboro;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black;
            this.dgBuscaConv.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgBuscaConv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgBuscaConv.Size = new System.Drawing.Size(484, 216);
            this.dgBuscaConv.TabIndex = 0;
            this.dgBuscaConv.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgBuscaConv_CellClick);
            this.dgBuscaConv.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgBuscaConv_CellMouseDoubleClick);
            this.dgBuscaConv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgBuscaConv_KeyDown);
            this.dgBuscaConv.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.dgBuscaConv_KeyPress);
            // 
            // CON_CODIGO
            // 
            this.CON_CODIGO.DataPropertyName = "CON_CODIGO";
            this.CON_CODIGO.HeaderText = "Cód. Conveniada";
            this.CON_CODIGO.MaxInputLength = 18;
            this.CON_CODIGO.Name = "CON_CODIGO";
            this.CON_CODIGO.ReadOnly = true;
            this.CON_CODIGO.Width = 130;
            // 
            // CON_NOME
            // 
            this.CON_NOME.DataPropertyName = "CON_NOME";
            this.CON_NOME.HeaderText = "Nome";
            this.CON_NOME.MaxInputLength = 50;
            this.CON_NOME.Name = "CON_NOME";
            this.CON_NOME.ReadOnly = true;
            this.CON_NOME.Width = 350;
            // 
            // CON_ID
            // 
            this.CON_ID.DataPropertyName = "CON_ID";
            this.CON_ID.HeaderText = "ID";
            this.CON_ID.Name = "CON_ID";
            this.CON_ID.ReadOnly = true;
            this.CON_ID.Visible = false;
            // 
            // CON_WEB
            // 
            this.CON_WEB.DataPropertyName = "CON_WEB";
            this.CON_WEB.HeaderText = "Tipo";
            this.CON_WEB.Name = "CON_WEB";
            this.CON_WEB.ReadOnly = true;
            this.CON_WEB.Visible = false;
            // 
            // frmBuscaConv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(484, 216);
            this.Controls.Add(this.dgBuscaConv);
            this.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBuscaConv";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pesquisa de Empresas Conveniadas";
            this.Load += new System.EventHandler(this.frmBuscaConv_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmBuscaConv_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.dgBuscaConv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView dgBuscaConv;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_CODIGO;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_NOME;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn CON_WEB;
    }
}