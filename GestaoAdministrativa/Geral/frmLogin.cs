﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GestaoAdministrativa.Classes;
using System.Diagnostics;
using GestaoAdministrativa.Negocio;

namespace GestaoAdministrativa
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
            RegisterFocusEvents(this.Controls);
        }

        private void btnCancela_Click(object sender, EventArgs e)
        {
            BancoDados.fecharConexao();
            Process.GetCurrentProcess().Kill();
        }

        private void txtUsuario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                txtSenha.Focus();
            }
        }

        private void txtSenha_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (String.IsNullOrEmpty(txtServidor.Text))
                {
                    txtServidor.Focus();
                }
                else
                    btnLogin.PerformClick();
            }
        }

        private void txtServidor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                btnLogin_Click(sender, e);
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtUsuario.Text.Trim().Length == 0)
                { 
                    MessageBox.Show("O usuário não pode estar em branco!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtUsuario.Focus();
                }
                else
                    if (txtSenha.Text.Trim().Length == 0)
                {
                    MessageBox.Show("A senha não pode estar em branco!", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtSenha.Focus();
                }
                else
                {
                    Cursor = Cursors.WaitCursor;
                    Principal.nomeServidor = txtServidor.Text;
                    BancoDados.verificarBanco();
                    if (BancoDados.conectar().Equals(true))
                    {
                        var dados = new Usuario();
                        List<Usuario> user = dados.DadosLogin(txtUsuario.Text.Trim(), txtSenha.Text.Trim());
                        if (user.Count == 0)
                        {
                            MessageBox.Show("Usuário ou Senha inválidos. Tente novamente.", "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtUsuario.Focus();
                        }
                        else
                        {
                            this.Hide();
                            Principal.usuario = txtUsuario.Text.Trim();
                            Principal.usuADM = user[0].Administrador;
                            
                            var inserirPermissoes = new Permissao();
                            inserirPermissoes.InsereNovasPermisssoes();

                            //VERIFICA SE FORM JÁ ESTÁ ABERTO//
                            if (Application.OpenForms.OfType<MDIPrincipal>().Count() == 0)
                            {
                                MDIPrincipal mdiPrincipal = new MDIPrincipal();
                                mdiPrincipal.Text = "SG Pharma - Versão: " + Principal.verSistema;
                                mdiPrincipal.ShowDialog();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor = Cursors.Default;
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            try
            {
                txtServidor.Text = Principal.nomeServidor;
                txtUsuario.Focus();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Erro: " + ex.Message, "SG Pharma", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cursor = Cursors.Default;
            }
        }

        private void btnCancela_MouseHover(object sender, EventArgs e)
        {
            toolTip1.SetToolTip(btnCancela, "Cancela Login");
        }

        private void btnLogin_MouseMove(object sender, MouseEventArgs e)
        {
            toolTip1.SetToolTip(btnLogin, "Confirma Login");
        }

        private void RegisterFocusEvents(Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if ((control is TextBox) ||
                (control is RichTextBox) ||
                (control is MaskedTextBox))
                {
                    control.Enter += new EventHandler(controlFocus_Enter);
                    control.Leave += new EventHandler(controlFocus_Leave);
                }

                RegisterFocusEvents(control.Controls);
            }
        }

        void controlFocus_Leave(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.White;
        }

        void controlFocus_Enter(object sender, EventArgs e)
        {
            (sender as Control).BackColor = Color.Honeydew;
        }

    }
}
